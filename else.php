<?php
/**
 * @var \app\models\Program[] $models
 * @var string $currency
 * @var $this \yii\web\View
 */

use app\helpers\Html;
use app\models\Transfer;
use yii\widgets\ActiveForm;

?>
<div class="program index" v-cloak>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="row">
			<?php foreach ( $models as $id => $model ): ?>
                <div id="program-<?= $model->id ?>"
                     v-bind:class="{program: true, '<?= strtolower( $model->name ) ?>': true, 'col-lg-6 col-md-6 col-sm-12 col-xs-12': true, active: <?= $model->id ?> === program}">
                    <div class="investing-plan">
                        <div class="header-background">
                            <h2><?= $model->name ?></h2>
                        </div>
                        <div class="body-form">
                            <ul>
                                <li class="min">
                                    <span class="label"><?= Yii::t( 'app', 'Min. investment:' ) ?></span>
                                    <span class="value" v-bind:data-currency="currency">{{ getRated(<?= $model->id ?>, 'min') | factor(currency) }}</span>
                                </li>
                                <li class="max hidden">
                                    <span class="label"><?= Yii::t( 'app', 'Max. investment:' ) ?></span>
                                    <span class="value" v-bind:data-currency="currency">{{ getRated(<?= $model->id ?>, 'max') | factor(currency) }}</span>
                                </li>
                                <li class="rate">
                                    <span class="label"><?= Yii::t( 'app', 'Daily profit:' ) ?></span>
                                    <span class="value">{{ programs[<?= $model->id ?>].percent * 100 }}%</span>
                                </li>
                                <li class="duration">
                                    <span class="label"><?= Yii::t( 'app', 'Duration:' ) ?></span>
                                    <span class="value"><?= Yii::t( 'app', '365 days' ) ?></span>
                                </li>
                                <li class="income">
                                    <span class="label"><?= Yii::t( 'app', 'Income:' ) ?></span>
                                    <span class="value percent"><?= $model->getIncome() ?></span>
                                </li>
                                <li>
                                    <p class="label"><?= Yii::t( 'app', 'The deposit amount is included<br>in daily payments' ) ?></p>
                                    <span></span>
                                </li>
                            </ul>
							<?= Html::a( Yii::t( 'app', 'Invest' ), '#' . $model->name, [
								'class'      => 'buy',
								'v-on:click' => 'program = ' . $model->id
							] ) ?>
							<?php $form = ActiveForm::begin( [ 'enableClientValidation' => false ] ) ?>
                            <p class="form-label"><?= Yii::t( 'app', 'Select currency' ) ?>:</p>

                            <div class="select-currency btn-group">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                    {{ currency }}
                                </button>
                                <div class="dropdown-menu" data-program="<?= $model->name ?>">
									<?php foreach ( Transfer::CURRENCIES as $currency ) {
										echo Html::tag( 'div', $currency, [
											'class'      => 'dropdown-item',
											'v-on:click' => "load('$currency')"
										] );
									} ?>
                                </div>
                            </div>
                            <p class="form-label"><?= Yii::t( 'app', 'Enter amount' ) ?>:</p>
                            <div class="fields">
								<?= $form->field( $model, 'id' )->hiddenInput()->label( false ) ?>
								<?= $form->field( $model, 'currency' )->hiddenInput( [ 'v-model' => 'currency' ] )->label( false ) ?>
								<?= $form->field( $model, 'name' )->hiddenInput()->label( false ) ?>
								<?= $form->field( $model, 'amount' )->textInput( [
									'v-bind:placeholder' => "getRated($model->id, 'min') / factor[currency]"
								] ) ?>
                            </div>
                            <div class="control">

								<?= Html::submitButton( Yii::t( 'app', 'Open' ) ) ?>
                            </div>
                        </div>

                    </div>
					<?php ActiveForm::end() ?>
                </div>
			<?php endforeach; ?>

        </div>
    </div>
</div>