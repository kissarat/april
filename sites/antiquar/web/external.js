(function (config) {
  function setupCalculator() {
    function factor(v, currency) {
      if (!currency) {
        currency = config.currency
      }
      const factor = config.factor[currency];
      return (v / factor).toFixed(Math.log(factor) / Math.log(10))
    }

    Vue.filter('factor', factor);
    Vue.filter('round', function (v, scale) {
      if ('number' !== typeof scale) {
        scale = 2
      }
      const factor = Math.pow(10, scale);
      return Math.round(v * factor) / factor
    });

    const calculator = new Vue({
      el: '#calculator-app',

      data: function () {
        return config
      },

      created: function () {
        this.id = 1
      },

      computed: {
        program: function () {
          const self = this
          return this.programs.find(function (p) {
              return self.id === p.id
            }
          )
        },

        isSelected: function () {
          return this.currency && this.program
        },

        cent: function () {
          return this.amount * this.factor[this.currency]
        },

        daily: function () {
          return this.isSelected ? this.cent * this.program.percent : 0
        },

        allIncome: function () {
          return this.isSelected ? this.daily * this.program.days : 0
        },

        income: function () {
          return this.isSelected ? this.allIncome - this.cent : 0
        },

        min: function () {
          return this.rate(this.program.min)
        },

        max: function () {
          return this.rate(this.program.max)
        }
      },

      methods: {
        rate: function (usd) {
          usd = usd / 100;
          const c = this.currency;
          return 'USD' === c
            ? usd
            : (usd / this.rates[c]).toFixed(Math.log(this.factor[c]) / Math.log(10))
        }
      },

      watch: {
        id: function () {
          this.amount = this.min
        },
        currency: function () {
          this.amount = this.min
        },
        amount: function (amount) {
          if (!Number.isFinite(+amount)) {
            const a = amount.replace(',', '.');
            if (Number.isFinite(+a)) {
              this.amount = a
            }
          }
        }
      }
    })
  }

  const s = document.createElement('script');
  s.src = 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js';
  s.addEventListener('load', setupCalculator);
  document.getElementById('calculator').appendChild(s);

  const t = document.getElementById('calculator-style');
  const style = document.createElement('style');
  style.innerHTML = t.innerHTML;
  document.head.appendChild(style);
  t.remove();
})
