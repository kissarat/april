document.addEventListener('DOMContentLoaded', function () {
  Vue.use(Vuex)

  function Cookies() {
    this.age = 12 * 30 * 24 * 3600
  }

  Cookies.prototype = {
    get: function (key) {
      const m = new RegExp(key + '=([^;]+)')
      const v = m.exec(document.cookie)
      if (v) {
        return v[1]
      }
    },

    set: function (key, value, age) {
      if (!age) {
        age = this.age
      }
      document.cookie = key + '=' + value + '; path=/; max-age=' + age
    }
  }

  const cookies = new Cookies()

  const NotFound = {
    name: "not-found",
    template: '<div>Not Found</div>'
  }

  const Alert = new Vue({
    el: '.widget-alert',
    data: {
      alerts: []
    },
    methods: {
      add: function (type, message) {
        this.alerts.push({
          type: type,
          message: message
        })
      },
      success: function (message) {
        this.add('success', message)
      },
      warning: function (message) {
        this.add('warning', message)
      },
      error: function (message) {
        this.add('error', message)
      },
      remove: function (i) {
        this.alerts.splice(i)
      },
      clear: function () {
        this.alerts.splice(0, this.alerts.length)
      }
    }
  })

  const Advertise = view({
    name: 'advertise'
  })

  const TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss'
  const DATE_FORMAT = 'LL'
  // const w = window
  const w = {}

  function formatTime() {
    const s = []
    for (var i = 0; i < arguments.length; i++) {
      s.push(('00' + Math.floor(arguments[i])).slice(-2))
    }
    return s.join(':')
  }

  function t(text, vars) {
    var out = translation[text]
    if (!out) {
      if (this.isDevMode) {
        console.warn('No translation: ' + text)
      }
      out = text
    }
    if (vars) {
      if ('object' === typeof vars) {
        for (var name in vars) {
          out = out.replace('{' + name + '}', vars[name])
        }
      }
      if (vars.test) {
        out += ' (test)'
      }
    }
    return out;
  }

  function Account(id, o) {
    this.id = id
    Object.assign(this, o)
    if (!this.wallet) {
      this.wallet = null
    }
  }

  Account.prototype = {
    createWallet: function () {
      const self = this
      $.getJSON('/wallet/' + this.id, function (r) {
        self.wallet = r.result
      })
    }
  }

  Account.list = []

  Account.list.blockchain = function (bool) {
    return this.filter(function (a) {
      return a.blockchain === bool
    })
  }

  function Program(options) {
    Object.assign(this, options)
    this.name = t(this.name)
  }

  Program.prototype = {
    get allPercent() {
      return this.days * this.percent
    }
  }

  function User(options) {
    Object.assign(this, options)
    this.active = false
  }

  User.prototype = {
    get localeCreated() {
      return moment(this.created, TIME_FORMAT).toDate().toLocaleString()
    },

    get name() {
      return ((this.forename || '') + ' ' + (this.surname || '')).trim() || this.nick
    }
  }

  function Node(options) {
    Object.assign(this, options)
    if (this.program) {
      this.program = new Program(this.program)
    }
  }

  Node.prototype = {
    fromStart(days) {
      const created = moment(this.created, TIME_FORMAT)
      return (created.unix() + 24 * 3600 * days) * 1000
    },

    get opened() {
      return moment(this.created, TIME_FORMAT).format(DATE_FORMAT)
    },

    get closed() {
      return moment(this.fromStart(this.program.days)).format(DATE_FORMAT)
    },

    get allIncome() {
      return this.program.allPercent * this.amount
    },

    get next() {
      return this.fromStart(this.count + 1)
    },

    get untilNext() {
      const delta = moment.duration(moment(this.next).diff(Date.now()))
      return formatTime(delta.asHours(), delta.minutes(), delta.seconds())
    },

    get percent() {
      return this.count / this.program.days * 100
    }

    // get income() {
    //   return this.count * this.program.percent * this.amount
    // }
  }

  const appConfig = {}
  const translation = {}
  w.appConfig = appConfig
  w.translation = translation

  function Transfer(options) {
    Object.assign(this, options)
  }

  Transfer.prototype = {
    get localeCreated() {
      return moment(this.created, TIME_FORMAT).toDate().toLocaleString()
    },

    get note() {
      if (this.vars && this.vars.name) {
        this.vars.name = t(this.vars.name)
      }
      switch (this.type) {
        case 'accrue':
          return t('{name} #{n}', {
            name: this.vars.name,
            n: this.n,
          })
        case 'bonus':
          return t('{name} benefit from {nick}', this.vars)
        case 'payment':
          if ('pending' === this.status) {
            return t('{number} confirmations (3 is expected)', {number: this.n}) + ' ' + (new Date()).toLocaleTimeString()
          }
          if (this.wallet) {
            return t('Refill from {wallet}', {wallet: this.wallet})
          }
          return t('Refill')
        case 'withdraw':
          var text = t('success' === this.status ? 'Withdrawn to {wallet}' : 'Withdrawal request to {wallet}', {wallet: this.wallet})
          if (this.text) {
            text = this.text + ' (' + text + ')'
          }
          return text
        case 'buy':
          return t('Buy {name}', this.vars)
        default:
          return this.type
      }
    }
  }

  Vue.component('clock', {
    name: 'clock',
    template: document.querySelector('.clock-template'),
    props: ['time'],
    data: function () {
      return {
        moment: null
      }
    },
    created: function () {
      this.moment = new Date(this.time)
    },
    computed: {
      secondsStyle: function () {
        var angle = this.moment.getSeconds() * 6
        return {
          transform: 'rotate(' + angle + 'deg)'
        }
      },
      minutesStyle: function () {
        var angle = (this.moment.getMinutes() - 9) * 6
        angle += this.moment.getSeconds() / 10
        return {
          transform: 'rotate(' + angle + 'deg)'
        }
      },
      hoursStyle: function () {
        var angle = (this.moment.getHours() - 10.16) * 30
        angle += (this.moment.getMinutes() - 9) / 2
        return {
          transform: 'rotate(' + angle + 'deg)'
        }
      }
    }
  })

  var _st = [95, 95, 86, 73, 69, 87, 83, 84, 65, 84, 69].map(function (c) {
    return String.fromCharCode(c)
  }).join('');
  try {
    const st = JSON.parse(atob(document.getElementById(_st).value.split('').reverse().join('')))
    for (var key in st) {
      for (var name in st[key]) {
        w[key][name] = st[key][name]
      }
      // delete w[key]
    }
    _st = null
  }
  catch (ex) {
    // console.error(ex)
  }
  $('#' + _st).remove()

  for (var accountId in appConfig.accounts) {
    Account.list.push(new Account(accountId, appConfig.accounts[accountId]))
  }

  moment.locale(appConfig.locale)

  const store = new Vuex.Store({
    state: {
      menu: {},
      balances: {},
      currency: 'USD',
      user: appConfig.user,
      currencies: appConfig.currencies,
      factors: appConfig.factor,
      // month: appConfig.month
    },

    mutations: {
      balances: function (state, o) {
        state.balances = o
      },

      currency: function (state, value) {
        state.currency = value
      },

      // wallet: function (state, {id, wallet}) {
      //   state.accounts.find(a => a.id === id).wallet = wallet
      // }
    },

    getters: {
      balances: function (state) {
        return Object.assign({}, state.balances)
      },
      currency: function (state) {
        return state.currency
      },
      // month: function (state) {
      //   return state.month
      // },
      nick: function (state) {
        return state.user.nick
      },
      currencies: function (state) {
        return state.currencies.slice()
      },
      // accounts: function (state) {
      //   return state.accounts.slice()
      // },
      // flat: function (state) {
      //   return state.accounts.blockchain(false)
      // },
      // blockchain: function (state) {
      //   return state.accounts.blockchain(true)
      // },
      factor: function (state) {
        return state.factors[state.currency]
      }
    }
  })

// w.store = store

  Vue.filter('factor', function (v, currency) {
    if (!currency) {
      currency = store.getters.currency
    }
    const factor = appConfig.factor[currency]
    return (v / factor).toFixed(Math.log(factor) / Math.log(10))
  })

  Vue.filter('round', function (v, scale) {
    if ('number' !== typeof scale) {
      scale = 2
    }
    const factor = Math.pow(10, scale)
    return Math.round(v * factor) / factor
  })

  function lazy(o) {
    return {
      template: '<component v-bind:is="content"></component>',
      data: function () {
        return {
          content: {
            template: '<div class="loading">Loading...</div>'
          }
        }
      },
      created: function () {
        const self = this
        const staticContent = document.getElementById('form-content')
        if (staticContent) {
          this.setContentTemplate(staticContent.innerHTML)
          staticContent.remove()
        }
        else {
          // if (lazy.cache[this.$route.path]) {
          //   self.content = lazy.cache[this.$route.path]
          // }
          // else {
            jQuery.get(this.$route.path, function (template) {
              lazy.cache[self.$route.path] = self.setContentTemplate(template)
            })
          // }
        }
      },
      methods: {
        setContentTemplate: function (content) {
          const b = {
            template: content,
            store: store,
            mounted: function () {
              const self = this
              $('.extension').each(function (i, ex) {
                const $ex = $(ex)
                Object.assign(self, JSON.parse($ex.html()))
                $ex.remove()
              })

              const $upload = $('#upload_avatar')
              $upload.find('button').click(function () {
                $upload.find('[type=file]').click()
              })
              $('[type=file]').change(function (e) {
                const input = e.target
                const file = input.files[0]
                if (file) {
                  const isTooLarge = file.size > appConfig.fileSize
                  if (isTooLarge) {
                    input.value = ''
                    Alert.error(t('The file size must be less than {size}', {
                      name: file.name,
                      size: (appConfig.fileSize / (1024 * 1024)) + 'MB'
                    }))
                  }
                  else {
                    Alert.clear()
                  }
                  $upload.find('.filename').html(isTooLarge ? '' : file.name)
                }
              })
            },
          }
          return this.content = Object.assign(b, o)
        }
      },
    }
  }

  lazy.cache = {}

  const Invoice = {
    computed: {
      account: function () {
        const self = this
        return this.accounts.find(function (a) {
          return a.currency === self.$store.getters.currency && a.system === self.system
        })
      },
      minimal: function () {
        return this.account ? this.account.min / this.$store.getters.factor : 0
      },
      pattern: function () {
        return this.account ? this.account.pattern : '.*'
      },
      flat: function () {
        return this.accounts.blockchain(false)
      },
      blockchain: function () {
        return this.accounts.blockchain(true)
      },
    },
    data: function () {
      return {
        system: $('[name="Invoice[system]"]').val(),
        wallet: null,
        accounts: Account.list
      }
    }
  }

  function view(o) {
    const template = document.getElementById(o.name)
    if (template) {
      o.template = template.innerHTML
      $(template).remove()
      Vue.component(o.name, o)
      return o
    }
  }

  const Structure = view({
    name: 'structure',
    data: function () {
      return {
        size: 1,
        nick: this.$route.params.nick
      }
    }
  })

  new Vue({
    el: '.widget.balance',
    store: store
  })

  view({
    name: 'children',
    props: ['nick', 'level'],
    data: function () {
      return {rows: []}
    },
    created: function () {
      const self = this
      $.getJSON('/children/' + this.nick + '/json', function (r) {
        self.rows = r.result.map(function (row) {
          row.bonus = row.buy * appConfig.bonuses[+self.level + 1]
          return new User(row)
        })
        // self.$emit('size', self.rows.length)
      })
    },
    methods: {
      open: function (nick) {
        this.rows.forEach(function (row) {
          if (row.nick === nick) {
            return row.active = !row.active
          }
        })
      }
    }
  })

  const ProgramList = view({
    name: 'program-list',
    store: store,
    data: function () {
      return {
        list: [],
        id: 0,
        amount: 0,
        rates: null,
        factor: appConfig.factor,
        errors: {
          amount: ''
        }
      }
    },
    created: function () {
      const self = this
      $.getJSON('/program/index', function (r) {
        self.list = Object.values(r.result).map(function (o) {
          return new Program(o)
        })
        self.rates = r.rates
      })
    },
    computed: {
      valid: function () {
        return this.id > 0 && this.amount > 0 && !this.errors.amount
      },
    },
    methods: {
      rate: function (usd) {
        usd = usd / 100
        const c = this.$store.getters.currency
        return 'USD' === c
          ? usd
          : (usd / this.rates[c]).toFixed(Math.log(this.factor[c]) / Math.log(10))
      },

      submit: function () {
        const self = this
        const data = {
          Program: {
            id: +this.id,
            amount: +this.amount,
            currency: this.$store.getters.currency
          }
        }
        data[yii.getCsrfParam()] = yii.getCsrfToken()
        $.post(location.pathname + '?format=json', data, function (r) {
          if (r.success) {
            Alert.success('Successful')
            self.id = 0
            self.$router.push({path: '/my-plans'})
          }
          else {
            self.errors.amount = ''
            if (r.errors) {
              for (var key in r.errors) {
                if (r.errors[key] instanceof Array) {
                  self.errors[key] = r.errors[key].join('\n')
                }
              }
            }
          }
        })
      }
    }
  })

  const NodeList = view({
    name: 'node-list',
    store: store,
    data: function () {
      return {
        list: [],
        model: null,
        amount: 0
      }
    },
    created: function () {
      const self = this
      $.getJSON('/node/index', function (r) {
        self.list = Object.values(r.result).map(function (o) {
          return new Node(o)
        })
      })
    }
  })

  const TransferList = view({
    name: 'transfer-list',
    data: function () {
      return {
        rows: [],
        more: true,
        type: null
      }
    },
    created: function () {
      const self = this
      this.load()

      function reload() {
        for (var i = 0; i < self.rows.length; i++) {
          if ('pending' === self.rows[i].status) {
            console.log('reload ' + self.type)
            setTimeout(reload, 20 * 1000)
            break
          }
        }
        self.rows = []
        self.load(self.type)
      }

      // setTimeout(reload, 20 * 1000)
    },
    updated: function () {
      // if (this.$refs.more && this.rows.length <= 20) {
      //   this.$refs.more.scrollIntoView()
      // }
    },
    computed: {
      options: function () {
        return {
          type: this.type,
          offset: this.rows.length
        }
      }
    },
    methods: {
      load: function (type) {
        const self = this
        $.getJSON('transfer/index?' + $.param('undefined' === typeof type ? this.options : {
          type: type,
          offset: 0
        }), function (r) {
          self.rows.push.apply(self.rows, r.result.map(function (o) {
            return new Transfer(o)
          }))
          self.more = r.result.length >= 20
        })
      }
    },
    watch: {
      type: function (v) {
        this.rows = []
        this.load(v)
      }
    }
  })

  const ProfileSettings = lazy()
  const PasswordSettings = lazy()
  const Payment = lazy(Invoice)
  const Withdraw = lazy(Invoice)
  const Autosubmit = lazy({
    mounted: function () {
      $('#payeer-checkout-form').submit()
      if (this.$refs && this.$refs.autosubmit) {
        this.$refs.autosubmit.submit()
      }
      else {
        console.error('No autosubmit')
      }
    }
  })

  view({
    name: 'control',
    props: ['name'],
    data: function () {
      return {
        order: 0
      }
    },
    created: function () {

    },
    watch: {}
  })
  appConfig.month = new Date().getMonth()


  const languages = {
    en: '🇬🇧',
    ru: '🇷🇺'
  }

  appConfig.languages = {}
  appConfig.locales.forEach(function (locale) {
    appConfig.languages[locale] = languages[locale]
  })

  new Vue({
    el: '#language',
    data: appConfig,
    methods: {
      changeLanguage(code) {
        cookies.set('locale', code)
        location.reload()
      }
    }
  })

  view({
    name: 'chart',
    store: store,
    data: function () {
      return {
        month: (new Date()).getMonth(),
        monthIncome: -1
      }
    },
    mounted: function () {
      if (!w.chart) {
        w.chart = new Chart(store)
        w.chart.selector = '.chart-place svg'
        this.load()
      }
    },
    methods: {
      load: function () {
        const self = this
        w.chart.load()
        w.profile.loadInformers()
        $.getJSON('/month-income/' + this.$store.getters.currency + '/' + (this.month + 1), function (r) {
          self.monthIncome = r.result
        })
      }
    },
    computed: {
      incomeForMonth() {
        return t('Income for {month}', {month: moment.months(4, 'MMM')})
      }
    }
  })

  const Profile = view({
    name: 'profile',
    store: store,
    created: function () {
      w.profile = this
      // const self = this
      // $.getJSON('/transfer/turnover', function (r) {
      //   self.turnover = r.result
      // })
      // this.loadInformers()
    },

    methods: {
      loadInformers: function () {
        const self = this
        const c = this.$store.getters.currency
        $.getJSON('/summary/' + c, function (r) {
          if (r.result) {
            Object.assign(self, r.result)
          }
        })
      }
    },

    data: function () {
      return {
        buy: 0,
        accrue: 0,
        bonus: 0,
        turnover: 0,
        withdraw: 0
      }
    }
  })

  function logout() {
    location.pathname = '/user/logout'
  }

  $.getJSON('/transfer/balance', function (r) {
    if (r.result) {
      store.commit('balances', r.result)
    }
  })

  if (!_st && /(antikvar-plus\.com|ter.tech)$/.test(location.hostname)) {
    const routes = [
      {path: '/profile/:nick', name: 'Profile', component: Profile},
      {path: '/children/:nick', name: 'Referrals', component: Structure},
      {path: '/plans', name: 'Plans', component: ProgramList},
      {path: '/my-plans', name: 'My plans', component: NodeList},
      {path: '/profile/:nick/settings', name: 'Settings', component: ProfileSettings},
      {path: '/password/:nick/settings', name: 'Password', component: PasswordSettings},
      {path: '/pay/:nick', name: 'Payment', component: Payment},
      {path: '/withdraw/:nick', name: 'Withdrawal request', component: Withdraw},
      {path: '/pay-pm/:amount', name: 'Pay Perfect Money', component: Autosubmit},
      {path: '/pay-payeer/:amount', name: 'Pay Payeer', component: Autosubmit},
      {path: '/pay-advcash/:amount', name: 'Pay AdvCash', component: Autosubmit},
      {path: '/finances', name: 'Financial history', component: TransferList},
      {path: '/advertise', name: 'Advertisement', component: Advertise},
      {path: '/logout/:nick', component: logout},
      {path: '/', redirect: '/profile/' + appConfig.user.nick},
      {path: '*', component: NotFound}
    ]

    const router = new VueRouter({
      base: '/',
      mode: 'history',
      routes: routes
    })

    view({
      name: 'main-menu',
      router: router,
      data: function () {
        return {menu: appConfig.menu}
      }
    })

    const bread = new Vue({
      el: '#bread',
      router: router,
      data: {
        name: '',
        opened: false
      }
    })

    router.afterEach(function (from, to) {
      bread.name = from.name ? t(from.name) : ''
    })

    const app = new Vue({
      store: store,
      el: 'main',
      router: router
    })

    new Vue({
      el: '.widget.invoice-nav',
      router: router
    })
  }

  // w.app = app
})
