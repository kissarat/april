document.addEventListener('DOMContentLoaded', function () {
  const cookieParam = 'sponsor'
  const office = '//office.' + location.hostname
  var sponsor = /sp=([\w_]+)/.exec(location.search)
  sponsor = sponsor ? sponsor[1] : null
  var cookieSponsor = new RegExp(cookieParam + '=([\\w_]+)').exec(document.cookie)
  cookieSponsor = cookieSponsor ? cookieSponsor[1] : null
  var isNew = !!sponsor
  if (cookieSponsor) {
    if (sponsor === cookieSponsor) {
      isNew = false
    }
    else if (!sponsor) {
      sponsor = cookieSponsor
    }
  }
  const callback = 'visit' + Date.now()
  if (sponsor) {
    document.cookie = cookieParam + '=' + sponsor + '; path=/; max-age=25920000'
    if (isNew) {
      const script = document.createElement('script')
      script.src = office + '/new-visit/' + sponsor + '?callback=' + callback
      document.head.appendChild(script)
    }
    [].forEach.call(document.querySelectorAll('[href$="/register"]'), function (a) {
      a.href = a.href.replace('/register', '/register/' + sponsor)
    });
    [].forEach.call(document.querySelectorAll('[href$="/login"]'), function (a) {
      a.href = a.href.replace('/login', '/login/' + sponsor)
    })
  }

  window[callback] = function () {
    // console.log('New visit', a)
    delete window[callback]
  }

  if (!/https?:\/\/(localhost:8888\/)?(www\.)?antikvar-plus\.com/.test(location.href)) {
    document.getElementById('wrapper').innerHTML = ''
  }
})
