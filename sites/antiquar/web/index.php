<?php
/**
 * Last modified: 18.05.15 00:43:48
 * Hash: 883aa79bb03e380447e9eaa8103973467e871eec
 */

$config = require '../config/web.php';

$config['language'] = 'en';
if (isset($_COOKIE['locale']) && in_array($_COOKIE['locale'], ['en', 'ru'])) {
    $config['language'] = $_COOKIE['locale'];
} elseif (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    preg_match('/(ru|uk|kk)/', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $m_locale);
    if ($m_locale) {
        $config['language'] = 'ru';
    }
}
if (isset($_GET['locale']) && in_array($_GET['locale'], ['en', 'ru'])) {
    $config['language'] = $_GET['locale'];
}

date_default_timezone_set('UTC');

require_once ROOT . '/vendor/yiisoft/yii2/Yii.php';
require_once ROOT . '/vendor/autoload.php';

unset($config['components']['reCaptcha']);

//$container->setDefinitions(optional('/config/class.php'));
Yii::setAlias('@site', SITE);
$app = new \yii\web\Application($config);
$app->run();
