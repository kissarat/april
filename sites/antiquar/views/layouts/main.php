<?php

use app\helpers\Html;
use app\sites\antiquar\assets\AntiquarAsset;
use app\widgets\Alert;
use yii\helpers\Url;

AntiquarAsset::register($this);

/**
 * @var \app\models\User $user
 */
$user = Yii::$app->user->identity;

$items = [
    ['label' => Yii::t('app', 'Profile'), 'url' => ['user/profile', 'nick' => $user->nick]],
    ['label' => Yii::t('app', 'Plans'), 'url' => ['program/index']],
    ['label' => Yii::t('app', 'My plans'), 'url' => ['node/index']],
    ['label' => Yii::t('app', 'Financial'), 'url' => ['transfer/index']],
    ['label' => Yii::t('app', 'Structure'), 'url' => ['structure/index', 'nick' => $user->nick]],
    ['label' => Yii::t('app', 'Settings'), 'url' => ['settings/profile', 'nick' => $user->nick]],
    ['label' => Yii::t('app', 'Advertisement'), 'url' => ['home/advertise']],
    ['label' => Yii::t('app', 'Logout'), 'url' => ['user/logout', 'nick' => $user->nick]],
];

foreach ($items as &$item) {
    $item['url'] = Url::to($item['url']);
}

$this->beginPage();

$homeUrl = Html::a('<i class="fa fa-home"></i> ' . Yii::t('app', 'Home'), ['user/profile', 'nick' => $user->nick]);
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Antikvar Plus</title>


    <link rel="shortcut icon" href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon2.png"
          type="image/x-icon"/>
    <link rel="apple-touch-icon" href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon2.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon.png">
    <link rel="apple-touch-icon" sizes="144x144"
          href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon.png">

    <?= Html::csrfMetaTags() ?>
    <?= $this->head() ?>
</head>
<body class="s1 s2 s3">
<?= $this->beginBody() ?>
<?php require_once '_widgets.php' ?>
<div id="app">
    <header>
        <div class="container">
            <div class="row">
                <div class="widget invoice-nav pay-button col-lg-offset-5 col-lg-4 col-md-offset-5 col-md-5 col-sm-12 col-xs-12">
                    <router-link
                            to="<?= Url::to(['invoice/payment', 'nick' => $user->nick]) ?>"><?= Yii::t('app', 'Pay ') ?></router-link>
                    <router-link
                            to="<?= Url::to(['invoice/withdraw', 'nick' => $user->nick]) ?>"><?= Yii::t('app', 'Withdraw') ?></router-link>
                </div>
                <div class="lang col-lg-1 col-lg-offset-2 col-md-offset-2 col-md-1 hidden-sm hidden-xs">
                    <div id="language" class="content btn-group langu" v-cloak>
                        <button id="lang" type="button" class=" dropdown-toggle lang-button" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            <span>{{ locale }}</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-item"
                                 v-for="(flag, code) in languages"
                                 v-on:click="changeLanguage(code)">
                                <span>{{ code }}</span>
                                <span class="name">{{ flag }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="balance-bg">
            <div class="container">
                <div class="row">
                    <div class="widget balance">
                        <div class="col-lg-2 col-lg-offset-3 col-md-2 col-md-offset-3 balance-position col-sm-4 col-xs-4">
                            <span class="txt-white"><?= Yii::t('app', 'Balances:') ?></span>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-8">
                            <div class="balances-mobile">
            <span v-for="(value, currency) in $store.getters.balances"
                  v-bind:data-currency="currency"
                  v-bind:class="{zero: !(value > 0)}">
                {{ value | factor(currency) }}
            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="main">
        <div class="container">
            <div class="row">
                <div id="bread">
                    <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                        <main-menu></main-menu>
                    </div>
                    <div class="col-lg-9 col-md-9 hidden-xs hidden-sm">
                        <div class="links-site" v-cloak>
                            <?= $homeUrl ?>
                            <span v-if="name"><span> > </span>{{ name }}</span>
                        </div>
                    </div>
                    <div class="mobile-block hidden-lg hidden-md">
                        <div class="menu-button col-sm-2 col-xs-2" @click="opened = true">
                            <i class="fa fa-bars"></i>
                        </div>
                        <div class="col-sm-8 col-xs-8">
                            <div class="links-site" v-cloak>
                                <?= $homeUrl ?>
                                <span v-if="name"><span> > </span>{{ name }}</span>
                            </div>
                        </div>
                        <div class="lang col-sm-2 col-xs-2">
                            <a href="#" class="lang-button">RU</a>
                        </div>
                        <div class="menu-container" @click="opened = false">
                            <main-menu v-if="opened"></main-menu>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 col-xs-2 col-lg-9 col-md-9">
                    <?= Alert::widget() ?>
                </div>
                <main>
                    <router-view></router-view>
                </main>
            </div>
        </div>
    </div>

    <footer>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="hidden-sm hidden-xs col-lg-3 col-md-3">
                        <img src="https://office.antikvar-plus.com/images/logo-white.png" alt="logo antikvar plus"
                             class="logo-footer">
                    </div>
                    <div class="hidden-sm hidden-xs col-lg-9 col-md-9">
                        <h4 data-fontsize="19" data-lineheight="32">О нас</h4>
                        <p> Зарабатывайте с инвестиционных планов от 15% до 88% прибыли сроком до 40 дней</p>
                    </div>
                    <div class="col-lg-12 col-md-12 hidden-xs hidden-sm">
                        <div class="divider"></div>
                    </div>
                    <div class="hidden-sm hidden-xs col-lg-3 col-md-3">
                        <ul class="footer-nav">
                            <li class="li">
                                <a href="https://antikvar-plus.com/" class="menu-item">Главная</a>
                            </li>
                            <li class="li">
                                <a href="https://antikvar-plus.com/?page_id=6" class="menu-item">О нас</a>
                            </li>
                            <li class="li">
                                <a href="https://antikvar-plus.com/?page_id=1360" class="menu-item">Новости</a>
                            </li>
                            <li class="li">
                                <a href="https://antikvar-plus.com/?page_id=11" class="menu-item">Частые вопросы</a>
                            </li>
                            <li class="li">
                                <a href="https://antikvar-plus.com/?page_id=13" class="menu-item">Контакты</a>
                            </li>
                        </ul>
                    </div>
                    <div class="hidden-sm hidden-xs col-lg-3 col-md-3">
                        <ul class="footer-nav">
                            <li class="li">
                                <a href="https://antikvar-plus.com/?page_id=1223" class="menu-item">Инвесторам</a>
                            </li>
                            <li class="li">
                                <a href="https://antikvar-plus.com/?page_id=845" class="menu-item">Партнерам</a>
                            </li>
                            <li class="li">
                                <a href="https://office.antikvar-plus.com/register" class="menu-item">Регистрация</a>
                            </li>
                            <li class="li">
                                <a href="https://office.antikvar-plus.com/login" class="menu-item">Вход</a>
                            </li>
                        </ul>
                    </div>
                    <div class="hidden-sm hidden-xs col-lg-3 col-md-3">
                        <ul class="contacts">

                            <div id="gallery-1" class="gallery galleryid-1008 gallery-columns-3 gallery-size-full">
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/xrp.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/pm.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/payeer.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <br style="clear: both">
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/ltc.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/eth.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/etc.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <br style="clear: both">
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/doge.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/btc.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <dl class="gallery-item">
                                    <dt class="gallery-icon landscape">
                                        <img src="https://antikvar-plus.com/wp-content/uploads/2018/05/adv.png"
                                             class="attachment-full size-full" alt=""
                                             width="60" height="60">
                                    </dt>
                                </dl>
                                <br style="clear: both">
                            </div>
                        </ul>
                    </div>


                    <div class="hidden-sm hidden-xs col-lg-3 col-md-3">
                        <ul class="contacts">
                            <li>
                                <a href="mailto:support@antikvar-plus.com" class="social-media email">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i> support@antikvar-plus.com
                                </a>
                            </li>
                            <li>
                                <a href="https://web.telegram.org/" class="social-media telegram">
                                    <i class="fa fa-telegram" aria-hidden="true"></i> Telegram ? </a>
                            </li>
                            <li>
                                <a href="#" class="social-media skype">
                                    <i class="fa fa-skype" aria-hidden="true"></i> Skype ? </a>
                            </li>
                            <li>
                                <a href="https://antikvar-plus.com/?page_id=13#form" class="social-media skype">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i> Написать письмо</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copywrite">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
                        <div class="copywrite-txt">
                            <p>© antikvar-plus.com 2018</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<div id="banners">
    <a href="https://payeer.com/?partner=6882198" target="_blank">
        <img src="https://payeer.com/bitrix/templates/difiz/img/banner/ru/468x60.gif" alt=""></a>
</div>
<?= Html::context(['menu' => $items, 'translation' => true]) ?>
<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
