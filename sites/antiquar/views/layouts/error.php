<?php

use app\helpers\Html;
use app\sites\antiquar\assets\AntiquarAsset;
use app\widgets\Alert;

AntiquarAsset::register( $this );

$title = $this->title ? $this->title . ' — ' . Yii::$app->name : Yii::$app->name;

$this->beginPage();

?>
<!DOCTYPE>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?></title>
    <?= $this->head() ?>
    <link rel="stylesheet" type="text/css" href="/style.css"/>
</head>
<body class="s1 s2 <?= Yii::$app->user->getIsGuest() ? 'indoor-error' : 'outdoor' ?>" data-mode="<?= YII_ENV ?>">
<?= $this->beginBody() ?>
<?php require '_analytics.php' ?>
<div id="app">
    <?= Alert::widget() ?>
    <main>
        <?= $content ?>
    </main>
</div>
<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
