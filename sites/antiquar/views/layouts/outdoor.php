<?php

use app\helpers\Html;
use app\sites\antiquar\assets\AntiquarAsset;

AntiquarAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Antikvar Plus</title>

    <link rel="shortcut icon" href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon2.png"
          type="image/x-icon"/>
    <link rel="apple-touch-icon" href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon2.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon.png">
    <link rel="apple-touch-icon" sizes="144x144"
          href="https://antikvar-plus.com/wp-content/uploads/2018/06/favicon.png">

    <?= Html::csrfMetaTags() ?>
    <?= $this->head() ?>
</head>
<body class="s1 s2">
<?= $this->beginBody() ?>

<div class="login-layout">
    <div class="container">
        <div class="row">
            <div class="content">
                <?= $content ?>
            </div>
        </div>
    </div>
    <div id="banners">
        <a href="https://payeer.com/?partner=6882198" target="_blank">
            <img src="https://payeer.com/bitrix/templates/difiz/img/banner/ru/468x60.gif" alt=""></a>
    </div>
</div>
<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
