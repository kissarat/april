<?php
/**
 * @version 18-05-06
 */
?>

<?php if ( Yii::$app->request->isPost ): ?>
    <template id="form-content">
		<?php require_once 'form.php' ?>
    </template>
<?php endif ?>

<template id="main-menu">
    <div class="widget menu">
        <div class="header-menu">
            <div class="logo">
                <img src="/images/logo-black.png" alt="">
            </div>
            <div class="user-photo">
                <div class="photo"
                     style="background-image: url('<?= $user->avatar ?: '/images/user-photo.jpg' ?>')"></div>
            </div>
            <div class="user-name">
                <span><?= $user->getName() ?></span>
            </div>
        </div>
        <ul class="main-menu">
            <li v-for="item in menu">
                <router-link v-bind:to="item.url">{{ item.label }}</router-link>
            </li>
        </ul>
    </div>
</template>

<template id="chart">
    <div class="widget chart">
        <div class="chart-header">
            <h4><?= Yii::t( 'app', 'Income' ) ?></h4>
            <div class="currency">
                <div v-for="currency in $store.getters.currencies"
                     v-bind:data-currency="currency"
                     v-on:click="$store.commit('currency', currency); load()"
                     v-bind:class="{active: $store.getters.currency === currency}">
                    {{ currency }}
                    <div class="chart-line"></div>
                </div>
            </div>
        </div>
        <svg width="100%" height="210"></svg>
        <div class="chart-footer">
            <p>{{incomeForMonth}}
                <span v-bind:data-currency="$store.getters.currency">{{monthIncome}}</span>
            </p>
        </div>
    </div>
</template>

<template id="profile">
    <div class="user profile">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
            <div class="invest-button">
                <router-link to="/plans"><?= Yii::t( 'app', 'Invest' ) ?></router-link>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="reflinks">
                <input v-bind:value="'https://antikvar-plus.com/ref/' + $store.getters.nick"/>
            </div>
        </div>
        <div class="informer col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <div>
                <div>
					<?= Yii::t( 'app', 'Purchased' ) ?>
                    <span v-bind:data-currency="$store.getters.currency">{{ buy | factor }}</span>
                </div>
                <div>
					<?= Yii::t( 'app', 'Income' ) ?>
                    <span v-bind:data-currency="$store.getters.currency">{{ accrue | factor }}</span>
                </div>
                <div>
					<?= Yii::t( 'app', 'Referral' ) ?>
                    <span v-bind:data-currency="$store.getters.currency">{{ bonus | factor }}</span>
                </div>
                <div>
					<?= Yii::t( 'app', 'Withdrawals' ) ?>
                    <span v-bind:data-currency="$store.getters.currency">{{ withdraw | factor }}</span>
                </div>
            </div>
        </div>
        <div class="chart-place col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <chart></chart>
        </div>
    </div>
</template>

<template id="children">
    <div class="children index" v-bind:data-level="level + 1">
        <div v-for="row in rows" v-bind:class="{referral: true, active: row.active}">
            <div v-on:click="row.children > 0 && level < 1 && open(row.nick)" class="info">
                <div class="nick" data-tooltip="<?= Yii::t( 'app', 'Login & Name' ) ?>">
                    <span class="avatar"
                          v-bind:style="{'background-image': 'url(' + (row.avatar || '/images/user-photo.jpg') + ')'}"></span>
                    <span>{{ row.name }}</span>
                </div>
                <div class="referral" data-tooltip="<?= Yii::t( 'app', 'Partner\'s Team' ) ?>">{{ row.children }}</div>
                <div class="buy" data-tooltip="<?= Yii::t( 'app', 'Invested' ) ?>">{{ row.buy | factor('USD') }}</div>
                <div class="bonus" data-tooltip="<?= Yii::t( 'app', 'Invested' ) ?>">{{ row.bonus | factor('USD') }}</div>
                <div class="skype" data-tooltip="<?= Yii::t( 'app', 'Skype' ) ?>">{{ row.skype }}</div>
                <div class="created" data-tooltip="<?= Yii::t( 'app', 'Registered Date' ) ?>">{{ row.localeCreated }}</div>
            </div>
            <children v-if="row.active"
                      v-bind:nick="row.nick"
                      v-bind:level="+level + 1">
            </children>
        </div>
    </div>
</template>

<template id="structure">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="structure index">
            <h2><?= Yii::t( 'app', 'Structure' ) ?></h2>
            <div v-if="size <= 0" class="empty"><?= Yii::t( 'app', 'Nobody' ) ?></div>
            <div v-else class="div-table">
                <div class="referrals-block">
                    <div class="referral header">
                        <div class="info">
                            <div class="nick" data-tooltip="<?= Yii::t( 'app', 'Login & Name' ) ?>">>
                                <span class="avatar">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </span>
                                <span></span>
                            </div>
                            <div class="referral" data-tooltip="<?= Yii::t( 'app', 'Partner\'s Team' ) ?>"><i class="fa fa-users" aria-hidden="true"></i></div>
                            <div class="buy" data-tooltip="<?= Yii::t( 'app', 'Invested' ) ?>"><i class="fa fa-usd" aria-hidden="true"></i></div>
                            <div class="bonus" data-tooltip="<?= Yii::t( 'app', 'Invested' ) ?>"><i class="fa fa-suitcase" aria-hidden="true"></i></div>
                            <div class="skype" data-tooltip="<?= Yii::t( 'app', 'Skype' ) ?>"><i class="fa fa-skype" aria-hidden="true"></i></div>
                            <div class="created" data-tooltip="<?= Yii::t( 'app', 'Registered Date' ) ?>"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                        </div>
                    </div>
                    <children v-bind:nick="nick" @size="size = $event" level="0"></children>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="transfer-list">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="transfer index">
            <h2><?= Yii::t( 'app', 'Financial' ) ?></h2>
            <div class="control">
                <button v-bind:class="{'antikvar-btn': true, active: !type}" v-on:click="type = null">
					<?= Yii::t( 'app', 'All' ) ?></button>
                <button v-bind:class="{'antikvar-btn': true, active: 'payment' === type}" v-on:click="type = 'payment'">
					<?= Yii::t( 'app', 'Payments' ) ?></button>
                <button v-bind:class="{'antikvar-btn': true, active: 'accrue' === type}" v-on:click="type = 'accrue'">
					<?= Yii::t( 'app', 'Income' ) ?></button>
                <button v-bind:class="{'antikvar-btn': true, active: 'bonus' === type}"
                        v-on:click="type = 'bonus'"><?= Yii::t( 'app', 'Referral bonuses' ) ?></button>
                <button v-bind:class="{'antikvar-btn': true, active: 'withdraw' === type}"
                        v-on:click="type = 'withdraw'">
					<?= Yii::t( 'app', 'Withdrawals' ) ?>
                </button>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th><control><?= Yii::t( 'app', 'Amount' ) ?></control></th>
                        <th><control><?= Yii::t( 'app', 'Currency' ) ?></control></th>
                        <th><control><?= Yii::t( 'app', 'Time' ) ?></control></th>
                        <th><control><?= Yii::t( 'app', 'Note' ) ?></control></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="row in rows" v-bind:data-status="row.status">
                        <td class="amount">{{ row.amount | factor(row.currency) }}</td>
                        <td class="currency">{{ row.currency }}</td>
                        <td class="created">{{ row.localeCreated }}</td>
                        <td class="created">{{ row.note }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="more-button" v-if="more && rows.length > 0">
                <div class="pattern-left">
                    <object type="image/svg+xml" data="/images/pattern.svg"></object>
                </div>
                <button ref="more" class="antikvar-btn more" v-on:click="load()">
                    <?= Yii::t( 'app', 'Show more' ) ?>
                </button>
                <div class="pattern-right">
                    <object type="image/svg+xml" data="/images/pattern.svg"></object>
                </div>
            </div>
        </div>
    </div>
</template>

<template class="clock-template">
    <div class="widget clock">
        <div class="circle seconds" v-bind:style="secondsStyle"></div>
        <div class="circle minutes" v-bind:style="minutesStyle"></div>
        <div class="circle hours" v-bind:style="hoursStyle"></div>
    </div>
</template>

<template id="program-list">
    <div class="programs index">
        <div v-bind:class="{program: true, active: id === p.id}"
             v-for="p in list" v-bind:id="'program' + p.id">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="plans">
                    <h2>{{ p.name }}</h2>
                    <ul>
                        <li class="range">
                            <span class="name"><?= Yii::t( 'app', 'Min. investment:' ) ?></span>
                            <span class="value money" v-bind:data-currency="$store.getters.currency">{{ rate(p.min) }}</span>
                            -
                            <span class="value money" v-bind:data-currency="$store.getters.currency">{{ rate(p.max) }}</span>
                        </li>
                        <li class="rate">
                            <span class="name"><?= Yii::t( 'app', 'Interest rate:' ) ?></span>
                            <span class="value percent">{{ p.percent * 100 }}%</span>
                        </li>
                        <li class="duration">
                            <span class="name"><?= Yii::t( 'app', 'Duration:' ) ?></span>
                            <span class="value">{{p.days}} <?= Yii::t('app','days') ?></span>
                        </li>
                        <li class="income">
                            <span class="name"><?= Yii::t( 'app', 'Profit:' ) ?></span>
                            <span class="value percent">{{ p.days * p.percent * 100 - 100 | round }}%</span>
                        </li>
                    </ul>
                    <form v-if="id === p.id" class="form">
                        <div class="form-header">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false">
                                {{ $store.getters.currency }}
                            </button>
                            <div class="dropdown-menu">
                                <div class="dropdown-item"
                                     v-for="c in $store.getters.currencies"
                                     v-on:click="$store.commit('currency', c)">{{ c }}
                                </div>
                            </div>
                            <div v-bind:class="{'form-group': true, 'has-error': !valid}">
                                <input class="form-control" v-model="amount"/>
                            </div>
                        </div>
                        <div class="help-block">{{ errors && errors.amount }}</div>

                        <button type="button" v-on:click="submit()"><?= Yii::t( 'app', 'Buy' ) ?></button>
                    </form>
                    <button v-else type="button" v-on:click="id = p.id"><?= Yii::t( 'app', 'Buy' ) ?></button>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="node-list">
    <div class="nodes index">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <h2><?= Yii::t( 'app', 'My plans' ) ?></h2>
            <div v-if="list.length <= 0" class="empty">
				<?= Yii::t( 'app', 'You do not yet have open investment plans' ) ?>
            </div>
            <div v-else class="node" v-for="n in list" v-bind:data-program="n.node">
                <div class="img-plan"></div>
                <div class="inform-plan">
                    <h2>{{ n.program.name }}</h2>
                    <div class="header-my-plan">
                        <div class="white-info">
                            <p><?= Yii::t( 'app', 'Investment amount:' ) ?>
                                <span class="value money"
                                      v-bind:data-currency="n.currency">{{ n.amount | factor(n.currency) }}</span>
                            </p>
                        </div>
                        <div class="white-info">
                            <p> <?= Yii::t( 'app', 'Amount of income:' ) ?>
                                <span v-bind:data-currency="n.currency">{{ n.income | factor(n.currency) }}</span>
                            </p>
                        </div>
                    </div>
                    <div class="midle-my-plan">
                        <div class="black-info"><p> <?= Yii::t( 'app', 'Opening date:' ) ?> <span>{{ n.opened }}</span>
                            </p></div>
                        <div class="black-info"><p> <?= Yii::t( 'app', 'Closing date:' ) ?> <span>{{ n.closed }}</span>
                            </p></div>
                        <div class="black-info">
                            <p><?= Yii::t( 'app', '<span>{{ n.count }}</span> from <span>{{ n.program.days }}</span>' ) ?></p>
                        </div>
                        <div>
                            <!--                                                <span class="value percent">{{ n.amount * n.days * n.percent * 100 - 100 | round }}%</span>-->
                        </div>
                    </div>
                    <div class="footer-my-plan">
                        <div class="plan-progress">
                            <div class="bg-progress">
                                <div class="success-progress" v-bind:style="{width: n.percent + '%'}"></div>
                                <div class="visual-progress-info"></div>
                            </div>
                            <div class="progress-info">
                                <span v-bind:data-currency="n.currency">0</span>
                                <span v-bind:data-currency="n.currency">{{ n.allIncome | factor(n.currency) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dinamic-info">
                    <div class="clock-box">
                        <div class="header-clock">
                            <clock v-bind:time="n.next"></clock>
                        </div>
                        <div class="footer-clock">
                            <div v-if="n.count < n.program.days" class="opened">
                                <h5><?= Yii::t( 'app', 'Until next payment:' ) ?></h5>
                                <div class="timer">{{ n.untilNext }}</div>
                            </div>
                            <div v-else class="closed">
								<?= Yii::t( 'app', 'The investment is closed' ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="advertise">
    <div>
        Реклама здесь
    </div>
</template>
