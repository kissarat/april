<?php

use app\helpers\Html;

$json = json_encode($state);
$style = file_get_contents(Yii::getAlias('@site/assets/custom/calculator.css'));
$script = file_get_contents(Yii::getAlias('@webroot/external.js'));
//$script = preg_replace( '/\s+/', ' ', $script );
//$script = preg_replace( '/(?<=[^\w]) | (?=[^\w])/', '', $script );
$script .= "($json)";
?>

<div id="calculator">
    <div id="calculator-app">
        <div class="header-calc">
            <img src="https://office.antikvar-plus.com/images/calc.jpg" alt="">
            <h2>Воспользуйтесь калькулятором для расчёта прибыли</h2>
            <p>Выберите инвестиционный план</p>
        </div>
        <div class="program-list">
            <div v-bind:class="{program: true, active: id === p.id}"
                 v-for="p in programs" v-bind:id="'program' + p.id"
                 v-on:click="id = p.id">
                <div class="plans">
                    <div class="cyrcle top left">
                        <div></div>
                    </div>
                    <div class="cyrcle top right">
                        <div></div>
                    </div>
                    <div class="cyrcle bottom left">
                        <div></div>
                    </div>
                    <div class="cyrcle bottom right">
                        <div></div>
                    </div>

                    <h2 v-cloak>{{ p.name }}</h2>
                    <div class="bg-head"></div>
                    <ul>
                        <li class="investment">
                            <span class="name"><?= Yii::t('app', 'Amount:') ?></span>
                            <span class="value money" v-bind:data-currency="currency" v-cloak>{{ rate(p.min) }} – {{ rate(p.max) }} </span>
                        </li>
                        <li class="rate">
                            <span class="name"><?= Yii::t('app', 'Interest rate:') ?></span>
                            <span class="value percent" v-cloak>{{ p.percent * 100 }}%</span>
                        </li>
                        <li class="duration">
                            <span class="name"><?= Yii::t('app', 'Duration:') ?></span>
                            <span class="value" v-cloak>{{ p.days }} <?= Yii::t('app', 'days') ?></span>
                        </li>
                        <li class="income">
                            <span class="name"><?= Yii::t('app', 'Profit:') ?></span>
                            <span class="value percent" v-cloak>{{ p.days * p.percent * 100 - 100 | round }}%</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="calculator-bottom">
            <div class="choose-currency">
                <h3>Выберите валюту:</h3>
                <div class="currencies" v-cloak>
                    <div v-for="c in currencies"
                         v-on:click="currency = c"
                         v-bind:class="{active: c === currency}">{{ c }}
                    </div>
                </div>
            </div>
            <div class="calculator-bottom-right">
                <div class="choose-amount">
                    <label for="invest-amount">Введите сумму инвестиции:</label> <br>
                    <input type="text" id="invest-amount" v-model="amount" v-bind:disabled="!isSelected"/>
                    <div v-if="!isSelected">Вы можете ввести сумму после выбора программы и валюты</div>
                    <div v-if="isSelected" class="error" v-cloak>
                        <p class="error-message" v-if="amount < min">Введите сумму больше <span
                                    v-bind:data-currency="currency">{{ min }}</span></p>
                        <p class="error-message" v-if="amount >= max">Введите сумму не более <span
                                    v-bind:data-currency="currency">{{ max }}</span></p>
                    </div>
                </div>
                <ul class="summary" v-if="isSelected">
                    <li>
                        <span class="name">Ваше ежедневное начисление составит:</span>
                        <span class="value" v-bind:data-currency="currency" v-cloak>{{ daily | factor }}</span>
                    </li>
                    <li>
                        <span class="name">Общая сумма дохода составит:</span>
                        <span class="value" v-bind:data-currency="currency" v-cloak>{{ allIncome | factor }}</span>
                    </li>
                    <li>
                        <span class="name">Чистая прибыль составит:</span>
                        <span class="value" v-bind:data-currency="currency" v-cloak>{{ income | factor }}</span>
                    </li>
                </ul>
                <a href="/" class="invested"><span>Инвестировать</span></a>
            </div>
        </div>
        <?= Html::tag('template', $style, ['id' => 'calculator-style']) ?>
        <?= Html::script($script) ?>
    </div>
</div>
