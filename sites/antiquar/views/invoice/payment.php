<?php

use app\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="invoice payment" xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <div class="crypto col-lg-3 col-md-3 col-sm-12 col-xs-12" v-if="blockchain.length > 0">
        <div class="crypto-bg">
            <div v-bind:id="s.currency" v-for="s in blockchain" v-cloak>
                <div v-if="s.wallet" class="wallet-block">
                    <span class="label">{{ s.name }}</span>
                    <div class="wallet">{{ s.wallet }}</div>
                </div>
                <div v-else class="get-wallet-block">
                    <div v-on:click="s.createWallet()" class="get-wallet">
                        <div class="wallet-border">
                            <span class="icon"></span><?= Yii::t( 'app', 'Get the address' ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="form-layout">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<?php $form = ActiveForm::begin() ?>
            <div class="systems" v-cloak>
                <button type="button"
                        v-for="s in flat"
                        v-bind:id="s.system"
                        v-bind:class="{active: system === s.system}"
                        v-on:click="$store.commit('currency', s.currency); system = s.system">
                    {{ s.name }}
                </button>
            </div>
            <div class="form">
                <h3><?= Yii::t( 'app', 'Payment' ) ?></h3>
                <div class="fields">
                    <div class="visible">
						<?= $form->field( $model, 'amount' )
						         ->textInput( [ 'v-bind:placeholder' => 'minimal' ] ) ?>
                    </div>
                    <div class="hidden">
						<?= $form->field( $model, 'system' )
						         ->hiddenInput( [ 'v-bind:value' => 'system' ] )->label( false ) ?>
						<?= $form->field( $model, 'currency' )
						         ->hiddenInput( [ 'v-bind:value' => '$store.getters.currency' ] )->label( false ) ?>
						<?= $form->field( $model, 'ip' )->hiddenInput()->label( false ) ?>
                    </div>
                </div>
                <div class="control" v-cloak>
					<?= Html::submitButton( Yii::t( 'app', 'Pay' ), [ 'v-bind:disabled' => '!account' ] ) ?>
                </div>
            </div>
			<?php ActiveForm::end() ?>
        </div>
    </div>
</div>
