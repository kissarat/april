<?php

use app\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model \app\models\form\Invoice */

?>
<div class="invoice withdraw">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <?php $form = ActiveForm::begin() ?>
        <h2><?= Yii::t('app', 'Withdrawal request') ?></h2>
        <div class="systems" v-cloak>
            <button v-bind:id="s.id"
                    type="button"
                    v-for="s in accounts"
                    v-bind:class="{active: system === s.system && $store.getters.currency === s.currency}"
                    v-on:click="$store.commit('currency', s.currency); system = s.system">
                {{ s.name }}
            </button>
        </div>
        <div class="form">
            <div class="fields">
                <div class="visible">
                    <?= $form->field($model, 'amount')
                        ->textInput(['v-bind:placeholder' => 'minimal']) ?>
                    <?= $form->field($model, 'wallet')
                        ->textInput([
                            'v-bind:pattern' => 'pattern',
                        ]) ?>
                    <?php
                    if (!empty(Yii::$app->user->identity->pin)) {
                        echo $form->field($model, 'pin');
                    }
                    ?>
                    <?= $form->field($model, 'text')
                        ->textarea() ?>
                </div>
                <div class="hidden">
                    <?= $form->field($model, 'currency')
                        ->hiddenInput(['v-bind:value' => '$store.getters.currency'])
                        ->label(false) ?>
                    <?= $form->field($model, 'system')
                        ->hiddenInput(['v-bind:value' => 'system'])
                        ->label(false) ?>
                    <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>
                </div>
            </div>
            <div class="control" v-cloak>
                <?= Html::submitButton(Yii::t('app', 'Withdraw'), ['v-bind:disabled' => '!account']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
