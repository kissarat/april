<?php

use app\models\User;
use himiklab\yii2\recaptcha\ReCaptcha;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user login">
    <div class=" col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12">
        <div class="form">
            <div class="login-header-form">
                <div class="logo">
                    <img src="/images/logo.png" alt="AI Logistics">
                </div>
            </div>
            <div class="form-content">
                <h1><?= Yii::t( 'app', 'Create your account' ) ?></h1>

                <p class="description"><?= Yii::t( 'app', '' ) ?></p>

				<?php $form = ActiveForm::begin(); ?>
                <div class="fields">
					<?= $form->field( $model, 'nick' )
					         ->textInput( [ 'maxlength' => User::NICK_LENGTH ] ) ?>
					<?= $form->field( $model, 'email' )
					         ->textInput( [ 'maxlength' => User::EMAIL_LENGTH ] ) ?>
					<?= $form->field( $model, 'password' )
					         ->passwordInput( [ 'maxlength' => User::PASSWORD_LENGTH ] ) ?>
					<?= $form->field( $model, 'sponsor' )
					         ->textInput( [ 'maxlength' => User::NICK_LENGTH ] ) ?>
					<?php if ( Yii::$app->has( 'reCaptcha' ) ): ?>
						<?= $form->field( $model, 'reCaptcha' )->widget( ReCaptcha::class, [
						] )->label( false ) ?>
					<?php endif ?>
                </div>

               <!--
            <div class="terms-of-use">
					<?= Yii::t( 'app', 'By clicking the submit button "Signup", I hereby agree '
					                   . 'to and accept the following {url}', [ 'url' => Html::a( 'terms and conditions', '#' ) ] ) ?>
                </div>
 -->
                <div class="links">
                    <div class="green-bg">
						<?= Html::submitButton( Yii::t( 'app', 'Signup' ) ) ?>
                    </div>

                    <div class="control sign-up">
						<?= Html::a( Yii::t( 'app', 'Already have an account? Sign in' ), [ 'user/login' ] ) ?>
                    </div>
					<?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

