<?php

/**
 * Last modified: 18.06.19 21:52:21
 * Hash: 1190732368ea25408495f18d6ef4b0e361843407
 * @var $this yii\web\View
 * @var $message string
 * @var $model app\models\form\RequestPasswordReset
 * @var $form ActiveForm
 */

use app\widgets\Alert;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t( 'app', 'Password recovery request' );
?>

<div class="user request">
    <div class=" col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12">
        <div class="form">
            <div class="login-header-form">
                <div class="logo">
                    <img src="/images/logo.png">
                </div>
            </div>

            <div class="form-content">
                <h1><?= Yii::t( 'app', 'Forgot password?' ) ?></h1>
				<?= Alert::widget() ?>
				<?php $form = ActiveForm::begin(); ?>
                <div class="fields">
                    <p><?= Yii::t( 'app', 'Email' ) ?></p>

					<?= $form->field( $model, 'email' )
					         ->input( 'email', [
						         'placeholder' => Yii::t( 'app', 'Enter Your Email' )
					         ] )
					         ->label( false ) ?>

					<?= $form->field( $model, 'ip' )
					         ->hiddenInput()
					         ->label( false ) ?>
                </div>


                <div class="links">
                    <div class="green-bg">
						<?= Html::submitButton( Yii::t( 'app', 'Send Email' ),
							[ 'class' => 'button full-width' ] ) ?>
                    </div>
                    <div class="sign-up">
						<?= Html::a( Yii::t( 'app', 'Signup' ), [ 'user/signup' ] ) ?>
						<?= Html::a( Yii::t( 'app', 'Login' ), [ 'user/login' ] ) ?>
                    </div>
					<?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
