<?php

use app\models\User;
use himiklab\yii2\recaptcha\ReCaptcha;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

/**
 * @var \app\models\form\Login $model
 */
?>
<div class="user login">
    <div class=" col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12">
        <div class="form">
            <div class="login-header-form">
                <div class="logo">
                    <img src="/images/logo.png">
                </div>
            </div>

            <div class="form-content">
                <h1><?= Yii::t('app', 'Login') ?></h1>
                <?php $form = ActiveForm::begin(); ?>
                <div class="fields">
                    <p><?= Yii::t('app', 'Login') ?></p>
                    <?= $form->field($model, 'login')
                        ->textInput(['maxlength' => User::NAME_LENGTH,
                            'placeholder' => Yii::t('app', "Login")])
                        ->label(false) ?>
                    <p><?= Yii::t('app', 'Password') ?></p>
                    <?= $form->field($model, 'password')
                        ->passwordInput(['maxlength' => User::PASSWORD_LENGTH,
                            'placeholder' => Yii::t('app', "Password")])
                        ->label(false) ?>
                </div>

                <?php if (Yii::$app->has('reCaptcha')): ?>
                    <?= $form->field($model, 'reCaptcha')->widget(ReCaptcha::class, [
                    ])->label(false) ?>
                <?php endif ?>

                <div class="links">
                    <div class="green-bg">
                        <?= Html::submitButton(Yii::t('app', 'Login')) ?>
                    </div>
                    <div class="sign-up">
                        <?= Html::a(Yii::t('app', 'Signup'), ['user/signup']) ?>
                        <?= Html::a(Yii::t('app', 'Forgot password?'), ['user/request']) ?>
                    </div>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

