<?php

namespace app\sites\antiquar\assets;


use app\assets\BootstrapAsset;
use Yii;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

class AntiquarAsset extends AssetBundle
{
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
    ];

    protected static function &postfix($array) {
        $new = [];
        foreach ($array as $src) {
            $src .= '?v=' . Yii::$app->version;
            if (YII_DEBUG && strpos($src,'antiquar') !== false) {
                $src .= '&t=' . time();
            }
            $new[] = $src;
        }
        return $new;
    }

    public function init()
    {
        parent::init();
        if (!Yii::$app->user->getIsGuest()) {
            $this->css[] = YII_DEBUG ? 'font-awesome.css' : 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css';
            $this->js[] = YII_DEBUG ? 'vue.js' : 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js';
            $this->js[] = YII_DEBUG ? 'vuex.js' : 'https://cdnjs.cloudflare.com/ajax/libs/vuex/3.0.1/vuex.min.js';
            $this->js[] = YII_DEBUG ? 'vue-router.js' : 'https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.1/vue-router.min.js';
            $this->js[] = YII_DEBUG ? 'd3.js' : 'https://cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.min.js';
            $this->js[] = YII_DEBUG ? 'moment-with-locales.js' : 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment-with-locales.min.js';
            $this->js[] = '/chart.js';
            $this->js[] = '/antiquar.js';
        }
        $this->css[] = '/antiquar.css';
        $this->css = static::postfix($this->css);
        $this->js = static::postfix($this->js);
    }
}
