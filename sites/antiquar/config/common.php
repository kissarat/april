<?php
/**
 * Last modified: 18.05.22 05:31:24
 * Hash: 44118910c8b0d510cd06a0297d52c30e5122410e
 */

defined('ROOT') or define('ROOT', dirname(__DIR__, 3));
defined('SITE') or define('SITE', dirname(__DIR__));

require_once ROOT . '/config/functions.php';
require_once ROOT . '/config/define.php';

$appId = 'antiquar';
$dbName = $appId;
if (!empty($_SERVER['HTTP_USER_AGENT'])) {
    $m = null;
    preg_match('/theJodoor1Quaof6die3Sei1=(\w+)$/', $_SERVER['HTTP_USER_AGENT'], $m);
    if ($m) {
        $dbName = $m[1];
    }
}
$dbName = getenv('YII_DATABASE_NAME') ?: $dbName;
defined('YII_TEST') or define('YII_TEST', strpos($dbName, 'test_') === 0);


return local('common', test([
    'id' => $appId,
    'name' => 'Antikvar Plus',
    'basePath' => ROOT,
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],

        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'app\base\JsonMessageSource'
                ],
                'Payeer' => [
                    'class' => 'app\base\JsonMessageSource'
                ]
            ]
        ],

        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "pgsql:host=localhost;dbname=" . $dbName,
            'username' => $appId,
            'password' => $appId,
            'charset' => 'utf8',
            'enableSchemaCache' => true, // !YII_DEBUG,
            'schemaCacheDuration' => 360,
            'on afterOpen' => function () {
                \app\helpers\SQL::execute("SET TIME ZONE 'UTC'");
            }
        ],

        'storage' => [
            'class' => 'app\components\RedisStorage',
            'db' => 0
        ],

        'settings' => [
            'class' => 'app\components\Settings',
            'defaults' => [
                'locales' => ['ru', 'en'],
                'currencies' => ['USD', 'BTC', 'ETH', 'ETC', 'LTC', 'DOGE'],
                'systems' => ['perfect', 'payeer', 'advcash', 'ethereum', 'blockio'],
            ]
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
            ],
        ],

        'btc' => [
            'class' => 'app\components\Blockio',
            'key' => '8335-9798-a909-8e9e',
            'wallet' => 'btc'
        ],

        'ltc' => [
            'class' => 'app\components\Blockio',
            'key' => '38c0-d419-3841-3e38',
            'wallet' => 'ltc'
        ],

        'doge' => [
            'class' => 'app\components\Blockio',
            'key' => '1c9b-024b-15e3-e33d',
            'wallet' => 'doge'
        ],

        'eth' => [
            'class' => 'app\components\Ethereum',
            'wallet' => 'eth',
            'origin' => 'http://parity.antiquar:8545'
        ],

        'etc' => [
            'class' => 'app\components\Parity',
            'wallet' => 'etc',
            'origin' => 'http://parity.antiquar:8547'
        ],

        'perfect' => [
            'class' => 'app\components\Perfect',
            'method' => 'GET',
            'wallet' => 'U16960455',
            'secret' => 'GKSHJULnV3pz341aIV6HTTaLT'
        ],

        'payeer' => [
            'class' => '\yarcode\payeer\Merchant',
            'shopId' => 556553813,
            'secret' => '7ewJBQjufho8UtXE',
        ],

        'advcash' => [
            'class' => '\yarcode\advcash\Merchant',
            'accountEmail' => 'antik.var.only@gmail.com',
            'merchantName' => 'Antikvar Plus',
            'merchantPassword' => 'nie6gahnohyeesharah3eef1Gahfiebi7loh'
        ],

        'telegram' => [
            'class' => 'app\components\Telegram',
            'token' => '599559025:AAFnIQi04IJp98N2T5GWS3zQPwgv7e2LOUI',
            'chat_id' => -265943079
        ],
    ],
    'params' => [
        'root' => 'antikvar',
        'fileSize' => 2 * 1024 * 1024,
        'database_name' => $dbName,
        'test' => YII_TEST
    ]
]));
