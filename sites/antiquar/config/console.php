<?php
$config = require 'common.php';

return array_merge_recursive($config, [
    'controllerNamespace' => 'app\console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'app\console\migrations'
            ]
        ],
    ]
]);
