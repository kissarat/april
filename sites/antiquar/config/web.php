<?php

$config = require 'common.php';

$config = extend('web', $config, [
    'defaultRoute' => 'home',

    'controllerNamespace' => 'app\sites\antiquar\controllers',
    'controllerMap' => [
        'user' => 'app\controllers\UserController',
        'transfer' => 'app\controllers\TransferController',
        'structure' => 'app\controllers\StructureController',
        'program' => 'app\controllers\ProgramController',
        'subject' => 'app\controllers\SubjectController',
        'money' => 'app\controllers\MoneyController',
//        'perfect' => 'app\controllers\PerfectController',
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'login/<nick>' => 'user/login',
                'login' => 'user/login',
                'go-<id>' => 'home/go',
                'ref/<nick>' => 'user/signup',
                'register/<nick>' => 'user/signup',
                'register' => 'user/signup',
                'new-visit/<nick>' => 'home/newbie',
                'sign-up' => 'user/signup',
                'summary/<currency>' => 'transfer/summary',
                'wallet/<nick>/settings' => 'settings/wallet',
                'wallet/<system>' => 'transfer/wallet',
                'finances' => 'transfer/index',
                'plans/<currency>' => 'program/index',
                'investment/<id>' => 'node/index',
                'plans' => 'program/index',
                'my-plans' => 'node/index',
                'loto' => 'lottery/participate',
                'children/<nick>/<format>' => 'structure/index',
                'children/<nick>' => 'structure/index',
                'children' => 'structure/index',
                'seed/<class>' => 'generate/seed',
                'pay/<nick>' => 'invoice/payment',
                'withdraw/<nick>' => 'invoice/withdraw',
                'profile/<nick>/settings' => 'settings/profile',
                'password/<nick>/settings' => 'settings/password',
                'profile/<nick>' => 'user/profile',
                'logout/<nick>' => 'user/logout',
                'pay-pm/<amount>' => 'perfect/pay',
                'pay-payeer/<amount>' => 'payeer/pay',
                'pay-advcash/<amount>' => 'advcash/pay',
                'success-pm' => 'perfect/success',
                'fail-pm' => 'perfect/fail',
                'status-pm' => 'perfect/status',
                'success-payeer' => 'payeer/success',
                'fail-payeer' => 'payeer/fail',
                'status-payeer' => 'payeer/status',
                'request-reset' => 'home/request',
                'success-advcash' => 'advcash/success',
                'status-advcash' => 'advcash/status',
                'fail-advcash' => 'advcash/fail',
                'reset/<code>' => 'home/reset',
                'buy/<id>' => 'program/buy',
                'advertise' => 'home/advertise',
                'month-income/<currency>/<month>' => 'transfer/month-income',
                'sitemap.xml' => 'home/sitemap',
                'url' => 'home/empty',
                'calculator/<locale>' => 'home/calculator',
                'calculator' => 'home/calculator',
                '~v' => 'home/visit',
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'home/error',
        ],

//        'view' => [
//            'class' => 'yii\web\View',
//            'theme' => [
//                'class' => 'app\sites\antiquar\Theme',
//                'baseUrl' => '/',
//                'pathMap' => [
//                    '@app/views' => '@app/sites/antiquar/views',
//                ],
//            ]
//        ],
        'request' => [
            'class' => 'app\base\Request'
        ],

        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'idParam' => 'id',
            'autoRenewCookie' => false,
            'loginUrl' => ['user/login'],
            'authTimeout' => 30 * 24 * 3600
        ],

        'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => !YII_DEBUG,
            'concatCss' => true, // concatenate css
            'minifyCss' => true, // minificate css
            'concatJs' => true, // concatenate js
            'minifyJs' => true, // minificate js
            'minifyOutput' => true, // minificate result html page
            'webPath' => '@web', // path alias to web base
            'basePath' => '@webroot', // path alias to web base
            'minifyPath' => '@webroot/assets', // path alias to save minify result
            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expandImports' => true, // whether to change @import on content
            'compressOptions' => ['extra' => true], // options for compress
            'excludeFiles' => [
                '\.min\.(js|css)$'
            ],
            'excludeBundles' => [
//                '\app\assets\ExternalAsset',
                'yii\web\JqueryAsset'
            ],
            'theme' => [
                'class' => 'app\sites\antiquar\Theme',
                'baseUrl' => '/',
                'pathMap' => [
                    '@app/views' => '@app/sites/antiquar/views',
                ],
            ]
        ]
    ],

    'params' => [
        'checkLoginAttempt' => false,
        'admin' => false,
        'referralLinkOrigin' => 'http://http://local.antiquar-plus.com'
    ]
]);

if (YII_DEBUG) {
    require_once '../../../config/debug.php';
}

return $config;
