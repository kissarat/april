<?php

namespace app\sites\antiquar\controllers;


use Yii;

class SettingsController extends \app\controllers\SettingsController
{
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isGet) {
            $this->layout = 'form';
        }
        return parent::beforeAction($action);
    }
}
