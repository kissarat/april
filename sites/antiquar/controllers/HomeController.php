<?php

namespace app\sites\antiquar\controllers;


use app\models\Program;
use app\models\Transfer;
use Yii;
use yii\helpers\Url;
use yii\web\Response;

class HomeController extends \app\controllers\HomeController
{
    public function actionIndex()
    {
        if (Yii::$app->user->getIsGuest()) {
            return $this->redirect(['user/login']);
        }
        return $this->render('@site/views/home/index');
    }

    public function actionEmpty() {
        Yii::$app->response->format = Response::FORMAT_RAW;
        return '';
    }

    public function actionCalculator() {
        $this->layout = false;
        $programs = [];
        foreach (Program::getProgramDataList() as $p) {
            $p['name'] = Yii::t('app', $p['name']);
            $programs[] = $p;
        }
        return $this->render('@site/views/home/calculator', [
            'state' => [
                'programs' => $programs,
                'id' => 0,
                'currency' => Yii::$app->request->getCurrency(),
                'factor' => Transfer::FACTOR,
                'currencies' => Transfer::getCurrencies(),
                'rates' => Transfer::getExchangeRate(),
                'css' => Url::to('/calculator.css', true),
                'amount' => 0
            ]
        ]);
    }
}
