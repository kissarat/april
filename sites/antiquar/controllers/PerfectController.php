<?php
/**
 * Last modified: 18-05-09 00:43:18
 * Hash: c45a608712033091536b77d130e73c2a041276d9
 */

namespace app\sites\antiquar\controllers;


use Yii;

class PerfectController extends \app\controllers\PerfectController
{
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = 'form';
        }
        return parent::beforeAction($action);
    }
}
