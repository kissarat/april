<?php

namespace app\sites\antiquar\controllers;


use app\controllers\base\RestController;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

class NodeController extends RestController
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        /**
         * @var \app\models\User $user
         */
        $user = Yii::$app->user->identity;
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $nodes = [];
            foreach ($user->getNodes() as $node) {
                $nodes[] = [
                    'created' => $node->created,
                    'amount' => $node->amount,
                    'currency' => $node->currency,
                    'status' => $node->status,
                    'type' => $node->type,
                    'income' => $node->getIncome(),
                    'count' => $node->getAccrueCount(),
                    'vars' => $node->vars,
                    'program' => $node->getProgramObject()->attributes
                ];
            }
            return ['result' => $nodes];
        }
        return $this->render('@site/views/home/index');
    }
}
