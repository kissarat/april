<?php
$r = $_GET['r'] ?? '';
$info = implode("\t", [
    date('y-m-d H:i:s'),
    $r ?: '-',
    $_SERVER['HTTP_CF_CONNECTING_IP']
    ?? $_SERVER['HTTP_X_FORWARDED_FOR']
    ?? $_SERVER['SERVER_ADDR']
    ?? $_SERVER['HTTP_IP']
    ?? '-',
    $_SERVER['HTTP_REFERER'] ?? '-',
    $_SERVER['HTTP_USER_AGENT'] ?? '-',
]) . "\n";

@file_put_contents('/var/log/referral.log', $info, FILE_APPEND);
header('location: /?sp=' . $r);
