"""
Last modified: 18.05.09 23:53:37
Hash: 113b8875ea428f9e10a65f5d7d8c25cc4ea8c213
"""
from json import dumps
from sys import argv

from store import Subject
from yii2 import Client, user_agent, config

command = argv[1] if len(argv) >= 2 else ''

if 'id' == command:
    print(config['id'])
elif 'me' == command:
    s = Subject()
    user = s.find_one()
    print(user['nick'])
elif 'get' == command:
    c = Client()
    conn = c.request('GET', argv[2], None)
    print(conn.headers)
    print(conn.read().decode('utf-8'))
elif 'config' == command:
    print(dumps(config, ensure_ascii=False, indent=2))
else:
    print(user_agent)
