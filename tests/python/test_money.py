"""
Last modified: 18.05.09 23:15:24
Hash: 17f939b965a91e98ac8b7a2d42b0309d5f61c82e
"""
import unittest
from datetime import datetime
from hashlib import md5, sha256
from random import randint
from re import compile
from time import time
from urllib.parse import urlencode
from uuid import uuid4

from store import Subject, Money
from util import TIME_FORMAT
from yii2 import authorized, config

guid_pattern = compile(r'^\w{8}$')

perfect = {
    'BAGGAGE_FIELDS': 'USER USER_ID BALANCE CREATED IP EMAIL',
    'PAYEE_ACCOUNT': config['components']['perfect']['wallet'],
    'PAYEE_NAME': config['name'],
    'PAYMENT_ID': guid_pattern,
    'PAYMENT_UNITS': 'USD',
    'PAYMENT_URL_METHOD': config['components']['perfect']['method'],
}

perfect_secret_hash = md5(
    config['components']['perfect']['secret'].encode('utf-8')).hexdigest().upper()


def usd_format(v):
    return '{0:.2f}'.format(v / 100)


def perfect_random_wallet():
    return 'U' + str(10000000 + randint(0, 9 * 10000000))


def perfect_hash(data):
    return {
        **data,
        'V2_HASH': md5(('%s:%s:%s:%s:%s:%s:%s:%s' % (
            data['PAYMENT_ID'],
            data['PAYEE_ACCOUNT'],
            data['PAYMENT_AMOUNT'],
            data['PAYMENT_UNITS'],
            data['PAYMENT_BATCH_NUM'],
            data['PAYER_ACCOUNT'],
            perfect_secret_hash,
            data['TIMESTAMPGMT']
        )).encode('utf-8')).hexdigest().upper()
    }


def advcash_random_wallet():
    return 'U' + str(100000000000 + randint(0, 9 * 10000000000))


def advcash_hash(data):
    return {
        **data,
        'ac_hash': sha256(('%s:%s:%s:%s:%s:%s:%s:%s:%s' % (
            data['ac_transfer'],
            data['ac_start_date'],
            data['ac_sci_name'],
            data['ac_src_wallet'],
            data['ac_dest_wallet'],
            data['ac_order_id'],
            data['ac_amount'],
            data['ac_merchant_currency'],
            config['components']['advcash']['merchantPassword']
        )).encode('utf-8')).hexdigest()
    }


def payeer_hash(data):
    return {
        **data,
        'm_sign': sha256(('%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s' % (
            data['m_operation_id'],
            data['m_operation_ps'],
            data['m_operation_date'],
            data['m_operation_pay_date'],
            data['m_shop'],
            data['m_orderid'],
            data['m_amount'],
            data['m_curr'],
            data['m_desc'],
            data['m_status'],
            config['components']['payeer']['secret']
        )).encode('utf-8')).hexdigest().upper()
    }


def payeer_random_wallet():
    return 'P' + str(10000000 + randint(0, 9 * 10000000))


class MoneyTestCase(unittest.TestCase):
    def setUp(self):
        self.store = Money()
        self.subject = Subject(self.client)
        self._user = None

    @staticmethod
    def random_amount():
        return int(float(usd_format(randint(100, 1000000))) * 100)

    @property
    def client(self):
        return self.store.client

    @property
    def user(self):
        if not self._user:
            self._user = self.subject.find_one()
        return self._user

    def assertType(self, actual, expected):
        t = type(actual)
        self.assertEqual(t, expected, 'Invalid type ' + t.__name__)

    def assertKeys(self, actual, expected):
        for expk in expected:
            exp = expected[expk]
            act = actual[expk]
            if hasattr(exp, 'match'):
                self.assertRegex(act, exp, expk)
            else:
                self.assertEqual(exp, act, expk)

    def assertRedirect(self, guid, res):
        self.assertRegex(res.headers['Location'], compile(r"(/%s/%s$|guid=%s)" % (self.user['nick'], guid, guid)))

    @authorized
    def test_perfect_pay(self):
        user = self.user
        amount = self.random_amount()
        r = self.client.get('/perfect/pay?amount=' + str(amount))
        guid = r['data']['PAYMENT_ID']
        t = self.store.find_one(guid=guid)
        self.assertRegex(guid, guid_pattern)
        self.assertEqual(r['url'], 'https://perfectmoney.is/api/step1.asp', 'url')
        self.assertType(r['data'], dict)
        self.assertTrue(t)
        expected_transfer = {
            'amount': amount,
            'type': 'payment',
            'system': 'perfect',
            'to': user['id'],
            'from': None
        }
        self.assertKeys(t, {**expected_transfer, 'status': 'created'})
        # created = t['created'].split('.')[0]
        expected = {
            **perfect,
            'CREATED': t['created'] + '.000000',
            'EMAIL': user['email'],
            'IP': t['ip'],
            'PAYMENT_AMOUNT': float(usd_format(amount)),
            'USER': user['nick'],
        }
        self.assertKeys(r['data'], expected)

        # verify
        batch = r['data'].copy()
        timestamp = time()
        wallet = perfect_random_wallet()
        txid = str(int(timestamp))
        batch['PAYMENT_BATCH_NUM'] = txid
        batch['TIMESTAMPGMT'] = str(int(timestamp))
        batch['PAYER_ACCOUNT'] = wallet
        data = perfect_hash(batch)
        res = self.client.request('GET', '/perfect/success?guid='
                                  + guid
                                  + '&' + urlencode(data))
        self.assertRedirect(guid, res)
        t = self.store.find_one(id=t['id'])
        self.assertKeys(t, {
            **expected_transfer,
            'status': 'success',
            'wallet': wallet,
            'txid': txid
        })

    @authorized
    def test_advcash_pay(self):
        amount = self.random_amount()
        r = self.client.get('/advcash/pay?amount=' + str(amount))
        self.assertRegex(r['guid'], guid_pattern)
        guid = r['guid']
        t = self.store.find_one(guid=guid)
        self.assertTrue(t)
        expected_transfer = {
            'amount': amount,
            'type': 'payment',
            'system': 'advcash',
            'to': self.client.user['id'],
            'from': None
        }
        self.assertKeys(t, {**expected_transfer, 'status': 'created'})
        wallet = advcash_random_wallet()
        txid = str(uuid4())
        timestamp = time()
        data = {
            'ac_src_wallet': wallet,
            'ac_dest_wallet': 'U720631106809',
            'ac_start_date': datetime.fromtimestamp(int(timestamp)).strftime(TIME_FORMAT),
            'ac_transfer': txid,
            'ac_merchant_currency': 'USD',
            'ac_amount': usd_format(amount),
            'ac_order_id': guid,
            'ac_transaction_status': 'COMPLETED',
            'ac_sci_name': config['components']['advcash']['merchantName']
        }
        batch = advcash_hash(data)
        status = self.client.read_response(self.client.post('/advcash/status', batch))
        self.assertTrue(status['success'])
        self.assertEqual(status['guid'], guid)
        t = self.store.find_one(id=t['id'])
        self.assertKeys(t, {
            **expected_transfer,
            'status': 'success',
            'wallet': wallet,
            'txid': txid
        })
        self.assertRedirect(guid, self.client.post('/advcash/success', data))

    @authorized
    def test_payeer_pay(self):
        amount = self.random_amount()
        r = self.client.get('/payeer/pay?amount=' + str(amount))
        self.assertRegex(r['guid'], guid_pattern)
        guid = r['guid']
        t = self.store.find_one(guid=guid)
        self.assertTrue(t)
        expected_transfer = {
            'amount': amount,
            'type': 'payment',
            'system': 'payeer',
            'to': self.client.user['id'],
            'from': None
        }
        self.assertKeys(t, {**expected_transfer, 'status': 'created'})
        # wallet = payeer_random_wallet()
        timestamp = time()
        txid = str(int(timestamp * 10000))
        date = datetime.fromtimestamp(int(timestamp)).strftime(TIME_FORMAT)
        batch = payeer_hash({
            'm_operation_id': txid,
            'm_operation_ps': '2609',
            'm_operation_date': date,
            'm_operation_pay_date': date,
            'm_orderid': guid,
            'm_shop': config['components']['payeer']['shopId'],
            'm_amount': usd_format(amount),
            'm_curr': 'USD',
            'm_desc': '',
            'm_status': 'success'
        })
        self.assertRedirect(guid, self.client.request('GET', '/payeer/success?' + urlencode(batch)))
        t = self.store.find_one(id=t['id'])
        self.assertKeys(t, {
            **expected_transfer,
            'status': 'success',
            'txid': txid
        })


perfect_success = {
    "PAYEE_ACCOUNT": "U15761435",
    "PAYMENT_AMOUNT": "1495",
    "PAYMENT_UNITS": "USD",
    "PAYMENT_BATCH_NUM": "213867890",
    "PAYMENT_ID": "rh1pbqzc",
    "SUGGESTED_MEMO": "",
    "V2_HASH": "4D1AFD876ADC863F294E4581C7FB02B3",
    "TIMESTAMPGMT": "1525595889",
    "PAYER_ACCOUNT": "U8441477",
    "USER": "Billionaire",
    "USER_ID": "18",
    "BALANCE": "0",
    "CREATED": "2018-05-06 09:41:26.000000",
    "IP": "176.114.3.11",
    "EMAIL": "sddsddsdd@ukr.net",
}

advcash_success = {
    'ac_src_wallet': 'U844377161119',
    'ac_dest_wallet': 'U720631106809',
    'ac_amount': '0.10',
    'ac_merchant_amount': '0.10',
    'ac_merchant_currency': 'USD',
    'ac_fee': '0.00',
    'ac_buyer_amount_without_commission': '0.10',
    'ac_buyer_amount_with_commission': '0.10',
    'ac_buyer_currency': 'USD',
    'ac_transfer': 'be3d9035-c781-4e4e-8e9a-e93ba4fa4027',
    'ac_sci_name': 'cabinet',
    'ac_start_date': '2018-04-17 13:39:44',
    'ac_order_id': '4',
    'ac_ps': 'ADVANCED_CASH',
    'ac_transaction_status': 'COMPLETED',
    'ac_buyer_email': 'trusttrade.corporation@gmail.com',
    'ac_buyer_verified': 'false'
}

if __name__ == '__main__':
    unittest.main()
