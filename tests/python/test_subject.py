"""
18-05-08 23:53:56
"""
import unittest

from yii2 import Client, authorized


class SubjectTestCase(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    @authorized
    def test_logout(self):
        self.assertTrue(self.client.logout(), 'Logout')
        return


if __name__ == '__main__':
    unittest.main()
