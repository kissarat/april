"""
Last modified: 18-05-08 22:51:55
"""
from urllib.parse import urlencode

from yii2 import Client


class Store:
    def __init__(self, table, client=None):
        self.client = client or Client()
        self.table = table

    def find_one(self, **where):
        return self.client.get('/%s/view?%s' % (self.table, urlencode(where)))


class Subject(Store):
    def __init__(self, client=None):
        super().__init__('subject', client)

    def find_one(self, **where):
        if 0 == len(where):
            where = {'id': self.client.user['id']}
        return super().find_one(**where)


class Money(Store):
    def __init__(self, client=None):
        super().__init__('money', client)
