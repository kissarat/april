<?php

namespace app\tests\fixtures;


use yii\test\ActiveFixture;

abstract class MorozovFixture extends ActiveFixture
{
    public static function getDataMorozov(string $class) {
        $class = 'app\tests\fixtures\\' . $class . 'Fixture';
        /**
         * @var MorozovFixture $fixture
         */
        $fixture = new $class();
        return $fixture->getData();
    }
}
