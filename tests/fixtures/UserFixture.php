<?php

namespace app\tests\fixtures;


use app\helpers\Utils;
use app\models\User;
use Yii;

class UserFixture extends MorozovFixture
{
    public $modelClass = User::class;

    protected function getData() {
        $users = [
            Yii::$app->params['root'] => ['type' => 'special'],
            'admin' => ['type' => 'special'],
            'cron' => ['type' => 'special'],
            'demo' => ['password' => 'demo'],
            'monday' => ['admin' => true, 'password' => 'Ne0oihatohgh7Ohw7AeJaa8t'],
            'tuesday' => ['admin' => true, 'password' => 'eitahfaeghoth3va3Jei1ohM'],
            'wednesday' => ['admin' => true, 'password' => 'Pufeexe5OoP9Eb6jieshooTuoj'],
            'telegram' => ['type' => 'special'],
            'perfect' => ['type' => 'special'],
            'payeer' => ['type' => 'special'],
            'advcash' => ['type' => 'special'],
            'blockio' => ['type' => 'special'],
//            'ai_logist' => [],
        ];
        foreach ($users as $nick => &$user) {
            $user['nick'] = $nick;
            $user['email'] = $nick . '@mail.local';
            $user['created'] = Utils::timestamp();
            if (empty($user['type'])) {
                $user['type'] = 'native';
            }
            if (isset($user['password'])) {
                $user['secret'] = Utils::generateSecret($user['password']);
                unset($user['password']);
            } else {
                $user['secret'] = Utils::generateSecret('ahtu6eejaed9oojeicaWaifa-' . $nick);
            }
        }
        return $users;
    }
}
