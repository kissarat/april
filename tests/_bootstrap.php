<?php
defined('ROOT') or define('ROOT', dirname(__DIR__));

$config = require ROOT . '/config/console.php';
define('VENDOR', ROOT . '/vendor');

define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);

require_once VENDOR . '/yiisoft/yii2/Yii.php';
require_once VENDOR . '/autoload.php';
