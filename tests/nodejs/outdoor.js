const assert = require('assert')
const {user, config, origin, nightmare} = require('./common')

describe('Auth', function () {
  it('login', function (done) {
    nightmare()
      .goto(origin + '/user/login')
      .wait('.user.login #login-password')
      .type('#login-login', user.nick)
      .type('#login-password', user.password)
      .click('.user.login [type="submit"]')
      .wait(800)
      .click(`[href^="/logout/${user.nick}"]`)
      .wait(800)
      .wait('.user.login')
      .run()
      .then(() => done())
  })
})
