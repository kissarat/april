const config = require(process.env.SITE + '/config/test')
const Nightmare = require('nightmare')
require('nightmare-wait-for-url')

module.exports = {
  config,
  origin: 'http://' + config.params.hostname,
  user: {
    nick: 'demo',
    password: 'demo'
  },
  nightmare() {
    const n = new Nightmare({
      show: true
    })
    return n.useragent('Nightmare theJodoor1Quaof6die3Sei1=' + config['params']['database_name'])
  }
}
