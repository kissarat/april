<?php

namespace app\components;


use app\base\JSONClient;
use app\base\Storage;
use Yii;
use yii\base\Component;

class Telegram extends Component
{
    public $token = '528731198:AAGLn6ZyD6qlhGkZqvO1WV66mDk6Bo2mVbM';
    public $wait = 90;
    public $chat_id = -1001126558077;
    protected $client;
    protected $_storage;

    public function getStorage(): Storage
    {
        if (!$this->_storage) {
            $this->_storage = Yii::$app->storage->derive('telegram-');
        }
        return $this->_storage;
    }

    public function init()
    {
        $this->client = new JSONClient([
            'origin' => "https://api.telegram.org/bot$this->token/"
        ]);
        parent::init();
    }

    public function invoke($method, $params = null)
    {
        return $this->client->get($method, $params);
    }

    public function getUpdates()
    {
        return $this->invoke('getUpdates', [
            'timeout' => $this->wait,
            'allowed_updates' => 'message'
        ]);
    }

    public function send($chat_id, $text)
    {
        return $this->invoke('sendMessage', [
            'chat_id' => $chat_id,
            'text' => $text
        ]);
    }

    public function sendText($text)
    {
//        if (!YII_TEST) {
            return $this->invoke('sendMessage', [
                'chat_id' => $this->chat_id,
                'text' => $text
            ]);
//        }
    }
}
