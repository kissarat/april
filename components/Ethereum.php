<?php
/**
 * Last modified: 18.11.13 22:38:50
 * Hash: df488dc333a06e5896bf3765c0dc4b2698cd0cc2
 */

namespace app\components;


use app\helpers\Utils;

class Ethereum extends Parity
{
    public const ETHERSCAN = 'https://api.etherscan.io/api';

    public function fetchEtherscan(array $params): array
    {
        return Utils::loadJSON(static::ETHERSCAN . '?' . \http_build_query($params));
    }

    /**
     * @param int|null $id
     * @param string|null $password
     * @return string
     * @throws \Exception
     */
    public function getUserAddress(int $id = null, string $password = null): string
    {
        $address = parent::getUserAddress($id, $password);
        $this->getPaymentQueue()->push($address);
        return $address;
    }

    public function receive(int $limit = -1): void
    {
        if ('eth' === $this->wallet) {
            $txs = [];
            foreach ($this->getPaymentQueue()->iterate() as $address) {
                $r = $this->fetchEtherscan([
                    'module' => 'account',
                    'action' => 'txlist',
                    'address' => $address
                ]);
                if (!empty($r['status']) && 1 === +$r['status'] && \is_array($r['result'])) {
                    foreach ($r['result'] as $t) {
                        $t['number'] = +$t['blockNumber'];
                        $t['created'] = Utils::timestamp(+$t['timeStamp']);
                        $txs[$t['to']] = $t;
                    }
                }
            }
            $this->loadPayments($txs);
        }
        parent::receive($limit);
    }
}
