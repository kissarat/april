<?php
/**
 * Last modified: 18.07.12 05:21:02
 * Hash: da02858293e6e2ddef600f3c72a586485fb2725a
 */

namespace app\components;


use app\base\Cryptocurrency;
use app\base\GatewayTrait;
use app\helpers\Utils;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\base\Component;
use yii\console\Exception;

class Blockio extends Component implements Cryptocurrency
{
    use GatewayTrait;

    public $version = 2;
    public $delay = 9 / 24;
    public $lastRequest = 0;
    public $key;
    public $headers;

    public function invoke($method, $params = [])
    {
        $delta = microtime(true) - $this->lastRequest;
        if ($delta < $this->delay) {
            $d = (int)(($this->delay - $delta) * 1000 * 1000);
            usleep($d);
        }
        $this->lastRequest = microtime(true);
        $params['api_key'] = $this->key;
        $c = curl_init("https://block.io/api/v2/$method/?" . http_build_query($params));
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($c);
        if ($data && ($data = json_decode($data, JSON_OBJECT_AS_ARRAY))) {
            return $data;
        }
        $this->lastRequest = microtime(true);
        return null;
    }

    public function unarchive($labels)
    {
        return $this->invoke('unarchive_addresses', ['labels' => implode(',', $labels)]);
    }

    public function getRate()
    {
        return $this->invoke('get_current_price', ['price_base' => 'USD']);
    }

    public function getNetworkName(): string
    {
        $r = $this->getRate();
        return 'success' === $r['status'] ? $r['data']['network'] : 'test';
    }

    public function getBalance()
    {
        $data = $this->invoke('get_balance');
        if ('success' === $data['status']) {
            return (float)$data['data']['available_balance'];
        }
        return -1;
    }

    public function getAddress($nick): string
    {
        $params = ['label' => $nick];
        $exist = $this->invoke('get_address_by_label', $params);
        $address = null;
        if ('success' === $exist['status']) {
            $this->unarchive([$nick]);
            $address = $exist['data']['address'];
        } else {
            $new = $this->invoke('get_new_address', $params);
            if ('success' === $new['status']) {
                $address = $new['data']['address'];
            }
        }
        return $address;
    }

    public function getUserAddress(int $id): string
    {
        $user = User::findOne($id);
        if ($user) {
            $wallet = $this->wallet;
            $address = $this->getAddress($user->nick);
            if (empty($user->$wallet) && $address) {
                $user->$wallet = $address;
                $user->update(false);
            }
            return $address;
        }
        return null;
    }

    public function getTransactions()
    {
        return $this->invoke('get_transactions', ['type' => 'received']);
    }

    /**
     * @param int $limit
     * @throws Exception
     */
    public function receive(int $limit = -1): void
    {
        $lastTime = $this->getLast();
        $now = Utils::timestamp();
        $d = $this->getTransactions();
        $currency = $this->getCurrency();
        if (!(isset($d['data']['txs']) && \is_array($d['data']['txs']))) {
            throw new Exception('Invalid format for data.txs');
        }
        foreach ($d['data']['txs'] as $t) {
            $txid = $t['txid'];
            if (!(isset($t['amounts_received']) && \is_array($t['amounts_received']))) {
                throw new Exception('Invalid format for amounts_received');
            }
            foreach ($t['amounts_received'] as $r) {
                $where = [
                    'txid' => $txid,
                    'wallet' => $r['recipient'],
                    'currency' => $currency
                ];
                $tr = Transfer::findOne($where);
                $status = $t['confirmations'] >= 3 ? Transfer::STATUS_SUCCESS : 'pending';
                $user = null;
                $isSuccess = false;
                if ($tr) {
                    if (Transfer::STATUS_SUCCESS === $tr->status || Transfer::STATUS_CANCELED === $tr->status) {
                        continue;
                    }
                    $tr->status = $status;
                    if ($tr->isAttributeChanged('status')) {
                        $tr->time = $now;
                        $isSuccess = true;
                    }
                } else {
                    $tr = new Transfer($where);
                    $tr->currency = $currency;
                    $tr->status = $status;
                    $tr->system = 'blockio';
                    $tr->type = Transfer::TYPE_PAYMENT;
                    $tr->created = Utils::timestamp($t['time']);
                    $lastTime = max($t['time'], $lastTime);
                    $user = User::find()
                        ->where([$this->wallet => $where['wallet']])
                        ->select(['id', 'nick'])
                        ->one();
                    if ($user) {
                        $tr->to = $user->id;
                    }
                    $tr->vars = [
                        'green' => $t['from_green_address'],
                        'senders' => $t['senders'],
                        'network' => $d['data']['network']
                    ];
                }
                $tr->n = min($t['confirmations'], 2 ** 15 - 1);
                $amount = $r['amount'];
                $tr->amount = floor($amount * Transfer::FACTOR[$currency]);
                if ($tr->save()) {
                    if ($isSuccess && Yii::$app->has('telegram')) {
                        $nick = $user ? $user->nick : '(unknown user)';
                        Yii::$app->telegram->sendText("Payment from $nick $amount $currency $tr->wallet");
                    }
                } else {
                    echo json_encode($tr->getErrors(), JSON_UNESCAPED_UNICODE);
                }
            }
        }
        $this->setLast($lastTime);
    }

    public function getDefaultLast(): int
    {
        return 0;
    }

    public function getLastUpdateTime(): string
    {
        return $this->getLast();
    }
}
