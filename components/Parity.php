<?php
/**
 * Last modified: 18.11.13 21:43:47
 * Hash: 6b2f56da0f8f72c9d511c58c989ccb4935184677
 */

namespace app\components;

use app\base\Cryptocurrency;
use app\base\GatewayTrait;
use app\helpers\Utils;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\base\Component;

function parseHex($v)
{
    return '0x' === strpos($v, '0x') ? hexdec($v) : (float)$v;
}

/**
 * Class Ethereum
 * @package app\components
 */
class Parity extends Component implements Cryptocurrency
{
    use GatewayTrait;

    public const WEI = 10 ** 18;
    public const START_ID = 15421381474110;
    public const ID_DIV = 10 ** 13;
    public const LAST = 'last';
    public const UNLOCK_TIMEOUT = 30;
    public const MINIMAL_GAS = 21947;
    /**
     * @var int
     */
    public $receiveLimit = 256;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $origin;
    /**
     * @var string
     */
    public $workingNetworkName = 'foundation';
    /**
     * @var int
     */
    public $gas = self::MINIMAL_GAS;

    /**
     * @return float
     */
    public function getWeiFactor(): float
    {
        return $this->getFactor() / static::WEI;
    }

    public function newID(): int
    {
        return (floor(microtime(true) * 10000) - static::START_ID) % static::ID_DIV;
    }

    public function invoke(string $method, array $params = [])
    {
        $json = [
            'jsonrpc' => '2.0',
            'id' => $this->newID(),
            'method' => $method,
            'params' => $params
        ];
        $content = json_encode($json);
        $data = file_get_contents($this->origin . '/', false, stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => 'content-type: application/json',
                'content' => $content
            ]
        ]));
        $data = json_decode($data, JSON_OBJECT_AS_ARRAY);
        if (isset($data['result'])) {
            return $data['result'];
        }
        return $data;
    }

    public function getUserAddress(int $id = null, string $password = null): string
    {
        if (!$id) {
            $id = Yii::$app->user->identity->id;
        }
        if (!$password) {
            $password = $this->password;
        }
        $name = $this->wallet;
        $user = User::find()
            ->where(['id' => $id])
            ->select(['id', 'nick', $name])
            ->one();
        if (!$user->$name) {
            foreach ($this->getAccountInfoList() as $address => $info) {
                if (!empty($info['name']) && $user->nick === $info['name']) {
                    $user->$name = $address;
                    break;
                }
            }
            if (!$user->$name) {
                $user->$name = $this->newAccount($password);
                $this->setAccountName($user->$name, $user->nick);
            }
            $this->unlockAccount($user->$name, static::UNLOCK_TIMEOUT, $password);
            if (!$user->save(true, [$name])) {
                throw new \Exception("Cannot save $name address");
            }
        }
        return $user->$name;
    }

    public function getBlockNumber(): int
    {
        return hexdec($this->invoke('eth_blockNumber'));
    }

    public function getDefaultLast(): int
    {
        return $this->getBlockNumber();
    }

    public function getBlock(int $number, bool $full = true)
    {
        return $this->invoke('eth_getBlockByNumber', ['0x' . dechex($number), $full]);
    }

    public function getBalance(string $address, string $tag = 'latest')
    {
        return $this->invoke('eth_getBalance', [$address, $tag]);
    }

    public function newAccount(string $password = '')
    {
        return $this->invoke('personal_newAccount', [$password]);
    }

    public function setAccountName(string $address, string $name)
    {
        return $this->invoke('parity_setAccountName', [$address, $name]);
    }

    public function getAccountList(): array
    {
        return $this->invoke('eth_accounts');
    }

    public function getAccountInfoList(): array
    {
        return $this->invoke('parity_allAccountsInfo');
    }

    public function getBalanceList(int $min = 0): array
    {
        $list = $this->getAccountList();
        $accounts = [];
        foreach ($list as $account) {
            $balance = hexdec($this->getBalance($account)) * $this->getWeiFactor();
            if ($balance >= $min) {
                $accounts[$account] = $balance;
            }
        }
        arsort($accounts);
        return $accounts;
    }

    public function getGasPrice()
    {
        return hexdec($this->invoke('eth_gasPrice'));
    }

    public function getWeiFee()
    {
        return $this->getGasPrice() * $this->gas;
    }

    public function getFee(): int
    {
        return ceil($this->getWeiFee() * $this->getWeiFactor());
    }

    public function lockAccount(string $address)
    {
        return $this->invoke('personal_lockAccount', [$address]);
    }

    public function unlockAccount(string $address, int $timeout = self::UNLOCK_TIMEOUT, ?string $password = null)
    {
        if (!$password) {
            $password = $this->password;
        }
        return $this->invoke('personal_unlockAccount', [$address, $password, $timeout]);
    }

    public function loadPayments(array $transactions): void
    {
        $wallet_name = $this->wallet;
        $currency = strtoupper($wallet_name);
        $users = User::find()
            ->where(['in', $wallet_name, array_keys($transactions)])
            ->select(['id', 'nick', $wallet_name])
            ->all();
        foreach ($users as $user) {
            $t = $transactions[$user->$wallet_name];
            $where = [
                'txid' => $t['hash'],
                'wallet' => $t['to'],
                'currency' => $currency,
                'type' => Transfer::TYPE_PAYMENT
            ];
            $tr = Transfer::findOne($where);
            if ($tr) {
                $this->getPaymentQueue()->del($tr->wallet);
            } else {
                $amount = parseHex($t['value']) / static::WEI;
                $tr = new Transfer($where);
                $tr->status = Transfer::STATUS_SUCCESS;
                $tr->created = $t['created'];
                $tr->amount = (int) ($amount * Transfer::FACTOR[$currency]);
                $tr->to = $user->id;
                $tr->node = $t['number'];
                $tr->vars = [
                    'sender' => $t['from'],
                    'gas' => (int) parseHex($t['gas']),
                    'fee' => (int) parseHex($t['gasPrice'])
                ];
                if ($tr->insert(false)) {
                    $this->getPaymentQueue()->del($tr->wallet);
                    if (Yii::$app->has('telegram')) {
                        Yii::$app->telegram->sendText("Payment from $user->nick $amount $currency $tr->wallet");
                    }
                } else {
                    echo json_encode($tr->getErrors(), JSON_UNESCAPED_UNICODE);
                }
            }
        }
    }

    public function receive(int $limit = -1): void
    {
        if (!($limit >= 0)) {
            $limit = $this->receiveLimit;
        }
//        echo "Limit is $limit for $this->wallet\n";
        $number = $this->getLast();
        for ($last = $number + $limit; $number <= $last; $number++) {
            $block = $this->getBlock($number);
            if ($block && isset($block['timestamp'])) {
//                echo $number . "\n";
                $created = Utils::timestamp(hexdec($block['timestamp']));
                $txs = [];
                /** @var array $block */
                foreach ($block['transactions'] as $t) {
                    $t['number'] = $number;
                    $t['created'] = $created;
                    $txs[$t['to']] = $t;
                }
                $this->loadPayments($txs);
            } else {
                break;
            }
            $this->setLast($number);
        }
    }

    public function getNetworkName(): string
    {
        return $this->invoke('parity_chain');
    }


    public function getLastUpdateTime(): string
    {
        $last = $this->getStorage()->get(static::LAST);
        if ($last > 0) {
            $block = $this->getBlock($last);
            return hexdec($block['timestamp']);
        }
        return -1;
    }
}
