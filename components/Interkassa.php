<?php
/**
 * Last modified: 18.08.17 16:05:03
 * Hash: 335dfa6e78678e35f2fbe16bf4800fcacafe541a
 */

namespace app\components;


use yii\base\Component;

class Interkassa extends Component
{
    public $id;
    public $rates = [
      'UAH' => 28,
      'RUB' => 68
    ];
}
