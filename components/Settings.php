<?php
/**
 * Last modified: 18.07.22 09:06:07
 * Hash: 2b0185c7f99ec18af97ee4a75b55010375c31a9c
 */

namespace app\components;


use app\base\Storage;
use Yii;
use yii\base\Component;
use yii\base\Exception;

class Settings extends Component
{
    public $defaults = [];
    public $data = [];
    public const LOCALES = 'locales';
    protected $storages = [];

    public function init()
    {
        parent::init();
//        $this->bootstrap();
    }

    public function getStorage(): Storage
    {
        return Yii::$app->storage;
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function checkDefault($name): void
    {
        if (!isset($this->defaults[$name])) {
            throw new Exception("Settings $name not found");
        }
    }

    public function getDefault($name)
    {
        return $this->defaults[$name];
    }

    public function getSummary(): array
    {
        $summary = [];
        if (YII_DEBUG) {
            $summary['n'] = Yii::$app->storage->db;
//            $summary['hostname'] = Yii::$app->storage->hostname;
            $summary['keys'] = Yii::$app->storage->redis->keys('*');
        }
        foreach ($this->defaults as $name => $_) {
            $summary[$name] = $this->getList($name);
        }
        return $summary;
    }

    public function get($key, $default = null) {
        return $this->getStorage()->get($key, $default);
    }

    /**
     * @param $name
     * @return array
     * @throws Exception
     */
    public function getList($name): array
    {
        if (!isset($this->data[$name])) {
            $this->checkDefault($name);
            $s = $this->getStorage();
            $list = $s->iterate($name);
            if (empty($list)) {
                $list = $this->getDefault($name);
                foreach ($list as $item) {
                    $s->push($item, $name);
                }
            }
            $this->data[$name] = $list;
        }
        return $this->data[$name];
    }

    public function getLocales(): array
    {
        return $this->getList(static::LOCALES);
    }

    public function bootstrap(): void
    {
        if (isset($_COOKIE)) {
            $locales = $this->getLocales();
            $locale = $_COOKIE['locale'] ?? null;
            if (!$locale && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                preg_match('/(ru|uk|kk)/', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $m_locale);
                if ($m_locale) {
                    $locale = 'ru';
                }
            }
            if (\in_array($locale, $locales, true)) {
                Yii::$app->language = $locale;
            } else {
                Yii::$app->language = \count($locales) > 0 ? $locales[0] : 'en';
            }
        }

        date_default_timezone_set('UTC');
    }
}
