<?php
/**
 * Last modified: 18.07.18 12:32:55
 * Hash: 8545beb278c5ec64fcc258c4b385f643b0415c6d
 */

namespace app\controllers;

use app\controllers\base\RestController;
use app\helpers\Utils;
use app\models\Log;
use app\models\Transfer;
use yarcode\advcash\Merchant;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

// /advcash/success?ac_src_wallet=U844377161119&ac_dest_wallet=U720631106809&ac_amount=0.10&ac_merchant_amount=0.10&ac_merchant_currency=USD&ac_fee=0.00&ac_buyer_amount_without_commission=0.10&ac_buyer_amount_with_commission=0.10&ac_buyer_currency=USD&ac_transfer=be3d9035-c781-4e4e-8e9a-e93ba4fa4027&ac_sci_name=cabinet&ac_start_date=2018-04-17%2013:39:44&ac_order_id=4&ac_ps=ADVANCED_CASH&ac_transaction_status=COMPLETED&ac_buyer_email=trusttrade.corporation@gmail.com&ac_buyer_verified=false&_csrf=GNg1x4NS4ztnu0PfSVEoxZi0TOkgebaEqdhjH3KHWrdrjXKP5wikFgXMMuk4BluH_Y1hsUUpwd724RZpLfMK3w==

class AdvcashController extends RestController
{
    /** @var string Your component configuration name */
    public $componentName = 'advcash';
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['success', 'fail', 'pay'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['success', 'fail', 'pay'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionSuccess()
    {
        $t = $this->handle(Yii::$app->request->post());
        if ($t) {
            return $this->redirect(['transfer/index',
                'guid' => $t->guid,
                'nick' => Yii::$app->user->identity->nick
            ]);
        }
        throw new BadRequestHttpException();
    }

    public function actionFail()
    {
        $t = $this->handle(Yii::$app->request->post());
        if ($t) {
            return $this->redirect(['transfer/index',
                'guid' => $t->guid,
                'nick' => Yii::$app->user->identity->nick
            ]);
        }
        throw new BadRequestHttpException();
    }

    public function actionStatus()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $t = $this->handle(Yii::$app->request->post());
        if ($t) {
            return [
                'success' => true,
                'guid' => $t->guid
            ];
        }
        throw new BadRequestHttpException();
    }

    protected function handle($data)
    {
        Yii::$app->request->save();
        $id = ArrayHelper::getValue($data, 'ac_order_id');
        $transfer = Transfer::findOne([
            'guid' => $id,
            'type' => Transfer::TYPE_PAYMENT,
            'system' => 'advcash',
            'currency' => 'USD'
        ]);
        if (!$transfer) {
            Log::log('advcash', 'not_found', $data);
            throw new NotFoundHttpException();
        }
        if (Transfer::STATUS_SUCCESS === $transfer->status) {
            return $transfer;
        }

        /**
         * @var $m Merchant
         */
        $m = Yii::$app->get($this->componentName);
        if (Transfer::STATUS_CREATED === $transfer->status && isset($data['ac_hash']) && $m->checkHash($data) && ArrayHelper::getValue($data, 'ac_sci_name') === $m->merchantName) {
            $amount = ArrayHelper::getValue($data, 'ac_amount');
            $transfer->wallet = ArrayHelper::getValue($data, 'ac_src_wallet');
            $transfer->txid = ArrayHelper::getValue($data, 'ac_transfer');
            if (empty($transfer->txid)) {
                throw new BadRequestHttpException('Invalid ac_transfer');
            }
            $transfer->amount = $amount * Transfer::FACTOR['USD'];
            $transfer->status = ArrayHelper::getValue($data, 'ac_transaction_status') === Merchant::TRANSACTION_STATUS_COMPLETED
                ? Transfer::STATUS_SUCCESS : 'fail';
            $transfer->time = Utils::timestamp();
            unset(
                $data['ac_sci_name'],
                $data['ac_amount'],
                $data['ac_src_wallet'],
                $data['ac_transfer'],
                $data['_csrf']
            );
            $transfer->vars = $data;
            $transfer->update(false);
            if (Yii::$app->has('telegram')) {
                $nick = $transfer->toObject->nick;
                Yii::$app->telegram->sendText("Payment from $nick \$$amount $transfer->wallet");
            }
            return $transfer;
        }
        Log::log('advcash', 'merchant', $data);
        return null;
    }

    public function actionPay($amount)
    {
        $transfer = new Transfer([
            'type' => Transfer::TYPE_PAYMENT,
            'status' => 'created',
            'system' => 'advcash',
            'currency' => 'USD',
            'to' => Yii::$app->user->identity->id,
            'ip' => Yii::$app->request->getUserIP(),
            'guid' => Utils::generateCode(),
            'amount' => (int)$amount
        ]);
        $transfer->save();
        return $this->render('pay', [
            'guid' => $transfer->guid,
            'amount' => (string)($amount / Transfer::FACTOR['USD']),
            'to' => $transfer->to
        ]);
    }
}
