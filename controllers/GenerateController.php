<?php

namespace app\controllers;


use app\helpers\Utils;
use app\models\form\UserGenerator;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class GenerateController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['user'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['user'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action) {
        if (YII_DEBUG) {
            return parent::beforeAction($action);
        }
        throw new ForbiddenHttpException();
    }

    public function actionUser() {
        if (!Utils::isAdmin()) {
            throw new ForbiddenHttpException();
        }
        $model = new UserGenerator();
        $errors = null;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $errors = $model->generate();
        }
        return $this->render('user', ['model' => $model, 'errors' => $errors]);
    }
}
