<?php
/**
 * Last modified: 18.08.28 00:32:30
 * Hash: 40afa4002635aa694acdcbfab9c6ac9abae59b04
 */

namespace app\controllers;

use app\base\Cryptocurrency;
use app\base\InsufficientFundsException;
use app\controllers\base\RestController;
use app\helpers\SQL;
use app\helpers\Utils;
use app\models\Link;
use app\models\Log;
use app\models\Transfer;
use app\models\Visit;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class HomeController extends RestController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['sitemap', 'promotion', 'banners'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['sitemap'],
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['promotion', 'banners'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action): bool
    {
        if ('visit' === $action->id) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionError()
    {
        if (Yii::$app->user->isGuest) {
            $this->layout = 'error';
        }

        $ex = Yii::$app->getErrorHandler()->exception;
        if ($ex instanceof InsufficientFundsException) {
            if (Yii::$app->user->getIsGuest()) {
                return $this->redirect(['user/login']);
            }
            return $this->redirect(['transfer/payment',
                'nick' => Yii::$app->user->identity->nick,
                'currency' => $ex->currency,
                'amount' => $ex->getNeed()
            ]);
        }

        if ($ex instanceof HttpException) {
//            Yii::$app->response->statusCode = $ex->statusCode;
            if ($ex instanceof UnauthorizedHttpException && !$this->isApiCall()) {
                return $this->redirect(['user/login']);
            }
            $this->view->title = Yii::t('app', $ex->getName());

            return $this->render('error', [
                'status' => $ex->statusCode,
                'error' => $this->handleException($ex),
                'backURL' => '/'
            ]);
        }
        return $this->render('exception', [
            'exception' => $ex
        ]);
    }

    protected function handleException(\Exception $exception)
    {
        $message = $exception->getMessage();
        if ($exception instanceof HttpException) {
            $message = $exception->getName() . ': ' . $message;
        }
        $json = [
            'message' => $message
        ];
        $previous = $exception->getPrevious();
        if ($previous) {
            $json['previous'] = $this->handleException($previous);
        }
        return $json;
    }

    public function actionIndex()
    {
        if (Yii::$app->user->getIsGuest()) {
            return $this->redirect(['user/login']);
        }
        return $this->redirect(['user/profile', 'nick' => Yii::$app->user->identity->nick]);
    }

    public function actionVisit($url, $spend, $uid = null, $ip = null): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($uid) && isset($_COOKIE[ini_get('session.name')])) {
            $uid = $_COOKIE[ini_get('session.name')];
        }
        $visit = new Visit([
            'token' => $uid,
            'url' => $url,
            'spend' => $spend,
            'ip' => $ip ?: Yii::$app->request->getUserIP(),
            'agent' => Yii::$app->request->getUserAgent(),
            'data' => Yii::$app->getRequest()->getBodyParams() ?: null
        ]);
        if ($visit->insert()) {
            return ['success' => true, 'id' => $visit->id];
        }
        Yii::$app->response->statusCode = 400;
        return ['success' => false, 'errors' => $visit->getErrors()];
    }

    public function actionWroclaw(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
//        $dump = explode("\n", trim(file_get_contents(Yii::getAlias('@app/web/dump-eegaefahVu1b.txt'))));
        $logs = (new Query())->from('log')
            ->select('entity, action, count(*) as count')
            ->where(['>', 'id', Log::lastTime()])
            ->groupBy(['entity', 'action'])
            ->all();
        $activity = [];
        foreach ($logs as $log) {
            $activity[$log['entity']][$log['action']] = $log['count'];
        }
        $activity['ip'] = SQL::scalar('select count(*) from (select ip from log where id > :time group by ip) t', [
            ':time' => Log::lastTime()
        ]);
        $networks = [];
//        foreach (Yii::$app->getComponents(false) as $name => $com) { ,
//        foreach (['btc', 'ltc', 'doge', 'xrp', 'eth', 'etc'] as $name) {
//            $com = Yii::$app->has($name) ? Yii::$app->get($name) : null;
//            if ($com instanceof Cryptocurrency) {
//                $currency = $com->getCurrency();
//                try {
//                    $networks[$currency] = $com->getSummary();
//                } catch (\Exception $ex) {
//                    $networks[$currency] = [
//                        'error' => preg_replace('/https?:\/\/[^\/]+\//', '/', $ex->getMessage())
//                    ];
//                }
//            }
//        }

        $gigabyte = 1024 * 1024 * 1024;

        return [
            'success' => true,
            'mode' => YII_ENV,
            'host' => Yii::$app->getRequest()->getHostInfo(),
            'debug' => YII_DEBUG,
            'time' => date('Y-m-d H:i:s'),
            'name' => Yii::$app->name,
            'language' => Yii::$app->language,
            'charset' => Yii::$app->charset,
            'cache' => Yii::$app->cache instanceof \yii\caching\ApcCache,
            'mirror' => Yii::$app->request->mirror,
            'currency' => Yii::$app->request->getCurrency(),
            'ip' => Yii::$app->request->getUserIP(),
            'timezone' => SQL::scalar('SHOW timezone'),
            'agent' => Yii::$app->request->getUserAgent(),
            'accept_language' => Yii::$app->request->getHeaders()->get('accept-language'),
//            'last' => explode(' ', end($dump))[1],
//            'aliases' => Yii::$aliases,
            'currencies' => Transfer::getCurrencies(),
            'networks' => $networks,
            'settings' => Yii::$app->settings->getSummary(),
            'space' => [
                disk_free_space('/') / $gigabyte,
                @disk_free_space('/mnt') / $gigabyte
            ],
            'activity' => $activity
        ];
    }

    public function actionTranslation($locale = 'ru')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!$locale) {
            $locale = Yii::$app->language;
        }
        return ['result' => Utils::getLocale($locale)];
    }

    public function actionSitemap()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $this->layout = false;
//        $articles = SQL::queryAll('SELECT path, created FROM article WHERE path IS NOT NULL');
        Yii::$app->response->getHeaders()->add('Content-Type', 'text/xml; charset=utf-8');
        return $this->render('sitemap', ['articles' => []]);
    }

    public function actionFlush($redis = false): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
//        if (YII_DEBUG) {
//            FileHelper::removeDirectory(Yii::getAlias('@app/runtime'));
//            FileHelper::removeDirectory(Yii::getAlias('@app/web/assets'));
//        }
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();
        if ($redis) {
            Yii::$app->storage->redis->flushAll();
        }
        return ['success' => true];
    }

    public function actionGet()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'result' => $_GET
        ];
    }

    public function actionGo(int $id)
    {
        $link = Link::findOne($id);
        if ($link) {
            $visit = new Visit([
                'token' => Yii::$app->request->getToken(),
                'url' => $link->id,
                'ip' => Yii::$app->request->getUserIP(),
                'agent' => Yii::$app->request->getUserAgent(),
                'data' => Yii::$app->getRequest()->getBodyParams() ?: null
            ]);
            if ($visit->insert()) {
                return $this->redirect($link->url);
            }
        }
        throw new NotFoundHttpException("Link ${id} not found");
    }

    public function actionNewbie($nick): array
    {
        $visit = new Visit([
            'token' => Yii::$app->request->getToken(),
            'url' => '/ref/' . $nick,
            'ip' => Yii::$app->request->getUserIP(),
            'agent' => Yii::$app->request->getUserAgent(),
        ]);
        return Utils::jsonp([
            'success' => $visit->insert(),
            'result' => $visit->id
        ]);
    }

    public function actionPromotion()
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Promotion'),
            'url' => ['home/promotion']
        ];

        return $this->render('promotion');
    }

    public function actionBanners()
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Banners'),
            'url' => ['home/promotion']
        ];
        $locale = Yii::$app->language;
        $dir = opendir(Yii::getAlias("@webroot/images/pr/$locale/banners"));
        $banners = [];
        while ($banner = readdir($dir)) {
            if ('.' !== $banner[0]) {
                $banners[] = $banner;
            }
        }
        return $this->render('banners', [
            'locale' => $locale,
            'banners' => $banners
        ]);
    }

    public function actionBook()
    {
        return $this->render('book');
    }

    public function actionPdf()
    {
        return $this->render('pdf');
    }

    public function actionContract()
    {
        return $this->render('contract');
    }

    public function actionVerif()
    {
        return $this->render('verif');
    }

    public function actionLanding()
    {
        return $this->render('landing');
    }
}
