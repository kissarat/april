<?php
/**
 * Last modified: 18.08.27 23:00:36
 * Hash: 644771805228cefb680e22b4c8f083a1a840e7f5
 */

namespace app\controllers;


use app\helpers\Utils;
use app\models\form\Password;
use app\models\form\Verification;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;

class SettingsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['profile', 'password', 'wallet', 'verification'],
                'rules' => [
                    'member' => [
                        'allow' => true,
                        'actions' => ['profile', 'password', 'wallet', 'verification'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionProfile(string $nick)
    {
        if (!Utils::isOwn($nick)) {
            throw new ForbiddenHttpException('You cannot access not your profile: ' . $nick);
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Profile Settings ({nick})', ['nick' => $nick]),
            'url' => ['settings/profile']
        ];
        /**
         * @var User $model
         */
        $model = User::findOne(['nick' => $nick]);
        $model->scenario = User::SCENARIO_PROFILE;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ('logist' !== Yii::$app->id && !empty($model->getOldAttribute('pin')) && $model->isAttributeChanged('pin')) {
                $model->addError('pin', Yii::t('app', 'Contact technical support for change'));
            }
            else {
                $file = UploadedFile::getInstance($model, 'avatar_file');

                if ($file) {
                    if ($model->getOldAttribute('avatar')) {
                        $old = Yii::getAlias('@app/web' . $model->getOldAttribute('avatar'));
                        if (file_exists($old)) {
                            unlink($old);
                        }
                    }
                    $md5 = md5_file($file->tempName);
//                header('FILE: ' . $file->tempName);
//                header('MD5: ' . $md5);
                    $url = "/avatars/$nick-$md5." . $file->getExtension();
                    $file->saveAs(Yii::getAlias('@webroot' . $url));
                    $model->avatar = $url;
                }
                $model->save(false);
                Yii::$app->user->identity->refresh();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully'));
            }
        }
        $this->view->params['state']['refback'] = isset($model->refback) && $model->refback > 0 ? $model->refback : 0;
        return $this->render('profile', [
            'model' => $model
        ]);
    }

    public function actionPassword()
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Change Password'),
            'url' => ['settings/password']
        ];
        $model = new Password();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findOne(['id' => Yii::$app->user->identity->id]);
            if ($user->validatePassword($model->current)) {
                $user->password = $model->new;
                $user->generateSecret();
                $user->save(false);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully'));
            } else {
                $model->addError('current', Yii::t('app', 'Invalid password'));
            }
        }
        return $this->render('password', [
            'model' => $model
        ]);
    }

    public function actionWallet(string $nick)
    {
        if (!Utils::isOwn($nick)) {
            throw new ForbiddenHttpException('You cannot access not your profile: ' . $nick);
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Save wallets for money withdraw'),
            'url' => ['settings/wallet']
        ];
        $model = User::findOne(['nick' => $nick]);
        $model->scenario = User::SCENARIO_WALLET;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $can = true;
            foreach ($model->safeAttributes() as $name) {
                if (!empty($model->getOldAttribute($name)) && $model->isAttributeChanged($name)) {
                    $model->addError($name, Yii::t('app', 'Contact technical support for change'));
                    $can = false;
                    break;
                }
            }
            if ($can) {
                $model->save(false);
                Yii::$app->session->addFlash('success', Yii::t('app', 'Successfully'));
            }
        }
        return $this->render('wallet', [
            'model' => $model
        ]);
    }

    /**
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionVerification() {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Verification'),
            'url' => ['settings/verification']
        ];
        $model = new Verification();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $time = date('ymd_his');
            $filename = null;
            foreach (['front', 'back'] as $name) {
                $file = UploadedFile::getInstance($model, $name);
                if ($file) {
                    $filename = implode('-', [
                        $time,
                        $user->nick,
                        $name,
                        md5_file($file->tempName)
                    ])
                    . '.' . $file->getExtension();
                    if ($file->saveAs(Yii::getAlias('@webroot/verification/' . $filename))) {
                        $user->$name = $filename;
                    }
                }
            }
            if ($filename) {
                $user->update(false);
            }
        }
        return $this->render('verification', ['model' => $model]);
    }
}
