<?php
/**
 * Last modified: 18.05.24 17:48:19
 * Hash: 97af82a23fb84cf38c1b217c49033836a121e5ac
 */

namespace app\controllers;

use app\controllers\base\RestController;
use app\helpers\Utils;
use app\models\Log;
use app\models\Transfer;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PerfectController extends RestController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['success', 'fail', 'status', 'pay'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['success', 'fail', 'status', 'pay'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public $enableCsrfValidation = false;

    private function accept()
    {
//        if (Yii::$app->request->getIsAjax()) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//        }
        $data = 'GET' === Yii::$app->perfect->method ? $_GET : $_POST;
        $guid = $data['PAYMENT_ID'] ?? Yii::$app->request->get('guid');
        $transfer = Transfer::findOne(['guid' => $guid]);
        if (!$transfer) {
            throw new NotFoundHttpException('Payment not found');
        }
        if (Yii::$app->perfect->verify($data)) {
            $transfer->wallet = $data['PAYER_ACCOUNT'];
            $transfer->txid = (int)$data['PAYMENT_BATCH_NUM'];
            $transfer->amount = ((float)$data['PAYMENT_AMOUNT']) * 100;
            $transfer->status = Transfer::STATUS_SUCCESS;
            $transfer->currency = 'USD';
            $transfer->system = 'perfect';
            $transfer->time = Utils::timestamp();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully'));
            $nick = Yii::$app->user->identity->nick;
            $amount = $data['PAYMENT_AMOUNT'];
            if (Yii::$app->has('telegram')) {
                Yii::$app->telegram->sendText("Payment from $nick \$$amount $transfer->wallet");
            }
        } else {
            $transfer->status = 'fail';
        }
        $transfer->save(false);
        return $transfer;
    }

    private function redirectAccept()
    {
        $transfer = $this->accept();
        return $this->redirect(['transfer/index',
            'guid' => $transfer->guid,
            'nick' => Yii::$app->user->identity->nick
        ]);
    }

    public function actionSuccess()
    {
        Log::log('perfect', 'success', Yii::$app->request->getQueryParams());
        return $this->redirectAccept();
    }

    public function actionFail($id)
    {
        $transfer = Transfer::findOne(['id' => $id]);
        if (!$transfer) {
            throw new NotFoundHttpException('Payment not found');
        }
        if ($transfer->to !== Yii::$app->user->identity->id) {
            throw new NotFoundHttpException('You can cancel only your payment');
        }
        $transfer->status = 'canceled';
        if ($transfer->save(false)) {
            return $this->redirect(['transfer/index',
                'guid' => $transfer->guid,
                'nick' => Yii::$app->user->identity->nick
            ]);
        }
        throw new \Error('Cannot save payment');
    }

    public function actionStatus()
    {
        $transfer = $this->accept();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            Transfer::STATUS_SUCCESS => Transfer::STATUS_SUCCESS === $transfer->status,
            'transfer' => $transfer
        ];
    }

    private function specialPage($page, $guid)
    {
        return Url::to(["/perfect/$page", 'guid' => $guid], true);
    }

    public function actionPay($amount = 0)
    {
        if (!($amount > 0)) {
            throw new BadRequestHttpException('Amount must be positive integer');
        }
        $created = Utils::timestamp();
        $transfer = new Transfer([
            'type' => 'payment',
            'status' => 'created',
            'to' => Yii::$app->user->identity->id,
            'ip' => Yii::$app->request->getUserIP(),
            'amount' => (int)$amount,
            'created' => $created,
            'system' => 'perfect',
            'currency' => 'USD',
            'guid' => Utils::generateCode()
        ]);
        $transfer->save(false);
        $balance = Transfer::getBalance(Yii::$app->user->identity->id, 'USD');
        $amount = $transfer->amount / 100;
        $nick = Yii::$app->user->identity->nick;
        $data = [
            'BAGGAGE_FIELDS' => 'USER USER_ID BALANCE CREATED IP EMAIL',
            'BALANCE' => $balance / 100,
            'EMAIL' => Yii::$app->user->identity->email,
            'IP' => $transfer->ip,
            'NOPAYMENT_URL' => $this->specialPage('fail', $transfer->guid),
            'PAYEE_ACCOUNT' => Yii::$app->perfect->wallet,
            'PAYEE_NAME' => Yii::$app->name,
            'PAYMENT_AMOUNT' => $amount,
            'PAYMENT_ID' => $transfer->guid,
            'PAYMENT_UNITS' => 'USD',
            'PAYMENT_URL' => $this->specialPage('success', $transfer->guid),
            'PAYMENT_URL_METHOD' => Yii::$app->perfect->method,
            'STATUS_URL' => $this->specialPage('status', $transfer->guid),
//            'SUGGESTED_MEMO' => "$nick $$amount",
            'CREATED' => $created,
            'USER' => $nick,
            'USER_ID' => Yii::$app->user->identity->id,
        ];
        return $this->render('@app/views/transfer/autosubmit', [
            'url' => 'https://perfectmoney.is/api/step1.asp',
            'data' => $data
        ]);
    }
}
