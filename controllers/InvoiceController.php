<?php
/**
 * Last modified: 18.08.06 11:11:05
 * Hash: b6ddfd3f04874cde36b5e109a24c7efc4626e144
 */

namespace app\controllers;


use app\controllers\base\RestController;
use app\helpers\Utils;
use app\models\form\Invoice;
use app\models\Log;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class InvoiceController extends RestController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['payment', 'withdraw'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['payment', 'withdraw'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @param string $nick
     * @return array|string|\yii\web\Response
     */
    public function actionPayment(string $nick)
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Pay'),
            'url' => ['transfer/payment', 'nick' => $nick]
        ];
        $model = new Invoice([
            'scenario' => Invoice::SCENARIO_PAYMENT,
            'currency' => Yii::$app->request->getCurrency()
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Log::log('transfer', 'payment', [
                'ip' => $model->ip,
                'amount' => $model->amount,
                'currency' => $model->currency
            ]);
            return $this->redirect([$model->system . '/pay', 'amount' => $model->amount * Transfer::FACTOR[Transfer::BASE_CURRENCY]]);
        }

        if ('GET' !== Yii::$app->request->getMethod()) {
            Yii::$app->request->save();
        }
        if (!Yii::$app->request->isPost) {
            $model->setupFirstSystem(Yii::$app->user->identity);
        }
        $this->view->params['state']['system'] = $model->system;
        return $this->render('payment', [
            'model' => $model
        ]);
    }

    /**
     * @param string $nick
     * @return array|string
     * @throws ForbiddenHttpException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionWithdraw(string $nick)
    {
        if (!Utils::isOwn($nick)) {
            throw new ForbiddenHttpException('You cannot access not your withdraw: ' . $nick);
        }

        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;

        if ('logist' !== Yii::$app->id && empty($user->pin)) {
            Yii::$app->session->addFlash('warning', Yii::t('app', 'Enter your transaction password'));
            return $this->redirect(['settings/profile', 'nick' => $user->nick]);
        }

        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Withdraw'),
            'url' => ['transfer/payment', 'nick' => $nick]
        ];
        $model = new Invoice([
            'scenario' => Invoice::SCENARIO_WITHDRAW,
            'currency' => Yii::$app->request->getCurrency()
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Log::log('transfer', 'withdraw', [
                'ip' => $model->ip,
                'wallet' => $model->wallet,
                'amount' => $model->amount,
                'currency' => $model->currency
            ]);
            if (!empty($user->pin) && $user->pin !== $model->pin) {
                $model->addError('pin', Yii::t('app', 'Invalid transaction password'));
            } else {
                $t = Yii::$app->db->beginTransaction('SERIALIZABLE');
                $where = [
                    'from' => $user->id,
                    'type' => Transfer::TYPE_WITHDRAW,
                    'status' => Transfer::STATUS_CREATED,
                    'currency' => $model->currency
                ];
                $previous = Transfer::findOne($where);
                if ($previous) {
                    Yii::$app->session->addFlash('error', Yii::t('app', 'You have submitted your withdrawal request for {currency}', [
                        'currency' => $model->currency
                    ]));
                } else {
                    $buy = Transfer::find()
                        ->where([
                            'from' => $user->id,
                            'type' => Transfer::TYPE_BUY,
                            'status' => Transfer::STATUS_SUCCESS
                        ])
                        ->one();
                    if ($buy) {
                        if ($model->validWallet($user)) {
                            $withdraw = new Transfer($where);
                            $withdraw->amount = $model->amount * Transfer::FACTOR[$model->currency];
                            if ($user->checkBalance($withdraw->amount, $withdraw->currency)) {
                                $withdraw->system = $model->system;
                                $withdraw->wallet = $model->wallet;
                                if ($model->text) {
                                    $withdraw->text = $model->text;
                                }
                                $withdraw->vars = [
                                    'ip' => $model->ip,
                                    'referrer' => Yii::$app->request->getHeaders()->get('referrer'),
                                    'agent' => Yii::$app->request->getUserAgent(),
                                ];
                                if ($withdraw->insert()) {
                                    $t->commit();
                                    return $this->redirect(['transfer/index', 'id' => $withdraw->id]);
                                }
                            } else {
                                $model->addError('amount', Yii::t('app', 'Insufficient funds'));
                            }
                        } else {
                            $model->addError('wallet', Yii::t('app', 'You cannot change your wallet'));
                        }

                    } else {
                        Yii::$app->session->addFlash('error', Yii::t('app', 'You have no investments'));
                    }
                }
            }
        }
        if (!Yii::$app->request->isPost) {
            $model->setupFirstSystem($user);
        }

        $this->view->params['state']['system'] = $model->system;
        $this->view->params['state']['wallet'] = $model->wallet;
        return $this->render('withdraw', [
            'model' => $model
        ]);
    }

}
