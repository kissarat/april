<?php
/**
 * Last modified: 18.07.09 17:40:48
 * Hash: 8442f0f57228b0019439bed34010d0d4a4646fc7
 */

namespace app\controllers;


use app\components\Parity;
use app\helpers\Utils;
use app\models\Transfer;
use function GuzzleHttp\json_encode;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class EthereumController extends Controller
{
    protected function getComponent(string $system = 'eth'): Parity
    {
        return Yii::$app->get($system);
    }

    public function beforeAction($action)
    {
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        Yii::$app->request->enableCsrfValidation = false;
//        return parent::beforeAction($action);
        return false;
    }
/*
    public function actionFee(string $system)
    {
        $com = $this->getComponent($system);
        return ['result' => $com->getFee()];
    }

    public function actionHook() {
        $id = dechex(floor(microtime(true) * 100000));
        file_put_contents("/tmp/telegram-$id.json", Utils::json(Yii::$app->request->getBodyParams()));
        Yii::$app->response->statusCode = 200;
        return ['ok' => true];
    }


    public function actionIndex(string $system, $min = 0)
    {
        $com = $this->getComponent($system);
        return ['result' => $com->getBalanceList($min)];
    }

    public function actionSend(string $system, int $amount, string $to, string $from, string $password = null)
    {
        $com = $this->getComponent($system);
        return ['result' => $com->send($amount, $to, $from, $password)];
    }

    public function actionApprove(int $id, string $from, string $password = null)
    {
        $t = Yii::$app->db->beginTransaction();
        $tr = Transfer::findOne($id);
        if ($tr) {
            $com = $this->getComponent(strtolower($tr->currency));
            if ($com instanceof Ethereum && Transfer::TYPE_WITHDRAW === $tr->type && Transfer::STATUS_CREATED === $tr->status) {
                $txid = $com->send($tr->amount, $tr->wallet, $from, $password);
                if ($txid) {
                    $tr->txid = $txid;
                    $tr->time = Utils::timestamp();
                    if ($tr->update()) {
                        $t->commit();
                        return ['result' => $txid];
                    }
                }
                throw new ServerErrorHttpException();
            }
            throw new BadRequestHttpException();
        }
        throw new NotFoundHttpException();
    }

    public function actionUnlock(string $system, string $address, string $password = null)
    {
        $com = $this->getComponent($system);
        return ['result' => $com->unlockAccount($address, $password)];
    }
*/
}
