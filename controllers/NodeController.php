<?php
/**
 * Last modified: 18.08.28 00:38:23
 * Hash: b3a26e7f3b8574fadaddaa4eca925ba9f9a87e88
 */

namespace app\controllers;


use app\helpers\SQL;
use app\models\Program;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class NodeController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'My Plans'),
            'url' => ['node/index']
        ];
        return $this->render('index', [
            'models' => $user->getNodes()
        ]);
    }

    public function actionPromo()
    {
        $nodes = SQL::queryAll('SELECT nick, node as program FROM "user" u JOIN transfer n ON u.id = n."from"
            WHERE n.created >= \'2018-08-17\' AND n.type = \'buy\' ORDER BY n.created DESC');
        $nicks = [];
        foreach ($nodes as $n) {
            $nicks[$n['program']][] = $n['nick'];
        }
        return $this->render('promo', [
            'programs' => Program::getProgramList(),
            'nicks' => $nicks
        ]);
    }
}
