<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 1092109b2243d8f2a7602c3fdd9845e9b317a54d
 */

namespace app\controllers;


use app\base\JsonResponseTrait;
use app\controllers\base\RestController;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class TransferController extends RestController
{
    use JsonResponseTrait;

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'index', 'payment', 'withdraw',
                    'summary', 'wallet', 'rates', 'histogram',
                    'balance'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'payment', 'withdraw',
                            'summary', 'wallet', 'rates', 'histogram',
                            'balance'
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex($offset = 0, $type = null)
    {
        $id = Yii::$app->user->identity->id;
        $q = Transfer::find()
            ->where(['and',
                ['or',
                    ['status' => Transfer::STATUS_SUCCESS],
                    ['status' => 'pending'],
                    ['type' => Transfer::TYPE_WITHDRAW],
                ],
                ['or', ['to' => $id], ['from' => $id]]
            ]);
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $q->offset($offset)
                ->orderBy(['id' => SORT_DESC])
                ->limit(20);
            if ($type) {
                $q->andWhere(['type' => $type]);
            }
            return ['result' => $q->all()];
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Finances'),
            'url' => ['transfer/index']
        ];
        return $this->render('index', [
            'provider' => new ActiveDataProvider([
                'query' => $q
            ])
        ]);
    }

    public function actionSummary($currency): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'result' => Transfer::getInformer(Yii::$app->user->id, $currency)
        ];
    }

    public function actionMonthIncome(int $month, string $currency): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $amount = (int)Transfer::find()
            ->andWhere([
                'currency' => $currency,
                'type' => Transfer::TYPE_ACCRUE,
                'to' => Yii::$app->user->identity->id,
                'status' => Transfer::STATUS_SUCCESS
            ])
            ->andWhere("date_part('month', created) = :month", [':month' => $month])
            ->sum('amount');

        return ['result' => $amount];
    }

    /**
     * @param $system
     * @return array
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionWallet($system): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!\in_array($system, Transfer::WALLETS, true)) {
            throw new NotFoundHttpException("Wallet $system not found");
        }
        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;
        if (empty($user->$system)) {
            if (isset(Yii::$app->$system)) {
                return ['result' => Yii::$app->$system->getUserAddress($user->id)];
            }
            if (YII_DEBUG) {
                $user->$system = 'invalid_wallet';
                $user->update(false);
                return ['result' => $user->$system];
            }
        }
        throw new ServerErrorHttpException('No configuration for wallet ' . $system);
    }

    public function actionRates()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Transfer::getExchangeRate(true);
    }

    public function actionHistogram(int $month, $currency): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => Transfer::getHistogram(Yii::$app->user->id, $currency, $month)];
    }

    public function actionTurnover(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => Transfer::getTurnover(Yii::$app->user->id)];
    }

    public function actionBalance(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => Transfer::getAllBalances(Yii::$app->user->id)];
    }
}
