<?php
/**
 * Last modified: 18.08.03 21:34:28
 * Hash: 2fdb72927d42a1d0a54c1161c9df55ab0969af88
 */

namespace app\controllers;

use app\helpers\Utils;
use app\models\Transfer;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class InterkassaController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['success', 'fail', 'pending'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['success', 'fail', 'pending'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['internal'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    protected function getParams()
    {
        return Yii::$app->request->getQueryParams();
    }

    /**
     * @param null|string $status
     * @return Response
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    protected function redirectTransfers(?string $status = null)
    {
        $params = $this->getParams();
        $t = Transfer::findOne(['guid' => $params['ik_pm_no']]);
        if (!$t) {
            throw new NotFoundHttpException('Payment not found');
        }
        if ($status) {
            $t->status = $status;
            if (!$t->update(false)) {
                throw new ServerErrorHttpException('Cannot change status');
            }
        }
        return $this->redirect(['transfer/index', 'id' => $t->id]);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSuccess()
    {
        $r = $this->redirectTransfers();
        Yii::$app->session->addFlash('success', Yii::t('app', 'Successfully'));
        return $r;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionFail()
    {
        $r = $this->redirectTransfers(Transfer::STATUS_FAIL);
        Yii::$app->session->addFlash('error', Yii::t('app', 'Fail'));
        return $r;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionPending()
    {
        $r = $this->redirectTransfers(Transfer::STATUS_PENDING);
        Yii::$app->session->addFlash('warning', Yii::t('app', 'Pending'));
        return $r;
    }

    /**
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionInternalaelae8naede8iekah9yae2pa()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $params = $this->getParams();
        $rate = Yii::$app->interkassa->rates[$params['ik_cur']] ?? 0;
        if (!($rate > 0)) {
            throw new BadRequestHttpException('Invalid currency ' . $params['ik_cur']);
        }
        $amount = (int)floor(100 * $params['ik_am'] / $rate);
        if (!($amount > 0)) {
            throw new BadRequestHttpException('Invalid amount ' . $params['ik_am']);
        }
        $t = Transfer::findOne(['guid' => $params['ik_pm_no']]);
        if (!$t) {
            throw new NotFoundHttpException('Payment not found');
        }
        $t->status = 'success' === $params['ik_inv_st'] ? Transfer::STATUS_SUCCESS : Transfer::STATUS_FAIL;
        $t->amount = $amount;
        if ($t->update(false)) {
            return ['success' => true];
        }
        throw new ServerErrorHttpException('Cannot save payment');
    }

    /**
     * @param int $amount
     * @return string
     * @throws BadRequestHttpException
     * @throws ServerErrorHttpException
     * @throws \Throwable
     */
    public function actionPay(int $amount)
    {
        if ($amount <= 0) {
            throw new BadRequestHttpException('Invalid amount');
        }
        $transfer = new Transfer([
            'type' => Transfer::TYPE_PAYMENT,
            'status' => 'created',
            'system' => 'interkassa',
            'currency' => 'USD',
            'to' => (int)Yii::$app->user->getId(),
            'ip' => Yii::$app->request->getUserIP(),
            'guid' => Utils::generateCode(),
            'amount' => $amount
        ]);
        if ($transfer->insert(false)) {
            return $this->render('pay', [
                'guid' => $transfer->guid,
                'amount' => (string)(ceil(Yii::$app->interkassa->rates['RUB'] * $amount) / Transfer::FACTOR['USD'])
            ]);
        }
        throw new ServerErrorHttpException('Cannot save payment');
    }
}
