<?php

namespace app\controllers;


use app\controllers\base\RestController;
use app\models\form\LotteryForm;
use app\models\Lottery;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Transaction;
use yii\filters\AccessControl;
use yii\web\ServerErrorHttpException;

class LotteryController extends RestController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['participate'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['participate'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array|string
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionParticipate()
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Lottery'),
            'url' => ['lottery/participate']
        ];
        $model = new LotteryForm(['amount' => 1, 'quantity' => 1, 'currency' => Yii::$app->request->getCurrency()]);
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $t = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
            if ($user->checkBalance($model->getCurrencyAmount(), $model->currency)) {
                if ($model->participate($user)) {
                    $t->commit();
                } else {
                    throw new ServerErrorHttpException('Cannot participate into lottery');
                }
            } else {
                $model->addError('amount', Yii::t('app', 'Insufficient funds'));
            }
        }
        $firstNumber = Lottery::getFirstNumber();
        $this->view->params['state']['rates'] = Transfer::getExchangeRate();
        $this->view->params['state']['amount'] = $model->amount;
        $this->view->params['state']['quantity'] = $model->quantity;
        return $this->render('participate', [
            'model' => $model,
            'fund' => Lottery::getFund(),
            'firstNumber' => $firstNumber,
            'firstLottery' => Lottery::getFirstLottery($user->id),
            'participants' => Lottery::getParticipants($firstNumber + 1)
        ]);
    }
}
