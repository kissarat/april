<?php
/**
 * Last modified: 18.05.09 23:39:35
 * Hash: 490cd588994a75b53736397f1272961d0c71e0aa
 */

namespace app\controllers\base;


use Yii;
use yii\rest\ActiveController;

abstract class AdminController extends ActiveController
{
    public function beforeAction($action)
    {
        if (YII_TEST) {
            return parent::beforeAction($action);
        }
        return false;
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'create' => [
                'class' => 'yii\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
            ],
            'delete' => [
                'class' => 'yii\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    public function actionView() {
        return $this->modelClass::findOne(Yii::$app->request->getQueryParams());
    }
}
