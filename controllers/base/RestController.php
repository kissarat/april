<?php

namespace app\controllers\base;


use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

abstract class RestController extends Controller
{
    public function beforeAction($action)
    {
        if (YII_TEST) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @return bool
     * Checks if request is api invocation
     */
    public function isApiCall()
    {
        return isset(Yii::$app->request->getAcceptableContentTypes()['application/json'])
            || 'json' === Yii::$app->request->get('format');
    }

    /**
     * @param string $view
     * @param array $params
     * @return array|string
     * Returns json data if it is api invocation renders view otherwise
     */
    public function render($view, $params = [])
    {
        if (isset($params['search']) && !isset($params['provider'])) {
            $params['provider'] = $params['search']->search(Yii::$app->request->get());
        }
        if ($this->isApiCall()) {
            return $this->json($params);
        }
        return parent::render($view, $params);
    }

    public function json($params)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!YII_DEBUG) {
            throw new ForbiddenHttpException();
        }
        $r = $params;
//        $r = isset($params['status']) ? $params : [];
//        $r['spend'] = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        if (isset($params['provider'])) {
            /**
             * @var \yii\data\ActiveDataProvider $q
             */
            $q = $params['provider'];
            $models = $q->getModels();
            $page = $q->getPagination();
            $sort = [];
            foreach ($q->sort->orders as $key => $order) {
                $sort[$key] = $order === SORT_ASC ? 1 : -1;
            }
            if (count($sort) > 0) {
                $r['sort'] = $sort;
            }
            $r['page'] = [
                'offset' => $page->offset,
                'limit' => $page->limit,
                'total' => $page->totalCount
            ];
            $r['result'] = $models;
        } else if (isset($params['model'])) {
            $r['result'] = $params['model'];
        } else if (isset($params['models'])) {
            $r['result'] = $params['models'];
        }
        unset($r['provider']);
        return $r;
    }
}
