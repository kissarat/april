<?php

namespace app\controllers\base;


use app\helpers\Utils;
use app\models\Transfer;
use yarcode\payeer\events\GatewayEvent;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Response;

abstract class YarcodeController extends RestController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['pay', 'success', 'fail'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['pay', 'success', 'fail'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    abstract protected function getPaymentSystemId(): string;

    abstract protected function handleRequest(array $data): Transfer;

    /**
     * @return \yii\base\Component
     */
    public function getComponent()
    {
        return Yii::$app->get($this->getPaymentSystemId());
    }

    public function init()
    {
        $this->getComponent()->on(GatewayEvent::EVENT_PAYMENT_REQUEST, [$this, 'onStatus']);
        parent::init();
    }

    public function actionPay($amount)
    {
        $amount = +$amount;
        $transfer = new Transfer([
            'type' => 'payment',
            'status' => 'created',
            'system' => $this->getPaymentSystemId(),
            'to' => Yii::$app->user->identity->id,
            'ip' => Yii::$app->request->getUserIP(),
            'currency' => 'USD',
            'guid' => Utils::generateCode(),
            'amount' => $amount,
            'vars' => ['agent' => Yii::$app->request->getUserAgent()]
        ]);
        $transfer->save();
        return $this->render('pay', [
            'guid' => $transfer->guid,
            'amount' => $amount / Transfer::FACTOR['USD'],
            'description' => '',
            'to' => $transfer->to
        ]);
    }

    protected function handleUserRequest()
    {
        $t = $this->handleRequest(Yii::$app->request->getQueryParams());
        if ($t) {
            return $this->redirect(['transfer/index', 'guid' => $t->guid]);
        }
        throw new BadRequestHttpException();
    }

    public function actionSuccess()
    {
        return $this->handleUserRequest();
    }

    public function actionFail()
    {
        return $this->handleUserRequest();
    }

    public function actionStatus()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $t = $this->handleRequest(Yii::$app->request->getQueryParams());
        if ($t) {
            return ['success' => true];
        }

        Yii::$app->response->statusCode = 403;
        return ['error' => ['message' => 'Invalid request']];
    }
}
