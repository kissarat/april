<?php

namespace app\controllers;


use app\controllers\base\RestController;
use app\models\Career;
use app\models\Lottery;
use app\models\Transfer;
use app\models\TransferRate;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class CareerController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex() {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Career'),
            'url' => ['career/index']
        ];
        $id = Yii::$app->user->id;
        return $this->render('index', [
            'lottery' => Lottery::countTickets($id),
            'personal' => TransferRate::sumNodes($id),
            'turnover' => Transfer::getTurnover($id),
        ]);
    }
}
