<?php
/**
 * Last modified: 18.05.09 23:31:53
 * Hash: a9cda581b0a4309760f92fdc22777584cc5dd3ff
 */

namespace app\controllers;


use app\controllers\base\AdminController;
use app\models\User;

class SubjectController extends AdminController
{
    public $modelClass = User::class;
}
