<?php

namespace app\controllers;


use app\models\Referral;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\PageCache;

class StructureController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ]
                ]
            ],
//            [
//                'class' => PageCache::class,
//                'only' => ['index'],
//                'duration' => YII_DEBUG ? 0 : 120,
//                'variations' => [
//                    Yii::$app->language,
//                    Yii::$app->user->id,
//                    Yii::$app->request->get('nick'),
//                    Yii::$app->request->get('format')
//                ],
//            ],
        ];
    }

    public function actionIndex(string $nick = null, $format = 'layout') {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Structure'),
            'url' => ['structure/index', 'nick' => $nick]
        ];
        if ('json' === $format) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        $parent = empty($nick) ? Yii::$app->user->identity : User::findByNick($nick);
        if ($parent) {
            $enable = $parent->checkStructure(Yii::$app->user->id, 'logist' === Yii::$app->id ? 5 : 2);
            if (!$enable && Yii::$app->user->id !== $parent->id) {
                throw new ForbiddenHttpException("This $nick not your structure");
            }
            if ('json' === $format) {
                return [
                    'result' => Referral::find()
                    ->where(['parent' => $parent->id])
                    ->orderBy(['id' => SORT_DESC])
                    ->all()
                ];
            }
            if ('view' === $format) {
                $this->layout = false;
            }
            return $this->render('view' === $format ? 'children' : 'index', [
                'nick' => $nick,
                'provider' => new ActiveDataProvider([
                    'query' => Referral::find()
                        ->where(['parent' => $parent->id])
                        ->orderBy(['id' => SORT_DESC])
                ])
            ]);
        }
        throw new NotFoundHttpException('Not found user ' . $nick);
    }
}
