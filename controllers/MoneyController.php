<?php
/**
 * Last modified: 18.05.09 23:31:53
 * Hash: d9e297fc085d927aa62f2ca4052bfd0327d9f674
 */

namespace app\controllers;


use app\controllers\base\AdminController;
use app\models\Transfer;
use yii\db\ActiveRecord;

class MoneyController extends AdminController
{
    /**
     * @var ActiveRecord
     */
    public $modelClass = Transfer::class;
}
