<?php
/**
 * Last modified: 18.10.30 03:03:41
 * Hash: e33f1f73709bef6fdd12e6f159d454d838271d76
 */

namespace app\controllers;

use app\controllers\base\RestController;
use app\helpers\Utils;
use app\models\Email;
use app\models\form\Login;
use app\models\form\Password;
use app\models\form\RequestPasswordReset;
use app\models\Log;
use app\models\Token;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;


class UserController extends RestController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'request', 'reset', 'profile'],
                'rules' => [
                    'guest' => [
                        'allow' => true,
                        'actions' => ['request', 'reset'],
                        'roles' => ['?']
                    ],
                    'member' => [
                        'allow' => true,
                        'actions' => ['profile', 'logout'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    protected function saveSponsor($nick)
    {
        $nick = $nick ?: Yii::$app->request->getCookie('sponsor', Yii::$app->params['root']);
        if (Yii::$app->params['root'] !== $nick) {
            $expire = time() + 3600 * 24 * 30 * 12 * 10;
//            Yii::$app->request->cookies->add(new Cookie([
//                'name' => 'sponsor',
//                'value' => $nick,
//                'expire' => $expire
//            ]));
            setcookie('sponsor', $nick, $expire, '/');
        }
        return $nick;
    }

    public function actionSignup($nick = null)
    {
        if (!Yii::$app->user->getIsGuest()) {
            return $this->redirect(['user/profile', 'nick' => Yii::$app->user->identity->nick]);
        }

        $this->layout = 'outdoor';
        $nick = $this->saveSponsor($nick);
        $model = new User(['scenario' => User::SCENARIO_REGISTER]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->parent = User::getIdByNick($model->sponsor ?: $nick ?: Yii::$app->params['root']);
            $model->generateSecret();
            $model->save(false);
            if (Yii::$app->user->login($model)) {
                Email::send($model->email,
                    Yii::t('app', '{name}: Welcome', ['name' => Yii::$app->name]),
                    Yii::t('app', "Dear, {nick}\nThank you for registration at {name}", [
                        'nick' => $model->nick,
                        'name' => Yii::$app->name
                    ]));
                Log::log('user', 'signup', ['ip' => $model->ip]);
                if (Yii::$app->has('telegram') && Yii::$app->telegram->getStorage()->get('signup')) {
                    Yii::$app->telegram->sendText("Welcome $model->nick ($model->email)! Invited by "
                        . $model->parentObject->nick);
                }
                return $this->redirect(['user/profile', 'nick' => $model->nick]);
            }
        } else {
            $model->sponsor = $nick;
//            if ('GET' !== Yii::$app->request->getMethod()) {
//                Yii::$app->request->save();
//            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionLogin($nick = null)
    {
        if (!Yii::$app->user->getIsGuest()) {
            return $this->redirect(['user/profile', 'nick' => Yii::$app->user->identity->nick]);
        }

        $this->layout = 'outdoor';
        $this->saveSponsor($nick);
        $data = empty(Yii::$app->params['demo']) ? [] : Yii::$app->params['demo'];
        $model = new Login($data);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = ['ip' => $model->ip];
            $user = null;
            if (Login::checkLoginAttempt(['ip' => Yii::$app->request->getUserIP()])) {
                $user = $model->getUser();
                if ($user) {
                    if (Login::checkLoginAttempt(['user' => $user->id])) {
                        if ($user->secret) {
                            if ($user->validatePassword($model->password)) {
                                if (Yii::$app->params['admin'] && !Utils::isAdmin($user->nick)) {
                                    $model->addError('login', Yii::t('app', 'User is not admin'));
                                } else {
                                    Yii::$app->user->login($user);
                                    Log::log('auth', 'login', $data);
                                    if (isset($user->refback) && $user->refback > 0) {
                                        Yii::$app->session->addFlash('warning',
                                            Yii::t('app', 'Your refback is {percent}%', [
                                                'percent' => (int)round($user->refback * 100)
                                            ]));
                                    }
                                    return $this->redirect(['user/profile', 'nick' => $user->nick]);
                                }
                            } else {
                                $model->addError('password', Yii::t('app', 'Invalid password'));
                            }
                        } else {
                            Yii::$app->session->addFlash('error', Yii::t('app', 'You are banned'));
                        }
                    }
                } else {
                    $model->addError('login', Yii::t('app', 'User does not exists'));
                }
            }
            if (!$user) {
                $data['login'] = $model->login;
            }
            Log::log('auth', 'invalid', $data, $user ? $user->id : null);
        }
//        elseif ('GET' !== Yii::$app->request->getMethod()) {
//            Yii::$app->request->save();
//        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout($ip = null)
    {
        if (YII_DEBUG || 'POST' === Yii::$app->request->getMethod()) {
            $id = Yii::$app->user->identity->id;
            Yii::$app->user->logout(false);
            Log::log('auth', 'logout', $ip ? ['ip' => $ip] : null, $id);
            return $this->redirect(['user/login']);
        }
        throw new MethodNotAllowedHttpException();
    }

    public function actionProfile(string $nick)
    {
        if (!Utils::isOwn($nick)) {
//            throw new ForbiddenHttpException('You cannot access not your profile: ' . $nick);
            return $this->redirect(['user/profile', 'nick' => Yii::$app->user->identity]);
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Dashboard'),
            'url' => ['user/profile', 'nick' => $nick]
        ];
        $vars = Yii::$app->user->identity->getInformers(Yii::$app->request->getCurrency());
        $vars['investments'] = Yii::$app->user->identity->getNodes();
        $vars['reflink'] = Yii::$app->user->identity->getReflink();
        return $this->render('profile', $vars);
    }

    public function actionRequest()
    {
        $this->layout = 'outdoor';
        $model = new RequestPasswordReset();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if ($user) {
                if ($user->secret) {
                    $code = $user->generateCode();
                    if (!$code) {
                        throw new ForbiddenHttpException();
                    }
                    $subject = Yii::t('app', '{name}: Password Recovery', ['name' => Yii::$app->name]);
                    $url = Url::to(['user/reset', 'code' => $code], true);
                    $text = Yii::t('app', 'Recover your password {url}', ['url' => $url]);
                    Email::send($user->email, $subject, $text);
                    Log::log('password', 'request', ['ip' => $model->ip]);
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Check your email'));
                } else {
                    Yii::$app->session->addFlash('error', Yii::t('app', 'You are banned'));
                }
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'User does not exists'));
            }
        }
        return $this->render('request', ['model' => $model]);
    }

    public function actionReset(string $code)
    {
        $this->layout = 'outdoor';
        $model = new Password(['scenario' => Password::SCENARIO_RESET]);
        $token = Token::findOne(['id' => $code]);
        if ($token) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user = User::findOne(['id' => $token->user]);
                $user->password = $model->new;
                $user->generateSecret();
                Log::log('password', 'reset', ['ip' => $model->ip], $user->id);
                if ($user->save(false) && $token->delete()) {
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Password changed'));
                }
                return $this->redirect(['user/login']);
            }
        } else {
            throw new ForbiddenHttpException('Password recovery code not found or already used');
        }
        return $this->render('reset', ['model' => $model]);
    }

    public function actionAuthorize($nick, $token)
    {
        Yii::$app->request->save();
        $auth = Token::findOne(['id' => $token]);
        if ($auth) {
            $user = User::findOne(['nick' => $nick]);
            if ($user) {
                if ($auth->userObject->admin) {
//            $allowed = YII_DEBUG ? true : Ip::can(Yii::$app->request->getUserIP());
                    if (Yii::$app->user->login($user)) {
                        Log::log('auth', 'admin', [
                            'nick' => $user->nick,
                            'token' => $auth->id,
                            'agent' => Yii::$app->request->getUserAgent()
                        ]);
                        return $this->redirect(['user/profile', 'nick' => $user->nick]);
                    }
                    throw new ForbiddenHttpException('cannot login ' . $nick);
                }
                throw new ForbiddenHttpException('admin ' . $auth->userObject->nick);
            }
            throw new ForbiddenHttpException('user ' . $nick);
        }
        throw new ForbiddenHttpException('token');
    }
}
