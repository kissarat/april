<?php

namespace app\controllers;


use app\models\Program;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\db\Transaction;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class ProgramController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array|string
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('app', 'Plans'),
            'url' => ['program/index']
        ];
        $isAjax = Yii::$app->request->getIsAjax();
        if ($isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;
        $programs = Program::getProgramList();
        $this->view->params['state']['program'] = 0;
        $post = Yii::$app->request->post('Program');
        $model = null;
        if (!empty($post['id'])) {
            foreach ($programs as $i => $program) {
                if ($program->id === +$post['id']) {
                    if ($program->load(Yii::$app->request->post()) && $program->validate()) {
                        $amount = $program->getAmountCents();
                        $t = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
                        if (Yii::$app->user->identity->checkBalance($amount, $program->currency)) {
                            $transfer = $program->buy($user);
                            if ($transfer) {
                                $t->commit();
                                if ($isAjax) {
                                    return ['success' => true];
                                }
                                return $this->redirect(['node/index', 'id' => $transfer->id]);
                            }
                            throw new ServerErrorHttpException('Cannot open investment ' . $program->name);
                        }
                        $program->addError('amount', Yii::t('app', 'Insufficient funds'));
                    }
                    $programs[$i] = $program;
                    $model = $program;
                    $this->view->params['state']['program'] = $i;
                }
            }
        }
        if ($isAjax) {
            $params = [
                'result' => Program::getProgramDataList(),
            ];
            if ($model) {
                $params['model'] = [
                    'id' => $model->id,
                    'amount' => $model->amount,
                    'currency' => $model->currency,
                ];
                if ($model->hasErrors()) {
                    $params['errors'] = $model->getErrors();
                }
            } else {
                $params['rates'] = Transfer::getExchangeRate();
            }
            return $params;
        }
        $this->view->params['state']['rates'] = Transfer::getExchangeRate();
        $this->view->params['state']['programs'] = Program::getProgramDataList();
        $parent = $user->parentObject;
        if ($parent && $parent->refback > 0) {
            Yii::$app->session->addFlash('info',
                Yii::t('app', 'Refback from your sponsor will be {percent}%', [
                    'percent' => (int)round($parent->refback * 100)
                ]));
        }
        return $this->render('index', [
            'models' => $programs
        ]);
    }

    public function actionCalculator(string $view = 'main')
    {
        $isEmbed = 'embed' === $view;
        if ($isEmbed) {
            $this->layout = 'embed';
            $this->view->params['breadcrumbs'][] = [
                'label' => Yii::t('app', 'Calculator'),
                'url' => ['program/calculator']
            ];
            $this->view->params['state']['rates'] = Transfer::getExchangeRate();
            $this->view->params['state']['programs'] = array_values(Program::getProgramDataList());
        }
        return $this->render($isEmbed ? 'calculator' : 'frame');
    }
}
