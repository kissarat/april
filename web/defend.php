<?php

//define('PRIVATE_NETWORKS', [
//    167772160 => 4278190080,
//    1681915904 => 4290772992,
//    2886729728 => 4293918720,
//    3232235520 => 4294901760
//]);

define('DEFEND_ADDRESS_HEADER', 'REMOTE_ADDR');
define('DEFEND_MASK', 0xffffff00);
define('DEFEND_AGE', 3600);
define('DEFEND_DELTA_THRESHOLD', 0.3);
define('DEFEND_POST_MIN_CHECK', 3);
define('DEFEND_GET_MIN_CHECK', 6);
define('DEFEND_LOG_FILENAME', __DIR__ . '/../ban.txt');
define('DEFEND_TIME_FORMAT', 'd.H:i:s');
define('DEFEND_URL_IGNORE', '/^\/(debug|transfer\/histogram|summary)/');

function defend_die()
{
    http_response_code(503);
    header('Content-Type: text/plain');
    die('Service Unavailable');
}

function defend()
{
    if (filter_var($_SERVER[DEFEND_ADDRESS_HEADER], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
//        die('IPv6 is not supported');
	return
    }
    $ip = $_SERVER[DEFEND_ADDRESS_HEADER];
    $masked = long2ip(ip2long($ip) & DEFEND_MASK);
    $banned = $ip . '_ban';
    if (apcu_fetch($banned)) {
        defend_die();
    }
    if (preg_match(DEFEND_URL_IGNORE, $_SERVER['REQUEST_URI'])) {
        return;
    }
//    apcu_store('a', ['a' => 1], DEFEND_AGE);
    $records = apcu_fetch($masked);
    if (!$records) {
        $records = [];
    }
    $size = count($records);
    $min = 'GET' === $_SERVER['REQUEST_METHOD'] ? DEFEND_GET_MIN_CHECK : DEFEND_POST_MIN_CHECK;
    $records[] = microtime(true);
    if ($size >= $min) {
        $last = $records[0];
        $delta = 0;
        for ($i = 1; $i < $size; $i++) {
            $current = $records[$i];
            $delta += $current - $last;
            $last = $current;
        }
        $delta /= $size;
        if ($delta < DEFEND_DELTA_THRESHOLD) {
            $log = implode("\t", [date(DEFEND_TIME_FORMAT), $ip, $delta, $_SERVER['HTTP_USER_AGENT'] ?? '']);
            file_put_contents(DEFEND_LOG_FILENAME, $log . "\n", FILE_APPEND);
            apcu_store($banned, true, DEFEND_AGE);
            defend_die();
        }
    }
    $records = array_slice($records, -(DEFEND_GET_MIN_CHECK + 2));
//    var_dump($records);
    apcu_store($masked, $records, DEFEND_AGE);
}

defend();
