<?php
/**
 * Last modified: 18.08.08 00:25:49
 * Hash: d770a88a80f8e9f5f74a2ea1aa0abf78a6f6e688
 */

//require 'defend.php';

define('ROOT', dirname(__DIR__));

$config = require ROOT . '/config/index.php';
$config['language'] = 'en';

require_once ROOT . '/vendor/yiisoft/yii2/Yii.php';
require_once ROOT . '/vendor/autoload.php';

Yii::setAlias('@site', ROOT);
Yii::$container->setDefinitions(optional('/config/class.php'));

$locales = ['en', 'ru'];
$locale = $_COOKIE['locale'] ?? null;
if (!$locale && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    preg_match('/(ru|uk|kk)/', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $m_locale);
    if ($m_locale) {
        $locale = 'ru';
    }
}
if (\in_array($locale, $locales, true)) {
    $config['language'] = $locale;
} else {
    $config['language'] = \count($locales) > 0 ? $locales[0] : 'en';
}

$app = new \yii\web\Application($config);
$app->run();
