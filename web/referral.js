document.addEventListener('DOMContentLoaded', function () {
  const cookieParam = 'sponsor'
  const office = '//cabinet.' + location.hostname
  var sponsor = /r=([\w_]+)/.exec(location.search)
  sponsor = sponsor ? sponsor[1] : null
  var cookieSponsor = new RegExp(cookieParam + '=([\\w_]+)').exec(document.cookie)
  cookieSponsor = cookieSponsor ? cookieSponsor[1] : null
  var isNew = !!sponsor
  if (cookieSponsor) {
    if (sponsor === cookieSponsor) {
      isNew = false
    }
    else if (!sponsor) {
      sponsor = cookieSponsor
    }
  }
  const callback = Date.now().toString(36)
  if (sponsor) {
    document.cookie = cookieParam + '=' + sponsor + '; path=/; max-age=315360000'
    if (isNew) {
      const script = document.createElement('script')
      script.src = office + '/new-visit/' + sponsor + '?callback=' + callback
      document.head.appendChild(script)
    }
    [].forEach.call(document.querySelectorAll('[href$="/sign-up"]'), function (a) {
      a.href = a.href.replace('/sign-up', '/ref/' + sponsor)
    });
    [].forEach.call(document.querySelectorAll('[href$="/login"]'), function (a) {
      a.href = a.href.replace('/login', '/login/' + sponsor)
    });
  }

  window[callback] = function () {
    // console.log('New visit', a)
    delete window[callback]
  }

  if (!/ai-logist\.com$/.test(location.hostname)) {
    document.querySelector('.ocm-effect-wrap').innerHTML = ''
  }
})
