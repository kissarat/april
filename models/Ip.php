<?php

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Ip
 * @package app\models
 * @property string $id [inet]
 * @property string $text [varchar(80)]
 * @property string $editor [integer]
 * @property string $ip [inet]
 * @property int $created [timestamp(0)]
 * @property int $time [timestamp(0)]
 */
class Ip extends ActiveRecord
{
    public static function can($ip)
    {
        $ip = static::findOne($ip);
        return $ip ? $ip->allow : false;
    }
}
