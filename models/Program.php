<?php

namespace app\models;


use app\helpers\SQL;
use app\helpers\Utils;
use DateTime;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Program
 * @property int $id
 * @property string $name
 * @property string $currency
 * @property float|int $minRated
 * @property float|int $maxRated
 * @property int $min [bigint]
 * @property int $max [bigint]
 * @property int $days [smallint]
 * @property  int $amountCents
 * @property int $income
 * @property float $percent
 * @property float|int $rate
 * @property Program[] $programs
 * @package app\models
 */
class Program extends ActiveRecord
{
    public $amount;
    public $currency = 'USD';
    public $ip;
    public static $list;

    public const DAY = 24 * 3600;
    public const MINUTE = 60;
    public const PERIOD = Program::MINUTE;
    public const DAYS = 365;

    public const SCENARIO_BUY = 'default';

    public function scenarios(): array
    {
        return [static::SCENARIO_BUY => ['id', 'amount', 'currency']];
    }

    public static function primaryKey(): array
    {
        return ['id'];
    }

    public const BONUSES = [
        1 => 0.07,
        2 => 0.04,
        3 => 0.02,
        4 => 0.01,
        5 => 0.01,
    ];

    public function rules(): array
    {
        return [
            [['id', 'currency', 'amount'], 'required'],
            ['amount', 'number', 'min' => $this->getMinRated(), 'max' => $this->getMaxRated()],
            ['currency', 'in', 'range' => Transfer::getCurrencies()],
            ['ip', 'ip']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'amount' => Yii::t('app', 'Amount'),
            'currency' => Yii::t('app', 'Currency'),
            'ip' => Yii::t('app', 'IP address'),
        ];
    }

    public function getRate()
    {
        return Transfer::getRate($this->currency) * Transfer::FACTOR['USD'];
    }

    public function getMinRated(): float
    {
        return $this->min / $this->getRate();
    }

    public function getMaxRated(): float
    {
        return $this->max / $this->getRate();
    }

    public function getIncome(): float
    {
        return $this->percent * $this->days;
    }

    public function getAmountCents(): float
    {
        return $this->amount * Transfer::FACTOR[$this->currency];
    }

    public static function difference(DateTime $t1, DateTime $t2)
    {
        // static::PERIOD === static::DAY
        return 'logist' === Yii::$app->id ? $t2->diff($t1)->days
            : floor(($t1->getTimestamp() - $t2->getTimestamp()) / static::PERIOD);
    }

    /**
     * @param int $amount
     * @param string $currency
     * @return Program
     */
    public static function getProgram(int $amount, string $currency = Transfer::BASE_CURRENCY)
    {
        $converted = Transfer::convert($amount, $currency);
        foreach (static::getProgramList() as $program) {
            if ($converted >= $program->min && $converted < $program->max) {
                return $program;
            }
        }
        return null;
    }

    /**
     * @param int $id
     * @return Program
     */
    public static function getProgramById(int $id): Program
    {
        return static::getProgramList()[$id];
    }

    public static function getBonuses(): array
    {
        return 'logist' === Yii::$app->id ? static::BONUSES : [
            1 => 0.05,
            2 => 0.02,
        ];
    }

    public static function getReward(int $program_id, int $root): array
    {
        return SQL::queryAll('select level, "user", percent from need_reward where "program" = :program and "root" = :root', [
            ':program' => $program_id,
            ':root' => $root
        ]);
    }

    /**
     * @param User $user
     * @return Transfer
     * @throws \Exception
     * @throws \Throwable
     */
    public function buy(User $user): Transfer
    {
        $amount = $this->getAmountCents();
        $node = new Transfer([
            'from' => $user->id,
            'currency' => $this->currency,
            'amount' => $amount,
            'type' => Transfer::TYPE_BUY,
            'status' => Transfer::STATUS_SUCCESS,
            'text' => 'Opening {name}',
            'node' => $this->id,
            'vars' => ['name' => $this->name]
        ]);

        \assert($node->insert(false));
        static::insertReward($node, $user);
        return $node;
    }

    /**
     * @param Transfer $node
     * @param User|null $user
     * @throws \Exception
     * @throws \Throwable
     */
    public static function insertReward(Transfer $node, ?User $user = null): void
    {
        if (null === $user) {
            $user = $node->fromObject;
        }
        $program = static::findOne($node->node);
        \assert($program);
        foreach (static::getReward($program->id, $user->id) as $reward) {
            /**
             * @var User $premium
             */
            $premium = User::findOne($reward['user']);
            if (null === $premium) {
                throw new \UnexpectedValueException('User does not exists');
            }
            $level = (int)$reward['level'];
            $t = new Transfer([
                'to' => (int)$premium->id,
                'currency' => $node->currency,
                'amount' => (int)floor($node->amount * $reward['percent']),
                'type' => Transfer::TYPE_BONUS,
                'status' => Transfer::STATUS_SUCCESS,
                'text' => 'Bonus at {level} level from {nick} for opening {name}',
                'node' => $node->id,
                'vars' => [
                    'level' => $level,
                    'nick' => $user->nick,
                    'name' => $program->name,
                ]
            ]);
            \assert($t->insert(false), 'Cannot insert');
            if ('logist' === Yii::$app->id && $premium->refback > 0 && 1 === $level) {
                $t = new Transfer([
                    'from' => $premium->id,
                    'to' => $user->id,
                    'currency' => $node->currency,
                    'amount' => (int)floor($node->amount * $premium->refback),
                    'type' => Transfer::TYPE_INTERNAL,
                    'status' => Transfer::STATUS_SUCCESS,
                    'node' => $node->id,
                    'vars' => [
                        'from_nick' => $premium->nick,
                        'to_nick' => $user->nick,
                        'name' => $program->name,
                        'percent' => $premium->refback
                    ]
                ]);
                \assert($t->insert(false), 'Cannot insert');
            }
        }
        $user->riseAncestors();
    }

    /**
     * @return int
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public static function accrue(): int
    {
        $t = Yii::$app->db->beginTransaction();
        /**
         * @var Transfer[] $nodes
         */
        $nodes = Transfer::find()
            ->where([
                'type' => Transfer::TYPE_BUY,
                'status' => Transfer::STATUS_SUCCESS
            ])
            ->select(['id', 'vars', 'created', 'from', 'currency', 'amount', 'node'])
            ->all();
        $time = new DateTime(Utils::timestamp());
        $j = 0;
        foreach ($nodes as $node) {
            $n = $node->getAccrueCount();
            $program = static::getProgramById($node->node);
            $passed = static::difference($time, $node->getCreatedDateTime());
            $passed = min($passed, $program->days);
            for ($i = $n + 1; $i <= $passed; $i++) {
                $transfer = new Transfer([
                    'to' => $node->from,
                    'currency' => $node->currency,
                    'amount' => $node->amount * $program->percent,
                    'type' => Transfer::TYPE_ACCRUE,
                    'status' => Transfer::STATUS_SUCCESS,
//                    'text' => 'Accrue {n} of {name}',
                    'n' => $i,
                    'node' => $node->id,
                    'vars' => ['name' => $program->name]
                ]);
                if (!$transfer->insert(false)) {
                    return -1;
                }
                $j++;
            }
        }
        $t->commit();
        return $j;
    }

    /**
     * @param string $scenario
     * @return Program[] &
     */
    public static function &getProgramList($scenario = self::SCENARIO_BUY): array
    {
        if (!static::$list) {
            static::$list = [];
            $programs = static::find()->orderBy(['id' => SORT_ASC])->all();
            foreach ($programs as $program) {
                $program->scenario = $scenario;
                static::$list[$program->id] = $program;
            }
        }
        return static::$list;
    }

    public static function getProgramDataList(): array
    {
        $list = [];
        foreach (static::getProgramList() as $program) {
            $list[$program->id] = $program->attributes;
        }
        return $list;
    }
}

//Program::$programs = Program::getProgramList();
