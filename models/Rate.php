<?php

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Rate
 * @package app\models
 * @property string $id [integer]
 * @property string $from [currency]
 * @property string $to [currency]
 * @property float $rate [double precision]
 * @property int $time [timestamp(0)]
 */
class Rate extends ActiveRecord
{

}
