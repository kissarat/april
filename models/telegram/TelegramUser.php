<?php

namespace app\models\telegram;


use Yii;
use yii\db\ActiveRecord;

class TelegramUser extends ActiveRecord
{
    public function send($text) {
        Yii::$app->telegram->send($this->id, $text);
    }
}
