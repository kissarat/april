<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: cb0c3ee1a61b6df02f16b33d4f51e70d239ebe3e
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Token
 * @property string $id
 * @property int $user
 * @property string $type
 * @property string $expires
 * @property array $data
 * @property string $ip
 * @property string $created
 * @property User $userObject
 * @property string $time
 * @property string $handshake
 * @property string $name
 * @package app\models
 */
class Token extends ActiveRecord {
    public const ID_LENGTH = 80;
    public const AGENT_LENGTH = 240;
    public const TYPES = ['server', 'browser', 'app', 'code'];

    public static function primaryKey() {
        return ['id'];
    }

    public function rules() {
        return [
            [['id', 'type'], 'string', 'max' => 240],
            [['id', 'type'], 'required']
        ];
    }

    public function getUserObject() {
        return $this->hasOne(User::class, ['id' => 'user']);
    }
}
