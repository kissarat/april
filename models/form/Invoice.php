<?php

namespace app\models\form;


use app\models\Transfer;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 *
 * @property \app\models\User $upFirstSystem
 */
class Invoice extends Model
{
    public $amount;
    public $system;
    public $currency;
    public $wallet;
    public $text;
    public $pin;
    public $ip;
    protected static $_minimal;

    public const TEXT_LENGTH = 240;
    public const ADVCASH_REGEX = '/^U\d{12}$/';
    public const BLOCKIO_REGEX = '/^\w{24,}$/';
    public const BTC_REGEX = '/^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/';
    public const DOGE_REGEX = '/^D[5-9A-HJ-NP-U][1-9A-HJ-NP-Za-km-z]{32}$/';
    public const ETH_REGEX = '/^0x[a-fA-F0-9]{40}$/';
    public const LTC_REGEX = '/^[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}$/';
    public const PAYEER_REGEX = '/^P\d+$/';
    public const PERFECT_REGEX = '/^U\d{7,8}$/';
    public const XRP_REGEX = '/^r[0-9a-zA-Z]{33}$/';
    public const ALL_REGEX = '/.*/';

    public const MIN = [
        'USD' => 1,
        'BTC' => 5000,
        'ETH' => 2000,
        'ETC' => 50000,
        'XRP' => 100,
        'LTC' => 10000,
        'DOGE' => 1000,
    ];

    public static function getMinimal($currency = null)
    {
        if (!static::$_minimal) {
            static::$_minimal = Yii::$app->storage->derive('minimal-');
        }
        if (null === $currency) {
            return static::$_minimal;
        }
        return static::$_minimal->get($currency, static::MIN[$currency]);
    }

    public const SCENARIO_PAYMENT = 'default';
    public const SCENARIO_WITHDRAW = 'withdraw';

    public function scenarios(): array
    {
        return [
            static::SCENARIO_PAYMENT => ['system', 'amount', 'currency', 'text', 'pin'],
            static::SCENARIO_WITHDRAW => ['system', 'amount', 'currency', 'wallet', 'pin', 'text']
        ];
    }

    public static function walletPattern($system, $currency): string
    {
        switch ($system) {
            case 'perfect':
                return static::PERFECT_REGEX;
            case 'advcash':
                return static::ADVCASH_REGEX;
            case 'payeer':
                return static::PAYEER_REGEX;
            case 'ethereum':
                return static::ETH_REGEX;
            case 'ripple':
                return static::XRP_REGEX;
            case 'interkassa':
                return static::ALL_REGEX;
            case 'blockio':
                if (YII_DEBUG) {
                    return static::BLOCKIO_REGEX;
                }
                switch ($currency) {
                    case 'BTC':
                        return static::BTC_REGEX;
                    case 'LTC':
                        return static::LTC_REGEX;
                    case 'DOGE':
                        return static::DOGE_REGEX;
                }
                break;
        }
        return null;
    }

    public function rules(): array
    {
        return [
            [['system', 'amount', 'currency', 'wallet', 'text'], 'filter', 'filter' => 'trim'],
            [['system', 'amount', 'currency', 'wallet', 'text'], 'filter', 'filter' => 'strip_tags'],
            [['system', 'amount', 'currency', 'wallet'], 'required'],
            ['text', 'string', 'max' => static::TEXT_LENGTH],
            ['system', 'in', 'range' => Transfer::SYSTEMS],
            ['currency', 'in', 'range' => Transfer::getCurrencies()],
            ['amount', 'number', 'min' => static::getMinimal($this->currency) / Transfer::FACTOR[$this->currency]],
            ['ip', 'ip']
        ];
    }

    public function validWallet(User $user)
    {
//        $pattern = static::walletPattern($this->system, $this->currency);
//        if ($pattern) {
//            $rules[] = ['wallet', 'match', 'pattern' => $pattern];
//        }
        if ('USD' === $this->currency) {
            $account = $user->getWallet($this->currency, $this->system);
            return empty($account['wallet']) ? true : $this->wallet === $account['wallet'];
        }
        return true;
    }

    public function attributeLabels()
    {
        return [
            'amount' => Yii::t('app', 'Amount'),
            'system' => Yii::t('app', 'Payment system'),
            'currency' => Yii::t('app', 'Currency'),
            'wallet' => Yii::t('app', 'Wallet'),
            'ip' => Yii::t('app', 'IP address'),
            'text' => Yii::t('app', 'Note'),
        ];
    }

    public function setupFirstSystem(User $user): void
    {
        if (empty($this->system) && !empty($this->currency)) {
            foreach ($user->getWallets() as $s) {
                if ($this->currency === $s['currency']) {
                    $this->system = $s['system'];
                    if (empty($this->wallet)) {
                        $this->wallet = $s['wallet'] ?? null;
                    }
                    break;
                }
            }
        }
    }
}
