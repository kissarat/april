<?php

namespace app\models\form;


use app\models\Log;
use app\models\User;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * @property string $login
 * @property string $password
 * @property User $user
 */
class Login extends Model
{
    public $login;
    public $password;
    public $ip;
    public $captcha;

//    public $remember;

    public function rules()
    {
        $rules = [
            [['login', 'password'], 'required'],
            ['login', 'string', 'min' => 4, 'max' => 48],
            ['password', 'string', 'min' => 1],
            ['ip', 'ip'],
//            ['remember', 'boolean'],
//            ['remember', 'default', 'value' => true],
        ];
        if (Yii::$app->settings->get('auth-recaptcha', 0) > 0) {
            $rules[] = ['captcha', ReCaptchaValidator::class,
                'secret' => Yii::$app->reCaptcha->secret,
                'uncheckedMessage' => Yii::t('app', 'Please confirm that you are not a bot')];
        }
        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'remember' => Yii::t('app', 'Remember Me'),
            'ip' => Yii::t('app', 'IP address'),
            'reCaptcha' => Yii::t('app', 'Not a robot'),
        ];
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return User::findOne(filter_var($this->login, FILTER_VALIDATE_EMAIL)
            ? ['email' => $this->login]
            : ['nick' => $this->login]
        );
//            ->select(['id', 'secret']);
    }

    public static function checkLoginAttempt($where)
    {
        $max = (int)Yii::$app->settings->get('auth-attempts', 0);
        if ($max > 0) {
            $count = Log::find()
                ->andWhere($where)
                ->andWhere(['>', 'id', Log::nanotime() - 15 * 60 * 1000000000])
                ->andWhere([
                    'entity' => 'auth',
                    'action' => 'invalid'
                ])
                ->count();
            if ($count > $max) {
                Yii::$app->session->addFlash('error',
                    Yii::t('app', 'You have exceeded the number of allowed login attempts. Please try again later.'));
                return false;
            }
        }
        return true;
    }
}
