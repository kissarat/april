<?php

namespace app\models\form;


use app\helpers\Utils;
use app\models\Program;
use app\models\Transfer;
use app\models\User;
use Faker\Factory;
use yii\base\Model;

class UserGenerator extends Model
{
    public $sponsor = 'monday';
    public $parent;
    public $count = 1;
    public $balance = 10;
    public $currency = 'USD';
    public $buy = false;

    public function rules() {
        return [
            [['sponsor', 'count'], 'required'],
            ['balance', 'number'],
//            ['currency', 'in', 'range' => Transfer::getCurrencies()]
//            ['created', 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    public function generate() {
        $factory = Factory::create();
        $parent = $this->parent > 0 ? $this->parent : User::findOne(['nick' => $this->sponsor])->id;
        $users = [];
        for ($i = 0; $i < $this->count; $i++) {
            $user = new User([
                'nick' => str_replace('.', '_', $factory->userName),
                'email' => $factory->email,
                'forename' => $factory->firstName,
                'surname' => $factory->lastName,
                'parent' => $parent,
                'type' => 'native',
                'password' => 'test',
                'created' => Utils::timestamp()
            ]);
            $user->generateSecret();
            $user->generateAuthKey();
            if ($user->insert(true)) {
                $users[$user->id] = $user;
            } else {
                $errors = $user->getErrors();
                throw new \Exception('ONE: ' . json_encode($errors) . json_encode($user->attributes));
//                foreach ($errors as $name => $error) {
//                    $errors[$name] = [
//                        'value' => $user->$name,
//                        'message' => $error
//                    ];
//                }
            }
        }
        $program = Program::getProgramById(1);
        $program->amount = $this->balance;
        $program->currency = $this->currency;
        foreach ($users as $id => $user) {
            if ($this->balance > 0 && $this->currency) {
                $amount = floor($this->balance * Transfer::FACTOR[$this->currency]);
                $transfer = new Transfer([
                    'to' => $id,
                    'type' => 'payment',
                    'status' => Transfer::STATUS_SUCCESS,
                    'amount' => $amount,
                    'currency' => $this->currency,
                    'created' => Utils::timestamp()
                ]);
                if ($transfer->insert(false)) {

                } else {
                    throw new \Exception('TWO');
                }
                if ($this->buy) {
                    $program->buy($user);
                }
            }
        }
        return $users;
    }
}
