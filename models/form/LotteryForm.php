<?php

namespace app\models\form;


use app\helpers\SQL;
use app\helpers\Utils;
use app\models\Log;
use app\models\Lottery;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Class LotteryForm
 * @property integer @amount
 * @property integer @quantity
 * @property string @currency
 * @package app\models\form
 */
class LotteryForm extends Model
{
    public $amount;
    public $quantity;
    public $currency;
    public $ip;

    public function rules()
    {
        return [
            [['amount', 'quantity', 'currency'], 'required'],
            [['amount', 'quantity'], 'integer', 'min' => 1],
            ['ip', 'ip']
        ];
    }

    public function attributeLabels()
    {
        return [
            'amount' => Yii::t('app', 'Number of tickets'),
            'quantity' => Yii::t('app', 'Number of lottery drawings'),
            'currency' => Yii::t('app', 'Currency'),
            'ip' => Yii::t('app', 'IP address'),
        ];
    }

    public function getSummary()
    {
        return $this->amount * $this->quantity;
    }

    public function getBaseAmount()
    {
        return $this->getSummary() * Transfer::FACTOR[Transfer::BASE_CURRENCY];
    }

    public function getCurrencyAmount()
    {
        return Transfer::convert($this->getBaseAmount(), $this->currency);
    }

    /**
     * @param User $user
     * @param bool $withdraw
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function participate(User $user, $withdraw = true)
    {
        if ($withdraw) {
            Log::log('lottery', 'participate', null, $user->id);
        }
        $created = Utils::timestamp();
        $amount = $this->getCurrencyAmount();
        $t = null;
        if ($withdraw) {
            $t = new Transfer([
                'from' => $user->id,
                'currency' => $this->currency,
                'amount' => $amount,
                'type' => Transfer::TYPE_LOTTERY,
                'status' => Transfer::STATUS_SUCCESS,
                'created' => $created,
                'vars' => [
                    'amount' => $this->amount,
                    'quantity' => $this->quantity,
                ]
            ]);
            \assert($t->insert(false), 'Cannot pay for ticket');
            \assert($t->id > 0,'Transfer id is null');
        }
        $count = Lottery::getFirstNumber();
        for ($i = 1; $i <= $this->quantity; $i++) {
            $number = $count + $i;
            $lottery = Lottery::getLottery($user->id, $number);
//            header('A')
            if ($lottery) {
                $lottery->amount += $this->amount;
                $lottery->time = $created;
            } else {
                $lottery = new Lottery([
                    'user' => $user->id,
                    'number' => $number,
                    'amount' => $this->amount,
                    'created' => $created
                ]);
            }
            \assert($lottery->save(true),'Cannot save ticket');
            \assert($lottery->id > 0,'Lottery id is null');
            if ($withdraw) {
                SQL::execute('insert into lottery_transfer(lottery, transfer, "time") values (:lottery, :transfer, :time)', [
                    ':lottery' => $lottery->id,
                    ':transfer' => $t->id,
                    ':time' => $created
                ]);
            }
        }
        $user->riseAncestors();
        return true;
    }
}
