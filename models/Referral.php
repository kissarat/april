<?php
/**
 * Last modified: 18.07.22 03:06:15
 * Hash: 6013797e5f2f992acb8802801dc6649075743396
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * Class Referral
 * @property string $nick
 * @property string $created
 * @property string $skype
 * @property string $surname
 * @property string $forename
 * @property integer $parent
 * @property integer $children
 * @property string $id [integer]
 * @property int $buy [bigint]
 * @property int $usd [numeric]
 * @property int $btc [numeric]
 * @property int $eth [numeric]
 * @property int $etc [numeric]
 * @property int $ltc [numeric]
 * @property int $doge [numeric]
 * @property int $xrp [numeric]
 * @package app\models
 */
class Referral extends ActiveRecord
{
    public function attributeLabels(): array
    {
        return [
            'avatar_file' => Yii::t('app', 'Avatar'),
            'country' => Yii::t('app', 'Country'),
            'email' => Yii::t('app', 'Email'),
            'forename' => Yii::t('app', 'First Name'),
            'nick' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'phone' => Yii::t('app', 'Phone'),
            'skype' => Yii::t('app', 'Skype'),
            'sponsor' => Yii::t('app', 'Sponsor'),
            'surname' => Yii::t('app', 'Last Name'),
            'telegram' => Yii::t('app', 'Telegram'),
            'site' => Yii::t('app', 'Site'),
            'perfect' => Yii::t('app', 'Perfect Money'),
            'advcash' => Yii::t('app', 'AdvCash'),
            'payeer' => Yii::t('app', 'Payeer'),
            'btc' => Yii::t('app', 'Bitcoin'),
            'eth' => Yii::t('app', 'Ethereum'),
            'etc' => Yii::t('app', 'Ethereum Classic'),
            'xrp' => Yii::t('app', 'Ripple'),
            'ltc' => Yii::t('app', 'Litecoin'),
            'doge' => Yii::t('app', 'Doge'),
            'children' => Yii::t('app', 'Referrals'),
            'ip' => Yii::t('app', 'IP address'),
            'created' => Yii::t('app', 'Registered'),
        ];
    }
}
