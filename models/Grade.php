<?php
/**
 * Last modified: 18.05.24 16:35:56
 * Hash: f5a32498a0b9e99d0e027a5beeff2435a6521961
 */


namespace app\models;


use Yii;
use yii\db\ActiveRecord;

class Grade extends ActiveRecord
{
    public const CACHE_DURATION = 600;

    public const USER = 0;
    public const PARTNER = 1;

    public const DEFAULT = [
        'id' => Grade::USER,
        'name' => 'User',
        'turnover' => 0,
        'personal' => 0,
        'lottery' => 0,
        'premium' => 0
    ];

    public static function getDefaultGrade(): array {
        $grade = static::DEFAULT;
        $grade['name'] = Yii::t('app', $grade['name']);
        return $grade;
    }

    public const LIST = [
        [
            'id' => Grade::PARTNER,
            'name' => 'Partner',
            'turnover' => 0,
            'personal' => 1000,
            'lottery' => 0,
            'premium' => 0
        ],
        [
            'id' => 2,
            'name' => 'Trade Manager',
            'turnover' => 200000,
            'personal' => 10000,
            'lottery' => 10,
            'premium' => 5000
        ],
        [
            'id' => 3,
            'name' => 'Senior Trade Manager',
            'turnover' => 400000,
            'personal' => 30000,
            'lottery' => 30,
            'premium' => 10000
        ],
        [
            'id' => 4,
            'name' => 'Regional Manager',
            'turnover' => 10 * 100000,
            'personal' => 50000,
            'lottery' => 50,
            'premium' => 25000
        ],
        [
            'id' => 5,
            'name' => 'Field Force Manager',
            'turnover' => 30 * 100000,
            'personal' => 100000,
            'lottery' => 100,
            'premium' => 100000
        ],
        [
            'id' => 6,
            'name' => 'Product Manager',
            'turnover' => 50 * 100000,
            'personal' => 150000,
            'lottery' => 150,
            'premium' => 250000
        ],
        [
            'id' => 7,
            'name' => 'Commercial Manager',
            'turnover' => 100 * 100000,
            'personal' => 250000,
            'lottery' => 250,
            'premium' => 400000
        ],
        [
            'id' => 8,
            'name' => 'Chief Commercial Manager',
            'turnover' => 200 * 100000,
            'personal' => 350000,
            'lottery' => 350,
            'premium' => 800000
        ],
        [
            'id' => 9,
            'name' => 'Representative',
            'turnover' => 500 * 100000,
            'personal' => 500000,
            'lottery' => 500,
            'premium' => 20 * 100000
        ],
        [
            'id' => 10,
            'name' => 'International Commercial Manager',
            'turnover' => 1000 * 100000,
            'personal' => 10 * 100000,
            'lottery' => 1000,
            'premium' => 40 * 100000
        ],
        [
            'id' => 11,
            'name' => 'Director',
            'turnover' => 3 * 1000 * 100000,
            'personal' => 25 * 100000,
            'lottery' => 2500,
            'premium' => 100 * 100000
        ],
        [
            'id' => 12,
            'name' => 'Vise President',
            'turnover' => 6 * 1000 * 100000,
            'personal' => 40 * 100000,
            'lottery' => 4000,
            'premium' => 150 * 100000
        ],
        [
            'id' => 13,
            'name' => 'President',
            'turnover' => 10 * 1000 * 100000,
            'personal' => 75 * 100000,
            'lottery' => 7500,
            'premium' => 300 * 100000
        ],
    ];

    public static function getList(): array
    {
        return static::getDb()->cache(function ($db) {
            $grades = [];
            /**
             * @var Grade $model
             */
            $all = Grade::find()
                ->orderBy(['id' => SORT_ASC])
                ->all();
            foreach ($all as $model) {
                $o = $model->getAttributes();
                unset($o['created'], $o['time']);
                $grades[] = $o;
            }
            return $grades;
        }, static::CACHE_DURATION);
    }
}
