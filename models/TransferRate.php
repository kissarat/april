<?php
/**
 * Last modified: 18.05.24 00:00:46
 * Hash: f03f4b54753c2c46a19ff0fdfc81056159e50d0b
 */


namespace app\models;


use yii\db\ActiveRecord;

class TransferRate extends ActiveRecord
{
    public static function tableName()
    {
        return 'transfer_rate';
    }

    public static function sumNodes(int $user_id)
    {
        return static::find()->where([
            'from' => $user_id,
            'type' => Transfer::TYPE_BUY,
            'status' => Transfer::STATUS_SUCCESS
        ])
            ->sum('usd') ?: 0;
    }
}
