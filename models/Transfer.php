<?php
/**
 * Last modified: 18.11.13 23:10:37
 * Hash: e1cf725e72b6ca63223a3a2807e05086d0cf9bfe
 */

namespace app\models;


use app\helpers\SQL;
use app\helpers\Utils;
use app\models\form\Invoice;
use DateTime;
use Yii;
use yii\db\ActiveRecord;
use yii\web\Application;

/**
 * Class Transfer
 * @property array $vars
 * @property bool $visible [boolean]
 * @property int $n
 * @property int $space [smallint]
 * @property integer $amount
 * @property integer $from
 * @property integer $id
 * @property integer $node
 * @property integer $to
 * @property string $created
 * @property string $currency
 * @property string $guid [char(8)]
 * @property string $ip
 * @property string $status
 * @property string $system
 * @property string $text
 * @property string $time
 * @property string $txid
 * @property string $type
 * @property string $wallet
 * @property Transfer nodeObject
 * @property User $fromObject
 * @property User $toObject
 * @package app\models
 */
class Transfer extends ActiveRecord
{
    protected $count = -1;
    protected static $currencies;
    protected static $systems;

    public const TEXT_LENGTH = 160;
    public const WALLET_LENGTH = 80;
    public const TXID_LENGTH = 160;

    public const BASE_CURRENCY = 'USD';
    public const RATE_PERCENT = 0.9;
    public const RATE_VARIATION = 1.01;

    public const TYPES = [
        'internal', 'payment', 'withdraw', 'accrue', 'buy', 'earn', 'support',
        'write-off', 'bonus', 'loan', 'lottery', 'win', 'premium'
    ];
    public const SYSTEMS = ['internal', 'payeer', 'perfect', 'advcash', 'blockio', 'ethereum', 'ripple', 'interkassa'];

    public const SYSTEM_CURRENCIES = [
        ['perfect', 'Perfect Money', 'USD', false],
        ['payeer', 'Payeer', 'USD', false],
        ['advcash', 'AdvCash', 'USD', false],
        ['blockio', 'Bitcoin', 'BTC', true],
        ['ethereum', 'Ethereum', 'ETH', true],
        ['ethereum', 'Ethereum Classic', 'ETC', true],
        ['ripple', 'Ripple', 'XRP', true],
        ['blockio', 'Litecoin', 'LTC', true],
        ['blockio', 'Dogecoin', 'DOGE', true],
        ['interkassa', 'Interkassa', 'USD', false]
    ];

    public const CURRENCIES = CONSOLE
        ? ['USD', 'BTC', 'ETH', 'ETC', 'LTC', 'DOGE', 'XRP']
        : ['USD', 'BTC', 'ETH', 'ETC', 'LTC', 'DOGE'];
    public const WALLETS = ['btc', 'eth', 'etc', 'xrp', 'ltc', 'doge'];

    public const TYPE_ACCRUE = 'accrue';
    public const TYPE_BONUS = 'bonus';
    public const TYPE_BUY = 'buy';
    public const TYPE_LOTTERY = 'lottery';
    public const TYPE_PAYMENT = 'payment';
    public const TYPE_PREMIUM = 'premium';
    public const TYPE_WIN = 'win';
    public const TYPE_WITHDRAW = 'withdraw';
    public const TYPE_INTERNAL = 'internal';
    public const TYPE_INTERKASSA = 'interkassa';

    public const STATUS_CREATED = 'created';
    public const STATUS_SUCCESS = 'success';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_PENDING = 'pending';
    public const STATUS_FAIL = 'fail';

    public const STATUSES = [
        Transfer::STATUS_CREATED,
        Transfer::STATUS_SUCCESS,
        Transfer::STATUS_PENDING,
        'fail', 'canceled', 'virtual'
    ];

    public const RATE_EXPIRES = 600;
    public const RATE_CACHE_KEY = 'rates-igho6ohdahTh';
    public const BALANCE_EXPIRES = 2;
    public const BALANCE_CACHE_KEY = 'balance-eiJ4ooxu';
    public const TURNOVER_EXPIRES = 2;
    public const TURNOVER_CACHE_KEY = 'turnover-ithoopa7AiX7ropa';
    public const HISTOGRAM_EXPIRES = 2;
    public const HISTOGRAM_CACHE_KEY = 'history-aeF9ee';
    public const SETTINGS_CURRENCIES = 'currencies';

    public const FACTOR = [
        'USD' => 100,
        'BTC' => 1000000,
        'ETH' => 100000,
        'ETC' => 100000,
        'XRP' => 100,
        'LTC' => 100000,
        'DOGE' => 10,
    ];

    public const DEFAULT_RATE = [
        'BTC' => 10000,
        'ETH' => 600,
        'ETC' => 20,
        'XRP' => 0.5,
        'LTC' => 100,
        'DOGE' => 0.002,
    ];

    public const LEVELS = [
        1,
        0.4,
        0.25,
        0.15,
        0.05
    ];

    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', 'Operation'),
            'created' => Yii::t('app', 'Time'),
            'amount' => Yii::t('app', 'Amount'),
            'type' => Yii::t('app', 'Type'),
            'text' => Yii::t('app', 'Note'),
            'user' => Yii::t('app', 'User'),
        ];
    }

    public static function &getSystems()
    {
        if (!static::$systems) {
            $list = [];
            $currencies = static::getCurrencies();
            $systems = Yii::$app->settings->getList('systems');
            foreach (static::SYSTEM_CURRENCIES as $s) {
                if (\in_array($s[0], $systems, true) && \in_array($s[2], $currencies, true)) {
                    $o = [
                        'system' => $s[0],
                        'name' => $s[1],
                        'currency' => $s[2],
                        'blockchain' => $s[3]
                    ];
                    $o['min'] = Invoice::getMinimal($o['currency']);
                    $id = $o['blockchain'] ? strtolower($o['currency']) : $o['system'];
                    $list[$id] = $o;
                }
            }
            static::$systems = $list;
        }
        return static::$systems;
    }

    public static function &getCurrencies()
    {
        if (!static::$currencies) {
            static::$currencies = Yii::$app->settings->getList(static::SETTINGS_CURRENCIES);
        }
        return static::$currencies;
    }

    public static function isCurrencyEnabled($currency): bool
    {
        return \in_array($currency, static::getCurrencies(), true);
    }

    public static function isSystemEnabled($system, $currency = null): bool
    {
        return isset(static::getSystems()[$system]) && ($currency ? static::isCurrencyEnabled($currency) : true);
    }

    public static function &getCurrenciesDictionary(): array
    {
        $currencies = [];
        foreach (static::getCurrencies() as $currency) {
            $currencies[$currency] = $currency;
        }
        return $currencies;
    }

    public static function convert(int $amount, string $currency = Transfer::BASE_CURRENCY)
    {
        if (self::BASE_CURRENCY === $currency) {
            return $amount;
        }
        $factor = self::FACTOR[$currency] / self::FACTOR[self::BASE_CURRENCY];
        return round($amount * $factor / self::getRate($currency));
    }

    public static function getRate($currency)
    {
        $rates = static::getExchangeRate();
        return $rates[$currency];
    }

    public static function fetchExchangeRate()
    {
        $data = Utils::loadJSON('https://api.coinmarketcap.com/v1/ticker/?limit=50');
        $rates = [];
        foreach ($data as $row) {
            $currency = $row['symbol'];
            if (\in_array($currency, static::getCurrencies(), false)) {
                $rates[$currency] = $row['price_usd'];
            }
        }
        return $rates;
    }

    public static function queryExchangeRate($percent = self::RATE_PERCENT)
    {
        $rates = [];
        foreach (SQL::queryAll('select "from", "to", "rate" from exchange where "to" = \'USD\'') as $rate) {
            $rates[$rate['from']] = 'USD' === $rate['from'] ? 1 : $rate['rate'] * $percent;
        }
        return $rates;
    }

    public static function getExchangeRate($fresh = false)
    {
        $rates = Yii::$app->cache->get(static::RATE_CACHE_KEY);
        if ($fresh || empty($rates)) {
            if (YII_DEBUG) {
                $rates['time'] = Utils::timestamp();
            }
            if ($fresh) {
                try {
//                    $rates = self::queryExchangeRate(1);
                    foreach (static::fetchExchangeRate() as $currency => $rate) {
//                        $old = $rates[$currency] ?? 0;
//                        echo $old . ' ' . ($rate * static::RATE_VARIATION) . "\n";
//                        if (0 === $old || !($rate >= $old / static::RATE_VARIATION && $rate <= $old * static::RATE_VARIATION)) {
                        $from = new Rate([
                            'from' => $currency,
                            'to' => 'USD',
                            'rate' => $rate,
                            'time' => Utils::timestamp()
                        ]);
                        if (!$from->insert(false)) {
//                            throw new \Exception(json_encode($from->getErrors()));
                            break;
                        }
                    }
//                    }
                } catch (\Exception $exception) {

                }
            }
            $rates = static::queryExchangeRate();
            $rates['USD'] = 1;
            Yii::$app->cache->set(static::RATE_CACHE_KEY, $rates, static::RATE_EXPIRES);
        }
        return $rates;
    }

    public static function invert(int $amount, string $currency = Transfer::BASE_CURRENCY)
    {
        if (self::BASE_CURRENCY === $currency) {
            return $amount;
        }
        return round($amount * self::getRate($currency) / self::FACTOR[$currency]);
    }

    public static function countNodes(int $user)
    {
        return static::find()->where(['from' => $user, 'type' => self::TYPE_BUY])
            ->count('*');
    }

    public static function getTurnover(int $user_id, $fresh = true)
    {
        $turnover = Yii::$app->cache->get(static::TURNOVER_CACHE_KEY);
        if (YII_DEBUG || $fresh || !empty($turnover)) {
            $turnover = 0;
            foreach (static::getLevels($user_id) as $level) {
                $turnover += $level['amount'] * self::LEVELS[$level['level']];
            }
            $turnover = floor($turnover);
            Yii::$app->cache->set(static::TURNOVER_CACHE_KEY, $turnover, static::TURNOVER_EXPIRES);
        }
        return (int)$turnover;
    }

    public static function getLevels(int $user_id): array
    {
        return SQL::queryAll('WITH RECURSIVE b(id, parent, level, amount) AS (
              WITH a AS (
                  SELECT
                    u.id,
                    u.parent,
                    coalesce(sum(t.usd), 0) AS amount
                  FROM "user" u
                    LEFT JOIN transfer_rate t ON t."from" = u.id AND t.type = \'buy\'
                  GROUP BY u.id, u.parent
              )
              SELECT
                id,
                parent,
                0 AS level,
                amount
              FROM a
              WHERE parent = :root
              UNION
              SELECT
                a.id,
                a.parent,
                b.level + 1 AS level,
                a.amount
              FROM b
                JOIN a ON a.parent = b.id
              WHERE b.level < 4
            )
            SELECT
              level,
              sum(amount) AS amount
            FROM b
            GROUP BY level ORDER BY level
        ', [':root' => $user_id]);
    }

    public static function getAllBalances(int $user): array
    {
        $balances = [];
        foreach (static::getCurrencies() as $currency) {
            $balances[$currency] = static::getBalance($user, $currency);
        }
        return $balances;
    }

    public static function getBalance(int $user, string $currency = Transfer::BASE_CURRENCY, $fresh = false)
    {
        $key = static::BALANCE_CACHE_KEY . $currency . '-' . $user;
        $balance = Yii::$app->cache->get($key);
        if ($fresh || YII_DEBUG || null === $balance) {
            $balance = static::getInput($user, $currency) - static::getOutput($user, $currency);
            Yii::$app->cache->set($key, $balance, static::BALANCE_EXPIRES);
        }
        return $balance;
    }

    public static function getInput($user, $currency): int
    {
        return static::find()
            ->where(['to' => $user, 'currency' => $currency, 'status' => static::STATUS_SUCCESS])
            ->sum('amount') ?: 0;
    }

    public static function getOutput($user, $currency): int
    {
        return static::find()
            ->where(['from' => $user, 'currency' => $currency, 'status' => static::STATUS_SUCCESS])
            ->sum('amount') ?: 0;
    }

    public static function getHistogram(int $user, $currency, $month = null)
    {
        if (empty($month)) {
            $month = +date('m');
        }
        $key = static::HISTOGRAM_CACHE_KEY . $currency . '-' . $user;
        $histogram = Yii::$app->cache->get($key);
        if (YII_DEBUG || !$histogram) {
            $series = SQL::queryAll('select created::date as date, sum(amount)::bigint as amount from transfer
            where type in (\'bonus\', \'accrue\', \'win\')
            and "to" = :user
            and date_part(\'month\', created) = :month
            and "currency" = :currency group by created::date', [
                ':user' => $user,
                ':month' => $month,
                ':currency' => $currency,
            ], \PDO::FETCH_NUM);
            $expense = +SQL::scalar('select sum(amount) from transfer
            where type in (\'buy\', \'lottery\')
            and "from" = :user
            and date_part(\'month\', created) = :month
            and "currency" = :currency', [
                ':user' => $user,
                ':month' => $month,
                ':currency' => $currency
            ]) ?: 0;
            $histogram = [
                'expense' => $expense,
                'series' => $series
            ];
            Yii::$app->cache->set($key, $histogram, self::HISTOGRAM_EXPIRES);
        }
        return $histogram;
    }

    public static function getInformer(int $user_id, $currency): array
    {
        return [
            'buy' => self::getSummary(['currency' => $currency, 'type' => self::TYPE_BUY, 'from' => $user_id]),
            'accrue' => self::getSummary(['currency' => $currency, 'type' => self::TYPE_ACCRUE, 'to' => $user_id]),
            'bonus' => self::getSummary(['currency' => $currency, 'type' => self::TYPE_BONUS, 'to' => $user_id]),
            'withdraw' => self::getSummary(['currency' => $currency, 'type' => self::TYPE_WITHDRAW, 'from' => $user_id])
        ];
    }

    public static function getSummary($where): int
    {
        $where['status'] = static::STATUS_SUCCESS;
        return floor(static::find()
            ->where($where)
            ->sum('amount')) ?: 0;
    }

    public function init()
    {
        if (Yii::$app instanceof Application) {
            $this->on(static::EVENT_BEFORE_INSERT, [$this, 'beforeInsert']);
        }
        parent::init();
    }

    public function beforeInsert(): void
    {
        if (empty($this->ip)) {
            $this->ip = Yii::$app->request->getUserIP();
        }
    }

    public function getNodeObject()
    {
        return $this->hasOne(static::class, ['node' => 'id']);
    }

    public function getAccrueCount(): int
    {
        if ($this->count < 0) {
            /**
             * @var Transfer $accrue
             */
            $n = static::find()
                ->where([
                    'node' => $this->id,
                    'type' => static::TYPE_ACCRUE,
                ])
                ->max('n');
            $this->count = $n ?: 0;
        }
        return $this->count;
    }

    public function getProgramObject()
    {
        return Program::getProgramById($this->node);
    }

    public function getClose()
    {
        return str_replace('2018', '2019', $this->created);
    }

    public function getCreatedDateTime()
    {
        return new DateTime($this->created);
    }

    public function getProgress()
    {
        return $this->getAccrueCount() / $this->getProgramObject()->days;
    }

    public function getIncome(): int
    {
        return Transfer::find()
            ->where([
                'to' => $this->from,
                'node' => $this->id,
                'type' => static::TYPE_ACCRUE,
                'status' => static::STATUS_SUCCESS
            ])
            ->sum('amount') ?: 0;
    }

    public function getProgramIncome()
    {
        return $this->getProgramObject()->getIncome() * $this->amount;
    }

    public function getFromObject()
    {
        return $this->hasOne(User::class, ['id' => 'from']);
    }

    public function getToObject()
    {
        return $this->hasOne(User::class, ['id' => 'to']);
    }
}
