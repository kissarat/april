<?php

namespace app\models;


use app\helpers\Utils;
use yii\db\ActiveRecord;

/**
 * Class Email
 * @property int $to
 * @property string $subject
 * @property string $text
 * @property bool $sent
 * @package app\models
 */
class Email extends ActiveRecord
{
    public static function tableName()
    {
        return 'email';
    }

    /**
     * @param $to
     * @param $subject
     * @param $text
     * @return null|static
     */
    public static function send($to, $subject, $text)
    {
        $email = new static([
            'to' => $to,
            'subject' => $subject,
            'text' => $text,
        ]);
        if ($email->save()) {
            Utils::mail($to, $subject, $text);
            $email->sent = Utils::timestamp();
            if ($email->save(false)) {
                return $email;
            }
        }
        return null;
    }
}
