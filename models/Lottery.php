<?php

namespace app\models;


use app\helpers\SQL;
use yii\db\ActiveRecord;

/**
 * Class Lottery
 * @property integer $id
 * @property integer $number
 * @property integer $amount
 * @property integer $user
 * @property string $win [integer]
 * @property int $created [timestamp(0)]
 * @property int $time [timestamp(0)]
 * @property int $space [smallint]
 * @package app\models
 */
class Lottery extends ActiveRecord
{
    public const PERCENTS = [
        1 => 0.5,
        2 => 0.2,
        3 => 0.1
    ];

    public const BONUSES = [
        1 => 0.05,
        2 => 0.03,
        3 => 0.02
    ];

    /**
     * @param int $number
     * @return float|int
     */
    public static function getPrizeFund(int $number = -1)
    {
        return static::getFund($number) * Transfer::FACTOR[Transfer::BASE_CURRENCY] * static::getPrizeFundPercent();
    }

    public static function getFund(int $number = -1)
    {
        return static::find()
            ->where(['number' => $number > 0 ? $number : static::getFirstNumber() + 1])
            ->sum('amount');
    }

    public static function getPrizeFundPercent()
    {
        return array_sum(static::PERCENTS);
    }

    public static function getFirstLottery(int $user_id)
    {
        $count = static::getFirstNumber();
        return static::getLottery($user_id, $count + 1);
    }

    /**
     * @param int $user_id
     * @param int $number
     * @return Lottery
     */
    public static function getLottery(int $user_id, int $number): ?Lottery
    {
        return static::find()
            ->where([
                'user' => $user_id,
                'number' => $number
            ])
            ->one();
    }

    public static function getLastTickets(int $user_id): int
    {
        $number = static::getLastNumber($user_id);
        if ($number > 0) {
            return static::getLottery($user_id, $number)->amount;
        }
        return 0;
    }

    public static function getLastNumber(int $user_id): int
    {
        return static::find()
            ->where(['user' => $user_id])
            ->max('number') ?: 0;
    }

    /**
     * @return int
     */
    public static function getFirstNumber(): int
    {
        return SQL::scalar("select max(number) from lottery l join transfer t on l.id = t.node and t.type = 'win'") ?: 0;
    }

    public static function getWinners(int $number): array
    {
        return SQL::queryAll('select l.n, l.nick, l.avatar, l.win, p.nick as sponsor, p.avatar as sponsor_avatar, t.amount as sponsor_win from admin_lottery l
          join "user" u on l."user" = u.id
          join transfer t on l.id = t.node and t.type = \'bonus\' and t.status = \'success\'
          join "user" p on u.parent = p.id
          where l.win > 0 and l.number = :number order by l.n limit 3', [
            ':number' => $number
        ]);
    }

    public static function getParticipants(int $number): array
    {
        return SQL::queryAll('select nick, amount, avatar
          from lottery l join "user" u on l."user" = u.id where number = :number order by amount desc, l.id desc', [
            ':number' => $number
        ]);
    }

    public static function countTickets(int $user_id): int
    {
        return static::find()
            ->where(['user' => $user_id])
            ->sum('amount') ?: 0;
    }
}
