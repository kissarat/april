<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * Class Log
 * @package app\models
 * @property int $id [bigint]
 * @property string $entity [varchar(16)]
 * @property string $action [varchar(32)]
 * @property string $user [integer]
 * @property string $token [varchar(80)]
 * @property string $data [json]
 * @property string $ip [inet]
 */
class Log extends ActiveRecord
{
    public static function log($entity, $action, $data = null, $user = null, $ip = null)
    {
        $id = static::nanotime();
        if (!empty($data) && empty($data['ip'])) {
            unset($data['ip']);
        }
        if (empty($data)) {
            $data = null;
        }
        $u = Yii::$app->user;
        $log = new Log([
            'id' => $id,
            'entity' => $entity,
            'action' => $action,
            'data' => $data,
            'token' => Yii::$app->request->getToken(),
            'user' => $user ?: ($u->getIsGuest() ? null : $u->identity->id),
            'ip' => $ip ?: Yii::$app->request->getUserIP(),
        ]);
        if ($log->save(false)) {
            return $log;
        }
        return false;
    }

    public static function nanotime()
    {
        return (int)floor(microtime(true) * 10000) * 100000;
    }

    public static function lastTime()
    {
        return (time() - 3600) * 1000 * 1000 * 1000;
    }
}
