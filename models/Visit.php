<?php

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Visit
 * @package app\models
 * @property string $id [integer]
 * @property string $token [varchar(80)]
 * @property string $url [varchar(120)]
 * @property int $spend [bigint]
 * @property string $ip [inet]
 * @property int $created [timestamp(0)]
 * @property string $agent [varchar(240)]
 * @property string $data [json]
 */
class Visit extends ActiveRecord
{
    public const URL_LENGTH = 120;

    public static function getFollows(User $user)
    {
        return static::find()
            ->where(['like', 'url', '/ref/' . $user->nick])
            ->count('*');
    }
}
