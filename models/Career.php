<?php

namespace app\models;


use app\models\form\LotteryForm;
use Yii;
use yii\base\Model;

class Career extends Model
{
    public static function getGrade(int $id)
    {
        foreach (Grade::getList() as $grade) {
            if ($id === $grade['id']) {
                return $grade;
            }
        }
        return null;
    }

    public static function getGradeByOrder($order)
    {
        return SORT_DESC === $order ? array_reverse(Grade::getList()) : Grade::getList();
    }

    public static function countUserGrade(int $user_id)
    {
        return Transfer::find()
            ->where(['type' => Transfer::TYPE_PREMIUM, 'to' => $user_id, 'status' => Transfer::STATUS_SUCCESS])
            ->max('node') ?: Grade::USER;
    }

    public static function getUserGrade(int $user_id)
    {
        $id = static::countUserGrade($user_id);
        if ($id > Grade::USER) {
            return static::getGrade($id);
        }
        return Grade::DEFAULT;
    }

    public static function getGradeByParam($name, $value)
    {
        foreach (static::getGradeByOrder(SORT_DESC) as $grade) {
//            echo $grade['id'] . " $name $value\n";
            if ($value >= $grade[$name]) {
                return $grade['id'];
            }
        }
        return Grade::USER;
    }

    public static function calculate(int $user_id)
    {
        return min(
            static::getGradeByParam('personal', TransferRate::sumNodes($user_id)),
            static::getGradeByParam('lottery', Lottery::countTickets($user_id)),
            static::getGradeByParam('turnover', Transfer::getTurnover($user_id, true))
        );
    }

    public static function rise(int $user_id, int $new = Grade::USER)
    {
        $actual = static::countUserGrade($user_id);
        $new = $new > Grade::USER ? $new : static::calculate($user_id);
        if ($new > $actual) {
            for ($i = $actual + 1; $i <= $new; $i++) {
                $grade = static::getGrade($i);
                $t = new Transfer([
                    'to' => $user_id,
                    'currency' => Transfer::BASE_CURRENCY,
                    'amount' => $grade['premium'],
                    'type' => Transfer::TYPE_PREMIUM,
                    'status' => Transfer::STATUS_SUCCESS,
                    'node' => $i
                ]);
                if (!$t->insert(false)) {
                    throw new \Exception();
                }
            }
            if (Grade::PARTNER === $new && Grade::USER === $actual) {
                $lottery = new LotteryForm([
                    'quantity' => 1,
                    'amount' => 1,
                    'currency' => Transfer::BASE_CURRENCY,
                ]);
                if ($lottery->participate(User::findOne($user_id), false) && Yii::$app->has('session')) {
                    Yii::$app->session->addFlash('success',
                        Yii::t('app', 'You have a bonus lottery ticket as a new partner'));
                }
            }
        }
    }
}
