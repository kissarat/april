<?php
/**
 * Last modified: 18.08.11 12:04:37
 * Hash: 4477068c00cb090e019a49bba900a91bd69e24db
 */

namespace app\models;


use app\helpers\SQL;
use app\helpers\Utils;
use app\models\form\Invoice;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Class User
 * @property \app\models\Transfer[] $nodes
 * @property array $informers
 * @property boolean $admin
 * @property boolean $agreement
 * @property float $refback [double precision]
 * @property int $created [timestamp(0)]
 * @property int $space [smallint]
 * @property int $time [timestamp(0)]
 * @property integer $id
 * @property integer $parent
 * @property mixed $authKey
 * @property mixed $wallets
 * @property string $advcash [varchar(128)]
 * @property string $avatar [varchar(48)]
 * @property string $btc [varchar(128)]
 * @property string $doge [varchar(128)]
 * @property string $email
 * @property string $etc [varchar(128)]
 * @property string $eth [varchar(128)]
 * @property string $forename
 * @property string $ltc [varchar(128)]
 * @property string $name
 * @property string $nick
 * @property string $password
 * @property string $payeer [varchar(128)]
 * @property string $perfect [varchar(128)]
 * @property string $phone
 * @property string $pin [char(4)]
 * @property string $reflink
 * @property string $secret
 * @property string $site [varchar(160)]
 * @property string $skype
 * @property string $surname
 * @property string $telegram [varchar(32)]
 * @property string $type
 * @property string $xrp [varchar(128)]
 * @property User $parentObject
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface
{
//    use BroadcastModelTrait;
    public const TYPES = ['new', 'native', 'leader', 'special'];
    public const ROOT_NICK = 'logist';
    public const INFORMERS_KEY = 'informers-Tai0oc0h-';
    public const INFORMERS_EXPIRES = 10;
    public const DEPTH = 5;
    public const SCENARIO_REGISTER = 'register';
    public const SCENARIO_WALLET = 'wallet';
    public const SCENARIO_PROFILE = 'default';

    public const REGEX_NICK = '/^[a-z][a-z0-9_]+$/i';
    public const REGEX_SKYPE = '/^[a-z][a-z0-9\.,\-_]{5,31}$/i';
    public const REGEX_TELEGRAM = '/^@?[a-z0-9_]+$/i';
    public const REGEX_PHONE = '/^\+?[ 0-9()\-]+$/i';
    public const REGEX_PIN = '/^\d{4}$/i';

    public const NICK_LENGTH = 24;
    public const NAME_LENGTH = 48;
    public const EMAIL_LENGTH = 48;
    public const PASSWORD_LENGTH = 100;

    public $password;
    public $sponsor;
    public $level;
    public $ip;
    public $avatar_file;
    public $children;
    public $usd;
    public $captcha;
    public $_wallets;

    public static function tableName()
    {
        return 'user';
    }

    /**
     * @param string $id
     * @return User
     */
    public static function findIdentity($id)
    {
        $user = parent::findOne(['id' => $id]);
        return $user && $user->secret ? $user : null;
    }

    public static function getIdByNick($nick)
    {
        return static::find()
            ->where(['nick' => $nick])
            ->select(['id'])
            ->one()
            ->id;
    }

    public static function findByNick(string $nick)
    {
        $cacheKey = 'user-nick-' . $nick;
        $params = Yii::$app->cache->get($cacheKey);
        if (empty($params)) {
            $user = User::findOne(['nick' => $nick]);
            if ($user) {
                Yii::$app->cache->set($cacheKey, $user->attributes);
            }
        } else {
            $user = new User($params);
        }
        return $user;
    }

    public function scenarios()
    {
        return [
            static::SCENARIO_PROFILE => ['email', 'forename', 'surname', 'phone', 'skype', 'site', 'telegram', 'avatar_file', 'refback', 'pin'],
            static::SCENARIO_WALLET => ['perfect', 'advcash', 'payeer', 'btc', 'eth', 'etc', 'xrp', 'ltc', 'doge'],
            static::SCENARIO_REGISTER => ['nick', 'email', 'parent', 'sponsor', 'password', 'reCaptcha'],
        ];
    }

    public function rules()
    {
        $rules = [
            ['id', 'integer'],
            [['nick', 'email'], 'required'],
            [['surname', 'forename'], 'string', 'min' => 2, 'max' => static::NAME_LENGTH],
            ['password', 'string', 'min' => 6, 'max' => static::PASSWORD_LENGTH],
            ['nick', 'string', 'min' => 4, 'max' => static::NICK_LENGTH],
            ['email', 'string', 'min' => 4, 'max' => static::EMAIL_LENGTH],
            ['nick', 'match', 'pattern' => static::REGEX_NICK],
            ['phone', 'match', 'pattern' => static::REGEX_PHONE],
            ['skype', 'match', 'pattern' => static::REGEX_SKYPE],
            ['telegram', 'match', 'pattern' => static::REGEX_TELEGRAM],
            ['perfect', 'match', 'pattern' => Invoice::PERFECT_REGEX],
            ['payeer', 'match', 'pattern' => Invoice::PAYEER_REGEX],
            ['advcash', 'match', 'pattern' => Invoice::ADVCASH_REGEX],
            ['email', 'email'],
            ['ip', 'ip'],
            ['type', 'in', 'range' => static::TYPES],
            ['telegram', 'string'],
            [['nick', 'email', 'skype', 'sponsor', 'phone', 'avatar', 'telegram', 'site',
                'perfect', 'advcash', 'payeer', 'btc', 'eth', 'etc', 'xrp', 'ltc', 'doge'], 'filter', 'filter' => function ($s) {
                return strip_tags(trim($s ?: '')) ?: null;
            }],
            [['phone', 'forename', 'surname'], 'filter', 'filter' => function ($s) {
                return strip_tags(trim(preg_replace('/ {2,}/', ' ', $s ?: ''))) ?: null;
            }],
            ['telegram', 'filter', 'filter' => function ($s) {
                return empty($s) ? null : preg_replace('/^@/', '', trim($s));
            }],
            [['nick', 'email'], 'unique',
                'message' => Yii::t('app', 'This value has already been taken'),
                'on' => static::SCENARIO_REGISTER],
            ['sponsor', 'exist',
                'targetAttribute' => 'nick',
                'message' => Yii::t('app', 'Sponsor not found')],

        ];
        if ('logist' === Yii::$app->id) {
            $rules[] = ['refback', 'number', 'min' => 0.0, 'max' => 0.7];
        } else {
            $rules[] = ['pin', 'match', 'pattern' => static::REGEX_PIN,
                'message' => Yii::t('app', 'Password for transactions must be four digits')];
        }
        if (Yii::$app->settings->get('auth-recaptcha', 0) > 0) {
            $rules[] = ['captcha', ReCaptchaValidator::class,
                'secret' => Yii::$app->reCaptcha->secret,
                'uncheckedMessage' => Yii::t('app', 'Please confirm that you are not a bot.')];
        }
        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'avatar_file' => Yii::t('app', 'Avatar'),
            'country' => Yii::t('app', 'Country'),
            'email' => Yii::t('app', 'Email'),
            'forename' => Yii::t('app', 'First Name'),
            'nick' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'phone' => Yii::t('app', 'Phone'),
            'skype' => Yii::t('app', 'Skype'),
            'sponsor' => Yii::t('app', 'Sponsor'),
            'surname' => Yii::t('app', 'Last Name'),
            'telegram' => Yii::t('app', 'Telegram'),
            'site' => Yii::t('app', 'Site'),
            'perfect' => Yii::t('app', 'Perfect Money'),
            'advcash' => Yii::t('app', 'AdvCash'),
            'payeer' => Yii::t('app', 'Payeer'),
            'btc' => Yii::t('app', 'Bitcoin'),
            'eth' => Yii::t('app', 'Ethereum'),
            'etc' => Yii::t('app', 'Ethereum Classic'),
            'xrp' => Yii::t('app', 'Ripple'),
            'ltc' => Yii::t('app', 'Litecoin'),
            'doge' => Yii::t('app', 'Doge'),
            'pin' => Yii::t('app', 'Transaction password'),
            'ip' => Yii::t('app', 'IP address'),
        ];
    }

    public function validatePassword(string $password)
    {
        return password_verify($password, $this->secret);
    }

    public function generateAuthKey()
    {
        return $this->generateToken('browser', Yii::$app->security->generateRandomString(36));
    }

    public function generateToken(string $type, $id)
    {
        if (Yii::$app->db->createCommand()->insert('token', [
            'id' => $id,
            'type' => $type,
            'user' => $this->id,
        ])
            ->execute()
        ) {
            return $id;
        }
        return false;
    }

    public function generateCode()
    {
        return $this->generateToken('code', Yii::$app->security->generateRandomString(48));
    }

    public function generateSecret()
    {
        $this->secret = Utils::generateSecret($this->password);
    }

    public function isAdmin()
    {
        return $this->admin;
    }

    public function __toString()
    {
        return $this->nick;
    }

    public function sendEmail($template, $params)
    {
        $template = Yii::getAlias('@app') . "/mail/$template.php";
        return Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom(['zbyszek@yopmail.com' => 'admin'])
            ->setSubject($params['subject'])
            ->setHtmlBody(Yii::$app->view->renderFile($template, $params))
            ->send();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        // TODO: what is auth key
        throw new \Exception('getAuthKey not implemented');
//        return $this->auth;
    }

    public function validateAuthKey($authKey)
    {
        // TODO: what is auth key
        throw new \Exception('validateAuthKey not implemented ' . $authKey);
//        return $authKey === $this->auth;
    }

    public function getParentObject()
    {
        return $this->hasOne(static::class, ['id' => 'parent']);
    }

    public function getInformers()
    {
        $key = static::INFORMERS_KEY . $this->id;
        $informers = Yii::$app->cache->get($key);
        if (empty($informers) || YII_DEBUG) {
            $informers = [
                'follows' => Visit::getFollows($this),
                'children' => $this->countChildren(),
                'nodes' => Transfer::countNodes($this->id),
                'turnover' => floor(Transfer::getTurnover($this->id))
            ];
            Yii::$app->cache->set($key, $informers, static::INFORMERS_EXPIRES);
        }
        return $informers;
    }

    public function countChildren()
    {
        return $this->getChildren()->count('*');
    }

    public function getChildren()
    {
        return $this->hasMany(static::class, ['parent' => 'id']);
    }

    public function checkBalance(int $expected, $currency = Transfer::BASE_CURRENCY)
    {
        $actual = Transfer::getBalance($this->id, $currency);
        return $actual >= $expected;
    }

    /**
     * @return Transfer[]
     */
    public function getNodes()
    {
        return Transfer::findAll([
            'from' => $this->id,
            'type' => Transfer::TYPE_BUY,
            'status' => Transfer::STATUS_SUCCESS
        ]);
    }

    public function &getWallets()
    {
        if (!$this->_wallets) {
            $systems = Transfer::getSystems();
            foreach ($systems as $id => $system) {
                if (!empty($this->$id)) {
                    $systems[$id]['wallet'] = $this->$id;
                }
                $systems[$id]['pattern'] = substr(Invoice::walletPattern($system['system'], $system['currency']), 1, -1);
            }
            $this->_wallets = $systems;
        }
        return $this->_wallets;
    }

    public function getWallet($currency, $system)
    {
        $id = 'USD' === $currency ? $system : strtolower($currency);
        return $this->$id;
    }

    public function canChange($name)
    {
        return empty($this->getOldAttribute($name)) || $this->isAttributeChanged($name);
    }

    public function getName()
    {
        return empty($this->forename) && empty($this->surname) ? $this->nick : trim($this->forename . ' ' . $this->surname);
    }

    public function getAncestors($includeSelf, $levels = 5)
    {
        if ($includeSelf) {
            yield $this;
        }
        $parent = $this;
        for ($i = 0; $i < $levels; $i++) {
            $parent = static::find()
                ->where(['id' => $parent->parent])
                ->select(['id', 'parent', 'nick'])
                ->one();
            if (!$parent) {
                break;
            }
            yield $parent;
        }
    }

    public function checkStructure($id, $level = 5)
    {
        if ($this->id === $id) {
            return true;
        }
        foreach ($this->getAncestors(false, $level) as $parent) {
            if ($parent->id === $id) {
                return true;
            }
        }
        return false;
    }

    public function getReflink()
    {
        return Yii::$app->params['referralLinkOrigin'] . '/~' . $this->nick;
    }

    public function riseAncestors()
    {
        if ('logist' === Yii::$app->id) {
            foreach ($this->getAncestors(true) as $ancestor) {
                Career::rise($ancestor->id);
            }
        }
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \Exception('Not implemented findIdentityByAccessToken');
    }

    public function hasInvestments()
    {
        return SQL::scalar("select count(*) from transfer where type = 'buy' and status = 'success' and \"from\" = :user limit 1", [
                ':user' => $this->id
            ]) > 0;
    }
}
