<?php

namespace app\base;


use app\helpers\Utils;
use yii\i18n\PhpMessageSource;

class JsonMessageSource extends PhpMessageSource
{
    public function getMessagesMorozov($language) {
        return static::loadMessages(null, $language);
    }

    protected function loadMessages($category, $language) {
        if ('en' === $language) {
            return [];
        }
        else {
            return Utils::getLocale($language);
        }
    }
}
