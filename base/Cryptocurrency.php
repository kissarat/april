<?php
/**
 * Last modified: 18.05.17 00:20:54
 * Hash: 7f8e13fd882019052c0cf9db89d1e0e8f03cea1c
 */


namespace app\base;


interface Cryptocurrency extends PaymentSystem
{
    public function getCurrency(): string;

    public function getLast(): int;

    public function getPaymentQueue(): Storage;

    public function getNetworkName(): string;

    public function getLastUpdateTime(): string;
}
