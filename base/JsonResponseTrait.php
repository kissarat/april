<?php

namespace app\base;

use Yii;
use yii\web\Response;

trait JsonResponseTrait {
    public function afterAction($action, $result) {
        if (Response::FORMAT_JSON === Yii::$app->response->format && !empty($this->view->params['breadcrumbs'])) {
            $result['breadcrumbs'] = $this->view->params['breadcrumbs'];
        }
        return parent::afterAction($action, $result);
    }
}
