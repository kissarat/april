<?php

namespace app\base;


use yii\db\Query;

class JSONQuery extends Query
{
    public static function jsonQuery(Query $q, array $params)
    {
        $o = [];
        $where = [];
        foreach ($params as $k => $v) {
            if ('_' === $k[0]) {
                $o[$k] = $v;
            } else {
                $where[$k] = $v;
            }
        }
        if (!$q->from && isset($o['_from'])) {
            $q->from($o['_from']);
        }
        if (!empty($where)) {
            $q->where($where);
        }
        return $q;
    }

    public function fromJson(array $params): self
    {
        return static::jsonQuery($this, $params);
    }

    public static function createFromJson($params): self
    {
        return (new static())
            ->fromJson($params);
    }
}
