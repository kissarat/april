<?php
/**
 * Last modified: 18.11.12 21:56:24
 * Hash: 110713786b9cf62d7f77ad4f118890864067beac
 */

namespace app\base;


use app\helpers\Utils;
use app\models\Transfer;
use Yii;
use yii\base\Exception;

trait GatewayTrait
{
    use LockTrait;
    public $lockTimeout = 20 * 60;
    public $wallet;
    protected $_storage;
    public $_paymentQueue;

    public function getCurrency() : string
    {
        return strtoupper($this->wallet);
    }

    public function getFactor()
    {
        return Transfer::FACTOR[$this->getCurrency()];
    }

    /**
     * @return Storage
     */
    public function getStorage(): Storage
    {
        if (!$this->_storage) {
            $this->_storage = Yii::$app->storage->derive($this->wallet . '-');
        }
        return $this->_storage;
    }

    public function getPaymentQueue(): Storage
    {
        if (!$this->_paymentQueue) {
            $this->_paymentQueue = $this->getStorage()->derive('payment');
        }
        return $this->_paymentQueue;
    }

    public function getLast() : int
    {
        return (int) $this->getStorage()->get('last', $this->getDefaultLast());
    }

    public function setLast($value) : void
    {
        $this->getStorage()->set((int) $value, 'last');
    }

    public function isCurrencyEnabled($checkEnvironment = true) : bool {
        if ($checkEnvironment && 'on' === getenv('CURRENCY_ENABLE')) {
            return true;
        }
        return Transfer::isCurrencyEnabled($this->getCurrency());
    }

    public function getSummary()
    {
        return [
            'name' => $this->getNetworkName(),
//            'last' => $this->getStorage()->get('last'),
            'last' => $this->getStorage()->get('last') ?: null,
//            'updated' => Utils::timestamp($this->getLastUpdateTime()),
            'payments' => $this->getPaymentQueue()->iterate()
        ];
    }
}
