<?php

namespace app\base;


use yii\base\BaseObject;

class JSONClient extends BaseObject
{
    public $origin = '';

    public function request(string $method, string $uri = null, array $params = null, array $content = null) {
        $options = [
            'method' => $method,
            'header' => 'content-type: application/json',
        ];
        $url = $this->origin;
        if ($uri) {
            $url .= $uri;
        }
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }
        if (!empty($content)) {
            $options['content'] = \json_encode($content, JSON_UNESCAPED_UNICODE);
        }
        $m = null;
        \preg_match('/^([^:]+):/', $url, $m);
        if ($m) {
            $protocol = $m[1];
        }
        else {
            throw new \Exception('Invalid protocol');
        }
        $data = file_get_contents($url, false, stream_context_create([
            $protocol => $options
        ]));
        return \json_decode($data, JSON_OBJECT_AS_ARRAY);
    }

    public function get(string $uri = null, array $params = null) {
        return $this->request('GET', $uri, $params);
    }
}
