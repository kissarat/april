<?php
/**
 * Last modified: 18.08.07 23:14:06
 * Hash: 4aa37984c07697d5cd574fc8937c4ba47083a9f4
 */

namespace app\base;

use app\helpers\Utils;
use app\models\Transfer;
use Yii;

function isPrivateNetwork($ip)
{
    $masks = [
        167772160 => 4278190080,
        1681915904 => 4290772992,
        2886729728 => 4293918720,
        3232235520 => 4294901760
    ];
    $ip = ip2long($ip);
    foreach ($masks as $network => $mask) {
        if (+$network === ($ip & $mask)) {
            return true;
        }
    }
    return false;
}

class Request extends \yii\web\Request
{
    public $mirror = false;

    public function getUserIP()
    {
        foreach (['HTTP_CF_CONNECTING_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_IP', 'HTTP_XIP', 'REMOTE_ADDR'] as $name) {
            if (isset($_SERVER[$name]) && '127.0.0.1' !== $_SERVER[$name] && !isPrivateNetwork($_SERVER[$name])) {
                return $_SERVER[$name];
            }
        }
        return parent::getUserIP();
    }

    public function getCookie($key, $default = null)
    {
        return strip_tags($_COOKIE[$key] ?? $default);
    }

    public function getCurrency()
    {
        return $this->getCookie('currency', Transfer::BASE_CURRENCY);
    }

    public function getIsAjax() {
        return parent::getIsAjax()
            || 0 === strpos($_SERVER['HTTP_ACCEPT'], 'application/json')
            || (isset($_GET['format']) && 'json' === $_GET['format']);
    }

    public function getToken()
    {
        return $this->getCookie(ini_get('session.name'));
    }

    public function save($runValidation = false) : bool {
        $request = new \app\models\Request([
            'time' => Utils::timestamp(),
            'ip' => $this->getUserIP(),
            'url' => $this->getUrl(),
            'method' => $this->getMethod(),
            'type' => $this->getContentType(),
            'agent' => $this->getUserAgent(),
            'data' => $this->getRawBody(),
            'token' => $this->getToken(),
            'user' => Yii::$app->user->getId()
        ]);
        return $request->insert($runValidation);
    }
}
