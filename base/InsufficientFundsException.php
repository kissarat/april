<?php

namespace app\base;


use app\models\Transfer;
use yii\base\Exception;

class InsufficientFundsException extends Exception
{
    public $actual;
    public $expected;
    public $currency;

    public function getNeed() {
        return $this->expected - $this->actual;
    }

    public function __construct(int $actual, int $expected, $currency = Transfer::BASE_CURRENCY) {
        $this->actual = $actual;
        $this->expected = $expected;
        $this->currency = $currency;
        parent::__construct("Insufficient Funds $actual expected $expected", 402);
    }
}
