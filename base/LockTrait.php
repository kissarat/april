<?php

namespace app\base;


trait LockTrait
{
    /**
     * @param bool $bool
     * @throws \Exception
     */
    public function lock($bool = true)
    {
        $s = $this->getStorage();
        $time = $s->get('lock');
        if ($bool && $time > 0) {
            throw new \Exception(get_class($this) . ' locked');
        }
        $s->set(($bool ? 1 : -1) * time(), 'lock', $this->lockTimeout);
    }
}
