<?php
/**
 * Last modified: 18.11.12 21:45:07
 * Hash: f33563c0916856f07c83e1b108e1ae6ee4689bf4
 */

namespace app\console\controllers;


use app\base\ConsoleGatewayTrait;
use app\components\Parity;
use Yii;

/**
 *
 * @property \app\components\Parity $component
 */
class EthereumController extends Controller
{
    use ConsoleGatewayTrait;

    /**
     * @return Parity
     * @throws \InvalidArgumentException
     * @throws \yii\base\InvalidConfigException
     */
    protected function getComponent(): Parity
    {
        $system = getenv('system');
        if (empty($system)) {
            throw new \InvalidArgumentException('No "system" environment variable');
        }
        return Yii::$app->get($system);
    }

    /**
     * @param bool $all
     * @throws \yii\base\InvalidConfigException
     */
    public function actionList(bool $all = false)
    {
        $c = $this->getComponent();
        $accounts = [];
        foreach ($c->getAccountList() as $account) {
            $balance = hexdec($c->getBalance($account));
            if ($all || $balance > 0) {
                $accounts[$account] = number_format($balance / Parity::WEI, 8, '.', '');
            }
        }
        arsort($accounts);
        echo json_encode($accounts, JSON_PRETTY_PRINT) . "\n";
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionNumber()
    {
        echo $this->getComponent()->getBlockNumber() . "\n";
    }

    /**
     * @param string $number
     * @throws \yii\base\InvalidConfigException
     */
    public function actionBlock(string $number = 'latest')
    {
        $com = $this->getComponent();
        if (strpos($number, '0x') === 0) {
            $number = dechex($number);
        } elseif (is_numeric($number)) {
            $number = +$number;
        } elseif ('latest' === $number) {
            $number = $com->getBlockNumber();
        }
        echo json_encode($com->getBlock($number), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSummary() {
        $com = $this->getComponent();
        echo json_encode($com->getSummary(), JSON_PRETTY_PRINT);
    }
}
