<?php

namespace app\console\controllers;


use app\models\Ip;

class IpController extends Controller
{
    static $modelClass = Ip::class;

    public function actionAllow(string $id, string $text = null) {
        $ip = new Ip([
            'id' => $id,
            'text' => $text,
        ]);
        if (!$ip->save()) {
            echo "Cannot insert\n";
        }
    }

    public function actionDeny(string $id) {
        $count = Ip::deleteAll(['id' => $id]);
        if (1 !== $count) {
            echo "IP $id not found: $count\n";
        }
    }
}
