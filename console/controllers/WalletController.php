<?php

namespace app\console\controllers;


use app\models\Transfer;
use app\models\User;
use Yii;

class WalletController extends Controller
{
    public static $modelClass = User::class;
    public static function tableName() : string {
        return 'user';
    }

    public function actionBalance($wallet = 'btc') {
        echo Yii::$app->$wallet->getBalance() . "\n";
    }

    public function actionUser(string $nick, $wallet = 'btc') {
        $user = $this->getUser($nick);
        echo Yii::$app->$wallet->getUserAddress($user->id) . "\n";
    }

    public static function actionClearUsers() {
        foreach (User::find()->all() as $user) {
            foreach (Transfer::WALLETS as $wallet) {
                $user->$wallet = null;
            }
            $user->update(false);
        }
    }
}
