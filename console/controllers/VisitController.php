<?php

namespace app\console\controllers;


use app\models\Visit;

class VisitController extends Controller
{
    public static $modelClass = Visit::class;
    public static function tableName() : string {
        return 'visit';
    }
}
