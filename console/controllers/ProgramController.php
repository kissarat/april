<?php

namespace app\console\controllers;


use app\helpers\Utils;
use app\models\Program;
use app\models\Transfer;
use app\models\User;
use DateTime;
use function GuzzleHttp\json_encode;
use Yii;
use yii\db\Transaction;

class ProgramController extends Controller
{
    public static $modelClass = Transfer::class;

    public static function tableName(): string
    {
        return 'transfer';
    }

    /**
     * @param string $nick
     * @param string $currency
     * @throws \Exception
     */
    public function actionBalance(string $nick, $currency = 'USD'): void
    {
        $user = $this->getUser($nick);
        $balance = Transfer::getBalance($user->id, $currency);
        echo $balance / Transfer::FACTOR[$currency];
        echo "\n";
    }

    /**
     * @param string $nick
     * @param int $amount
     * @param string $currency
     * @param string $type
     * @throws \Exception
     */
    public function actionPay(string $nick, $amount = 1, $currency = 'USD', $type = 'payment'): void
    {
        $user = $this->getUser($nick);
        $transfer = new Transfer([
            'to' => $user->id,
            'currency' => $currency,
            'amount' => $amount * Transfer::FACTOR[$currency],
            'type' => $type,
            'status' => Transfer::STATUS_SUCCESS,
            'vars' => ['console' => true]
        ]);
        $transfer->save(false);
    }

    public function actionConvert(int $amount = 1, $currency = 'BTC'): void
    {
        echo Transfer::convert($amount * Transfer::FACTOR[Transfer::BASE_CURRENCY], $currency)
            / Transfer::FACTOR[$currency];
        echo "\n";
    }

    /**
     * @param string $nick
     * @param float $amount
     * @param int $program_id
     * @param string $currency
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionBuy(string $nick, float $amount, int $program_id, string $currency = Transfer::BASE_CURRENCY): void
    {
        $user = $this->getUser($nick);
        $program = Program::getProgramById($program_id);
        if ($program) {
            $program->amount = $amount;
            $program->currency = $currency;
            if ($program->validate()) {
                $transfer = $program->buy($user);
                if ($transfer) {
                    echo $this->echoJSON($transfer->attributes);
                } else {
                    die("Error\n");
                }
            }
            else {
                $this->echoJSON($program->getErrors());
            }
        }
        else {
            die("Program not found\n");
        }
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionAccrue(): void
    {
        echo Program::accrue();
        echo "\n";
    }

    /**
     * @param int $id
     */
    public function actionCount(int $id)
    {
        $transfer = Transfer::findOne($id);
        echo $transfer->getAccrueCount();
        echo "\n";
    }

    public function actionDiff($t1, $t2): void
    {
        echo Program::difference(new DateTime($t1), new DateTime($t2));
        echo "\n";
    }

    /**
     * @param int $n
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionGenerate(int $n = 100): void
    {
        $users = User::find()->select(['id'])->all();
        for ($i = 0; $i < $n; $i++) {
            $node = Program::buy(Utils::random($users),
                random_int(1000, 10000000), Utils::random(Transfer::getCurrencies()));
            if ($node) {
                $max = random_int(0, floor(Program::DAYS / 4));
                $program = $node->getProgramObject();
                for ($day = 0; $day < $max; $day++) {
                    $transfer = new Transfer([
                        'to' => $node->from,
                        'currency' => $node->currency,
                        'amount' => $node->amount * $program->percent,
                        'type' => Transfer::TYPE_ACCRUE,
                        'status' => Transfer::STATUS_SUCCESS,
                        'text' => 'Accrue {n} of {name}',
                        'n' => $day,
                        'node' => $node->id,
                        'vars' => ['n' => $day, 'name' => $program->name]
                    ]);
                    $transfer->insert(false);
                }
            }
        }
    }

    public function actionGet(int $id, $currency = 'USD') {
        $program = Program::getProgramById($id);
        $program->currency = $currency;
        if ($program) {
            $this->echoJSON([
                'min' => $program->getMinRated(),
                'max' => $program->getMaxRated()
            ]);
        }
        else {
            die("Program not found\n");
        }
    }
    
    /**
     * @param int $node_id
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionReward(int $node_id) {
        $t = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
        $node = Transfer::findOne($node_id);
        \assert($node);
        Program::insertReward($node);
        $t->commit();
    }
}
