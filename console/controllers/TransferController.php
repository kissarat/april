<?php
/**
 * Last modified: 18.05.23 23:27:37
 * Hash: c6de6535e6fe374256652b44622e4d15385dad51
 */

namespace app\console\controllers;


use app\helpers\Utils;
use app\models\Transfer;
use app\models\User;

class TransferController extends Controller
{
    public static $modelClass = Transfer::class;

    public static function tableName(): string
    {
        return 'transfer';
    }

    public function actionBalance(string $nick, $currency = Transfer::BASE_CURRENCY)
    {
        $user = $this->getUser($nick);
        $balance = Transfer::getBalance($user->id, $currency);
        echo $balance / Transfer::FACTOR[$currency];
        echo "\n";
    }

    public function actionPay(string $nick, $amount = 10, $currency = Transfer::BASE_CURRENCY, $type = 'payment')
    {
        $user = $this->getUser($nick);
        $transfer = new Transfer([
            'to' => $user->id,
            'currency' => $currency,
            'amount' => $amount * Transfer::FACTOR[$currency],
            'type' => $type,
            'status' => Transfer::STATUS_SUCCESS,
        ]);
        $transfer->save(false);
    }

    public function actionConvert(int $amount = 1, $currency = 'BTC')
    {
        echo Transfer::convert($amount * Transfer::FACTOR[Transfer::BASE_CURRENCY], $currency)
            / Transfer::FACTOR[$currency];
        echo "\n";
    }

    public function actionInvert(int $amount = 1, $currency = Transfer::BASE_CURRENCY)
    {
        echo Transfer::invert($amount * Transfer::FACTOR[$currency], $currency) / Transfer::FACTOR[$currency];
        echo "\n";
    }

    public function actionUpdateRate() {
        Transfer::getExchangeRate(true);
    }

    public function actionRate()
    {
        $this->echoJSON(Transfer::getExchangeRate());
    }

    public function actionGenerate(int $n = 1000, string $nick, string $types_string = 'payment')
    {
        $to = User::findOne(['nick' => $nick]);
        $users = User::find()->select(['id'])->all();
        $types = explode(',', $types_string);
        for ($i = 0; $i < $n; $i++) {
            $currency = Utils::random(Transfer::getCurrencies());
            $id = $to ? $to->id : +Utils::random($users)->id;
            $type = Utils::random($types);
            $isWithdraw = 'withdraw' === $type;
            $transfer = new Transfer([
                'from' => $isWithdraw ? $id : null,
                'to' => $isWithdraw ? null : $id,
                'currency' => $currency,
                'amount' => random_int(1, 10000000),
                'type' => $type,
                'status' => $isWithdraw ? Transfer::STATUS_CREATED : Transfer::STATUS_SUCCESS,
                'vars' => ['test' => true],
                'ip' => '127.0.0.99',
                'created' => Utils::timestamp(time() - $i * 3600 * random_int(1, 3))
            ]);
            if (!$transfer->insert(false)) {
                return;
            }
        }
    }

    public function actionSummary(string $nick, $currency = Transfer::BASE_CURRENCY)
    {
        $user = $this->getUser($nick);
        foreach (Transfer::getInformer($user->id, $currency) as $key => $value) {
            $this->dump($key, $value);
        }
    }

    public function actionLevels($nick) {
        $user = $this->getUser($nick);
        foreach (Transfer::getLevels($user->id) as $level) {
            echo $level['level'] . ' ' . ($level['amount'] * Transfer::LEVELS[$level['level']]) . "\n";
        }
    }
}
