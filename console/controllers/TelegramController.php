<?php

namespace app\console\controllers;


use app\helpers\Utils;
use app\models\telegram\TelegramMessage;
use app\models\telegram\TelegramUser;
use Yii;

class TelegramController extends Controller
{
    public function actionGet($method = 'getUpdates') {
        $data = Yii::$app->telegram->getUpdates($method);
        $uodate_id = TelegramMessage::find()->max('id') ?: 0;
        foreach ($data['result'] as $update) {
            if ($update['update_id'] > $uodate_id) {
                $m = $update['message'];
                $from = $m['from'];
                $message = new TelegramMessage([
                    'id' => $update['update_id'],
                    'message_id' => $m['message_id'],
                    'from' => $from['id'],
                    'text' => $m['text'],
                    'time' => Utils::timestamp($m['date'])
                ]);
                $message->insert(false);
                $user = TelegramUser::findOne($from['id']);
                if (!$user) {
                    $user = new TelegramUser(['id' => $from['id']]);
                }
                $user->nick = $from['username'];
                $user->surname = $from['first_name'];
                $user->forename = $from['last_name'];
                $user->locale = $from['language_code'];
                $user->bot = $from['is_bot'];
                $user->save(false);
                $this->handleMessage($user, $message);
            }
        }
    }

    protected function handleMessage($user, $message) {
        $text = preg_split('/\s+/', $message['text']);
        $command = $text[0];
        $resp = [];
        switch ($command) {
            case 'history':
                $history = TelegramMessage::find()->orderBy(['id' => SORT_DESC])->limit(10)->all();
                $resp = [];
                foreach ($history as $m) {
                    $resp[] = $m['time'] . ' ' . $m['text'];
                }
//            case 'balance':

        }
        if (!empty($resp)) {
            $user->send(implode("\n", $resp));
        }
    }
}
