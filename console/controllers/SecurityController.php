<?php
/**
 * Last modified: 18.11.12 22:08:47
 * Hash: 1ede8b947385f66d6c3d0d969240cb3c6ad6a5bd
 */


namespace app\console\controllers;


use app\helpers\Utils;
use Yii;

class SecurityController extends Controller
{
    protected function loadJSON($url)
    {
        try {
            return Utils::loadJSON($url);
        } catch (\Exception $ex) {
            $m = null;
            echo $ex->getMessage();
            preg_match('/https?:\/\/([^\/]+)/', $url, $m);
            throw new \Exception('Cannot open: ' . ($m ? $m[1] : ''));
        }
    }

    public function actionCheck($url)
    {
        $networks = [
            'BTC' => ['name' => 'BTC'],
            'LTC' => ['name' => 'LTC'],
            'DOGE' => ['name' => 'DOGE'],
            'ETH' => ['name' => 'foundation'],
            'ETC' => ['name' => 'ethereum classic'],
        ];
        try {
            $r = $this->loadJSON($url);
            $this->validateByArray('OFFICE', $r, [
                'success' => true,
                'charset' => 'utf-8',
                'timezone' => 'UTC',
            ]);
            foreach ($r['networks'] as $currency => $network) {
                if (isset($network['error']) && \in_array($currency, $r['currencies'], true)) {
                    throw new \UnexpectedValueException($currency . ': ' . $network['error']);
                }
//                $this->validateByArray($currency, $network, $networks[$currency]);
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage() . "\n";
            $this->report('https://cabinet.ai-logist.com ' . $ex->getMessage());
        }
    }

    public function actionWordpress($url)
    {
        try {
            $r = $this->loadJSON($url);
            $this->validateByArray('WORDPRESS', $r, [
//                'success' => true,
//                'charset' => 'latin1',
//                'timezone' => 'UTC',
            ]);
        } catch (\Exception $ex) {
            echo $ex->getMessage() . "\n";
            $this->report('https://ai-logist.com ' . $ex->getMessage());
        }
    }

    public function actionId()
    {
        echo Yii::$app->id . "\n";
    }

    public function actionUnlock()
    {
        Yii::$app->get('eth')->lock(false);
    }
}
