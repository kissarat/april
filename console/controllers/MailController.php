<?php

namespace app\console\controllers;


use app\helpers\Utils;

class MailController extends Controller
{
    public function actionSend($email, $subject, $content) {
        Utils::mail($email, $subject, $content);
    }
}
