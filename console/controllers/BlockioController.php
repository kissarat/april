<?php

namespace app\console\controllers;


use app\base\ConsoleGatewayTrait;
use app\components\Blockio;
use app\helpers\Utils;
use Yii;

class BlockioController extends \yii\console\Controller
{
    use ConsoleGatewayTrait;

    protected function getComponent(): Blockio
    {
        return Yii::$app->get(getenv('system'));
    }

    public function actionIndex()
    {
        echo Utils::json($this->getComponent()->receive());
    }

    public function actionSummary() {
        $com = $this->getComponent();
        echo json_encode($com->getSummary(), JSON_PRETTY_PRINT);
    }
}
