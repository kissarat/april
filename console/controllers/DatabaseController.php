<?php

namespace app\console\controllers;

use app\console\migrations\Migration;
use Yii;
use yii\console\Controller;

class DatabaseController extends Controller
{
    public function actionDropAll() {

        $tables = Yii::$app->db->schema->getTableNames();
        foreach ($tables as $table) {
            Yii::$app->db->createCommand("ALTER TABLE \"$table\" DISABLE TRIGGER USER")->execute();
        }
        foreach ($tables as $table) {
            echo $table . "\n";
            Yii::$app->db->createCommand()->dropTable($table)->execute();
        }
    }

    public function actionViews() {
        var_dump(Migration::loadViews());
    }
}
