<?php

namespace app\console\controllers;


use app\models\Link;

class LinkController extends Controller
{
    static $modelClass = Link::class;
}
