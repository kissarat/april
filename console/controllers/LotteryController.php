<?php

namespace app\console\controllers;


use app\models\form\LotteryForm;
use app\models\Lottery;
use app\models\Transfer;

class LotteryController extends Controller
{
    public static $modelClass = Lottery::class;

    public static function tableName(): string
    {
        return 'lottery';
    }

    /**
     * @param string $nick
     * @param int $amount
     * @param int $quantity
     * @param string $currency
     * @throws \Exception
     */
    public function actionParticipate(string $nick, int $amount = 1, int $quantity = 1, $currency = 'USD'): void
    {
        $user = $this->getUser($nick);
        $lottery = new LotteryForm([
            'amount' => $amount,
            'quantity' => $quantity,
            'currency' => $currency
        ]);
        $lottery->participate($user);
    }

    /**
     * @param $nick
     * @throws \Exception
     */
    public function actionCount($nick): void
    {
        $user = $this->getUser($nick);
        echo Lottery::countTickets($user->id) ?: 0;
        echo "\n";
    }

    public function actionFirst(): void
    {
        echo Lottery::getFirstNumber() . "\n";
    }

    /**
     * @param string $nick
     * @param int $n
     * @param int $number
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionPrize(string $nick, int $n = 1, int $number = 0): void
    {
        $user = $this->getUser($nick);
        $lottery = $number > 0
            ? Lottery::getLottery($user->id, $number)
            : Lottery::getFirstLottery($user->id);
        \assert($lottery, 'Lottery not found');
        $t = new Transfer([
            'to' => $user->id,
            'type' => Transfer::TYPE_WIN,
            'status' => Transfer::STATUS_SUCCESS,
            'node' => $lottery->id,
            'n' => $n,
            'amount' => 100 * Lottery::getFund() * Lottery::PERCENTS[$n],
            'currency' => 'USD'
        ]);
        \assert($t->insert());
    }
}
