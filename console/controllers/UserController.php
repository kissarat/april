<?php

namespace app\console\controllers;


use app\helpers\SQL;
use app\helpers\Utils;
use app\models\form\UserGenerator;
use app\models\User;
use app\tests\fixtures\MorozovFixture;
use Yii;

class UserController extends Controller
{
    public static $modelClass = User::class;

    public static function tableName(): string
    {
        return 'user';
    }

    protected function tree(int $root, int $size = 1, int $level = 5)
    {
        $gen = new UserGenerator([
            'parent' => $root,
            'buy' => true,
            'count' => $size
        ]);
        $users = $gen->generate();
        echo count($users) . "\t" . $level . "\n";
        if ($level > 0) {
            foreach ($users as $user) {
                $this->tree($user->id, $size, $level - 1);
            }
        }
    }

    public function actionTree($nick, $size = 1, $level = 5)
    {
        $t = Yii::$app->db->beginTransaction();
        $user = $this->getUser($nick);
        $this->tree($user->id, $size, $level);
        $t->commit();
    }

    /**
     * @throws \Exception
     */
    public function actionClear()
    {
        if (YII_DEBUG) {
            $users = MorozovFixture::getDataMorozov('User');
            $user = $this->getUser(end($users));
            User::deleteAll(['>', 'id', $user->id]);
            SQL::restartSequence('user', $user->id + 1);
        } else {
            die("Operation available in debug mode only\n");
        }
    }

    public function actionCreate(string $nick, string $sponsor = null)
    {
        if (!$sponsor) {
            $sponsor = Yii::$app->params['root'];
        }
        $parent = $this->getUser($sponsor);
        $user = new User([
            'nick' => $nick,
            'email' => $nick . '@mail.local',
            'type' => 'native',
            'secret' => Utils::generateSecret('soska'),
            'parent' => $parent->id
        ]);
        if (!$user->insert()) {
            $this->echoJSON($user->getErrors());
        }
    }

    public function actionAncestors($nick)
    {
        $user = $this->getUser($nick);
        $i = 0;
        foreach ($user->getAncestors() as $user) {
            echo (++$i) . ' ' . $user->nick . "\n";
        }
    }

    public function actionCheckAncestor($to_nick, $from_nick)
    {
        $from = $this->getUser($from_nick);
        $to = $this->getUser($to_nick);
        $this->echoJSON($from->checkStructure($to->id));
    }

    public function actionCode(int $n = 64) {
        echo Utils::generateCode($n) . "\n";
    }
}
