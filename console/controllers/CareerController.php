<?php

namespace app\console\controllers;


use app\models\Career;
use app\models\Lottery;
use app\models\Transfer;
use app\models\TransferRate;

class CareerController extends Controller
{
    public static $modelClass = Transfer::class;

    public static function tableName(): string
    {
        return 'transfer';
    }

    public function actionGet($nick)
    {
        $user = $this->getUser($nick);
        $grade = Career::getUserGrade($user->id);
        if ($grade) {
            echo $grade['name'] . "\n";
        } else {
            echo "0\n";
        }
    }

    public function actionCount($nick)
    {
        $user = $this->getUser($nick);
        $i = Career::calculate($user->id);
        if ($i > 0) {
            $grade = Career::getGrade($i);
            echo "$i ${grade['name']}\n";
        } else {
            echo "0\n";
        }
    }

    public function actionRise(string $nick, int $new = 0)
    {
        $user = $this->getUser($nick);
        Career::rise($user->id, $new);
        $i = Career::calculate($user->id);
        $grade = Career::getGrade($i);
        echo "$i ${grade['name']}\n";
    }

    public function actionRiseAncestors($nick)
    {
        $user = $this->getUser($nick);
        $user->riseAncestors();
    }

    public function actionGrade($nick)
    {
        $user = $this->getUser($nick);
        $this->dump('lottery', Lottery::countTickets($user->id));
        $this->dump('personal', TransferRate::sumNodes($user->id));
        $this->dump('turnover', Transfer::getTurnover($user->id));
    }

    public function actionList($sort = 'desc')
    {
        foreach (Career::getGradeByOrder('desc' === $sort ? SORT_DESC : SORT_ASC) as $grade) {
            echo "${grade['id']} ${grade['name']}  \n";
        }
    }
}
