<?php
/**
 * Last modified: 18.05.09 18:36:35
 * Hash: addf3ff3cf13652f020b47d7bcab116c7197b621
 */

$config = require (getenv('SITE') ?: ROOT) . '/config/console.php';
defined('SITE') or define('SITE', getenv('SITE', ROOT));
define('VENDOR', ROOT . '/vendor');

//require_once VENDOR . '/yiisoft/yii2/BaseYii.php';
require VENDOR . '/yiisoft/yii2/Yii.php';
require VENDOR . '/autoload.php';

date_default_timezone_set('UTC');

$app = new yii\console\Application($config);
$app->setVendorPath(VENDOR);
