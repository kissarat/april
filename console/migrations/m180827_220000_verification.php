<?php
/**
 * Last modified: 18.08.27 21:56:15
 * Hash: 511110461df58ec2aec7a55a5ac43f78cb024a6d
 */

namespace app\console\migrations;

class m180827_220000_verification extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user',  'front', $this->string());
        $this->addColumn('user',  'back', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'back');
        $this->dropColumn('user', 'front');
    }
}
