<?php
/**
 * Last modified: 18.06.03 18:17:23
 * Hash: 841fbb929dd8854240c0d21f30c007f6c8b8bbba
 */

namespace app\console\migrations;

use app\helpers\SQL;
use app\models\Grade;
use app\models\Transfer;
use yii\db\Schema;

class m180512_000000_career extends Migration
{
    public function safeUp() {
        $this->createTimestampTable('grade', [
            'id' => Schema::TYPE_PK,
            'name' => $this->string(48),
            'turnover' => $this->bigInteger(),
            'personal' => $this->bigInteger(),
            'lottery' => $this->bigInteger(),
            'premium' => $this->bigInteger(),
        ]);

        foreach (Grade::LIST as $grade) {
            $this->insert('grade', $grade);
        }

        $status = Transfer::TYPE_PREMIUM;
        SQL::execute("UPDATE transfer SET node = n, n = null WHERE type = '$status'");
    }

    public function safeDown() {
        $status = Transfer::TYPE_PREMIUM;
        SQL::execute("UPDATE transfer SET n = node, node = null WHERE type = '$status'");
        $this->dropTable('grade');
    }
}
