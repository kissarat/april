<?php

namespace app\console\migrations;

use app\models\Token;
use app\models\Visit;
use yii\db\Schema;

/**
 * Class m180328_000245_visit
 */
class m180328_000245_visit extends Migration
{
    public function safeUp() {
        $this->createTable('visit', [
            'id' => Schema::TYPE_PK,
            'token' => $this->string(Token::ID_LENGTH),
            'url' => $this->string(Visit::URL_LENGTH)->notNull(),
            'spend' => $this->bigInteger(),
            'ip' => 'INET',
            'created' => $this->created(),
            'agent' => $this->string(Token::AGENT_LENGTH),
            'data' => 'JSON'
        ]);
    }

    public function safeDown() {
        $this->dropTable('visit');
    }
}
