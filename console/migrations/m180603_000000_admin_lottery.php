<?php
/**
 * Last modified: 18.07.10 11:53:02
 * Hash: bc0af61e10ecf8cb828f298e67984b2176498a29
 */

namespace app\console\migrations;

class m180603_000000_admin_lottery extends Migration
{
    public const VIEW = 'select l.*, u.nick from lottery l join "user" u on l."user" = u.id';

    public function safeUp()
    {
        $this->createTable('lottery_transfer', [
            'lottery' => $this->integer()->notNull(),
            'transfer' => $this->integer()->notNull(),
            'time' => $this->timestamp()->notNull(),
        ]);
        $this->addForeignKey('fk_lottery_transfer_lottery', 'lottery_transfer',
            'lottery', 'lottery', 'id');
        $this->addForeignKey('fk_lottery_transfer_transfer', 'lottery_transfer',
            'transfer', 'transfer', 'id');
        $this->execute('alter table lottery_transfer add constraint uniq_lottery_transfer unique (lottery, transfer)');

        $this->createView('admin_lottery', static::VIEW);
    }

    public function safeDown()
    {
        $this->dropView('admin_lottery');
        $this->dropTable('lottery_transfer');
    }
}
