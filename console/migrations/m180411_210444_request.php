<?php

namespace app\console\migrations;


use app\models\Token;
use app\models\Visit;
use yii\db\Schema;

class m180411_210444_request extends Migration
{
    public function safeUp() {
        $this->createEnum('request_method', ['OPTIONS', 'GET', 'HEAD', 'POST', 'PUT',
            'PATCH', 'DELETE', 'TRACE', 'CONNECT']);
        $this->createTable('request', [
            'id' => Schema::TYPE_PK,
            'time' => $this->created(),
            'ip' => 'INET',
            'url' => $this->string(Visit::URL_LENGTH)->notNull(),
            'method' => '"request_method" NOT NULL',
            'type' => $this->string(64),
            'agent' => $this->string(Token::AGENT_LENGTH),
            'data' => 'TEXT',
            'guid' => $this->char(32),
            'token' => $this->string(Token::ID_LENGTH),
            'user' => $this->integer()
        ]);
        $this->addUserForeignKey('request');

        $this->createTimestampTable('ip', [
            'id' => 'INET PRIMARY KEY',
            'text' => $this->string(80),
            'editor' => $this->integer(),
            'ip' => 'INET',
            'allow' => $this->boolean()->notNull()->defaultValue(true)
        ]);
        $this->addUserForeignKey('ip', 'editor');
    }

    public function safeDown() {
        $this->dropTable('ip');
        $this->dropTable('request');
        $this->dropType('request_method');
    }
}
