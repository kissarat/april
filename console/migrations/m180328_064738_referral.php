<?php

namespace app\console\migrations;

/**
 * Class m180328_064738_referral
 */
class m180328_064738_referral extends Migration
{
    public const VIEW = 'SELECT
        id, nick, created, skype, surname, forename, parent,
        (SELECT count(*) FROM "user" c WHERE c.parent = p.id) as children
        FROM "user" p';

    public function safeUp() {
        $this->createView('referral', static::VIEW);
    }

    public function safeDown() {
        $this->dropView('referral');
    }
}
