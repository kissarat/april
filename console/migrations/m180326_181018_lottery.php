<?php

namespace app\console\migrations;

use yii\db\Schema;

class m180326_181018_lottery extends Migration
{
    use WinTrait;

    public function safeUp() {
        $this->createTimestampTable('lottery', [
            'id' => Schema::TYPE_PK,
            'user' => $this->integer()->notNull(),
            'number' => $this->smallInteger()->notNull(),
            'amount' => $this->smallInteger()->notNull()
        ]);
        $this->createWin();
    }

    public function safeDown() {
        $this->dropTable('lottery');
        $this->dropTable('win');
    }
}
