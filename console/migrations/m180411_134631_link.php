<?php

namespace app\console\migrations;


use yii\db\Schema;

/**
 * Class m180411_134631_link
 */
class m180411_134631_link extends Migration
{
    public function safeUp() {
        $this->createTimestampTable('link', [
            'id' => Schema::TYPE_PK,
            'url' => $this->string(192)
        ]);
    }

    public function safeDown() {
        $this->dropTable('link');
    }
}
