<?php
/**
 * Last modified: 18.07.08 16:38:05
 * Hash: b05851ca56d085fc39a28162bd4eee4ee3ce515a
 */

namespace app\console\migrations;

class m180811_120000_agreement extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user',  'agreement', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'agreement');
    }
}
