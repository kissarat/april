<?php
/**
 * Last modified: 18.06.03 18:17:23
 * Hash: e4c22840226b0c3c9d661cf6b5b2f2db431c02b3
 */

namespace app\console\migrations;

class m180512_000000_refback extends Migration
{
    public function safeUp() {
        $this->addColumn('user', 'refback', 'FLOAT');
        $this->addColumn('user', 'pin', 'CHAR(4)');
    }

    public function safeDown() {
        $this->dropColumn('user', 'pin');
        $this->dropColumn('user', 'refback');
    }
}
