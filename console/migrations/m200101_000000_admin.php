<?php
/**
 * Last modified: 18.06.03 18:17:23
 * Hash: fc969d671c7aee1bd7807bf47637c75d5c9bee86
 */

namespace app\console\migrations;


use app\helpers\SQL;

class m200101_000000_admin extends Migration
{
    public function safeUp() {
        foreach (static::loadViews() as $view) {
            $this->execute($view);
        }
    }

    public function safeDown() {
        $found = SQL::column("SELECT table_name FROM information_schema.tables
          WHERE table_type = 'VIEW' AND table_schema = 'public'");
        foreach (array_reverse(static::loadViews()) as $name => $view) {
            if (in_array($name, $found)) {
                $this->dropView($name);
            }
        }
    }
}
