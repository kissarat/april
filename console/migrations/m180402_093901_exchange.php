<?php

namespace app\console\migrations;


use app\models\Transfer;
use Yii;
use yii\db\Schema;

/**
 * Class m180402_093901_exchange
 */
class m180402_093901_exchange extends Migration
{
    public function safeUp()
    {
        $this->createTable('rate', [
            'id' => Schema::TYPE_PK,
            'from' => '"currency" not null',
            'to' => '"currency" not null',
            'rate' => $this->float(),
            'time' => $this->created()
        ]);
        $this->insert('rate', [
            'from' => 'USD',
            'to' => 'USD',
            'rate' => 100,
            'time' => '2010-01-01'
        ]);
        foreach (Transfer::FACTOR as $currency => $rate) {
            $this->insert('rate', [
                'from' => $currency,
                'to' => $currency,
                'rate' => $rate,
                'time' => '2010-01-01'
            ]);
        }
        $this->createView('exchange',
            'WITH r2 AS (SELECT "from", "to", max(id) as id FROM rate GROUP BY "from", "to")
            SELECT r.* FROM "rate" r JOIN r2 ON r2.id = r.id');
        Transfer::getExchangeRate(true);

        $this->createView('transfer_rate',
            'SELECT *,
              (CASE WHEN \'USD\' = currency THEN amount ELSE
              floor(' . (Transfer::RATE_PERCENT * 100) . ' * amount * (SELECT "rate" FROM "rate" r WHERE
              r."from" = t."currency" AND r."to" = \'USD\'
              AND t.created >= r.time
              ORDER BY t.time DESC LIMIT 1)
              / (SELECT "rate" FROM "rate" WHERE "from" = t.currency AND "to" = t.currency))::BIGINT END) AS "usd"
              FROM "transfer" t');
        if ('logist' !== Yii::$app->id) {
            $this->dropView('referral');
            $this->createView('referral', 'SELECT
            id, nick, created, skype, surname, forename, parent,
            (SELECT count(*) FROM "user" c WHERE c.parent = p.id) as children,
            coalesce((SELECT sum(usd) FROM transfer_rate t WHERE t."from" = p.id AND t."type" = \'buy\' AND t."status" = \'success\'), 0)::BIGINT as "buy"
            FROM "user" p');
        }
    }

    public function safeDown()
    {
        if ('logist' !== Yii::$app->id) {
            $this->dropView('referral');
            $this->createView('referral', m180328_064738_referral::VIEW);
        }
        $this->dropView('transfer_rate');
        $this->dropView('exchange');
        $this->dropTable('rate');
    }
}
