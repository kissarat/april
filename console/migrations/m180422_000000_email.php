<?php

namespace app\console\migrations;
use yii\db\Schema;

/**
 * Class m180422_000000_email
 */
class m180422_000000_email extends Migration
{
    public function safeUp() {
        $this->createTable('email', [
            'id' => Schema::TYPE_PK,
            'to' => $this->string(192),
            'subject' => $this->string(192),
            'text' => $this->text(),
            'sent' => $this->boolean()->notNull()->defaultValue(false),
            'time' => $this->created()
        ]);
    }

    public function safeDown() {
        $this->dropTable('email');
    }
}
