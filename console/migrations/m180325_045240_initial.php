<?php

namespace app\console\migrations;

use app\models\Token;
use app\models\Transfer;
use app\models\User;
use app\tests\fixtures\MorozovFixture;
use yii\db\Schema;

/**
 * Class m180325_045240_initial
 */
class m180325_045240_initial extends Migration
{
    public function safeUp() {
        $this->createEnum('user_type', User::TYPES);
        $this->createTimestampTable('user', [
            'id' => Schema::TYPE_PK,
            'nick' => $this->string(24)->notNull(),
            'email' => $this->string(48)->notNull(),
            'type' => "user_type not null default 'new'::user_type",
            'parent' => $this->integer(),
            'secret' => $this->string(60),
            'admin' => $this->boolean()->notNull()->defaultValue(false),
            'surname' => $this->string(48),
            'forename' => $this->string(48),
            'avatar' => $this->string(60),
            'phone' => $this->string(32),
            'telegram' => $this->string(32),
            'skype' => $this->string(32),
            'site' => $this->string(160),
        ]);
        $this->addUserForeignKey('user', 'parent');
        foreach (MorozovFixture::getDataMorozov('User') as $user) {
            $this->insert('user', $user);
        }

        $this->createEnum('token_type', Token::TYPES);
        $this->createTimestampTable('token', [
            'id' => $this->string(Token::ID_LENGTH),
            'user' => $this->integer(),
            'type' => "token_type not null default 'browser'::token_type",
            'expires' => $this->timestamp()->notNull()->defaultValue('2030-01-01 00:00:00'),
            'data' => 'JSON',
            'ip' => 'INET',
            'handshake' => $this->timestamp(),
            'name' => $this->string(Token::AGENT_LENGTH)
        ]);
        $this->addUserForeignKey('token', 'user');

        $this->createEnum('transfer_type', Transfer::TYPES);
        $this->createEnum('transfer_status', Transfer::STATUSES);
        $this->createEnum('transfer_system', Transfer::SYSTEMS);
        $this->createEnum('currency', Transfer::CURRENCIES);
        $this->createTimestampTable('transfer', [
            'id' => Schema::TYPE_PK,
            'from' => $this->integer(),
            'to' => $this->integer(),
            'amount' => $this->bigInteger()->notNull(),
            'type' => "transfer_type not null",
            'status' => "transfer_status not null",
            'currency' => "currency not null",
            'system' => "transfer_system",
            'wallet' => $this->string(Transfer::WALLET_LENGTH),
            'text' => $this->string(Transfer::TEXT_LENGTH),
            'txid' => $this->string(Transfer::TXID_LENGTH),
            'vars' => 'JSON',
            'ip' => 'INET',
            'guid' => $this->char(8)->unique(),
            'visible' => $this->boolean()->notNull()->defaultValue(true),
        ]);
        $this->addUserForeignKey('transfer', 'from');
        $this->addUserForeignKey('transfer', 'to');

        $this->createTable('log', [
            'id' => Schema::TYPE_BIGPK,
            'entity' => $this->string(16)->notNull(),
            'action' => $this->string(32)->notNull(),
            'user' => $this->integer(),
            'token' => $this->string(Token::ID_LENGTH),
            'data' => 'JSON',
            'ip' => 'INET'
        ]);
//        $this->addUserForeignKey('log', 'user');
    }

    public function safeDown() {
        $this->dropTable('log');

        $this->dropTable('transfer');
        $this->dropType('currency');
        $this->dropType('transfer_system');
        $this->dropType('transfer_status');
        $this->dropType('transfer_type');

        $this->dropTable('token');
        $this->dropType('token_type');

        $this->dropTable('user');
        $this->dropType('user_type');
    }
}
