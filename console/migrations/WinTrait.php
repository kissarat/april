<?php
/**
 * Last modified: 18.07.10 12:12:36
 * Hash: c92826780b65937280da84f1d965d69643efdf1c
 */


namespace app\console\migrations;


use yii\db\Schema;

trait WinTrait
{
    protected function createWin() : void {
        $this->createTimestampTable('win', [
            'id' => Schema::TYPE_PK,
            'number' => $this->smallInteger()->notNull(),
            'first' => $this->integer()->notNull(),
            'second' => $this->integer()->notNull(),
            'third' => $this->integer()->notNull(),
        ]);
        $this->addUserForeignKey('win', 'first');
        $this->addUserForeignKey('win', 'second');
        $this->addUserForeignKey('win', 'third');

        $this->addColumn('lottery', 'win', $this->integer());
        $this->addSingleForeignKey('lottery', 'win', 'win', 'id');
    }
}
