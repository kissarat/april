<?php

namespace app\console\migrations;
use yii\db\Schema;

/**
 * Class m180420_194941_telegram
 */
class m180420_194941_telegram extends Migration
{
    public function safeUp() {
        $this->createTimestampTable('telegram_user', [
            'id' => Schema::TYPE_BIGPK,
            'nick' => $this->string(),
            'surname' => $this->string(),
            'forename' => $this->string(),
            'user_id' => $this->integer(),
            'locale' => $this->string(),
            'bot' => $this->boolean()
        ]);

        $this->createTable('telegram_message', [
            'id' => Schema::TYPE_BIGPK,
            'message_id' => $this->bigInteger(),
            'from' => $this->bigInteger(),
            'text' => $this->string(),
            'time' => $this->timestamp()
        ]);

    }

    public function safeDown() {
        $this->dropTable('telegram_message');
        $this->dropTable('telegram_user');
    }
}
