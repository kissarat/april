<?php

namespace app\console\migrations;

use app\models\Program;

class m180710_120000_reward extends Migration
{
    public const SPONSOR_VIEW = 'WITH RECURSIVE r(id, "nick", type, parent, root, level) AS (
        SELECT
          id,
          nick,
          type,
          parent,
          id AS root,
          0  AS level
        FROM "user"
        UNION
        SELECT
          u.id,
          u.nick,
          u.type,
          u.parent,
          r.root,
          r.level + 1 AS level
        FROM "user" u
          JOIN r ON u.id = r.parent
        WHERE r.level <= 50
      )
      SELECT
        id,
        nick,
        type,
        parent,
        root,
        level
      FROM r
      ORDER BY root, level, id';

    public const NEED_REWARD_VIEW = 'SELECT
        s.id                            AS "user",
        coalesce(ur.percent, w.percent) AS percent,
        coalesce(ur.level, s.level)     AS level,
        s.root,
        w.program
      FROM sponsor s
        JOIN reward w ON s.level = w.level
        LEFT JOIN user_reward ur ON s."id" = ur."user" AND w.level = ur.level AND w.program = ur.program
      WHERE s.type <> \'special\'';

    public function safeUp()
    {
        $this->createTable('reward', [
            'level' => $this->smallInteger()->notNull(),
            'percent' => $this->float()->notNull(),
            'program' => $this->smallInteger()->notNull()
        ]);
        $this->execute('alter table reward add constraint pk_reward unique ("level","program")');
        foreach (Program::getBonuses() as $level => $percent) {
            foreach (m180414_063012_program::getPrograms() as $program) {
                $this->insert('reward', [
                    'level' => (int) $level,
                    'percent' => (float) $percent,
                    'program' => (int) $program['id']
                ]);
            }
        }

        $this->createTimestampTable('user_reward', [
            'user' => $this->integer()->notNull(),
            'level' => $this->smallInteger()->notNull(),
            'program' => $this->smallInteger()->notNull(),
            'percent' => $this->float()->notNull(),
        ]);
        $this->execute('alter table user_reward add constraint pk_user_reward unique ("user","level","program")');

        $this->createView('sponsor', static::SPONSOR_VIEW);
        $this->createView('need_reward', static::NEED_REWARD_VIEW);
    }

    public function safeDown()
    {
        $this->dropView('need_reward');
        $this->dropView('sponsor');
        $this->dropTable('user_reward');
        $this->dropTable('reward');
    }
}
