<?php
/**
 * Last modified: 18.06.03 18:22:15
 * Hash: ca24c5aaa6bc4e64bf61ad3be9094d1228954ded
 */

namespace app\console\migrations;


use Yii;

abstract class Migration extends \yii\db\Migration
{
    public function createTimestampTable($table, $columns, $options = null) {
        $columns['created'] = $this->created();
        $columns['time'] = $this->timestamp();
        return $this->createTable($table, $columns, $options);
    }

    public function createEnum($type, $values) {
        $values = implode(',', array_map(function ($v) {
            return "'$v'";
        }, $values));
        return $this->execute("CREATE TYPE \"$type\" AS ENUM ($values)");
    }

    public function addEnumValue($type, $value) {
        return $this->execute("ALTER TYPE \"$type\" ADD VALUE '$value'");
    }

    public function created() {
        return $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP');
    }

    public function dropType($type) {
        return $this->execute("DROP TYPE \"$type\"");
    }

    public function addSingleForeignKey($table, $columns, $refTable, $refColumns, $delete = null, $update = null) {
        return $this->addForeignKey($table . '_' . $columns, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    public function addUserForeignKey($table, $columns = 'user', $delete = null, $update = null) {
        return $this->addSingleForeignKey($table, $columns, 'user', 'id', $delete, $update);
    }

    public function createView($name, $sql) {
        return $this->execute("CREATE VIEW \"$name\" AS " . $sql);
    }

    public function dropView($name) {
        return $this->execute("DROP VIEW \"$name\"");
    }

    public static function loadViews() {
        $views = [];
        foreach (['views', 'statistics', 'admin_user', 'withdrawable', 'transfer', 'node'] as $name) {
            $file = file_get_contents(Yii::getAlias("@app/admin/data/$name.sql"));
            foreach (preg_split('/\s*;\s*/', $file) as $s) {
                if (empty(trim($s))) {
                    continue;
                }
                preg_match('/VIEW\s+"([\w_]+)"/i', $s, $m);
                if (!empty($m) && !empty($m[1])) {
                    $views[$m[1]] = $s;
                } else {
                    throw new \Exception("Invalid view: $s\n\n\n");
                }
            }
        }
        return $views;
    }
}
