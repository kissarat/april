<?php

namespace app\console\migrations;

/*
alter table lottery_transfer
  drop constraint fk_lottery_transfer_lottery;
alter table lottery_transfer
  drop constraint fk_lottery_transfer_transfer;

alter table lottery_transfer
  add constraint fk_lottery_transfer_lottery
foreign key (lottery)
references lottery (id)
on delete cascade on update cascade;

alter table lottery_transfer
  add constraint fk_lottery_transfer_transfer
foreign key (transfer)
references transfer (id)
on delete cascade on update cascade;
 */

class m180710_200000_lottery_without_win extends Migration
{
    use WinTrait;
    public const ADMIN_LOTTERY_VIEW = 'select l.*, t.n, t.amount as win, u.nick, u.avatar, u.surname, u.forename
      from lottery l
      join "user" u on l."user" = u.id
      left join transfer t on l.id = t.node and t.type = \'win\' and t.status = \'success\'';

    public const TREE_VIEW = 'WITH RECURSIVE r(id, "nick", parent, email, root, level, phone, forename, surname, skype, avatar, telegram, created) AS (
        SELECT
          id,
          nick,
          parent,
          email,
          id AS root,
          0  AS level,
          phone,
          forename,
          surname,
          skype,
          avatar,
          telegram,
          created
        FROM "user"
        UNION
        SELECT
          u.id,
          u.nick,
          u.parent,
          u.email,
          r.root,
          r.level + 1 AS level,
          u.phone,
          u.forename,
          u.surname,
          u.skype,
          u.avatar,
          u.telegram,
          u.created
        FROM "user" u
          JOIN r ON u.parent = r.id
        WHERE r.level < 50
      )
      SELECT
        id,
        nick,
        parent,
        email,
        root,
        level,
        phone,
        forename,
        surname,
        skype,
        avatar,
        telegram,
        created
      FROM r
      ORDER BY root, level, id';

    public const TURNOVER_LEVEL_VIEW = 'select
        r.root        as "user",
        b.currency,
        r.level,
        sum(b.amount) as amount
      from transfer b
        join tree r on r.id = b."from"
      where r."level" > 0 and b.type = \'buy\'
      group by r.root, b.currency, r.level';

    public const ALL_TURNOVER_VIEW = 'select
        r.root        as "user",
        b.currency,
        sum(b.amount) as amount
      from transfer b
        join tree r on r.id = b."from"
        where r.level <= 5
      group by r.root, b.currency';

    public const CONSOLIDATED_TURNOVER_VIEW = 'WITH a AS (
          SELECT
            b."user",
            CASE WHEN currency = \'USD\'
              THEN amount
            ELSE amount * rate END AS amount
          FROM all_turnover b
            JOIN exchange x ON b.currency = x."to" AND x."from" = \'USD\'
      )
      SELECT
        "user",
        sum(amount) AS amount
      FROM a
      GROUP BY "user"';

    public const TURNOVER_VIEW = 'select
        u.id,
        u.nick,
        coalesce((select amount
                  from consolidated_turnover c
                  where c."user" = u.id), 0)                              as consolidated,
        (select nick
         from "user" p
         where p.id = u.parent
         limit 1)                                                         as sponsor,
        (select array_to_json(array_agg(t))
         from (select
                 currency,
                 sum(amount) as amount
               from transfer
               where "from" = u.id and type = \'buy\' and status = \'success\'
               group by currency) t)                                      as buy,
        (select array_to_json(array_agg(t))
         from (select
                 currency,
                 amount
               from turnover_level l
               where l."user" = u.id and l.level = 1) t)                  as first,
        (select array_to_json(array_agg(t))
         from (select
                 currency,
                 level,
                 amount
               from turnover_level l
               where l."user" = u.id) t)                                  as "all",
        (select count(*)
         from "user" c
         where c.parent = u.id)                                           as referral,
        (select count(*)
         from tree r
         where r.root = u.id and r.level > 0)                             as team,
        (select array_to_json(array_agg(t))
         from (select
                 currency,
                 sum(amount) as amount
               from transfer
               where "from" = u.id and type = \'withdraw\' and status = \'success\'
               group by currency) t)                                      as withdraw,
        (select array_to_json(array_agg(t))
         from (select
                 currency,
                 sum(amount) as amount
               from transfer
               where "to" = u.id and type = \'accrue\' and status = \'success\'
               group by currency) t)                                      as accrue
      from "user" u';

    public function safeUp()
    {
        $this->dropView('admin_lottery');
        $this->dropColumn('lottery', 'win');
        $this->dropTable('win');
        $this->createView('admin_lottery', static::ADMIN_LOTTERY_VIEW);
        $this->createView('tree', static::TREE_VIEW);
        $this->createView('turnover_level', static::TURNOVER_LEVEL_VIEW);
        $this->createView('all_turnover', static::ALL_TURNOVER_VIEW);
        $this->createView('consolidated_turnover', static::CONSOLIDATED_TURNOVER_VIEW);
        $this->createView('user_turnover', static::TURNOVER_VIEW);
    }

    public function safeDown()
    {
        $this->dropView('user_turnover');
        $this->dropView('consolidated_turnover');
        $this->dropView('all_turnover');
        $this->dropView('turnover_level');
        $this->dropView('tree');
        $this->dropView('admin_lottery');
        $this->createWin();
        $this->createView('admin_lottery', m180603_000000_admin_lottery::VIEW);
    }
}
