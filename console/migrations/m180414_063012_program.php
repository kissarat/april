<?php

namespace app\console\migrations;

use Yii;
use yii\db\Migration;

/**
 * Class m180414_063012_program
 */
class m180414_063012_program extends Migration
{
    public static function getPrograms(): array
    {
        return 'logist' === Yii::$app->id ?
            [
                ['id' => 1, 'name' => 'Test', 'min' => 10 * 100, 'max' => 100000000, 'percent' => 0.008, 'days' => 365],
                ['id' => 2, 'name' => 'Start', 'min' => 500 * 100, 'max' => 100000000, 'percent' => 0.010, 'days' => 365],
                ['id' => 3, 'name' => 'VIP', 'min' => 1000000, 'max' => 100000000, 'percent' => 0.012, 'days' => 365],
            ] : [
                ['id' => 1, 'name' => 'Philately', 'min' => 1500, 'max' => 15000, 'percent' => 0.0575, 'days' => 20],
                ['id' => 2, 'name' => 'Numismatics', 'min' => 15000, 'max' => 100000, 'percent' => 0.05, 'days' => 30],
                ['id' => 3, 'name' => 'Painting', 'min' => 100000, 'max' => 150000, 'percent' => 0.0525, 'days' => 25],
            ];
    }

    public function safeUp()
    {
        $this->createTable('program', [
            'id' => $this->integer(),
            'name' => $this->string(),
            'min' => $this->bigInteger(),
            'max' => $this->bigInteger(),
            'percent' => $this->float(),
            'days' => $this->smallInteger(),
        ]);

        foreach (static::getPrograms() as $program) {
            $this->insert('program', $program);
        }
    }

    public function safeDown()
    {
        $this->dropTable('program');
    }
}
