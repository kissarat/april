<?php
/**
 * Last modified: 18.07.08 16:38:05
 * Hash: b05851ca56d085fc39a28162bd4eee4ee3ce515a
 */

namespace app\console\migrations;

/**
 * Class m180328_064738_referral
 */
class m180708_000000_referral_balance extends Migration
{
    public const VIEW = 'with t as (
    select * from transfer where "type" = \'buy\' and "status" = \'success\'
    )
        select
        id, nick, created, skype, surname, forename, parent,
        (select count(*) from "user" c where c.parent = p.id) as children,
        (select sum(amount) from t where currency = \'USD\' and t."from" = p.id) as usd,
        (select sum(amount) from t where currency = \'BTC\' and t."from" = p.id) as btc,
        (select sum(amount) from t where currency = \'ETH\' and t."from" = p.id) as eth,
        (select sum(amount) from t where currency = \'ETC\' and t."from" = p.id) as etc,
        (select sum(amount) from t where currency = \'LTC\' and t."from" = p.id) as ltc,
        (select sum(amount) from t where currency = \'DOGE\' and t."from" = p.id) as doge,
        (select sum(amount) from t where currency = \'XRP\' and t."from" = p.id) as xrp
        from "user" p';

    public function safeUp()
    {
        $this->dropView('referral');
        $this->createView('referral', static::VIEW);
    }

    public function safeDown()
    {
        $this->dropView('referral');
        $this->createView('referral', m180328_064738_referral::VIEW);
    }
}
