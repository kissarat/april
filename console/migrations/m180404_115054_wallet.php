<?php

namespace app\console\migrations;

// 'perfect','advcash','payeer','btc','eth','etc','xrp','ltc','doge'

/**
 * Class m180404_115054_wallet
 */
class m180404_115054_wallet extends Migration
{
    public function safeUp() {
        $this->addColumn('user', 'perfect', $this->string(128));
        $this->addColumn('user', 'advcash', $this->string(128));
        $this->addColumn('user', 'payeer', $this->string(128));
        $this->addColumn('user', 'btc', $this->string(128));
        $this->addColumn('user', 'eth', $this->string(128));
        $this->addColumn('user', 'etc', $this->string(128));
        $this->addColumn('user', 'xrp', $this->string(128));
        $this->addColumn('user', 'ltc', $this->string(128));
        $this->addColumn('user', 'doge', $this->string(128));
    }

    public function safeDown() {
        $this->dropColumn('user', 'perfect');
        $this->dropColumn('user', 'advcash');
        $this->dropColumn('user', 'payeer');
        $this->dropColumn('user', 'btc');
        $this->dropColumn('user', 'eth');
        $this->dropColumn('user', 'etc');
        $this->dropColumn('user', 'xrp');
        $this->dropColumn('user', 'ltc');
        $this->dropColumn('user', 'doge');
    }
}
