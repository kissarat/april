<?php

namespace app\console\migrations;


class m180330_193801_node extends Migration
{
    public function safeUp() {
        $this->addColumn('transfer', 'node', 'INT');
        $this->addColumn('transfer', 'n', 'SMALLINT');
    }

    public function safeDown() {
        $this->dropColumn('transfer', 'n');
        $this->dropColumn('transfer', 'node');
    }
}
