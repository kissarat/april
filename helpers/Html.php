<?php

namespace app\helpers;

use app\models\Program;
use app\models\Transfer;
use app\models\User;
use Yii;

class Html extends \yii\bootstrap\Html
{
    public static function email($address)
    {
        return "<a href=\"mailto:$address\">$address</a>";
    }

    public static function item($text, $url, $icon = '')
    {
        if (!Yii::$app->user->getIsGuest() && is_array($url)) {
            $url['nick'] = Yii::$app->user->identity->nick;
        }
        $text = Yii::t('app', $text);
        return static::a("<i class=\"zmdi zmdi-$icon\"></i> <span>$text</span>", $url);
    }

    public static function avatar($url)
    {
        $options = ['class' => 'avatar'];
        if ($url) {
            $options['style'] = "background-image: url('$url')";
        } else {
            $options['class'] .= ' absent';
        }
        return self::tag('div', '', $options);
    }

    public static function money(int $amount, string $currency)
    {
        return self::tag('span', $amount, [
            'class' => 'money ' . strtolower($currency)
        ]);
    }
    
    public static function context($state = null)
    {
        $config = [
            'id' => Yii::$app->id,
            'locales' => Yii::$app->settings->getLocales(),
            'locale' => Yii::$app->language,
            'mode' => YII_ENV,
            'timezone' => date_default_timezone_get(),
            'factor' => Transfer::FACTOR,
            'currencies' => Transfer::getCurrencies(),
            'levels' => Transfer::LEVELS,
            'currency' => Yii::$app->request->getCurrency(),
            'fileSize' => Yii::$app->params['fileSize'] ?? 1024 * 1024,
        ];

        if (!Yii::$app->user->getIsGuest()) {
            /**
             * @var User $user
             */
            $user = Yii::$app->user->identity;
            $config['accounts'] = $user->getWallets();
            $config['bonuses'] = Program::getBonuses();
            $config['user'] = [
                'id' => $user->id,
                'nick' => $user->nick,
                'created' => $user->created,
            ];
        }

        if (!empty($state)) {
            $config = array_merge($config, $state);
        }

        $vars = [
            'appConfig' => $config
        ];

        if ($state['translation'] ?? false) {
            $messages = [];
            foreach (Utils::getLocale(Yii::$app->language) as $src => $dst) {
                if ('_' !== $src[0]) {
                    $messages[$src] = $dst;
                }
            }
            $vars['translation'] = $messages;
        }

        if ('logist' === Yii::$app->id) {
            $script = [];
            foreach ($vars as $name => $value) {
                $json = Utils::json($value);
                $script[] = "const $name=$json;";
            }
            return implode(YII_DEBUG ? "\n// -------------------------------------\n" : '', $script);
        }
        else {
            $name = '__VIEWSTATE';
            // JSON_UNESCAPED_UNICODE
            $data = strrev(base64_encode(json_encode($vars)));
            return static::hiddenInput($name, $data, ['id' => $name]);
        }
    }

    public static function extendContext($state) {
        if ($state) {
            return static::tag('div', Utils::json($state), ['class' => 'extension', 'style' => 'display: none']);
        }
        return '';
    }

    public static function stamp() {
        $info = [ 'Request at ' . date( 'Y-m-d H:i:s' ) ];
        foreach ( [ 'HOST', 'IP', 'USER_AGENT', 'ACCEPT', 'ACCEPT_LANGUAGE' ] as $header ) {
            if ( isset( $_SERVER[ 'HTTP_' . $header ] ) ) {
                $info[] = "\t$header: " . $_SERVER[ 'HTTP_' . $header ];
            }
        }
        return implode( "\n", $info );
    }

    public static function resource($filename) {
        $locale = Yii::$app->language;
        return Yii::getAlias("@web/images/pr/$locale/" . $filename);
    }

    public static function banner($filename, $options = []) {
        return static::img(static::resource('banners/' . $filename), $options);
    }

    public static function promo($filename, $options = []) {
        return static::img(static::resource('promo/' . $filename), $options);
    }

    public static function xmlns() {
        $ns = [];
        foreach (['v-on', 'v-bind', 'v-if', 'v-for', 'v-clock'] as $xmlns) {
            $ns[] = "xmlns:$xmlns=\"http://www.w3.org/1999/xhtml\"";
        }
        return implode(' ', $ns);
    }
}
