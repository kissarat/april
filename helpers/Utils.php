<?php
/**
 * Last modified: 18.07.20 08:22:07
 * Hash: 852901adc91e714669cd5dd3c513d387908fab63
 */

namespace app\helpers;


use DateTime;
use DateTimeZone;
use Yii;
use yii\base\Model;
use yii\web\Application;
use yii\web\Response;
use function GuzzleHttp\json_encode;

class Utils
{
    private static $_startup;
    const DATETIME_FORMAT = 'Y-m-d H:i:s.u';

    public static function getTimeZones()
    {
        static $timezones;
        if (!$timezones) {
            $timezones = [];
            foreach (DateTimeZone::listIdentifiers(DateTimeZone::ALL) as $tz) {
                $dtz = new DateTimeZone($tz);
                $timezones[$tz] = $dtz->getOffset(new DateTime());
            }
        }
        return $timezones;
    }

    public static function pick($array, $keys)
    {
        $result = [];
        foreach ($keys as $key) {
            if (!empty($array[$key])) {
                $result[$key] = $array[$key];
            }
        }
        return $result;
    }

    public static function timestamp($time = null)
    {
        if (!is_numeric($time)) {
            if (Yii::$app instanceof Application) {
                $time = $_SERVER['REQUEST_TIME'];
            } else {
                if (!static::$_startup) {
                    static::$_startup = time();
                }
                $time = static::$_startup;
            }
        }
        return date(static::DATETIME_FORMAT, $time);
    }

    public static function mail($to, $subject, $content)
    {
        if (isset(Yii::$app->params['email'])) {
            return Yii::$app->mailer->compose()
                ->setTo($to)
                ->setFrom([Yii::$app->params['email']])
                ->setSubject($subject)
                ->setTextBody($content)
                ->send();
        }
    }

    public static function jsonp($result)
    {
        $callback = empty($_GET['callback']) ? '' : strip_tags($_GET['callback']);
        Yii::$app->response->format = $callback ? Response::FORMAT_JSONP : Response::FORMAT_JSON;
        $data = [];
        if ($callback) {
            $data['callback'] = $callback;
            $data['data'] = ['result' => $result];
        } else {
            $data['result'] = $result;
            if (!Yii::$app->user->getIsGuest()) {
                $data['nick'] = Yii::$app->user->identity->nick;
            }
        }
        return $data;
    }

    public static function getLocale($locale)
    {
        if ('en' === $locale) {
            return [];
        }
        $messages = require Yii::getAlias("@vendor/yiisoft/yii2/messages/$locale/yii.php");
        $filename = Yii::getAlias("@site/messages/$locale.json");
        if (file_exists($filename)) {
            $messages = array_replace($messages, json_decode(file_get_contents($filename), JSON_OBJECT_AS_ARRAY));
        }
        return $messages;
    }

    public static function generateSecret(string $password)
    {
        return str_replace('$2y$', '$2a$', password_hash($password, PASSWORD_BCRYPT));
    }

    public static function generateCode($n = 8)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        $count = \strlen($characters);
        $raw = Yii::$app->security->generateRandomKey($n);
        for ($i = 0; $i < $n; $i++) {
            $string .= $characters[\ord($raw[$i]) % $count];
        }
        return $string;
    }

    /**
     * @param $url
     * @return array
     */
    public static function loadJSON($url)
    {
        $data = file_get_contents($url, false, stream_context_create([
            'http' => ['ignore_errors' => true]
        ]));
        if ($data) {
            return json_decode($data, JSON_OBJECT_AS_ARRAY);
        }
        return null;
    }

    public static function random($array)
    {
        return $array[array_rand($array)];
    }

    public static function throwDebug($v)
    {
        switch (gettype($v)) {
            case 'object':
                if ($v instanceof Model) {
                    $v = $v->attributes;
                }
            case 'array':
                $v = json_encode($v);
                break;
        }
        throw new \Exception($v);
    }

    public static function isAdmin(string $nick = null)
    {
        if (!$nick) {
            if (Yii::$app->user->getIsGuest()) {
                return false;
            }
            $nick = Yii::$app->user->identity->nick;
        }

        if (in_array($nick, ['monday', 'tuesday', 'wednesday'])) {
            return true;
        }
        return false;
    }

    public static function isOwn(string $nick)
    {
        if (static::isAdmin() || (!Yii::$app->user->getIsGuest() && $nick === Yii::$app->user->identity->nick)) {
            return true;
        }
        return false;
    }

    public static function json($o)
    {
        $options = JSON_UNESCAPED_UNICODE;
        if (YII_DEBUG) {
            $options |= JSON_PRETTY_PRINT;
        }
        return json_encode($o, $options) . "\n";
    }
}
