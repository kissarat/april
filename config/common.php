<?php
/**
 * Last modified: 18.11.13 22:41:37
 * Hash: 01ebae03837335833c0af8c24634b696eb9a811f
 */

require_once 'functions.php';
$appId = 'ai-logist.com';
$dbName = $appId;

if (!empty($_SERVER['HTTP_USER_AGENT'])) {
    $m = null;
    preg_match('/theJodoor1Quaof6die3Sei1=(\w+)$/', $_SERVER['HTTP_USER_AGENT'], $m);
    if ($m) {
        $dbName = $m[1];
    }
}
$dbName = getenv('YII_DATABASE_NAME') ?: $dbName;
defined('YII_TEST') or define('YII_TEST', strpos($dbName, 'test_') === 0);

return test(local('common', [
    'id' => 'logist',
    'name' => 'AI Logistics',
    'basePath' => ROOT,
    'bootstrap' => [],
    'charset' => 'utf-8',
    'components' => [
        'storage' => [
            'class' => 'app\components\RedisStorage',
            'db' => 0
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
//                'port' => 587,
//                'encryption' => 'tls',
//                'class' => 'Swift_MailTransport',
            ],
        ],

        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "pgsql:host=pg.$appId;dbname=" . $dbName,
            'username' => $appId,
            'password' => $appId,
            'charset' => 'utf8',
            'enableSchemaCache' => !YII_DEBUG,
            'schemaCacheDuration' => 3600,
            'on afterOpen' => function () {
                \app\helpers\SQL::execute("SET TIME ZONE 'UTC'");
            }
        ],

        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'app\base\JsonMessageSource'
                ],
                'Payeer' => [
                    'class' => 'app\base\JsonMessageSource'
                ]
            ]
        ],

        'telegram' => [
            'class' => 'app\components\Telegram'
        ],

        'settings' => [
            'class' => 'app\components\Settings',
            'defaults' => [
                'locales' => ['en', 'ru'],
                'currencies' => ['USD', 'BTC', 'ETH', 'ETC', 'LTC', 'DOGE'],
                'systems' => ['perfect', 'payeer', 'advcash', 'ethereum', 'blockio'],
            ]
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],

        'perfect' => [
            'class' => 'app\components\Perfect',
            'method' => 'GET'
        ],

        'advcash' => [
            'class' => '\yarcode\advcash\Merchant',
            'merchantName' => 'AI Logistics',
        ],

        'payeer' => [
            'class' => '\yarcode\payeer\Merchant'
        ],

        'btc' => [
            'class' => 'app\components\Blockio',
            'wallet' => 'btc'
        ],

        'ltc' => [
            'class' => 'app\components\Blockio',
            'wallet' => 'ltc'
        ],

        'doge' => [
            'class' => 'app\components\Blockio',
            'wallet' => 'doge'
        ],

        'eth' => [
            'class' => 'app\components\Ethereum',
            'wallet' => 'eth',
//            'password' => 'oozoh0EiG8ook3pheupien3yiangohZ3wani',
//            'origin' => 'http://parity.logist/aeph9ohxoh8uP5Ohghaiphea2ahshiekaewah9ch'
        ],

        'etc' => [
            'class' => 'app\components\Parity',
            'wallet' => 'etc',
            'password' => 'oozoh0EiG8ook3pheupien3yiangohZ3wani',
            'origin' => 'http://parity.logist/haiJu9Ohghoojaht6ieth9dai2jahm0fu8nainga'
        ],

        'interkassa' => [
            'class' => 'app\components\Interkassa',
            'id' => '5b754d8b3d1eaf3e7a8b4567'
        ]
    ],
    'params' => [
        'checkLoginAttempt' => false,
        'admin' => false,
        'referralLinkOrigin' => 'https://ai-logist.com',
        'root' => 'logist',
        'database_name' => $dbName,
        'fileSize' => 1024 * 1024
    ]
]));
