<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: e95ee2eb424e7d4e345df08ce28af24d33c1a38b
 */

$config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    'allowedIPs' => ['127.0.0.1', '185.53.170.57', '::1'],
];
$config['bootstrap'][] = 'debug';

//$config['bootstrap'][] = 'gii';
//$config['modules']['gii'] = [
//    'class' => 'yii\gii\Module',
//];
