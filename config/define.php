<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 1a10e2d492cb098352f0ba44e2a5923096d3be61
 */

defined('YII_ENV') or define('YII_ENV', getenv('YII_ENV') ?: 'dev');
defined('YII_DEBUG') or define('YII_DEBUG', 'dev' === YII_ENV);
defined('YII_TEST') or define('YII_TEST', YII_DEBUG);
define('CONSOLE', php_sapi_name() === 'cli');
