<?php
/**
 * Last modified: 18.05.15 07:13:57
 * Hash: a2d7aabcc6e0dcba32b9250f887e43d107e78b7f
 */

defined('ROOT') or define('ROOT', dirname(__DIR__));
defined('SITE') or define('SITE', ROOT);

function optional($filename)
{
    $path = SITE . $filename;
    if (file_exists($path)) {
        return require $path;
    }
    return [];
}

optional('/config/local/define.php');
require_once ROOT . '/config/define.php';

function local($name, array $defaults)
{
    return array_replace_recursive($defaults, optional("/config/local/$name.php"));
}

function extend($name, $baseName, array $defaults)
{
    $o = is_array($baseName) ? $baseName : optional("/config/$baseName.php");
    return local($name, array_replace_recursive(
        $o,
        $defaults
    ));
}

function test($config) {
    if (YII_TEST) {
        $config['params']['hostname'] = 'logist' === $config['id']
            ? 'local.ai-logist.com'
            : 'local.antikvar-plus.com';
    }
    return $config;
}
