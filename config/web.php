<?php
/**
 * Last modified: 18.08.28 00:31:45
 * Hash: 78edd710a986f58876f96da9874e9a8badd0fe66
 */

require_once 'functions.php';

$webConfig = [
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/index',
    'controllerMap' => [
        'apiuser' => 'app\controllers\api\SubjectController'
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'go-<id>' => 'home/go',
                'ref/<nick>' => 'user/signup',
                'new-visit/<nick>' => 'home/newbie',
                'sign-up' => 'user/signup',
                'summary/<currency>' => 'transfer/summary',
                'dashboard/<nick>' => 'user/profile',
                'wallet/<system>' => 'transfer/wallet',
                'finances/<nick>/<guid>' => 'transfer/index',
                'finances/<nick>' => 'transfer/index',
                'finances' => 'transfer/index',
                'plans/<currency>' => 'program/index',
                'investment/<id>' => 'node/index',
                'plans' => 'program/index',
                'my-plans' => 'node/index',
                'loto' => 'lottery/participate',
                'children/<nick>/<format>' => 'structure/index',
                'children/<nick>' => 'structure/index',
                'children' => 'structure/index',
                'seed/<class>' => 'generate/seed',
                'pay/<nick>/<amount>-<currency>' => 'invoice/payment',
                'pay/<nick>' => 'invoice/payment',
                'withdraw/<nick>' => 'invoice/withdraw',
                'profile/<nick>/settings' => 'settings/profile',
                'password/<nick>/settings' => 'settings/password',
                'wallet/<nick>/settings' => 'settings/wallet',
                'request-reset' => 'user/request',
                'reset/<code>' => 'user/reset',
                'login/<nick>' => 'user/login',
                'login' => 'user/login',
                'logout/<nick>/<ip>' => 'user/logout',
                'logout/<nick>' => 'user/logout',
                'logout' => 'user/logout',
                'buy/<id>' => 'program/buy',
                'banners' => 'home/banners',
                'promotion' => 'home/promotion',
                'translation/<locale>' => 'home/translation',
                'sitemap.xml' => 'home/sitemap',
                'ik7opiThuQuah6cai0ieDaug' => 'interkassa/internalaelae8naede8iekah9yae2pa',
                '~v' => 'home/visit',
                'book' => 'home/book',
                'pdf' => 'home/pdf',
                'contract' => 'home/contract',
                'verif' => 'home/verif',
                'landing' => 'home/landing',
                'calculator/<view>' => 'program/calculator',
                'calculator' => 'program/calculator',
                'august-promo' => 'node/promo',
            ],
        ],

        'authManager' => [
            'class' => 'yii\rbac\PhpManager'
        ],

        'session' => [
            'class' => 'yii\web\CacheSession',
            'flashParam' => '_',
            'timeout' => 10800,
            'cookieParams' => ['lifetime' => 10800]
        ],

        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'enableSession' => true,
            'idParam' => 'id',
            'autoRenewCookie' => false,
            'loginUrl' => ['user/login'],
            'authTimeout' => 10800
        ],

        'request' => [
            'class' => 'app\base\Request',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],

        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LfPm2UUAAAAAJA7-gwC0y4lIYGp6qq1QlyIydNh',
            'secret' => '6LfPm2UUAAAAAEsgqiBkn4s16OuytcFoOPQBj985'
        ],

        'errorHandler' => [
            'errorAction' => 'home/error',
        ],

        'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => !YII_DEBUG,
            'concatCss' => true, // concatenate css
            'minifyCss' => true, // minificate css
            'concatJs' => true, // concatenate js
            'minifyJs' => true, // minificate js
            'minifyOutput' => true, // minificate result html page
            'webPath' => '@web', // path alias to web base
            'basePath' => '@webroot', // path alias to web base
            'minifyPath' => '@webroot/assets', // path alias to save minify result
            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expandImports' => true, // whether to change @import on content
            'compressOptions' => ['extra' => true], // options for compress
            'excludeFiles' => [
                '\.min\.(js|css)$'
            ],
            'excludeBundles' => [
//                '\app\assets\ExternalAsset',
                'yii\web\JqueryAsset'
            ],
        ]
    ]
];

unset($webConfig['components']['view']);

if (!YII_DEBUG) {
    $webConfig['components']['assetManager'] = [
        'bundles' => [
            'yii\web\JqueryAsset' => [
                'sourcePath' => null,
                'js' => [
                    '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js',
                ]
            ],
        ],
    ];
}

return extend('web', 'common', $webConfig);
