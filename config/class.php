<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 0bb6a74ef34fc9714682dfed212e951798ed60b6
 */

require_once 'functions.php';

return local('class', [
    'yii\data\Pagination' => [
        'defaultPageSize' => 30,
        'pageSizeParam' => 'limit',
    ],
    'yii\data\Sort' => ['defaultOrder' => ['id' => SORT_DESC]],
    'yii\web\AssetConverter' => [
        'commands' => ['scss' => ['css', '/usr/local/bin/sass {from} {to}']],
    ]
]);
