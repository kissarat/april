<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: aae62cd8f4ed0ff6d7ec2450794cdb67922bde7a
 */

require_once 'functions.php';

return extend('console', 'common', [
    'controllerNamespace' => 'app\console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'app\console\migrations'
            ]
        ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ]
    ]
]);
