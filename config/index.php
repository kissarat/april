<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 6ff288b8d23f645d3abdbaa4f31b5434d8d63dcc
 */

require_once 'functions.php';

$config = CONSOLE
    ? optional('/config/console.php')
    : optional('/config/web.php');

if (YII_DEBUG) {
    require_once 'debug.php';
}

return $config;
