<?php

namespace app\admin\controllers;


use app\models\User;
use yii\data\ActiveDataProvider;

class UserController extends \app\controllers\UserController
{
    public function behaviors() {
        return array_merge_recursive(parent::behaviors(), [
            'access' => [
                'only' => ['index'],
                'rules' => ['member' => ['actions' => ['index']]]
            ]
        ]);
    }

    public function beforeAction($action) {
        if (in_array($action->id, ['index'])) {
            $this->layout = 'admin';
        }
        return parent::beforeAction($action);
    }

    public function actionIndex() {
        return $this->render('@app/admin/views/user/index', [
            'provider' => new ActiveDataProvider([
                'query' => User::find()
                    ->select(['nick', 'surname', 'forename', 'children', 'email', 'usd', 'btc', 'eth'])
                    ->from('admin_user')
            ])
        ]);
    }
}
