<?php

namespace app\admin\controllers;


use app\helpers\SQL;
use yii\web\Controller;

class AdminController extends Controller
{
    public function beforeAction($action) {
        $this->layout = 'admin';
        return parent::beforeAction($action);
    }

    public function actionIndex() {
        return $this->render('@app/admin/views/admin/index',
            SQL::queryOne('SELECT * FROM "statistics" LIMIT 1'));
    }
}
