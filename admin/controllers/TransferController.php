<?php

namespace app\admin\controllers;


use app\models\Transfer;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class TransferController extends Controller
{
    public function beforeAction($action) {
        if (in_array($action->id, ['index'])) {
            $this->layout = 'admin';
        }
        return parent::beforeAction($action);
    }

    public function actionIndex() {
        return $this->render('@app/admin/views/transfer/index', [
            'provider' => new ActiveDataProvider([
                'query' => Transfer::find()
            ])
        ]);
    }
}
