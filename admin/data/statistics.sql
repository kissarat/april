CREATE VIEW "statistics" AS
  SELECT
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'payment' AND status = 'success'), 0) :: BIGINT  AS payment,
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'payment' AND status = 'pending'), 0) :: BIGINT  AS pending,
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'withdraw' AND status = 'success'), 0) :: BIGINT AS withdraw,
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'accrue' AND status = 'success'), 0) :: BIGINT   AS accrue,
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'buy' AND status = 'success'), 0) :: BIGINT      AS buy,
    (SELECT count(*)
     FROM "user")                                                           AS users,
    (SELECT count(*)
     FROM "user" ud
     WHERE ud.created > CURRENT_TIMESTAMP -
                        INTERVAL '1 day')                                   AS users_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires >
                                CURRENT_TIMESTAMP)                          AS visitors,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                                AS visitors_day,
    0                                                                       AS new_visitors_day,
    0                                                                       AS activities_day,
    (SELECT count(*)
     FROM visit
     WHERE url LIKE '/ref/%' AND visit.created > CURRENT_TIMESTAMP -
                                                 INTERVAL '1 day')          AS referral_day,
    0                                                                       AS balance;

CREATE OR REPLACE VIEW "transfer_statistics" AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW "transfer_week" AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE created > CURRENT_TIMESTAMP - INTERVAL '1 week' AND status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW "transfer_day" AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE created > CURRENT_TIMESTAMP - INTERVAL '1 day' AND status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW "top_referrals" AS
  WITH t AS (
      SELECT
        url,
        count(*) as "count"
      FROM visit
      WHERE url LIKE '/ref/%'
      GROUP BY url
  )
  SELECT
    replace(url, '/ref/', '') AS nick,
    "count"
  FROM t
  WHERE "count" > 1
  ORDER BY "count" DESC;
