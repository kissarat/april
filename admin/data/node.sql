create view "node_summary" as
  with a as (select
               id,
               node,
               amount,
               n
             from transfer
             where type = 'accrue' and status = 'success')
  select
    b.id,
    b."from"                                            as "user",
    b.amount,
    b.currency,
    b.created,
    p.percent,
    p.days,
    p.name,
    u.nick,
    (b.created + ("days" * interval '1 day')) :: date   as closing,
    coalesce((select max(n)
              from a
              where a.node = b.id) :: int, 0) :: int    as "count",
    coalesce((select sum(amount)
              from a
              where a.node = b.id) :: int, 0) :: bigint as "income"
  from transfer b
    join "user" u on b."from" = u.id
    join program p on b.node = p.id
  where b.type = 'buy';
