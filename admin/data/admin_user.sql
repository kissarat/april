CREATE VIEW "admin_user" AS
  SELECT
    u.id,
    u.nick,
    u.type,
    u.admin,
    u.surname,
    u.forename,
    u.skype,
    u.avatar,
    u.email,
    u.parent,
    s.nick                                                      AS sponsor,
    coalesce((SELECT count(*) :: INT
              FROM "user" c
              WHERE c.parent = u.id), 0)                        AS children,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'USD'), 0) AS usd,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'BTC'), 0) AS btc,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'ETH'), 0) AS eth,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'LTC'), 0) AS ltc,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'ETC'), 0) AS etc,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id AND b.currency = 'DOGE'), 0) AS doge,
    coalesce((SELECT b.amount
              FROM consolidated b
              WHERE b."user" = u.id), 0) AS consolidated
  FROM "user" u LEFT JOIN "user" s ON u.parent = s.id;
