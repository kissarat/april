CREATE OR REPLACE VIEW "withdrawable" AS
  WITH a AS (
      SELECT
        t.id,
        t.amount,
        t.wallet,
        t.system,
        t.currency,
        t."from",
        u.nick                           AS "from_nick",
        coalesce(b.amount, 0)            AS balance,
        t.txid,
        coalesce(b.amount, 0) - t.amount AS remain,
        t.ip,
        t.text,
        t.vars,
        t.created
      FROM transfer t
        LEFT JOIN balance b ON t."from" = b."user" AND t.currency = b.currency
        LEFT JOIN "user" u ON t."from" = u.id
      WHERE t.type = 'withdraw' AND t.status = 'created' AND t.wallet IS NOT NULL
            AND t.txid IS NULL AND t.node IS NULL AND t.amount > 0 AND t.ip IS NOT NULL
  )
  SELECT
    *,
    (CASE WHEN remain >= 0
      THEN 'success'
     ELSE 'canceled' END) AS will
  FROM a;
