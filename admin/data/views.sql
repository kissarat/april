CREATE VIEW "token_user" AS
  SELECT
    t.created,
    t.id,
    t.ip,
    t.time,
    t.type,
    t.expires,
    u.email,
    u.nick,
    u.admin,
    u.avatar,
    u.parent,
    row_to_json(u.*) AS "user"
  FROM token t
    LEFT JOIN "user" u ON t.user = u.id;

CREATE VIEW "balance" AS
  WITH a AS (
    SELECT
      "to" AS "user",
      "currency",
      amount
    FROM transfer
    WHERE "to" IS NOT NULL AND status = 'success'
    UNION ALL
    SELECT
      "from"  AS "user",
      "currency",
      -amount AS amount
    FROM transfer
    WHERE "from" IS NOT NULL AND status = 'success'
  )
  SELECT
    "user",
    "currency",
    sum("amount") as "amount"
  FROM a
  GROUP BY "user", "currency";

CREATE VIEW "consolidated" AS
  WITH a AS (
    SELECT
      "to" AS "user",
      amount
    FROM transfer_rate
    WHERE "to" IS NOT NULL
    UNION ALL
    SELECT
      "from"  AS "user",
      -amount AS amount
    FROM transfer_rate
    WHERE "from" IS NOT NULL
  )
  SELECT
    "user",
    sum("amount") as "amount"
  FROM a
  GROUP BY "user";

CREATE OR REPLACE VIEW "user_log" AS
  SELECT
    l.id,
    l."user",
    u.nick,
    l.entity,
    l.action,
    l.ip,
    l.data
  FROM log l LEFT JOIN "user" u ON l."user" = u.id;

