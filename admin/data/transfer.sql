CREATE OR REPLACE VIEW "transfer_user" AS
  SELECT
    tr.id,
    tr.amount,
    tr.type,
    tr.status,
    tr.wallet,
    tr.ip,
    tr.created,
    tr.time,
    tr."from",
    tr."to",
    tr.currency,
    tr.txid,
    f.nick AS from_nick,
    t.nick AS to_nick
  FROM transfer tr
    LEFT JOIN "user" f ON f.id = tr."from"
    LEFT JOIN "user" t ON t.id = tr."to";
