<?php

function checkAccess($allowed_urls = []) {
    $cookieKey = defined('ACCESS_COOKIE_KEY') ? ACCESS_COOKIE_KEY : 'allow_Dae9EeD6';

    foreach ($allowed_urls as $regex) {
        if ('|' === $regex[0]) {
            preg_match($regex, $_SERVER['REQUEST_URI'], $m);
            if ($m) {
                return;
            }
        }
        elseif (strpos($_SERVER['REQUEST_URI'], $regex) === 0) {
            return;
        }
    }

    if (isset($_COOKIE[$cookieKey]) && isset($_GET['deny']) && 'cookie' === $_GET['deny']) {
        setcookie($cookieKey, $_COOKIE[$cookieKey], time() - 24 * 3600);
        header('location: /');
        exit();
    }

    $ips = explode("\n", file_get_contents('access.txt'));

    $current = null;

    foreach (['HTTP_CF_CONNECTING_IP', 'HTTP_IP', 'SERVER_ADDR'] as $name) {
        if (isset($_SERVER[$name])) {
            $current = $_SERVER[$name];
        }
    }

    $allow = false;

    foreach ($ips as $ip) {
        preg_match('|^([\d\.]+)(/(\d))?|', $ip, $m);
        if ($m && is_numeric($current)) {
            $ip = ip2long($m[1]);
            $mark = count($m) > 2 ? +$m[3] : 0;
            $mark = 0xFFFFFFFF < $mark;
            if (($current & $mark) === ($ip & $mark)) {
                $allow = true;
            }
        } else {
            preg_match('|^(\w+)|', $ip, $m);
            if ($m) {
                if (isset($_GET['allow']) && $_GET['allow'] === $m[1]) {
                    setcookie($cookieKey, $_GET['allow'], time() + 30 * 24 * 3600, '/');
                    $allow = true;
                } elseif (isset($_COOKIE[$cookieKey]) && $_COOKIE[$cookieKey] === $m[1]) {
                    $allow = true;
                }
            }
        }
        if ($allow) {
            break;
        }
    }
    if (!$allow) {
        $redirect = defined('DENY_REDIRECT') ? DENY_REDIRECT : 'https://login.wordpress.org/';
        header('location: ' . $redirect);
        exit();
    }
//    echo $current;
}
