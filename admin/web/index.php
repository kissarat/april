<?php

require_once 'access.php';
checkAccess(['/go-']);

$config = require '../config/web.php';

//require_once '../vendor/yiisoft/yii2/BaseYii.php';
require '../../vendor/yiisoft/yii2/Yii.php';
require '../../vendor/autoload.php';

if (YII_DEBUG) {
    require '../../config/debug.php';
}

$config['language'] = empty($_COOKIE['locale']) ? 'ru' : $_COOKIE['locale'];

$app = new yii\web\Application($config);

foreach (require '../../config/class.php' as $class => $config) {
    \Yii::$container->set($class, $config);
}

$app->run();
