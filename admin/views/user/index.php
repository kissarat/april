<?php
/**
 * @var \yii\data\ActiveDataProvider $provider
 */
use app\helpers\Html;
use app\models\User;
use yii\grid\GridView;

?>

<div class="user index">
    <h1>Users</h1>
    <?= GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            [
                'attribute' => 'nick',
                'format' => 'html',
                'value' => function (User $model) {
                    return Html::a($model->nick, ['user/profile', 'nick' => $model->nick]);
                }
            ],
            'surname',
            'forename',
            'children',
            'email',
            'usd',
            'btc',
            'eth',
            [
                'format' => 'html',
                'value' => function (User $model) {
                    return Html::a('edit', ['settings/profile', 'nick' => $model->nick]);
                }
            ]
        ]
    ]) ?>
</div>
