<div class="admin index">
    <div class="informer">
        <div id="payment">
            <div class="name">Пополнения</div>
            <div class="value usd"><?= $payment ?></div>
        </div>
        <div id="pending">
            <div class="name">Ожидания подтверждения</div>
            <div class="value usd"><?= $pending ?></div>
        </div>
        <div id="withdraw">
            <div class="name">Вывод</div>
            <div class="value usd"><?= $withdraw ?></div>
        </div>
        <div id="accrue">
            <div class="name">Начисления</div>
            <div class="value usd"><?= $accrue ?></div>
        </div>
        <div id="buy">
            <div class="name">Куплено на сумму</div>
            <div class="value usd"><?= $buy ?></div>
        </div>
        <div id="balance">
            <div class="name">На балансах</div>
            <div class="value usd"><?= $balance ?></div>
        </div>
        <div id="users">
            <div class="name">Зарегистрировано</div>
            <div class="value"><?= $users ?></div>
        </div>
        <div id="users_day">
            <div class="name">Зарегистрировано за сутки</div>
            <div class="value"><?= $users_day ?></div>
        </div>
        <div id="visitors">
            <div class="name">Посетителей</div>
            <div class="value"><?= $visitors ?></div>
        </div>
        <div id="visitors_day">
            <div class="name">Посетителей за сутки</div>
            <div class="value"><?= $visitors_day ?></div>
        </div>
        <div id="new_visitors_day">
            <div class="name">Новых посетителей за сутки</div>
            <div class="value"><?= $new_visitors_day ?></div>
        </div>
        <div id="activities_day">
            <div class="name">Посещений за сутки</div>
            <div class="value"><?= $activities_day ?></div>
        </div>
        <div id="referral_day">
            <div class="name">Переходов по рефcсылке за сутки</div>
            <div class="value"><?= $referral_day ?></div>
        </div>
    </div>
</div>
