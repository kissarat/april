<?php
$config = require_once '../../config/web.php';

return array_replace_recursive($config, [
    'controllerMap' => [
        'admin' => 'app\admin\controllers\AdminController',
        'user' => 'app\admin\controllers\UserController',
    ],
    'params' => [
        'admin' => true
    ]
]);
