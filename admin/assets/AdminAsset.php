<?php

namespace app\admin\assets;


use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@app/admin/web';
    public $baseUrl = '@web';
    public $css = [
        '/style.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\ExternalAsset',
    ];
}
