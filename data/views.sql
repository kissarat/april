create view referral as
  with recursive r(root, id, parent, level) as (
    select
      id as root,
      id,
      parent,
      0  as level
    from "user"
    union
    select
      r.root,
      u.id,
      u.parent,
      r.level + 1 as level
    from "user" u
      join r on u.parent = r.id
    where level < 5
  )
  select
    id,
    nick,
    created,
    skype,
    surname,
    forename,
    parent,
    (select count(*)
     from "user" c
     where c.parent = p.id) as children
  from "user" p;
