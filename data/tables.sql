CREATE TYPE "USER_TYPE" AS ENUM ('new', 'native', 'leader', 'special');

-- delete from rate where date_part('minute', time) <> 0;

CREATE TABLE "user" (
  id       SERIAL PRIMARY KEY                                  NOT NULL,
  nick     CHARACTER VARYING(24)                               NOT NULL,
  email    CHARACTER VARYING(48)                               NOT NULL,
  type     "USER_TYPE" DEFAULT 'new' :: "USER_TYPE"            NOT NULL,
  parent   INTEGER,
  secret   CHARACTER(60),
  admin    BOOLEAN DEFAULT FALSE                               NOT NULL,
  surname  CHARACTER VARYING(48),
  forename CHARACTER VARYING(48),
  avatar   CHARACTER VARYING(192),
  skype    CHARACTER VARYING(32),
  address  CHARACTER VARYING(128),
  ethereum CHARACTER VARYING(42),
  created  TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT now(),
  "time"   TIMESTAMP(0) WITHOUT TIME ZONE,
  auth     CHAR(64)
);

CREATE TYPE TOKEN_TYPE AS ENUM (
  'server',
  'browser',
  'app',
  'code'
);

CREATE TABLE token (
  id        CHARACTER VARYING(192)                                                                      NOT NULL,
  "user"    INTEGER,
  type      TOKEN_TYPE                                                                                  NOT NULL DEFAULT 'browser',
  expires   TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT '2030-01-01 00:00:00' :: TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  data      JSON,
  ip        INET,
  handshake TIMESTAMP,
  name      CHARACTER VARYING(240),
  created   TIMESTAMP(0) WITHOUT TIME ZONE                                                                       DEFAULT now(),
  "time"    TIMESTAMP(0) WITHOUT TIME ZONE
);

CREATE TYPE "ARTICLE_TYPE" AS ENUM ('cat', 'page', 'product', 'ticket', 'note');

CREATE TABLE article (
  id       SERIAL PRIMARY KEY NOT NULL,
  path     VARCHAR(96) UNIQUE,
  "type"   "ARTICLE_TYPE"     NOT NULL DEFAULT 'page',
  price    INT,
  name     VARCHAR(96)        NOT NULL,
  short    VARCHAR(512),
  time     TIMESTAMP,
  created  TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text     VARCHAR(48000),
  image    VARCHAR(256),
  "user"   INT REFERENCES "user" (id),
  priority SMALLINT           NOT NULL DEFAULT 0,
  active   BOOLEAN            NOT NULL DEFAULT TRUE
);

CREATE TABLE "visit" (
  "id"      SERIAL PRIMARY KEY,
  "token"   VARCHAR(240),
  "url"     VARCHAR(240) NOT NULL,
  "spend"   INT,
  ip        INET,
  agent     VARCHAR(240),
  "created" TIMESTAMP DEFAULT current_timestamp
);

CREATE TYPE "TRANSFER_TYPE" AS ENUM ('internal', 'payment', 'withdraw', 'accrue', 'buy', 'earn', 'support',
  'write-off', 'bonus', 'loan');
CREATE TYPE "TRANSFER_STATUS" AS ENUM ('created', 'success', 'fail', 'canceled', 'pending', 'virtual');
CREATE TYPE "TRANSFER_SYSTEM" AS ENUM ('internal', 'payeer', 'perfect', 'advcash', 'crypto');
CREATE TYPE CURRENCY AS ENUM ('USD', 'BTC', 'ETH', 'XRP', 'ETC', 'LTC', 'DOGE');

CREATE TABLE transfer (
  id       SERIAL PRIMARY KEY,
  "from"   INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"     INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  amount   INT8              NOT NULL,
  "type"   "TRANSFER_TYPE"   NOT NULL,
  "status" "TRANSFER_STATUS" NOT NULL,
  node     INT,
  text     VARCHAR(192),
  vars     JSON,
  wallet   VARCHAR(64),
  txid     VARCHAR(130),
  currency CURRENCY,
  system   "TRANSFER_SYSTEM",
  ip       INET,
  created  TIMESTAMP         NOT NULL        DEFAULT CURRENT_TIMESTAMP,
  time     TIMESTAMP,
  visible  BOOLEAN           NOT NULL        DEFAULT TRUE
);

CREATE TABLE log (
  id     BIGINT PRIMARY KEY,
  entity VARCHAR(16) NOT NULL,
  action VARCHAR(32) NOT NULL,
  ip     INET,
  "user" INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  token  CHARACTER VARYING(192),
  data   JSON
);

CREATE TABLE email (
  id      BIGSERIAL PRIMARY KEY,
  "to"    VARCHAR(192),
  subject VARCHAR(192),
  content TEXT,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sent    TIMESTAMP
);

CREATE TABLE program (
  id       SMALLINT PRIMARY KEY,
  currency CURRENCY  NOT NULL,
  range    INT8RANGE NOT NULL
);

INSERT INTO program (id, range, currency) VALUES
  (1, '[1000,10000)', 'USD'),
  (2, '[10000,1000000)', 'USD'),
  (3, '[1000000,100000000)', 'USD');
