<?php

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\YiiAsset;

class ExternalAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/lib';

    public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css',
        '/style.css'
    ];

    public $js = [
//        YII_DEBUG ? 'es6-shim.js' : 'https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js',
//        YII_DEBUG ? 'vue.js' : 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js',
//        YII_DEBUG ? 'd3.js' : 'https://cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-shim.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/feather-icons/4.7.3/feather.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment-with-locales.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/fetch/2.0.4/fetch.min.js',
        '/js/track.js',
        '/js/chart.js',
        '/js/app.js',
        '/js/script.js'
    ];

    public $depends = [
        YiiAsset::class,
//        BootstrapAsset::class,
    ];
}
