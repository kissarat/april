/**
 * Works in Internet Explorer 11
 */

function Logist() {
  const data = this.state
  if (!this.isGuest) {
    $.extend(data, {
      buy: 0,
      accrue: 0,
      bonus: 0,
      withdraw: 0,
      monthIncome: 0,
      monthExpense: 0,
      menu: false,
      month: new Date().getMonth(),
      months: {}
    })
    for (var i = new Date(data.user.created).getMonth(); i <= data.month; i++) {
      data.months[i] = moment.months('MMMM', i)
    }
    if (data.accounts) {
      const accounts = {};
      for (var id in data.accounts) {
        accounts[id] = new App.Account(data.accounts[id], id)
      }
      data.accounts = accounts
    }
  }
}

Logist.loadVue = function (self, selector) {
  const vue = new Vue({
    el: selector,
    data: self.state,
    computed: {
      refbackPercent: function () {
        return Math.round(this.refback * 100)
      },
      balance: function () {
        return this.balances[this.currency]
      },
      summary: function () {
        return self.rate(this.amount * this.quantity)
      },
      canPay: function () {
        return this.system && this.currency
      },
      nextGrade: function () {
        const id = this.grade.id + 1;
        return this.grades.find(function (g) {
          return id === g.id
        })
      },
      account: function () {
        for (var id in this.accounts) {
          const a = this.accounts[id]
          if (this.currency === a.currency && this.system === a.system) {
            return a
          }
        }
      },
      minimal: function () {
        return this.account ? this.account.min / this.factor[this.currency] : null
      },
      accountWallet: function () {
        return this.account ? this.account.wallet : null
      },
      isWalletReadonly: function () {
        return 'USD' === this.currency && this.accountWallet
      },
      pattern: function () {
        return this.account && this.account.pattern ? this.account.pattern : '.*'
      }
    },
    methods: {
      load: function (currency) {
        if (self.isDashboard) {
          $.getJSON('/summary/' + currency, function (r) {
            $.extend(self.state, r.result)
            self.state.currency = currency
            self.loadChart()
          })
        }
        else {
          this.currency = currency
        }
      },

      t: function (text, vars) {
        return self.t(text, vars)
      },

      changeLanguage: function (code) {
        self.cookies.set('locale', code)
        location.reload()
      },

      getRated: function (id, prop) {
        // var v =
        // const factor = this.factor[this.currency]
        // if ('USD' !== this.currency) {
        //     v = v * factor / (this.rates[this.currency] * 100)
        // }
        return self.rate(this.programs[id][prop] / 100) * this.factor[this.currency]
      },

      filterAccounts: function (blockchain) {
        return Object.values(this.accounts).filter(function (account) {
          return blockchain === account.blockchain
        })
      },

      setAccount: function (currency, system) {
        this.currency = currency
        this.system = system
        this.wallet = 'USD' === currency ? this.accountWallet : null
      }
    },
    watch: {
      currency: function (value) {
        self.cookies.set('currency', value)
      }
    }
  })
  if (self.isDevMode) {
    self.features[selector] = vue
  }
  return vue
}

Logist.prototype = {
  init: function () {
    this.bootstrap({
      structure: ['.structure.index tbody', 5]
    })
    $('.autosubmit').submit()
  },

  view: function (o) {
    return new Vue(o)
  }
}

Logist.features = {
  redirectToProfile: function () {
    if (!this.isGuest && /^\/(login|sign-up|reset|ref\/)/.test(location.pathname)) {
      location.pathname = '/'
    }
  },

  lotteryDraw: function () {
    const draw = $('#draw-time')
    const html = draw.html()
    if ('string' === typeof html && 'function' === typeof html.trim) {
      draw.html(moment(html.trim()).format('D MMM'))
    }
  },

  verification: function () {
    this.later(function () {
      $('.settings.verification .choose-file button').click(function (e) {
        $(e.target).parent().find('[type="file"]').click()
      })
      $('.settings.verification .choose-file [type="file"]').change(function (e) {
        if (e.target.files.length > 0) {
          const p = $(e.target).parent().parent();
          p.find('button').html(e.target.files[0].name)
          p.parent().find('img').remove()
        }
      })
    })
  }
}

document.addEventListener('DOMContentLoaded', function () {
  feather.replace()

  const app = App.create(appConfig, Logist)
  app.init();

  if (document.querySelector('#language')) {
    Logist.loadVue(app, '#language')
  }
  if (document.querySelector('#balance')) {
    Logist.loadVue(app, '#balance')
  }

  if (document.querySelector('#content') && (['/promotion', '/banners'].indexOf(location.pathname) < 0)) {
    const vue = Logist.loadVue(app, '#content')
    vue.load(vue.currency)
  }

  if (document.querySelector('.menu-button')) {
    const menuState = {menu: false}
    new Vue({
      el: '.menu-button',
      data: menuState
    })
    new Vue({
      el: '#menu-container',
      data: menuState
    })
  }

  // if ('/banners' === location.pathname) {
  //   $('#banners-fancybox-list').fancybox({
  //     modal: true,
  //     selector: '[data-fancybox="banners"]',
  //     loop: true,
  //     animationEffect: true,
  //     'transitionIn': 'elastic',
  //     'transitionOut': 'elastic',
  //     'speedIn': 600,
  //     'speedOut': 200,
  //     'overlayShow': true,
  //     'protect': true,
  //     showNavArrows: true
  //   })
  // }
})
