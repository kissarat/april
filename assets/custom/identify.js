function *$combine(...seq) {
  for (let i = 1; i <= seq.length; i++) {
    for (let j = 0; j <= seq.length - i; j++) {
      yield seq.slice(j, j + i)
    }
  }
}

const denyAttrs = ['class', 'style', 'width', 'height', 'value', 'd', 'viewBox', 'transform']

const rareTags = ['form',
  'footer',
  'header', 'article', 'section', 'aside', 'img', 'cite', 'optgroup', 'tbody', 'tr', 'td',
  'table', 'acronym', 'address', 'ul', 'ins', 'fieldset', 'abbr', 'rel', 'wbr', 'svg']

function *y(el) {
  const tagName = el.tagName.toLowerCase()
  const names = [].filter.call(el.attributes, a => a.value && denyAttrs.indexOf(a.name) < 0
  && a.name.indexOf(':') < 0
  && a.value.indexOf('{') < 0)
  for (const attrs of $combine(...names)) {
    yield attrs.map(a => `[${a.name}="${a.value}"]`).join('')
  }
  for (const c of $combine(...el.classList.values())) {
    yield c.map(cc => '.' + cc).join('')
  }
  const count = el.parentNode.childElementCount
  if (count > 1) {
    let i
    switch (tagName) {
      case 'td':
        i = el.cellIndex + 1
        break
      case 'tr':
        i = el.rowIndex + 1
        break
      default:
        i = [].indexOf.call(el.parentNode.children, el) + 1
        break
    }
    if (i > 0) {
      yield tagName + `:nth-child(${i})`
    }
  }
  if (rareTags.indexOf(tagName) >= 0) {
    yield tagName
  }
}

function $check(selector) {
  return document.querySelectorAll(selector).length === 1 ? selector : false
}

function * $identity(el) {
  if (el.id) {
    yield el.id.indexOf(':') >= 0 ? `[id="${el.id}"]` : '#' + el.id
  }

  for (const parts of y(el)) {
    yield parts
  }
}

function * deep(array, ...head) {
  if (head.length > 0) {
    for (const a of array) {
      for (const b of deep(...head)) {
        yield [a, ...b]
      }
    }
  }
  else {
    for (const a of array) {
      yield [a]
    }
  }
}

function * parental(el) {
  let parent = el
  while ((parent = parent.parentNode) && parent !== document.documentElement) {
    yield parent
  }
}

function checkElement(el) {
  for (const selector of $identity(el)) {
    if ($check(selector)) {
      return selector
    }
  }
}

function * group(...parents) {
  for (let i = 1; i <= parents.length; i++) {
    for (const comb of deep(...(parents.slice(0, i).reverse().map($identity)))) {
      yield comb.join(' ')
      // if (comb.length > 1) {
      //   yield comb.join('>')
      // }
    }
  }
}

function $selector(el) {
  for (const selector of group(el, ...parental(el))) {
    // console.log(selector)
    if ($check(selector)) {
      return selector
    }
  }
}

$identity.initialized = false
$identity.active = false
var zen;
var informer;

(function () {
  if (!$identity.initialized) {
    zen = document.createElement('div')
    zen.id = 'zen'
    informer = document.createElement('div')
    zen.appendChild(informer)
    document.body.appendChild(zen)
    $identity.initialized = true
  }

  var selector

  $identity.mouseDown = function mouseDown(e) {
    e.preventDefault()
    if (window.clipboardData && clipboardData.setData instanceof Function) {
      clipboardData.setData('text/plain', informer.innerHTML)
    }
    else if (document.execCommand instanceof Function) {
      const input = document.querySelector('[name="zen"]')
      input.value = selector
      input.style.display = 'block'
      input.focus()
      input.select()
      document.execCommand('copy')
    }
    // zen.innerHTML = 'Copied'
    // setTimeout(function () {
    //   zen.innerHTML = ''
    // }, 600)
  }

  $identity.mouseMove = function mouseMove(e) {
    const el = document.elementFromPoint(e.x, e.y)
    selector = $selector(el)
    if (selector && informer.innerHTML !== selector) {
      const el = document.querySelector(selector)
      const rect = el.getBoundingClientRect()
      // if (e.y < 70) {
      //   informer.style.top = '100px'
      // }
      informer.innerHTML = selector
      Object.assign(zen.style, {
        left: rect.left - 1 + 'px',
        top: rect.top - 1 + 'px',
        width: rect.width + 'px',
        height: rect.height + 'px'
      })
    }
  }
})()

addEventListener('keyup', function (e) {

  if ('`' === e.key) {
    if (!$identity.active) {
      zen.style.removeProperty('display')
      document.body.addEventListener('mousedown', $identity.mouseDown)
      document.body.addEventListener('mousemove', $identity.mouseMove)
    }
    $identity.active = true
  }
  else if ('Escape' === e.key) {
    document.body.removeEventListener('mousemove',$identity. mouseMove)
    document.body.removeEventListener('mousedown', $identity.mouseDown)
    zen.style.display = 'none'
  }
})
