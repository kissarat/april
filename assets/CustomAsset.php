<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 56dfb76c30298cb9379fc4d9e735dc9e8850e935
 */

namespace app\assets;


use yii\web\AssetBundle;


class CustomAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/custom';

    public $css = [
//        'style.scss'
    ];
    public $js = [
//        'identify.js',
//        'chart.js',
//        'track.js',
//        'app.js',
//        'script.js'
    ];

    public $depends = [
        ExternalAsset::class
    ];

    protected static function &postfix($array)
    {
        $new = [];
        foreach ($array as $src) {
//            $src .= '?v=' . Yii::$app->version;
            if (YII_DEBUG && strpos($src, 'antiquar') !== false) {
                $src .= '?t=' . time();
            }
            $new[] = $src;
        }
        return $new;
    }

    public function init()
    {
        parent::init();
        $this->css = static::postfix($this->css);
        $this->js = static::postfix($this->js);
    }

    /**
     * @param \yii\web\AssetManager $am
     * @throws \yii\base\ErrorException
     */
//    public function publish($am)
//    {
//        if (!YII_DEBUG) {
//            $am->converter->commands['scss'] = ['css', 'sass {from} {to} --sourcemap=none && rm {from}'];
//        }
//        parent::publish($am);
//        if (!YII_DEBUG) {
//            FileHelper::removeDirectory($this->basePath . '/.sass-cache');
//        }
//    }
}
