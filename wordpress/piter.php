<?php

require_once 'wp-config.php';

$pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
$answer = [
    'success' => false,
    'time' => date('Y-m-d H:i:s'),
    'ip' => $_SERVER['REMOTE_ADDR'],
    'timezone' => date_default_timezone_get(),
    'space' => [
        disk_free_space('/') / (1000 * 1000 * 1000)
    ]
];
foreach ($pdo->query('show variables like "character_set_database"') as $row) {
    $answer['success'] = true;
    $answer['charset'] = $row['Value'];
}
$pdo = null;

http_response_code($answer['status'] ? 200 : 500);
header('content-type: application/json');
echo json_encode($answer);
