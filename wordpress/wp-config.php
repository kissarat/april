<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'ai-logist');

/** Имя пользователя MySQL */
define('DB_USER', 'ai-logist');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'theetat4uuvahk0ooj7Chifo');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%s$xf^GaNj-05pB-Zxx<blfvb/o =]9P3>ii!^V^kG=9>:QDB6Xo!;9t!^M:j!PD');
define('SECURE_AUTH_KEY',  '%).v_QiZmJqk296[bCg=r/_@~2Y$}EmXF9l8i*IZS%znxe5_H%j2;gKE}-.{,UX!');
define('LOGGED_IN_KEY',    ':W]_BQhDB<C/1I7[in)<U92|*qu<fG2@UX$rk`tFo}Y#[HK1#< `=2v|a];~hlsf');
define('NONCE_KEY',        '|MyV|t}@_R<Ck6}i%{GI-Y.+PG[<r~<Uif6J<.s|$spmN!oVf7Ib[0<D0~5&#U67');
define('AUTH_SALT',        '/fXF4 0Ggg-S|.p04;#>(HHapQv*1lVi4UVu=@h_j@}AB=2.$U+rVA G/,h:bweY');
define('SECURE_AUTH_SALT', 'Q,j&|LsVshpe~?%1;(,8BNK4@e)%A^$G$y01MB*TFlMQ&7p^[1L&t`-`sp2>!YVi');
define('LOGGED_IN_SALT',   'c)~b_m%AFvAd(]j5:442pId6.qp%@JqA`ra0t2pYO(R[9~`Wt8%KquS;FGL M-LH');
define('NONCE_SALT',       'u?n:Pgit8pwg9MY87J2[743Fb)#:t?r:p .dup6m|Q9)n5=<9`EaXNs[u.:*c|p2');

define('WPLANG', 'ru_RU');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
