<?php

namespace app\widgets;


use app\helpers\Html;
use Yii;
use yii\base\Widget;

class Alert extends Widget
{
    public $types = [
        'error' => 'alert-danger',
        'info' => 'alert-info',
        'success' => 'alert-success',
        'warning' => 'alert-warning',
    ];


    public function run()
    {
        $alerts = [Html::tag('div', '{{ a.message }}
                <button type="button" class="close" aria-label="Close" v-on:click="remove(i)">
                    <span aria-hidden="true">&times;</span></button>', [
            'v-for' => '(a, i) in alerts',
            'v-bind:class' => "'alert alert-' + ('error' === a.type ? 'danger' : a.type)",
            'role' => 'alert'
        ])];
        $flashes = Yii::$app->session->getAllFlashes(true);
        if (YII_DEBUG) {
            foreach ($this->types as $type => $class) {
                $v = Yii::$app->request->get($type);
                if (!empty($v)) {
                    $flashes[$type][] = $type . ' message';
                }
            }
        }
        foreach ($flashes as $type => $messages) {
            $type = $this->types[$type] ?? 'alert-info';
            if (!is_array($messages)) {
                $messages = [$messages];
            }
            foreach ($messages as $message) {
                $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>';
                $alerts[] = Html::tag('div', $message, [
                    'class' => 'alert ' . $type,
                    'role' => 'alert'
                ]);
            }
        }
        return Html::tag('div', implode("\n", $alerts), [
            'class' => 'widget-alert',
            'v-cloak' => true,
            'data-count' => \count($alerts)
        ]);
    }
}
