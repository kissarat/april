<?php

$json = [];

$node = ast\parse_file(__DIR__ . '/../models/Career.php', $code = 50);

function &walk(&$node)
{
    $k = ast\get_kind_name($node->kind);
    $k = str_replace('AST_', '', $k);
    $json = [$k];
    if (count($node->children) > 0) {
//        $a = [];
        foreach ($node->children as $key => $value) {
            if (is_object($value)) {
                $value = walk($value);
            }
            $json[] = $value;
        }

//        $json[] = $a;
    }
    return $json;
}

//$node = walk($node);
$node = walk($node);
//var_dump($node);
echo json_encode($node, JSON_PRETTY_PRINT);
