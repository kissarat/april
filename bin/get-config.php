<?php
/**
 * Last modified: 18.05.09 18:08:45
 * Hash: b1b7076dd4fce952513ae8c1e025d8cdbc3f24be
 */

define('ROOT', getenv('ROOT') ?: dirname(__DIR__));
defined('SITE') or define('SITE', getenv('SITE') ?: ROOT);

$data = json_encode(require SITE . '/config/console.php', JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
if (!CONSOLE) {
    header('content-type: application/json');
}
echo $data;
