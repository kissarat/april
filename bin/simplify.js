function visit(n) {
    if ('object' === typeof n) {
        if (!n) {
            return n
        }
        switch (n[0]) {
            case 'CONST':
                return 'CONST:' + n[1][1]
            case 'NAME':
                return 'NAME:' + n[1]
            case 'ARRAY_ELEM':
                return n[2] ? [n[2], n[1]] : n[1]
            case 'VAR':
                return '$' + n[1]
            // case 'ASSIGN':
            //     return [n[0], n[1][1], visit(n[2])]
            case 'CALL':
                return [n[0], visit(n[1]), ...n[2].slice(1).map(visit)]
            case 'STATIC_CALL':
                return [n[0], n[1][1], n[2], ...n[3].slice(1).map(visit)]
        }
        return [n[0], ...n.slice(1).map(visit)]
    }
    return n
}

process.stdin.resume()
process.stdin.setEncoding('utf8')
let buffer = ''
process.stdin.on('data', function(chunk) {
    buffer += chunk;
});

process.stdin.on('end', function() {
    console.log(JSON.stringify(visit(JSON.parse(buffer)), null, '   '))
});
