<?php
$r = $_GET['r'] ?? '';
$info = implode("\t", [
    date('y-m-d H:i:s'),
    $r ?: '-',
    $_SERVER['HTTP_IP'] ?? '-',
    $_SERVER['HTTP_REFERER'] ?? '-',
    $_SERVER['HTTP_USER_AGENT'] ?? '-',
]);

@file_put_contents('/var/log/referral.log', $info, FILE_APPEND);
header('location: /?r=' . $r);
