<?php
/**
 * Last modified: 18.05.22 10:55:21
 * Hash: 656596ffd8f6e5f960af4c167bae43d3f9f11dc3
 */

$headers = [];
foreach ($_SERVER as $key => $value) {
    if (strpos($key, 'HTTP_') === 0) {
        $key = preg_replace('/^HTTP_/', '', $key);
        $key = str_replace('_', '-', strtolower($key));
        $headers[$key] = $value;
    }
}

$json = [
    'url' => $_SERVER['REQUEST_URI'],
    'method' => $_SERVER['REQUEST_METHOD'],
    'headers' => $headers
];
if ('POST' === $json['method']) {
    $json['data'] = file_get_contents('php://input');
}
header('content-type: application/json');
echo json_encode($json);
