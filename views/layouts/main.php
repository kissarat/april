<?php
/**
 * Last modified: 18.08.26 16:09:26
 * Hash: 950afa7902d64e7cb8121023128bc0c29e15c1be
 */

use app\assets\CustomAsset;
use app\helpers\Html;
use app\helpers\SQL;
use app\models\Career;
use app\models\Grade;
use app\models\Transfer;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\widgets\Breadcrumbs;

CustomAsset::register($this);

$title = $this->title ? $this->title . ' — ' . Yii::$app->name : Yii::$app->name;

/**
 * @var \app\models\User $user
 */
$user = Yii::$app->user->identity;
$grade = Career::getUserGrade($user->id);

$state = [
    'grade' => $grade,
    'grades' => Grade::getList(),
    'balances' => Transfer::getAllBalances($user->id),
    'translation' => true
];

$context = Html::context(empty($this->params['state']) ? $state : array_merge($state, $this->params['state']));

$items = [
    [
        'label' => '<i data-feather="home"></i> ' . Yii::t('app', 'Profile'),
        'url' => [
            'user/profile',
            'nick' => $user->nick
        ]
    ],
    ['label' => '<i data-feather="dollar-sign"></i> ' . Yii::t('app', 'Investment plans'), 'url' => ['program/index']],
    ['visible' => $user->hasInvestments(), 'label' => '<i data-feather="calendar"></i> ' . Yii::t('app', 'My plans'), 'url' => ['node/index']],
    ['label' => '<i data-feather="heart"></i> ' . Yii::t('app', 'Lottery'), 'url' => ['lottery/participate']],
    ['label' => '<i data-feather="archive"></i> ' . Yii::t('app', 'Finances'), 'url' => ['transfer/index']],
    [
        'label' => '<i data-feather="activity"></i> ' . Yii::t('app', 'Structure'),
        'url' => [
            'structure/index',
            'nick' => $user->nick,
        ]
    ],
    ['label' => '<i data-feather="award"></i> ' . Yii::t('app', 'Career'), 'url' => ['career/index']],
    ['label' => '<i data-feather="instagram"></i> ' . Yii::t('app', 'Promotion'), 'url' => ['home/promotion']],
    ['label' => '<i data-feather="sliders"></i> ' . Yii::t('app', 'Calculator'), 'url' => ['program/calculator']],
    ['label' => '<i data-feather="image"></i> ' . Yii::t('app', 'Banners'), 'url' => ['home/banners']],
    ['label' => '<i data-feather="gift"></i> ' . Yii::t('app', 'Presentation'), 'url' => ['home/pdf']],
    ['label' => '<i data-feather="paperclip"></i> ' . Yii::t('app', 'Contract'), 'url' => ['home/contract']],
    ['label' => '<i data-feather="book"></i> ' . Yii::t('app', 'Book'), 'url' => ['home/book']],
];

foreach ($items as $i => $item) {
    $items[$i]['encode'] = false;
}

// 5KEWgQnwfLTACKtvv8LUi6ZqfvbQAXjRmByP2eq1vDczgcSSjrv15R3rJPiVDGzygCYb4AWfc1QjUbdGTLWYm

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>AI Logistics | <?= $title ?></title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?php require '_analytics_head.php' ?>
    <?= $this->head() ?>
    <link rel="shortcut icon" href="https://ai-logist.com/wp-content/uploads/2018/04/favicon2.png" type="image/x-icon">
    <?= Html::script($context) ?>
</head>
<body class="s1 s2 <?= $user->nick ?> <?= Yii::$app->user->getIsGuest() ? 'indoor' : 'outdoor' ?>"
      data-mode="<?= YII_ENV ?>"
      data-nick="<?= $user->nick ?>">
<?= $this->beginBody() ?>
<?php require '_analytics_body.php' ?>
<div id="app" <?= Html::xmlns() ?>>
    <div class="header">
        <div class="container">
            <div class="row">
                <header id="header">
                    <div v-on:click="menu = true" class="menu-button col-sm-2 col-xs-2 hidden-lg hidden-md">
                        <a href="#">
                            <i data-feather="menu"></i>
                        </a>
                    </div>
                    <div class="content col-lg-offset-3 col-lg-3 col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8">
                        <div id="balance" v-cloak>
                            <div class="value">{{ balances[currency] | factor(currency) }}</div>
                            <div class="btn-group">
                                <button type="button"
                                        class="btn dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        v-bind:data-currency="currency">
                                    {{ currency }}
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a v-for="currency in currencies"
                                       v-bind:data-currency="currency"
                                       v-on:click="load(currency)"
                                       v-bind:href="'#' + currency"
                                       class="select-balance dropdown-item">
                                        {{ balances[currency] | factor(currency) }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content col-lg-2 col-md-2 col-sm-3 col-xs-4">
                        <?= Html::a(Yii::t('app', 'Pay'),
                            ['invoice/payment', 'nick' => $user->nick], [
                                'id' => 'pay',
                                'class' => 'pay'
                            ]) ?>
                    </div>
                    <div class="content col-lg-2 col-md-2 col-sm-3 col-xs-5">
                        <?= Html::a(Yii::t('app', 'Withdraw'),
                            ['invoice/withdraw', 'nick' => $user->nick], [
                                'id' => 'withdraw',
                                'class' => 'withdraw'
                            ]) ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                        <div id="language" class="content btn-group lang" v-cloak>
                            <button id="lang" type="button" class="btn dropdown-toggle lang" data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false">
                                <i data-feather="globe"></i>
                                <span>{{ locale.toUpperCase() }}</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-item"
                                     v-for="(flag, code) in languages"
                                     v-on:click="changeLanguage(code)">
                                    <span>{{ code }}</span>
                                    <span class="name">{{ flag }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            <div data-feather="camera"></div>
        </div>
    </div>

    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-9 col-md-offset-3 col-md-9 col-sm-12 col-xs-12">
                    <?= Breadcrumbs::widget([
                        //        'homeLink' => false,
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'options' => ['class' => '']
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div id="main">
        <div class="container">
            <div class="row">
                <div id="menu-container" v-bind:class="{'open-menu': menu}" v-on:click="menu = false">
                    <div class="mobile-menu-bg"></div>
                    <div class="menu-mobile col-lg-3 col-md-3 col-sm-8 col-xs-8">
                        <div class="menu ">
                            <div class="logo">
                                <a href="http://ai-logist.com/" target="_blank">
                                    <img src="/images/logo-black.png" title="Logo AI Logistics"/>
                                </a>
                            </div>
                            <div class="info">
                                <div class="avatar-container">
                                    <?= Html::tag('div', '', [
                                        'class' => 'avatar',
                                        'title' => $user->getName(),
                                        'style' => empty($user->avatar) ? '' : "background-image: url('$user->avatar')"
                                    ]) ?>
                                </div>
                                <div class="name"><?= $user->getName() ?></div>
                                <div class="status"><i data-feather="award"></i> <?= Yii::t('app', $grade['name']) ?></div>
                            </div>

                            <div class="icons">
                                <span id="tooltip-settings" data-description="<?= Yii::t('app', 'Profile Settings') ?>">
                                    <?= Html::a('<i data-feather="settings"></i>', [
                                        'settings/profile',
                                        'nick' => $user->nick
                                    ]) ?>
                                </span>
                                <span id="tooltip-settings" data-description="<?= Yii::t('app', 'Change Password') ?>">
                                    <?= Html::a('<i data-feather="lock"></i>', [
                                        'settings/password',
                                        'nick' => Yii::$app->user->identity->nick
                                    ]) ?>
                                </span>
                                <span id="tooltip-settings" data-description="<?= Yii::t('app', 'Save Wallets') ?>">
                                    <?= Html::a('<i data-feather="shield"></i>', [
                                        'settings/wallet',
                                        'nick' => Yii::$app->user->identity->nick
                                    ]) ?>
                                </span>

                        

                                <span id="tooltip-settings" data-description="<?= Yii::t('app', 'Exit') ?>">
                                <?= Html::a('<i data-feather="power"></i>', [
                                        'user/logout', 'nick' => $user->nick
                                ]) ?>
                                </span>
                            </div>
                            <?= Nav::widget([
                                'items' => $items,
                                'class' => 'nav'
                            ]) ?>
                        </div>
                    </div>
                </div>
                <main>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <?= Alert::widget() ?>
                    </div>
                    <div id="content">
                        <?= $content ?>
                    </div>
                </main>
            </div>
        </div>
        <footer>
            <div class="main-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <div class="footer-logo">
                                <img src="/images/logo.png" alt="Logo AI Logistics">
                            </div>
                            <h5><?= Yii::t('app', 'Invest at 438% per annum') ?></h5>

                            <p class="footer-txt h6">
                                <?= Yii::t('app', 'Earn with large trade transactions and logistics') ?>
                            </p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="footer-h1">
                                <h3><?= Yii::t('app', 'Terms of cooperation') ?></h3>

                            </div>
                            <p class="footer-txt">
                                <?= Yii::t('app', '{name}, conclusion of transactions, as well as financial transactions have the right enjoy a person of 18 years. By creating a personal account, you agree to regulations and {name}\'s privacy policy. The Company undertakes to fulfill the declared obligations to partners and investors.', ['name' => Yii::$app->name]) ?>
                            </p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="footer-h1">
                                <h3><?= Yii::t('app', 'Contacts') ?></h3>

                            </div>
                            <div class="contacts-footer">
                                <div><img src="https://ai-logist.com/wp-content/uploads/2018/04/email.png" width="30"
                                          height="30"><a href="mailto:support@ai-logist.com" target="_blank">support@ai-logist.com</a>
                                </div>
                                <div><img src="https://ai-logist.com/wp-content/uploads/2018/04/telegram.png" width="30"
                                          height="30"><a href=" https://t.me/AI_Logistics_Official"
                                                         target="_blank"><?= Yii::t('app', 'Telegram support') ?></a>
                                </div>
                                <div><img src="https://ai-logist.com/wp-content/uploads/2018/04/telegram.png" width="30"
                                          height="30"><a href="https://t.me/AI_Logistics_Russian"
                                                         target="_blank"><?= Yii::t('app', 'The Telegram Community') ?></a>
                                </div>
                                <div><img src="https://ai-logist.com/wp-content/uploads/2018/04/vk.png" width="30"
                                          height="30"><a href="https://vk.com/ai.logistics.official"
                                                         target="_blank"><?= Yii::t('app', 'Vkontakte') ?></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copy">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p>
                                &copy; <?= date('Y') ?>.
                                AI Logistics.
                                <?= Yii::t('app', 'All rights reserved') ?>.</p>

                            <a href="https://payeer.com/?partner=" target="_blank" id="payeer">
                                <img src="https://payeer.com/bitrix/templates/difiz/img/banner/ru/468x60.gif" alt="Payeer">
                            </a>
                            <a href="https://www.interkassa.com/" target="_blank" id="interkassa">
                                <img src="https://www.interkassa.com/resource/images/brandbook/b-logo-horizontal.png" alt="Interkassa">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

<?= $this->endBody() ?>
<!-- <?= include '_info.php' ?> -->
</body>
</html>
<?= $this->endPage() ?>
