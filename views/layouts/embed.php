<?php

use app\helpers\Html;

$title = $this->title ? $this->title . ' — ' . Yii::$app->name : Yii::$app->name;
$state = [];
$context = Html::context(empty($this->params['state']) ? $state : array_merge($state, $this->params['state']));

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?= Html::script($context) ?>
    <?= $this->head() ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
</head>
<body class="<?= Yii::$app->user->getIsGuest() ? 'indoor' : 'outdoor' ?>" data-mode="<?= YII_ENV ?>">
<?= $this->beginBody() ?>
<div id="app">
    <?= $content ?>
</div>
<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
