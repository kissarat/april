<?php
/**
 * Last modified: 18.08.06 11:22:34
 * Hash: 3183eaba24c2bdbc5982b6e19d0c2ee345a910c1
 */

use app\assets\CustomAsset;
use app\helpers\Html;

CustomAsset::register($this);

$title = $this->title ? $this->title . ' — ' . Yii::$app->name : Yii::$app->name;

$this->beginPage();

?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?= Html::tag('title', $title) ?>
    <link rel="shortcut icon"
          href="https://ai-logist.com/wp-content/uploads/2018/04/favicon2.png"
          type="image/x-icon"/>
    <?= Html::csrfMetaTags() ?>
    <?php require '_analytics_head.php' ?>
    <?= $this->head() ?>
    <?= Html::script(Html::context()) ?>
</head>
<body class="s1 s2 <?= Yii::$app->user->getIsGuest() ? 'indoor' : 'outdoor' ?>"
      data-mode="<?= YII_ENV ?>"
      data-nick="<?= Yii::$app->user->getIsGuest() ? 'guest' : Yii::$app->user->identity->nick ?>">
<?= $this->beginBody() ?>
<?php require '_analytics_body.php' ?>
<div id="app" <?= Html::xmlns() ?>>
    <div class="outdoor-layout">
        <div class="header-indoor">
            <div class="full-width">
                <div class="logo">
                    <img src="/images/logo.png" alt="AI Logistics">
                </div>
                <div id="language" class="btn-group" v-cloak>
                    <button type="button" class="btn dropdown-toggle lang" data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                        <i data-feather="globe"></i>
                        {{ locale.toUpperCase() }}
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-item"
                             v-for="(flag, code) in languages"
                             v-on:click="changeLanguage(code)">
                            <span>{{ code }}</span>
                            <span class="name">{{ flag }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="news">
            <h1>
                <?= Yii::t('app', 'Last news:') ?>
            </h1>
            <div class="news-list">
                <!-- Start post for copy -->

                <div class="post">
                    <?php
                    $postlink = 'https://ai-logist.com/ai-logistics-obyavlyaet-ofitsialnyj-zapusk/';
                    $postimg = 'https://ai-logist.com/wp-content/uploads/2018/07/News-2-1024x536.jpg';
                    $postdate = '7.07.2018';
                    ?>
                    <div class="left">
                        <a href="<?= $postlink; ?>" target="_blank">
                            <div class="img" style="background-image: url('<?= $postimg; ?>');"></div>
                        </a>
                    </div>
                    <div class="post-info right">
                        <a href="<?= $postlink; ?>" target="_blank">
                            <h3>
                                <?= Yii::t('app', 'Компания AI Logistics объявляет официальный запуск!') ?>
                            </h3>
                        </a>
                        <div class="date2"><?= $postdate; ?></div>
                        <div class="line-post"></div>
                        <div class="lead">
                            <p><?= Yii::t('app', 'Приветствуем, дорогие партнёры и инвесторы компании AI Logistics.') ?></p>
                            <p> <?= Yii::t('app', 'Обязательно дочитайте до конца!') ?> </p>
                            <p> <?= Yii::t('app', 'Прошла неделя с момента официального запуска инвестиционной платформы AI Logistics. С 15 мая по 1 июля компания работала над улучшениями функционала личного кабинета и усовершенствованием искусственного интеллекта AI Logistics. Но это не главное…') ?> </p>
                        </div>
                        <a href="<?php echo $postlink; ?>" target="_blank"
                           class="readmore"><?= Yii::t('app', 'Read More >') ?></a>
                    </div>
                </div>
                <!-- End post for copy -->
                <br>
                <!-- Start post for copy -->

                <div class="post">
                    <?php
                    $postlink = 'https://ai-logist.com/zapusk-investitsionnoj-programmy-ot-ai-logistics/';
                    $postimg = 'https://ai-logist.com/wp-content/uploads/2018/04/start.jpg';
                    $postdate = '15.05.2018';
                    ?>
                    <div class="left">
                        <a href="<?= $postlink; ?>" target="_blank">
                            <div class="img" style="background-image: url('<?= $postimg; ?>');"></div>
                        </a>
                    </div>
                    <div class="post-info right">
                        <a href="<?= $postlink; ?>" target="_blank">
                            <h3>
                                <?= Yii::t('app', 'Запуск инвестиционной программы от AI Logistics') ?>
                            </h3>
                        </a>
                        <div class="date2"><?= $postdate; ?></div>
                        <div class="line-post"></div>
                        <div class="lead">
                            <p><?= Yii::t('app', '15 мая 2018 года состоялся запуск AI Logistics. Рады приветствовать инвесторов и партнёров компании. Теперь каждый может обеспечить себе пассивный доход до 438% в год!') ?></p>
                        </div>
                        <a href="<?php echo $postlink; ?>" target="_blank"
                           class="readmore"><?= Yii::t('app', 'Read More >') ?></a>
                    </div>
                </div>
                <!-- End post for copy -->
            </div>
        </div>
        <div class="content">
            <main>
                <?= $content ?>
            </main>
        </div>

        <footer>
            <p>
                &copy; AI Logistics <?= date('Y') ?>.
                <?= Yii::t('app', 'All rights reserved') ?>.
            </p>
            <a href="https://payeer.com/?partner=" target="_blank" id="payeer">
                <img src="https://payeer.com/bitrix/templates/difiz/img/banner/ru/468x60.gif" alt="Payeer">
            </a>
            <a href="https://www.interkassa.com/" target="_blank" id="interkassa">
                <img src="https://www.interkassa.com/resource/images/brandbook/b-logo-horizontal.png" alt="Interkassa">
            </a>
        </footer>
    </div>
</div>

<?= $this->endBody() ?>
<!-- <?= include '_info.php' ?> -->
</body>
</html>
<?= $this->endPage() ?>
