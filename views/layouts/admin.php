<?php

use app\admin\assets\AdminAsset;
use app\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

AdminAsset::register($this);

$title = $this->title ? $this->title . ' — ' . Yii::$app->name : Yii::$app->name;

$items = [
    ['label' => Yii::t('app', 'Statistics'), 'url' => ['admin/index']],
    ['label' => Yii::t('app', 'Users'), 'url' => ['user/index']]
];

//foreach ($items as $item) {
//    $item['linkOptions'] = ['class' => 'nav-link'];
//}

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>
    <?= $this->head() ?>
</head>
<body class="s1 s2 admin <?= Yii::$app->user->getIsGuest() ? 'indoor' : 'outdoor' ?>" data-mode="<?= YII_ENV ?>">
<?= $this->beginBody() ?>
<?php require '_analytics_head.php' ?>
<div id="app">
    <header>
        <?php NavBar::begin(['brandLabel' => Yii::$app->name]) ?>
        <?= Nav::widget([
            'items' => $items,
            'options' => ['class' => 'navbar-nav']
        ]) ?>
        <?php NavBar::end() ?>
    </header>
    <main class="container">
        <?= $content ?>
    </main>
</div>

<?= $this->endBody() ?>
<!-- <?= include '_info.php' ?> -->
</body>
</html>
<?= $this->endPage() ?>
