<?php
// Request information
$info = ['Request at ' . date('Y-m-d h:i:s')];
foreach (['HOST', 'IP', 'USER_AGENT', 'ACCEPT', 'ACCEPT_LANGUAGE'] as $header) {
    if (isset($_SERVER['HTTP_' . $header])) {
        $info[] = "\t$header: " . $_SERVER['HTTP_' . $header];
    }
}

return implode("\n", $info);
