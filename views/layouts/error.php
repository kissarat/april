<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 56dfb76c30298cb9379fc4d9e735dc9e8850e935
 */

use app\assets\CustomAsset;
use app\helpers\Html;
use app\widgets\Alert;

CustomAsset::register($this);

$title = $this->title ? $this->title . ' — ' . Yii::$app->name : Yii::$app->name;

$this->beginPage();

?>
<!DOCTYPE>
<html lang="<?= Yii::$app->language ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <?php require '_analytics_head.php' ?>
    <?= $this->head() ?>
    <?= Html::script(Html::context()) ?>
</head>
<body class="s1 s2 <?= Yii::$app->user->getIsGuest() ? 'indoor' : 'outdoor' ?>"
      data-mode="<?= YII_ENV ?>"
      data-nick="<?= Yii::$app->user->getIsGuest() ? 'guest' : Yii::$app->user->identity->nick ?>">
<?= $this->beginBody() ?>
<?php require '_analytics_body.php' ?>
<div id="app" <?= Html::xmlns() ?>>
    <?= Alert::widget() ?>
    <main>
        <?= $content ?>
    </main>
</div>
<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
