<?php
/**
 * Last modified: 18.07.22 10:39:28
 * Hash: fe0340a49f56def8126ae1d065b1c9abbabe3381
 * @var $this yii\web\View
 * @var $model app\models\User
 * @var $form yii\widgets\ActiveForm
 */

use app\models\User;
use himiklab\yii2\recaptcha\ReCaptcha;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="user signup">
    <div class="logo">
        <img src="/images/logo-black.png" alt="AI Logistics">
    </div>
    <h1><?= Yii::t('app', 'Create an account') ?></h1>

    <p class="description"><?= Yii::t('app', '') ?></p>

    <?php $form = ActiveForm::begin(); ?>
    <div class="fields">
        <?= $form->field($model, 'nick')
            ->textInput([
                'maxlength' => User::NICK_LENGTH,
                'placeholder' => Yii::t('app', 'Enter Your Login')
            ])
            ->label(false) ?>

        <?= $form->field($model, 'email')
            ->textInput([
                'maxlength' => User::EMAIL_LENGTH,
                'placeholder' => Yii::t('app', 'Enter Your Email')
            ])
            ->label(false) ?>

        <?= $form->field($model, 'password')
            ->passwordInput([
                'maxlength' => User::PASSWORD_LENGTH,
                'placeholder' => Yii::t('app', 'Enter Your Password')
            ])
            ->label(false) ?>

        <?= $form->field($model, 'sponsor')
            ->textInput([
                'maxlength' => User::NICK_LENGTH,
                'placeholder' => Yii::t('app', 'Enter Your Sponsor Login')
            ])
            ->label(false) ?>

        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>

        <p class="sponsor">
            <?= Yii::t('app', '(Your Sponsor)') ?>
        </p>
    </div>

    <?php if (Yii::$app->settings->get('auth-recaptcha', 0) > 0) {
        echo $form->field($model, 'captcha')->widget(ReCaptcha::class, [
                'size' => ReCaptcha::SIZE_NORMAL
        ])->label(false);
    } ?>

    <div class="control">
        <?= Html::submitButton(Yii::t('app', 'Signup')) ?>
        <?= Html::a(Yii::t('app', 'Login'), ['user/login']) ?>
        <?= Html::a(Yii::t('app', 'Forgot password?'), ['user/request']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
