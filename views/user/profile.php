<?php
/**
 * Last modified: 18.08.11 12:14:26
 * Hash: 331bf117ebedb67550f40c842f3a56f5d406c886
 * @var $this \yii\web\View
 */

use app\helpers\Html;

?>

<div class="user profile">
    <?php if (Yii::$app->user->identity->agreement): ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 verif">
            <p>Вывод активирован</p>
        </div>
    <?php else: ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noverif">
            <p>
                Отправьте договор для активации вывода.
                <a href="//ai-logist.com/wp-content/uploads/2018/08/Dogovor.pdf" target="_black">Скачать договор</a>.
                Договор отправлять на Email: <a href="mailto:ai.logist.verif@gmail.com">ai.logist.verif@gmail.com</a>
            </p>
        </div>
    <?php endif ?>

    <div class="sortable col-lg-6 col-md-6 col-sm-12 col-xs-12" id="informer">
        <div class="row">
            <div class="invested col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="informer" v-cloak>
                    <span class="name"><?= Yii::t('app', 'Invested') ?></span>
                    <div class="value money" v-bind:data-currency="currency">{{ buy | factor(currency) }}</div>
                </div>
            </div>
            <div class="income col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="informer" v-cloak>
                    <span class="name"><?= Yii::t('app', 'Income') ?></span>
                    <div class="value money" v-bind:data-currency="currency">{{ accrue | factor(currency) }}</div>
                </div>
            </div>
            <div class="withdraw col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="informer no-margin-bottom" v-cloak>
                    <span class="name"><?= Yii::t('app', 'Withdrawn') ?></span>
                    <div class="value money" v-bind:data-currency="currency">{{ withdraw | factor(currency) }}</div>
                </div>
            </div>
            <div class="referral col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="informer no-margin-bottom" v-cloak>
                    <span class="name"><?= Yii::t('app', 'Affiliate profit') ?></span>
                    <div class="value money" v-bind:data-currency="currency">{{ bonus | factor(currency) }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="turnover col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="informer no-margin-bottom">
            <span class="name"><?= Yii::t('app', 'Turnover') ?></span>
            <span class="description"><?= Yii::t('app', 'Career progress in {name}', ['name' => Yii::$app->name]) ?></span>
            <?= Html::tag('span', $turnover, ['class' => 'money usd']) ?>
            <div class="line-grey"></div>
            <span class="name"><?= Yii::t('app', 'Career') ?></span>
            <div class="padding-progress">
                <div class="progres">
                    <div class="progress-bg">
                        <div class="progress-bar" v-bind:style="{width: (100 * grade.id / grades.length) + '%' }"></div>
                        <div class="progress-check"></div>
                    </div>
                </div>
            </div>
            <div class="stats-rung" v-cloak>
                <div class="your-rung">
                    <p>{{ t(grade.name) }}</p>
                </div>
                <div v-if="nextGrade" class="next-rung">
                    <p>{{ t(nextGrade.name) }}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="referral-link col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <h2><?= Yii::t('app', 'Your referral link') ?></h2>
        <?= Html::input('text', 'reflink', $reflink) ?>
        <div class="statistics">
            <div class="follow">
                <span class="value"><?= $follows ?></span>&nbsp;
                <span class="name"><?= Yii::t('app', 'follows') ?></span>
            </div>
            <div class="registration">
                <span class="value"><?= $children ?></span>&nbsp;
                <span class="name"><?= Yii::t('app', 'registrations') ?></span>
            </div>
            <div class="node">
                <span class="value"><?= $nodes ?></span>&nbsp;
                <span class="name"><?= Yii::t('app', 'investments') ?></span>
            </div>
        </div>
    </div>
    <div class="col-lg-3 invest-button col-md-3 col-sm-12 col-xs-12">
        <?= Html::a(Yii::t('app', 'Invest'), ['program/index'], ['id' => 'invest']) ?>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div id="chart">
            <div class="chart-header">
                <h3><?= Yii::t('app', 'Income') ?></h3>
                <div class="control" v-cloak>
                    <div class="currency">
                        <p class="name"><?= Yii::t('app', 'Choose currency:') ?> </p>
                        <div class="widget dropdown">
                            <div class="value dropdown-toggle"
                                 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ currency }}
                            </div>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a v-for="c in currencies"
                                   v-on:click="load(c)"
                                   v-bind:href="'#' + c"
                                   class="dropdown-item">
                                    {{ c }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="choose-mount">
                        <p class="name"><?= Yii::t('app', 'Choose month:') ?></p>
                        <div class="widget dropdown">
                            <div data-toggle="dropdown" class="value dropdown-toggle mounth">
                                {{ months[month] }}
                            </div>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a v-bind:href="'#' + m"
                                   v-bind:class="{'dropdown-item': true, current: m === month}"
                                   v-for="(label, m) in months"
                                   v-on:click="month = +m; loadChart()">{{ label }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <svg width="100%" height="210"></svg>

            <div class="line-box">
                <div class="line-grey"></div>
            </div>

            <div class="summary" v-cloak>
                <div class="income">
                    <div class="name"><?= Yii::t('app', 'Monthly income:') ?></div>
                    <div class="value money" v-bind:data-currency="currency">{{ monthIncome }}</div>
                </div>
                <div class="expense">
                    <div class="name"><?= Yii::t('app', 'Monthly expense:') ?></div>
                    <div class="value money" v-bind:data-currency="currency">{{ monthExpense }}</div>
                </div>
            </div>
        </div>
    </div>
    <div id="nodes">
        <?= $this->render('@app/views/node/index', ['models' => $investments]) ?>
    </div>
</div>
