<?php

/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 803bae603b5941d73afc48b0d80b762e2d0d1382
 * @var $this yii\web\View
 * @var $message string
 * @var $model app\models\form\RequestPasswordReset
 * @var $form ActiveForm
 */

use app\widgets\Alert;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Password recovery request');
?>

<div class="user request">
    <div class="logo">
        <img src="/images/logo-black.png" alt="AI Logistics">
    </div>

    <?= Alert::widget() ?>

    <?php $form = ActiveForm::begin(); ?>
    <h1><?= Yii::t('app', 'Reset password') ?></h1>

    <div class="fields">
        <?= $form->field($model, 'email')
            ->input('email', ['placeholder' => Yii::t('app', 'Enter Your Email')])
            ->label(false)
        ?>
        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>
    </div>

    <div class="control">
        <?= Html::submitButton(Yii::t('app', 'Send Email'),
            ['class' => 'button full-width']) ?>

        <?= Html::a(Yii::t('app', 'Signup'), ['user/signup']) ?>
        <?= Html::a(Yii::t('app', 'Login'), ['user/login']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
