<?php
/**
 * Last modified: 18.07.22 10:35:21
 * Hash: 5b0cc5085a59b2c2eedc902986cb45ac5e53399a
 * @var \app\models\form\Login $model
 * @var $this \yii\web\View
 */

use app\models\User;
use app\widgets\Alert;
use himiklab\yii2\recaptcha\ReCaptcha;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

?>
<div class="user login">
    <div class="logo">
        <img src="/images/logo-black.png" alt="AI Logistics">
    </div>
    <h1><?= Yii::t('app', 'Login') ?></h1>
    <p class="description"><?= Yii::t('app', 'Enter your login and password to use your personal account') ?></p>

    <?= Alert::widget() ?>

    <?php $form = ActiveForm::begin(); ?>
    <div class="fields">
        <?= $form->field($model, 'login')
            ->textInput([
                'maxlength' => User::NICK_LENGTH,
                'placeholder' => Yii::t('app', 'Enter Your Login'
                )])
            ->label(false) ?>
        <?= $form->field($model, 'password')
            ->passwordInput([
                'maxlength' => User::PASSWORD_LENGTH,
                'placeholder' => Yii::t('app', 'Enter Your Password')
            ])
            ->label(false) ?>
        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>
    </div>
    <div class="captcha">
        <?php
        //        if (Yii::$app->settings->get('auth-captcha', 0) > 0) {
        //            echo $form->field($model, 'captcha')->widget(Captcha::class)->label(false);
        //        }
        if (Yii::$app->settings->get('auth-recaptcha', 0) > 0) {
            echo $form->field($model, 'captcha')->widget(ReCaptcha::class, [
                'size' => ReCaptcha::SIZE_NORMAL
            ])->label(false);
        }
        ?>
    </div>
    <div class="control">
        <?= Html::submitButton(Yii::t('app', 'Login')) ?>
        <?= Html::a(Yii::t('app', 'Signup'), ['user/signup']) ?>
        <?= Html::a(Yii::t('app', 'Forgot password?'), ['user/request']) ?>
    </div>

    <?php ActiveForm::end() ?>
</div>
