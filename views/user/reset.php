<?php

/**
 * Last modified: 18.05.22 03:13:36
 * Hash: 32fc74d1da56f2c6b37e22761fbef0e512644484
 * @var $this yii\web\View
 * @var $message string
 * @var $form ActiveForm
 * @var $model \app\models\form\Password
 */

use app\models\User;
use app\widgets\Alert;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t( 'app', 'Password Recovery' );

?>

<div class="user login">
    <div class="logo">
        <img src="/images/logo-black.png" alt="AI Logistics">
    </div>
    <h1><?= Yii::t( 'app', 'Reset password' ) ?></h1>

    <div class="logo">
        <img src="/images/logo-black.png" alt="AI Logistics">
    </div>

	<?= Alert::widget() ?>

	<?php $form = ActiveForm::begin(); ?>

    <div class="fields">
		<?= $form->field( $model, 'new' )
            ->passwordInput(['maxlength' => User::PASSWORD_LENGTH]) ?>
		<?= $form->field( $model, 'repeat' )
            ->passwordInput(['maxlength' => User::PASSWORD_LENGTH]) ?>
		<?= $form->field( $model, 'ip' )->hiddenInput()->label( false ) ?>
    </div>

    <div class="control">
		<?= Html::submitButton( Yii::t( 'app', 'Reset' ),
			[ 'class' => 'button full-width' ] ) ?>
		<?= Html::a(Yii::t('app', 'Signup'), ['user/signup']) ?>
		<?= Html::a(Yii::t('app', 'Login'), ['user/login']) ?>
    </div>
	<?php ActiveForm::end(); ?>
</div>
