<?php

use app\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 07dad784e714361132ff7e4c68d4d81cfb2774aa
 * @var $this \yii\web\View
 * @var $model \app\models\form\Invoice
 */

?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="transfer withdraw invoice">
        <h2 class="page-title"><?= Yii::t('app', 'Payment systems') ?></h2>
        <?php $form = ActiveForm::begin(['enableClientValidation' => false]) ?>
        <div class="systems" v-cloak>
            <button v-bind:id="s.id"
                    type="button"
                    v-for="s in accounts"
                    v-bind:class="{active: system === s.system && currency === s.currency}"
                    v-on:click="setAccount(s.currency, s.system)">
                {{ s.label }}
            </button>
        </div>
        <div class="form">
            <h3><?= Yii::t('app', 'Withdraw Cash') ?></h3>
            <div class="fields">
                <div class="visible">
                    <?= $form->field($model, 'amount')
                        ->textInput(['v-bind:placeholder' => 'minimal']) ?>
                    <?= $form->field($model, 'wallet')
                        ->textInput([
                            'v-bind:pattern' => 'pattern',
                            'v-bind:readonly' => 'isWalletReadonly',
                            'v-model' => 'wallet'
                        ]) ?>
                </div>
                <div class="hidden">
                    <?= $form->field($model, 'currency')
                        ->hiddenInput(['v-bind:value' => 'currency'])
                        ->label(false) ?>
                    <?= $form->field($model, 'system')
                        ->hiddenInput(['v-bind:value' => 'system'])
                        ->label(false) ?>
                    <?= $form->field($model, 'ip')
                        ->hiddenInput()
                        ->label(false) ?>
                </div>
            </div>
            <div class="control" v-cloak>
                <?= Html::submitButton(Yii::t('app', 'Withdraw'), ['v-if' => 'account']) ?>
                <div v-else>
                    <i><?= Yii::t('app', 'Select a payment system') ?></i>
                </div>
            </div>
            
        </div>
        <?php ActiveForm::end() ?>
        <div class="white-bg">
            <h3>Регламент вывода</h3>
            <p>Заявку на вывод средств можно подать в любое удобное для Вас время.</p>
            <p>Все заявки будут обработаны согласно регламенту — в ближайший четверг.</p>
            <p>Заявки, поданные в четверг до 13:00, будут обработаны в течении дня — до 00:00.</p>
            <p>Обработка заявок, поданных в четверг после 13:00, переносится на следующий четверг.</p>
            <p>Одновременно Вы можете подать 1 заявку в 1 валюте. Вы не можете создать новую заявку в той же валюте, пока не будет обработана предыдущая.</p>
            <p>Заявки на вывод обрабатываются только тем партнёрам, которые прошли верификацию договора о покупке торгового плана.</p>
            <p><i>* В криптовалюте деньги поступают на кошелёк от 2 минут до нескольких часов с момента обработки заявки, в зависимости от загруженности криптовалютной сети.</i></p>
        </div>
    </div>
</div>