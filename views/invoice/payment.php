<?php
/**
 * Last modified: 18.08.03 12:51:54
 * Hash: 84f917c5bd6ea3677adb5915b3f4702b6973c6d4
 * @var $this \yii\web\View
 * @var $model \app\models\form\Invoice
 */

use app\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="transfer payment invoice">
    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
        <div class="left">
            <h2 class="page-title"><?= Yii::t('app', 'Payment systems') ?></h2>
            <?php $form = ActiveForm::begin() ?>
            <div class="systems" v-cloak>
                <button v-bind:id="system"
                        type="button"
                        v-for="s in filterAccounts(false)"
                        v-bind:class="{active: system === s.system}"
                        v-on:click="currency = s.currency; system = s.system">
                    {{ s.label }}
                </button>
            </div>
            <div class="form">
                <h3><?= Yii::t('app', 'Payment') ?></h3>
                <div class="fields">
                    <div class="visible">
                        <?= $form->field($model, 'amount')
                            ->textInput(['v-bind:placeholder' => 'minimal']) ?>
                    </div>
                    <div class="hidden">
                        <?= $form->field($model, 'system')
                            ->hiddenInput(['v-bind:value' => 'system'])->label(false) ?>
                        <?= $form->field($model, 'currency')
                            ->hiddenInput(['v-bind:value' => 'currency'])->label(false) ?>
                        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>
                    </div>
                </div>
                <div class="control" v-cloak>
                    <?= Html::submitButton(Yii::t('app', 'Pay'), ['v-if' => 'account']) ?>
                    <div v-else>
                        <i><?= Yii::t('app', 'Select a payment system') ?></i>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
        <br>
        <h3>AdvCash</h3>
        <p>Для пополнения баланса в AdvCash – переведите на кошелек <b>U 7206 3110 6809</b> нужную сумму, указав в
            примечании Ваш логин</p>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
        <div class="right crypto">
            <h2 class="page-title"><?= Yii::t('app', 'Crypto currency') ?></h2>
            <div v-bind:id="currency" v-for='s in filterAccounts(true)'
                 v-bind:title="currency.toUpperCase()" v-cloak>
                <div v-if="s.wallet" class="wallet-block">
                    <div class="up">
                        <div class="cripto-icon">
                            <img v-bind:src="'/images/' + s.currency + '.png'"/>
                            <span>{{ s.label }}</span>
                        </div>
                    </div>
                    <div class="down">
                        <div class="wallet">{{ s.wallet }}</div>
                    </div>
                </div>
                <div v-else class="get-wallet-block">
                    <img v-bind:src="'/images/' + s.currency + '.png'"/>
                    <span>{{ s.label }}</span>
                    <div v-on:click="s.createWallet()"
                         class="get-wallet"><?= Yii::t('app', 'Get the address') ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
