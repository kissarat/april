<?php
/**
 * Last modified: 18.08.17 17:40:47
 * Hash: c62715d50712140fb459400e47198d5aead8ffa1
 */
/** @var string $guid */
/** @var string $amount */
/* @var $this \yii\web\View */

use app\helpers\Html;
?>

<div class="interkassa pay col-lg-9 transfer payment invoice">
    <form name="payment" method="get" action="https://sci.interkassa.com/" accept-charset="UTF-8">
    <p>Введите сумму и выберите валюту</p>
        <?= Html::hiddenInput('ik_co_id', Yii::$app->interkassa->id) ?>
        <?= Html::hiddenInput('ik_pm_no', $guid) ?>
        <?= Html::hiddenInput('ik_ltm', 12 * 3600) ?>
        <?= Html::hiddenInput('ik_cli', Yii::$app->user->identity->email) ?>
        <?= Html::hiddenInput('ik_x_user', Yii::$app->user->getId()) ?>
        <?= Html::hiddenInput('ik_x_nick', Yii::$app->user->identity->nick) ?>
        <input type="hidden" name="ik_int" value="web"/>
        <input type="hidden" name="ik_desc" value=""/>
        <!--    <input type="hidden" name="ik_am_ed" value="0"/>-->
        <input type="hidden" name="ik_enc" value="utf-8"/>
        <div class="inter">
        <input type="hidden" name="ik_am_t" value="invoice"/>
    <!--    <input type="hidden" name="ik_int" value="json"/>-->
        <?= Html::textInput('ik_am', $amount, ['required' => true]) ?>
        <select required name="ik_cur">
            <option value="RUB" selected>RUB</option>
            <option value="UAH">UAH</option>
        </select>
        <?= Html::submitButton(Yii::t('app', 'Pay')) ?>
        </div>
        <div>
            <h3>Курс валют:</h3>
            <ul>
                <li>RUB/USD - <?= Yii::$app->interkassa->rates['RUB'] ?> руб</li>
                <li>UAH/USD - <?= Yii::$app->interkassa->rates['UAH'] ?> грн</li>
            </ul>
        </div>
    </form>
</div>
