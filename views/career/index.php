<?php
/**
 * Last modified: 18.05.24 00:20:18
 * Hash: 0c4aab882ab3bb09d517e8b969cdc523475685ca
 * @var $this \yii\web\View
 * @var $lottery int
 * @var $personal int
 * @var $turnover int
 * @var $this \yii\web\View
 */
?>

<div class="career index">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="carier-sectiom">
            <div class="left">
                <div class="header-txt">
                    <h1><?= Yii::t( 'app', 'Career' ) ?></h1>
                    <p><?= Yii::t( 'app', 'Your career achievements with {name}', ['name' => Yii::$app->name]) ?></p>
                </div>
                <div class="informer-carier">
                    <div id="turnover">
                        <div class="name"><?= Yii::t( 'app', 'Your turnover' ) ?></div>
                        <div class="money usd"><?= $turnover ?></div>
                    </div>
                </div>
                <div class="informer-carier">
                    <div id="payment">
                        <div class="name"><?= Yii::t( 'app', 'Personal investment' ) ?></div>
                        <div class="money usd"><?= $personal ?></div>
                    </div>
                </div>
                <div class="informer-carier">
                    <div id="lottery">
                        <div class="name"><?= Yii::t( 'app', 'Purchased tickets' ) ?></div>
                        <div class="money"><?= $lottery ?></div>
                    </div>
                </div>
            </div>


            <div class="right">
                <div class="middle">
                    <div class="progress-vertical">
                        <div class="top-line"></div>
                        <div class="circke" v-bind:style="{top: (100 * grade.id / grades.length) + '%'}"></div>
                    </div>
                </div>
                <div v-cloak>
                    <div v-for="g in grades"
                         v-bind:class="{'rang-block': true, before: grade.id > g.id, current: g.id === grade.id, next: g.id - 1 === grade.id, after: grade.id < g.id - 1}">
                        <div class="header-rang">
                            <h3>{{ t(g.name) }}</h3>
                            <p><?= Yii::t( 'app', 'Current Status' ) ?></p>
                        </div>
                        <div class="next-bonus">
                            <p><?= Yii::t( 'app', 'Turnover' ) ?> <span class="money" data-currency="USD">{{ g.turnover | factor }}</span>
                            </p>
                            <p><?= Yii::t( 'app', 'Personal investment' ) ?> <span
                                        class="money"
                                        data-currency="USD">{{ g.personal | factor('USD') }}</span></p>
                            <p><?= Yii::t( 'app', 'Lottery tickets' ) ?> <span>{{ g.lottery }}</span>
                            </p>
                            <p><?= Yii::t( 'app', 'Award' ) ?> <span class="money"
                                                                    data-currency="USD">{{ g.premium | factor }}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>