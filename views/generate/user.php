<?php
use app\helpers\Html;
use app\models\Transfer;
use yii\bootstrap\ActiveForm;

/**
 * @var \app\models\form\UserGenerator $model
 * @var array $errors
 */
/* @var $this \yii\web\View */
?>

<div class="generate user">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'sponsor') ?>
    <?= $form->field($model, 'count') ?>
    <?= $form->field($model, 'balance') ?>
    <?= $form->field($model, 'currency')->dropDownList(Transfer::getCurrenciesDictionary()) ?>
    <div>
        <?= Html::submitButton('Generate') ?>
    </div>
    <?php ActiveForm::end() ?>
    <?php
    if (!empty($errors)) {
      echo Html::tag('pre', json_encode($errors, JSON_PRETTY_PRINT));
    }
    ?>
</div>
