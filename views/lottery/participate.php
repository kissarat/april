<?php
/**
 * Last modified: 18.08.08 02:03:25
 * Hash: 0dcd7569e5ac1e87e72f07800716fc570a5d00a3
 * @var \app\models\form\LotteryForm $model
 * @var $fund mixed
 * @var $firstLottery \app\models\Lottery
 * @var $this \yii\web\View
 * @var $participants array
 * @var $provider \yii\data\ActiveDataProvider
 */
/* @var $firstNumber int */

use app\helpers\Html;
use app\models\Lottery;
use app\models\Transfer;
use yii\bootstrap\ActiveForm;

$part = $firstLottery ? $firstLottery->amount : 0;
?>
<div class="lottery participate">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="lotoheader">
            <div class="infoheader">
                <h1><?= Yii::t('app', 'Lottery AI Loto') ?></h1>
                <p><?= Yii::t('app', 'Buy tickets for $ 1 to participate in the weekly lottery from AI Logistics. <a href="https://ai-logist.com/marketing/" target="_blank">Read more</a>') ?></p>
            </div>
            <div class="koeficient">
                <h3><?= Yii::t('app', 'Your chance of winning is:') ?></h3>
                <div class="percent"><?= $fund > 0 ? round( 1000 * $part / $fund ) / 1000 : 0 ?></div>
            </div>
        </div>
    </div>

    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="panel-loto">
            <div class="left">
                <h1>AI LOTO</h1>
                <div class="fund"><i data-feather="award"></i> <?= Yii::t( 'app', 'Prize {amount} USD', [
						'amount' => $fund * 0.9
					] ) ?></div>
                <div class="places">
					<?php foreach ( Lottery::PERCENTS as $i => $percent ): ?>
                        <div>
                            <div class="usd"><?= $fund * $percent ?></div>
                            <div class="number"><?= Yii::t( 'app', '{number} place', [ 'number' => $i ] ) ?></div>
                            <div class="sponsor"><?= Yii::t( 'app', '{percent} USD sponsor bonus',
									[ 'percent' => $fund * Lottery::BONUSES[ $i ] ] ) ?></div>
                        </div>
					<?php endforeach ?>
                </div>
                <div class="loto-btn-begin">
                    <span class="begin">
                        <i data-feather="clock"></i>
	                    <?= Yii::t( 'app', 'Draw <span id="draw-time">{time}</span>', [
		                    'time' => Yii::$app->storage->get( 'lottery-time', '2018-09-01' )
	                    ] ) ?>
                    </span>

                </div>
            </div>
            <div class="right">
                <h2><?= Yii::t( 'app', 'Participate' ) ?></h2>
				<?php $form = ActiveForm::begin() ?>
                <div class="fields">
					<?= $form->field( $model, 'amount' )->textInput( [ 'v-model' => 'amount' ] ) ?>
					<?= $form->field( $model, 'quantity' )->textInput( [ 'v-model' => 'quantity' ] ) ?>
					<?= $form->field( $model, 'currency' )->radioList( Transfer::getCurrenciesDictionary(),
						[ 'itemOptions' => [ 'v-model' => 'currency' ] ]
					) ?>
                </div>
                <div class="line-grey"></div>
                <div v-cloak>
                    <p class="txt-small">
						<?= Yii::t( 'app', 'You buy <span>{{amount}}</span> tickets for <span>{{quantity}}</span> lotteries' ) ?>
                    </p>
                    <p class="txt-head">
                        <span class="name"><?= Yii::t( 'app', 'Summary' ) ?>:</span>
                        <span>{{summary}}</span>
                        <span>{{currency}}</span>
                    </p>
                </div>
				<?= Html::submitButton( Yii::t( 'app', 'Buy tickets' ) ) ?>
                <p class="eth-txt-small">
					<?= Yii::t( 'app', 'In this lottery you have {number} tickets', [
						'number' => $part
					] ) ?></p>

				<?php ActiveForm::end() ?>
            </div>
        </div>
        
        <div class="participant-block">
            <h2><?= Yii::t('app', 'Congratulations to the winners of the previous lottery #{number}', [
                    'number' => $firstNumber
                ]) ?></h2>
            <div id="winners">
				<?php foreach ( Lottery::getWinners($firstNumber) as $winner ): ?>
                    <div class="winner">
                        <h5><?= Yii::t('app', '{n} place', ['n' => $winner['n']]) ?></h5>
                        <div class="winner-cart">
							<?= Html::tag( 'div', '', [
								'class' => 'winner-avatar',
								'style' => empty( $p['avatar'] ) ? '' : "background-image: url('${p['avatar']}')"
							] ) ?>
                            <div class="winner-info">
                                <span><?= $winner['nick'] ?><br></span>
                                <span><?= $winner['win'] / 100 ?> USD</span>
                            </div>
                        </div>
                        <div class="winner-sponsor-cart">
							<?= Html::tag( 'div', '', [
								'class' => 'winner-sponsor-avatar',
								'style' => empty( $p['sponsor_avatar'] ) ? '' : "background-image: url('${p['sponsor_avatar']}')"
							] ) ?>
                            <span><?= $winner['sponsor'] ?> <br></span>
                            <span><?= $winner['sponsor_win'] / 100 ?> USD</span>
                        </div>
                    </div>
				<?php endforeach ?>
            </div>

            <h2><?= Yii::t('app', 'We are rooting for the participants of the current lottery') ?></h2>
            <table class="participants table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th><?= Yii::t('app', 'Participant') ?></th>
                    <th><?= Yii::t('app', 'Number of tickets') ?></th>
                    <th><?= Yii::t('app', 'Part') ?></th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ( $participants as $p ): ?>
                    <tr>
                        <td class="name">
							<?= Html::tag( 'div', '', [
								'class' => 'winner-avatar',
								'style' => empty( $p['avatar'] ) ? '' : "background-image: url('${p['avatar']}')"
							] ) ?>
							<?= Html::tag( 'div', substr( $p['nick'], 0, strlen($p['nick']) > 5 ? 2 : 1 )
							                      . '<span>***</span>' . substr( $p['nick'], - (strlen($p['nick']) > 5 ? 2 : 1) ) ) ?>
                        </td>
                        <td><?= $p['amount'] ?></td>
                        <td class="percent"><?= round( 1000 * $p['amount'] / $fund ) / 1000 ?></td>
                    </tr>
				<?php endforeach ?>
                </tbody>
            </table>

            <?php for($i = $firstNumber - 1; $i > 1; $i--): ?>
            <h2><?= Yii::t('app', 'Congratulations to the winners of the previous lottery #{number}', [
                    'number' => $i
                ]) ?></h2>
            <div id="winners">
                <?php foreach ( Lottery::getWinners($i) as $winner ): ?>
                    <div class="winner">
                        <h5><?= Yii::t('app', '{n} place', ['n' => $winner['n']]) ?></h5>
                        <div class="winner-cart">
                            <?= Html::tag( 'div', '', [
                                'class' => 'winner-avatar',
                                'style' => empty( $p['avatar'] ) ? '' : "background-image: url('${p['avatar']}')"
                            ] ) ?>
                            <div class="winner-info">
                                <span><?= $winner['nick'] ?><br></span>
                                <span><?= $winner['win'] / 100 ?> USD</span>
                            </div>
                        </div>
                        <div class="winner-sponsor-cart">
                            <?= Html::tag( 'div', '', [
                                'class' => 'winner-sponsor-avatar',
                                'style' => empty( $p['sponsor_avatar'] ) ? '' : "background-image: url('${p['sponsor_avatar']}')"
                            ] ) ?>
                            <span><?= $winner['sponsor'] ?> <br></span>
                            <span><?= $winner['sponsor_win'] / 100 ?> USD</span>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
            <?php endfor ?>
        </div>

    </div>
</div>
