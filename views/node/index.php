<?php
use app\models\Program;
use app\models\Transfer;

/**
 * @var Transfer[] $models
 */
/* @var $this \yii\web\View */
?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="node index">
        <ul>
			<?php
            foreach ( $models as $model ): ?>
                <li data-currency="<?= $model->currency ?>">
                    <div class="plans-header">
                        <div class="left">
                            <span class="up-header"><?= Yii::t( 'app', 'Plan' ) ?></span>
                            <h2> <?= $model->getProgramObject()->name ?></h2>
                        </div>
                        <div class="right">
                            <div class="plans-invest plans-info">
                                <p><?= Yii::t( 'app', 'Invested:' ) ?>
                                    <span class="invested money <?= strtolower( $model->currency ) ?>">
							        <?= $model->amount ?></span>
                                </p>
                            </div>

                            <div class="plans-income plans-info">
                                <p><?= Yii::t( 'app', 'Current revenue:' ) ?>
                                    <span class="income money <?= strtolower( $model->currency ) ?>"><?= $model->getIncome() ?></span>
                                </p>
                            </div>
                            <div class="plans-accrue plans-info">
                                <p><?= Yii::t( 'app', 'Accrue:' ) ?>
                                    <span>
                                        <?= Yii::t( 'app', '{passed} from {max}' , [
                                                'passed' => $model->getAccrueCount(),
                                            'max' => Program::DAYS
                                        ]) ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="plants-dates">
                        <div class="plans-date-open">
                            <p><?= Yii::t( 'app', 'open date' ) ?></p>
                            <span class="open date"><?= $model->created ?></span>
                        </div>
                        <div class="plans-date-close">
                            <p><?= Yii::t( 'app', 'close date' ) ?></p>
                            <span class="closes date"><?= $model->getClose() ?></span>
                        </div>
                    </div>
                    <div class="progres">
                      <div class="progress-bg">
                          <div class="progress-bar" style="width: <?= $model->getProgress() * 100 ?>%"></div>
                          <div class="progress-check"></div>
                      </div>
                    </div>
                    <div class="plans-stat">
                        <span class="money <?= strtolower( $model->currency ) ?>">0</span>
                        <span class="income money <?= strtolower( $model->currency ) ?>"><?= $model->getIncome() ?></span>
                        <span class="money <?= strtolower( $model->currency ) ?>"><?= $model->getProgramIncome() ?></span>
                    </div>
                </li>
			<?php endforeach ?>
        </ul>
    </div>
</div>
