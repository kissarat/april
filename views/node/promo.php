<?php
/**
 * Last modified: 18.08.28 00:31:45
 * Hash: c38622cb9318a733d54133a3f77eb1a6f2343c61
 */

/* @var $this \yii\web\View */
/* @var $programs  */
/* @var $nicks array */
?>
<div class="node promo">
    <h1>Промо</h1>
    <div class="program-list">
        <?php foreach ($programs as $id => $program): ?>
            <div class="program">
                <h2><?= $program->name ?></h2>
                <?php if (isset($nicks[$id])): ?>
                    <ul>
                        <?php foreach ($nicks[$id] as $nick): ?>
                            <li><?= $nick ?></li>
                        <?php endforeach ?>
                    </ul>
                <?php endif ?>
            </div>
        <?php endforeach ?>
    </div>
</div>
