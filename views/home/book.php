<div class="promotion page-promo">
    <div class="col-lg-9">
        <h2 class="page-title">Книги AI Logistics LTD</h2>
    </div>
    <div class="col-lg-9">
        <div class="promo">
            <div class="promo-item">
                <img src="/web/pdf/book1/cover-book.jpg" alt="">
                <h5>Тимур Князев «Что, инвестиции под 30%... в месяц?»</h5>
                <p>В этой книге расскрыты все подводные камни инвестиционного бизнеса, которые помогут инвестору или же интернет-предпринимателю обезопасить себя от рисков.</p>   
                <a href="/web/pdf/book1/30inmounth.pdf" target="_black">Скачать PDF-книгу</a>
            </div>
        </div>
    </div>
</div>

