<?php

use app\helpers\Html;
use yii\helpers\Url;

$order = [
    "300x600.jpg",
    "160x600.jpg",
    "120x600.jpg",
    "240x400.jpg",
    "336x280.jpg",
    "250x250.jpg",
    "970x250.jpg",
    "300x200.jpg",
    "200x200.jpg",
    "320x100.jpg",
    "728x90.jpg",
    "970x90.jpg",
    "468x60.jpg",
    "320x50.jpg",
];

asort($banners);
usort($banners, function ($a, $b) use ($order) {
    $a = array_search($a, $order, false);
    $b = array_search($b, $order, false);
    return (false === $a ? 1000 : $a) - (false === $b ? 1000 : $b);
});

$elements = [];
foreach ($banners as $filename) {
    $p = pathinfo($filename);
    $banner = $p['filename'];
    $elements[$banner] = Url::to([
        Html::resource('banners/' . $filename)
    ], true);
}

?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="home banners page-promo">
        <h2 class="page-title"><?= Yii::t('app', 'Banners') ?></h2>
        <img class="img-responsive" src="https://cabinet.ai-logist.com/images/pr/ru/banners/970x250.jpg" alt="">
        <div class="banner-menu" >

            <p><?= Yii::t('app', 'Download:') ?></p>


            <div class="banner-menu-item">
                <?php foreach ($elements as $banner => $url): ?>
                    <?= Html::a($banner . 'px', $url, ['download' => true]) ?>
                <?php endforeach ?>
            </div>
        </div>


        <a data-src="#banner" data-fancybox="banners" class="fancybox" rel="gallery" href="javascript:;" style="display:none;">
            Demonstation
        </a>

        <div id="banners-fancybox-list" style="display: none">
            <?php
            foreach ($elements as $banner => $url):
                $img = Html::img($url, ['alt' => Yii::$app->name]);
                ?>
                <div class="banner fancybox" id="banner" data-fancybox="banners">
                    <div class="image">
                        <?= $img ?></div>
                    <div class="name">
                        <?= Html::a(Yii::t('app', 'Download {name} size banner', ['name' => $banner . 'px']), $url, [
                            'download' => true
                        ]) ?>
                    </div>
                    <textarea><?= Html::a($img, Yii::$app->user->identity->getReflink(), ['title' => 'AI Logistics']) ?></textarea>
                </div>
            <?php endforeach ?>
        </div>

    </div>
</div>