<div class="promotion page-promo">
    <div class="col-lg-9">
        <h2 class="page-title">Договор с AI Logistics LTD</h2>
    </div>
    <div class="col-lg-9">
        <div class="promo">
            <div class="promo-item">
                <img src="/web/pdf/book1/contract.png" alt="">
                <h5>ДОГОВОР О ПРИОБРЕТЕНИИ ТОРГОВЫХ ПЛАНОВ</h5>
                <p>Это обязательный договор к заполнению для каждого инвестора.</p>
                <a href="https://ai-logist.com/wp-content/uploads/2018/08/Dogovor.pdf" target="_black">Скачать документ</a>
            </div>
        </div>
    </div>
</div>

