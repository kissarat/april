<?php
/**
 * Last modified: 18.08.28 00:32:30
 * Hash: 5df751329a22557738a7733343d5bf0536908a3f
 */

/* @var $this \yii\web\View */
/** @var \app\models\Program[] $programs */
/** @var string[][] $nicks */

use app\helpers\Html;

?>

<div class="home promotion page-promo">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <h2 class="page-title"><?= Yii::t('app', 'Promo materials') ?></h2>
    </div>
    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
        <div class="promo">
            <div class="promo-item promo-item-1">
                <?= Html::promo('ai-banners.jpg') ?>
                <h5><?= Yii::t('app', 'Banners') ?></h5>
                <div class="line-header"></div>
                <p><?= Yii::t('app', 'Use {name} banners in all popular sizes for inserting on a website', ['name' => Yii::$app->name]) ?></p>
                <?= Html::a(Yii::t('app', 'Go to banners'), ['home/banners']) ?>
            </div>
        </div>
    </div>
</div>
