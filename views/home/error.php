<?php

use app\helpers\Html;

/**
 * @var integer $status
 * @var object $error
 */
?>

<div class="home error">
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="error-404">
            <h1><?= $status ?></h1>
            <p><?= Html::encode( $error['message'] ) ?></p>
			<?= Html::a( Yii::t( 'app', 'Back' ), $backURL ) ?>
        </div>
    </div>
</div>
