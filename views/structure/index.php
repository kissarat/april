<?php

use app\helpers\Html;
use yii\grid\GridView;

require '_grid.php';

/** @var \app\models\User $user */
/** @var \app\models\User $sponsor */
$user = Yii::$app->user->identity;
$sponsor = $user->parent > 0 ? $user->parentObject : null;

/**
 * @var yii\data\ArrayDataProvider $provider
 */
?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="structure index">
        <?php if ($sponsor): ?>
            <div class="sponsor">
                <div class="avatar-container">
                    <?= Html::tag('div', '', [
                        'class' => 'avatar',
                        'title' => $sponsor->getName(),
                        'style' => empty($sponsor->avatar) ? '' : "background-image: url('$sponsor->avatar')"
                    ]) ?>
                </div>

                <?= $sponsor->nick ?>
                <?= $sponsor->forename ?>
                <?= $sponsor->surname ?>
                <?= $sponsor->email ?>
                <?= $sponsor->phone ?>
                <?= $sponsor->skype ?>
                <?= $sponsor->telegram ?>
            </div>
        <?php endif ?>
        <?php if ($user->nick !== $nick): ?>
            <div class="user-nick">
                <?= Yii::t('app', 'Referrals of user <span>{nick}</span>', ['nick' => $nick]) ?>
            </div>
        <?php endif ?>
        <?= GridView::widget(configGrid([
            'dataProvider' => $provider,
            'layout' => '{errors}{pager}{items}{summary}',
//        'tableOptions' => ['class' => 'striped bordered'],
        ])) ?>
    </div>
</div>
