<?php
use yii\grid\GridView;

require '_grid.php';

?>
<div class="structure children">
    <div class="level" data-nick="<?= $nick ?>">Unknown</div>
    <?= GridView::widget(configGrid([
        'dataProvider' => $provider,
        'layout' => '{items}'
    ]));
    ?>
</div>
