<?php

use app\helpers\Html;
use app\models\Referral;
use app\models\Transfer;

/**
 * @param $config
 * @return array
 */
function configGrid(array $config)
{
    $columns = [
        [
            'attribute' => 'children',
            'format' => 'html',
            'contentOptions' => function (Referral $model) {
                return ['data-children' => $model->children];
            },
            'value' => function (Referral $model) {
                if ($model->children > 0) {
                    return $model->children . ' ' .
                        Html::a(Yii::t('app', 'view'), ['structure/index', 'nick' => $model->nick]);
                }
                return $model->children;
            }
        ],
        'nick',
        [
            'attribute' => 'created',
            'label' => Yii::t('app', 'Registered'),
            'format' => 'date',
            'contentOptions' => function (Referral $model) {
                return ['data-time' => strtotime($model->created)];
            }
        ],
        'skype',
        [
            'attribute' => 'surname',
            'label' => Yii::t('app', 'Name'),
            'value' => function (Referral $model) {
                return $model->forename . ' ' . $model->surname;
            }
        ],
    ];
    foreach (Transfer::getCurrencies() as $currency) {
        $lower = strtolower($currency);
        $columns[] = [
            'attribute' => strtolower($currency),
            'label' => $currency,
            'contentOptions' => function (Referral $model) use ($lower) {
                return ['class' => 'money ' . $lower];
            }
        ];
    }
    return array_merge_recursive([
        'emptyText' => 'Nobody',
        'rowOptions' => function (Referral $model) {
            return [
                'class' => $model->children > 0 ? 'has-children' : '',
                'id' => 'user-' . $model->nick
            ];
        },
        'columns' => $columns
    ],
        $config);
}
