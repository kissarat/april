<?php
/**
 * Last modified: 18-05-09 00:46:29
 * Hash: 38dd5a06250828693bb6dbf6f2ab1bea476ad307
 * @var $model \app\models\form\Password
 * @var $this \yii\web\View
 */

use app\helpers\Html;
use app\models\User;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Password');
?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="settings password">
        <h2><?= Yii::t('app', 'Change Password') ?></h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="fields">
            <?= $form->field($model, 'current')
                ->passwordInput(['maxlength' => User::PASSWORD_LENGTH]) ?>
            <?= $form->field($model, 'new')
                ->passwordInput(['maxlength' => User::PASSWORD_LENGTH]) ?>
            <?= $form->field($model, 'repeat')
                ->passwordInput(['maxlength' => User::PASSWORD_LENGTH]) ?>
            <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>
        </div>

        <div class="control">
            <?= Html::submitButton(Yii::t('app', 'Change Password')) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
