<?php

use app\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \app\models\User $model
 * @var $this \yii\web\View
 */
?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="settings wallet">
        <h2><?= Yii::t('app', 'Save wallets for money withdraw') ?></h2>
        <div class="alert alert-warning" role="alert">
            <?= Yii::t('app', 'You can change wallets through technical support only') ?>
        </div>

        <?php $form = ActiveForm::begin() ?>
        <div class="fields">
            <?= $form->field($model, 'perfect')
                ->textInput(['disabled' => !$model->canChange('perfect')]) ?>
            <span class="info-wallets">Пример ввода кошелька Perfect Money: U1234567</span>
            <?= $form->field($model, 'payeer')
                ->textInput(['disabled' => !$model->canChange('payeer')]) ?>
            <span class="info-wallets">Пример ввода кошелька Payeer: P12345678</span>
            <?= $form->field($model, 'advcash')
                ->textInput(['disabled' => !$model->canChange('advcash')])?>
            <span class="info-wallets">Пример ввода кошелька AdvCash: U123456789012</span>
        </div>
        <div class="control">
            <?= Html::submitButton(Yii::t('app', 'Save')) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
