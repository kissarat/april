<?php

use app\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t( 'app', 'Profile' );
?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="settings profile">
        <h2><?= Yii::t( 'app', 'Change profile settings',
				[ 'name' => $model->getName() ] ) ?></h2>
		<?php if ( 'logist' !== Yii::$app->id ): ?>
			<?php if ( $model->canChange( 'pin' ) ): ?>
                <div class="alert alert-warning" role="alert">
					<?= Yii::t( 'app', 'By specifying the transaction password, you can change it only by contacting the technical support' ) ?>
                </div>
			<?php else: ?>
                <div class="alert alert-info" role="alert">
					<?= Yii::t( 'app', 'Transaction password is set, you can change it only by contacting the technical support' ) ?>
                </div>
			<?php endif ?>
		<?php endif ?>

		<?php $form = ActiveForm::begin( [ 'options' => [ 'enctype' => 'multipart/form-data' ] ] ); ?>
        <div class="fields">
			<?= $form->field( $model, 'forename' )->textInput( [ 'placeholder' => 'Your First name' ] ) ?>
			<?= $form->field( $model, 'surname' )->textInput( [ 'placeholder' => 'Your Last name' ] ) ?>
			<?= $form->field( $model, 'email' )->textInput( [ 'disabled' => 'logist' !== Yii::$app->id ] ) ?>
			<?= $form->field( $model, 'phone' )->textInput( [ 'placeholder' => 'Your phone number' ] ) ?>
			<?= $form->field( $model, 'skype' )->textInput( [ 'placeholder' => 'Your skype' ] ) ?>
			<?= $form->field( $model, 'telegram' )->textInput( [ 'placeholder' => 'Your telegram' ] ) ?>
            <hr>
            <div id="upload_avatar" class="form-group">
                <label for="upload_avatar" class="control-label">
					<?= Yii::t( 'app', 'Upload Photo' ) ?>
                </label>
				<?= Html::button( Yii::t( 'app', 'Upload Photo' ) ) ?>
                <div class="filename"></div>
				<?= $form->field( $model, 'avatar_file' )
				         ->fileInput( [ 'accept' => 'image/jpeg, image/png, image/gif, image/webp, image/bmp' ] )
				         ->label( false ) ?>
            </div>
            <hr>
			<?php if ( 'logist' === Yii::$app->id ): ?>
                <div class="user-refback" v-cloak>
					<?= $form->field( $model, 'refback' )->input( 'range', [
						'min'     => 0,
						'max'     => 0.07,
						'step'    => 0.01,
						'v-model' => 'refback'
					] ) ?>
                    <div class="info-refbeck">
                        <span id="refback-number">{{ refbackPercent }}%</span>
                        <span><?= Yii::t( 'app', 'of the partner\'s deposit amount will be returned to him automatically' ) ?></span>
                    </div>
                </div>
			<?php elseif ( $model->canChange( 'pin' ) ): ?>
				<?= $form->field( $model, 'pin' )->textInput() ?>
			<?php endif ?>
            <hr>
        </div>

        <div class="control">
			<?= Html::submitButton( Yii::t( 'app', 'Save' ) ) ?>
            <div class="hidden">
				<?= Html::a( Yii::t( 'app', 'Change Password' ),
					[ 'settings/password', 'nick' => $model->nick ] ) ?>
				<?= Html::a( Yii::t( 'app', 'Wallets' ),
					[ 'settings/wallet', 'nick' => $model->nick ] ) ?>
            </div>
        </div>

		<?php ActiveForm::end(); ?>
    </div>
</div>
