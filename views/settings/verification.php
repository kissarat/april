<?php
/**
 * Last modified: 18.08.27 22:56:41
 * Hash: 60f51932df8d455836db84e3dfaa4bbe611a765c
 */

use yii\widgets\ActiveForm;
use app\helpers\Html;

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;
?>

<div class="settings verification">
    <h1><?= Yii::t('app', 'Verification') ?></h1>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <p>
        Для пополнения счёта личного кабинета с помощью Visa\MasterCard RUB необходимо пройти
        дополнительную идентификацию личности. Это требование платёжной системы и не может быть изменено компанией.
        Процедуру идентификации необходимо пройти только тем пользователям, которые будут пополнять счёт и выводить
        средства с помощью данного палётжного направления.
    </p>
    <p>Для прохождения идентификации необходимо:</p>
    <div class="images">
        <div id="front-image">
            <p>
                Ваше Фото с паспортом в руках возле лица и картой, с которой будет произведена оплата так,
                чтобы на карте были видны 6 первых цифр в начале кода карты и 2 последние.
                Остальную информацию на карте можно закрасить.
            </p>
            <div class="choose-file">
                <?= $form->field($model, 'front')
                    ->fileInput(['accept' => 'image/jpeg, image/png, image/gif, image/webp, image/bmp'])
                    ->label(false) ?>
                <button type="button"><?= Yii::t('app', 'Choose file') ?></button>
            </div>
            <?php
            if ($user->front) {
                $url = '/verification/' . $user->front;
                echo Html::a(Html::img($url, ['title' => Yii::t('app', 'View image in full size')]), $url, [
                    'class' => 'view-link',
                    'target' => '_blank'
                ]);
            }
            ?>
        </div>
        <div id="back-image">
            <p>
                Отправить Ваше Фото с паспортом в руках возле лица и картой, с которой будет произведена оплата так,
                чтобы был виден CVV-код. Остальную информацию на карте можно закрасить.
            </p>
            <div class="choose-file">
                <?= $form->field($model, 'back')
                    ->fileInput(['accept' => 'image/jpeg, image/png, image/gif, image/webp, image/bmp'])
                    ->label(false) ?>
                <button type="button"><?= Yii::t('app', 'Choose file') ?></button>
            </div>
            <?php
            if ($user->back) {
                $url = '/verification/' . $user->back;
                echo Html::a(Html::img($url, ['title' => Yii::t('app', 'View image in full size')]), $url, [
                    'class' => 'view-link',
                    'target' => '_blank'
                ]);
            }
            ?>
        </div>
    </div>
    <p>
        Фотография должна быть качественной, текст на фото - читабельным. Ваше лицо, карта и паспорт не
        должны быть обрезаны или размыты. Вывод средств рекомендовано производить на ту же карту, с которой
        было произведено пополнение счёта личного кабинета.
    </p>
    <div class="controls">
        <?= Html::submitButton(Yii::t('app', 'Upload')) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
