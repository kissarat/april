<div class="program calculator">
    <style id="calculator-style">

        @import "https://cabinet.ai-logist.com/style.css" screen; 

        .currencies > *, .program {
            cursor: pointer;
        }

        [data-currency="USD"]:after {
            content: ' USD';
        }
        [data-currency="BTC"]:after {
            content: ' BTC';
        }
        [data-currency="ETH"]:after {
            content: ' ETH';
        }
        [data-currency="ETC"]:after {
            content: ' ETC';
        }
        [data-currency="LTC"]:after {
            content: ' LTC'; 
        }
        [data-currency="DOGE"]:after {
            content: ' DOGE';
        }
     
        .program.calculator * {
            font-family: comfortaa,cursive !important;
        }
        .program.calculator {
  max-width: 1170px;
  margin: 20px auto 0;
  background-image: url("https://ai-logist.com/wp-content/uploads/2018/04/2.jpg");
  padding: 15px;
  background-size: cover;
  background-repeat: no-repeat; }
  .program.calculator .program-list {
    display: flex;
    width: 100%;
    margin-bottom: 30px; }
    .program.calculator .program-list .program {
      width: 100%;
      padding: 30px 30px 20px;
      border: 2px solid transparent; }
      .program.calculator .program-list .program .plans h2 {
        text-align: center;
        text-transform: uppercase;
        margin: 0 0 25px;
        font-size: 36px; }
      .program.calculator .program-list .program .plans ul {
        margin: 0;
        padding: 0; }
        .program.calculator .program-list .program .plans ul li {
          list-style: none;
          display: flex;
          justify-content: space-between;
          font-size: 16px;
          width: 100%; }
          .program.calculator .program-list .program .plans ul li span:nth-child(2) {
            font-size: 20px;
            font-weight: 600; }
          .program.calculator .program-list .program .plans ul li span:first-child {
            line-height: 22px; }
      .program.calculator .program-list .program.active {
        color: #007bff;
        border: 2px solid #3e7bdd;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        -webkit-border-radius: 20px;
        -moz-border-radius: 20px;
        border-radius: 20px; }
      .program.calculator .program-list .program#program1 .plans ul {
        margin: 0;
        padding: 0; }
      .program.calculator .program-list .program#program2 {
        margin: 0 25px 25px; }
        .program.calculator .program-list .program#program2 .plans ul {
          margin: 0;
          padding: 0; }
      .program.calculator .program-list .program#program3 .plans ul {
        margin: 0;
        padding: 0; }
  .program.calculator .calculator-bottom {
    display: flex;
    max-width: 700px;
    margin: 0 auto; }
    .program.calculator .calculator-bottom .choose-currency {
      width: 300px; }
      .program.calculator .calculator-bottom .choose-currency h3 {
        margin: 0 0 10px;
        font-size: 24px; }
      .program.calculator .calculator-bottom .choose-currency .currencies {
        display: flex;
        flex-wrap: wrap; }
        .program.calculator .calculator-bottom .choose-currency .currencies > div {
          padding: 20px 20px 17px;
          margin-right: 10px;
          margin-bottom: 10px;
          color: #007bff;
          border: 1px solid rgba(0, 123, 255, 0.75);
          -webkit-border-radius: 25px;
          -moz-border-radius: 25px;
          border-radius: 25px;
          width: 23%;
          text-align: center; }
          .program.calculator .calculator-bottom .choose-currency .currencies > div:hover, .program.calculator .calculator-bottom .choose-currency .currencies > div.active {
            background-color: #007bff;
            color: #ffffff; }
    .program.calculator .calculator-bottom .calculator-bottom-right {
      width: 400px;
      padding-left: 5%; }
      .program.calculator .calculator-bottom .calculator-bottom-right .choose-amount h3 {
        font-size: 24px;
        margin: 0 0 10px; }
      .program.calculator .calculator-bottom .calculator-bottom-right .choose-amount input {
        margin-bottom: 10px;
        padding: 10px;
        font-size: 18px;
        width: 100%; }
      .program.calculator .calculator-bottom .calculator-bottom-right .summary {
        padding: 0;
        margin: 0; }
        .program.calculator .calculator-bottom .calculator-bottom-right .summary li {
          list-style-type: none;
          margin: 0 0 10px;
          display: flex;
          justify-content: space-between; }
          .program.calculator .calculator-bottom .calculator-bottom-right .summary li span:first-child {
            font-size: 16px;
            line-height: 30px; }
          .program.calculator .calculator-bottom .calculator-bottom-right .summary li span:last-child {
            font-size: 20px;
            font-weight: 600; }

/*# sourceMappingURL=calc.css.map */


        
    </style>
    <div class="program-list">
        <div v-bind:class="{program: true, active: id === p.id}"
             v-for="p in programs" v-bind:id="'program' + p.id"
             v-on:click="id = p.id">
            <div class="plans">
                <h2>{{ p.name }}</h2>
                <div class="bg-head"></div>
                <ul>
                    <li class="investment">
                        <span class="name">Сумма от</span>
                        <span class="value money" v-bind:data-currency="currency" v-cloak>{{ rate(p.min) }}</span>
                    </li>
                    <li class="rate">
                        <span class="name">Начисления</span>
                        <span class="value percent" v-cloak>{{ p.percent * 100 }}%</span>
                    </li>
                    <li class="duration">
                        <span class="name">Срок</span>
                        <span class="value" v-cloak>{{p.days}} дней</span>
                    </li>
                    <li class="income">
                        <span class="name">Прибыль</span>
                        <span class="value percent" v-cloak>{{ p.days * p.percent * 100 - 100 | round }}%</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="calculator-bottom">
        <div class="choose-currency">
            <h3>Выберите валюту:</h3>
            <div class="currencies" v-cloak>
                <div v-for="c in currencies"
                     v-on:click="currency = c"
                     v-bind:class="{active: c === currency}">{{ c }}
                </div>
            </div>
        </div>
        <div class="calculator-bottom-right">
            <div class="choose-amount">
                <h3>Введите сумму инвестиции:</h3>
                <input type="text" id="invest-amount" v-model="amount" v-bind:disabled="!isSelected"/>
                <div v-if="!isSelected">Вы можете ввести сумму после выбора программы и валюты</div>
                <div v-if="isSelected" class="error" v-cloak>
                    <p class="error-message" v-if="amount < min">Введите сумму больше <span
                                v-bind:data-currency="currency">{{ min }}</span></p>
                    <!--p class="error-message" v-if="amount >= max">Введите сумму не более <span
                                v-bind:data-currency="currency">{{ max }}</span></p-->
                </div>
            </div>
            <ul class="summary" v-if="isSelected && amount >= min">
                <li>
                    <span class="name">Ежедневное начисление:</span>
                    <span class="value" v-bind:data-currency="currency" v-cloak>{{ daily | factor }}</span>
                </li>
                <li>
                    <span class="name">Сумма дохода:</span>
                    <span class="value" v-bind:data-currency="currency" v-cloak>{{ allIncome | factor }}</span>
                </li>
                <li>
                    <span class="name">Прибыль:</span>
                    <span class="value" v-bind:data-currency="currency" v-cloak>{{ income | factor }}</span>
                </li>
            </ul>
        </div>
    </div>
    <script>
        function calculator(config) {
            function factor(v, currency) {
                if (!currency) {
                    currency = config.currency
                }
                const factor = config.factor[currency];
                return (v / factor).toFixed(Math.log(factor) / Math.log(10))
            }

            Vue.filter('factor', factor);
            Vue.filter('round', function (v, scale) {
                if ('number' !== typeof scale) {
                    scale = 2
                }
                const factor = Math.pow(10, scale);
                return Math.round(v * factor) / factor
            });

            new Vue({
                el: '.program.calculator',

                data: function () {
                    Object.assign(config, {
                        amount: 0,
                        id: 0,
                        currency: 'USD'
                    })
                    return config
                },

                created: function () {
                    this.id = 1
                },

                computed: {
                    program: function () {
                        const self = this
                        return this.programs.find(function (p) {
                            return self.id === p.id
                        })
                    },

                    isSelected: function () {
                        return this.currency && this.program
                    },

                    cent: function () {
                        return this.amount * this.factor[this.currency]
                    },

                    daily: function () {
                        return this.isSelected ? this.cent * this.program.percent : 0
                    },

                    allIncome: function () {
                        return this.isSelected ? this.daily * this.program.days : 0
                    },

                    income: function () {
                        return this.isSelected ? this.allIncome - this.cent : 0
                    },

                    min: function () {
                        return this.rate(this.program.min)
                    },

                    max: function () {
                        return this.rate(this.program.max)
                    }
                },

                methods: {
                    rate: function (usd) {
                        usd = usd / 100;
                        const c = this.currency;
                        return 'USD' === c
                            ? usd
                            : (usd / this.rates[c]).toFixed(Math.log(this.factor[c]) / Math.log(10))
                    }
                },

                watch: {
                    id: function () {
                        this.amount = this.min
                    },
                    currency: function () {
                        this.amount = this.min
                    },
                    amount: function (amount) {
                        if ('string' === typeof amount) {
                            const a = amount.replace(',', '.');
                            if (Number.isFinite(+a)) {
                                this.amount = a
                            }
                        }
                    }
                }
            })
        }

        addEventListener('DOMContentLoaded', function () {
            document.head.appendChild(document.getElementById('calculator-style'))
            calculator(appConfig)
        })
    </script>
</div>
