<?php

use app\models\Career;
use app\models\Transfer;
use yii\grid\GridView;

$focus = empty($_GET['id']) ? null : +$_GET['id'];
/**
 * @var yii\data\ArrayDataProvider $provider
 * @var $this \yii\web\View
 */
?>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="transfer index">

        <?= GridView::widget([
            'dataProvider' => $provider,
            'layout' => '{errors}{items}{summary}{pager}',
//        'tableOptions' => [ 'id' => 'selectable' ],
            'emptyText' => 'Nothing found',
            'rowOptions' => function ($model) use ($focus) {
                $class = $focus === $model->guid ? 'focus success' : 'normal';

                return [
                    'class' => $class,
                    'data-amount' => $model->amount,
                    'data-status' => $model->status,
//                    'data-currency' => $model->currency,
                ];
            },
            'columns' => [
                [
                    'attribute' => 'created',
                    'label' => Yii::t('app', 'Time'),
                    'format' => 'datetime',
                    'contentOptions' => function ($model) {
                        return ['data-time' => strtotime($model->created)];
                    }
                ],
                [
                    'attribute' => 'amount',
                    'format' => 'html',
                    'value' => function ($model) {
                        $sign = $model->to === Yii::$app->user->identity->id ? '+' : '-';
                        $currency = strtolower($model->currency);

                        return "<span class=\"sign\">$sign</span><span class=\"money $currency\">$model->amount</span>";
//                        return "<span class=\"sign\">+</span><span class=\"$currency money\">$model->amount</span>";
                    }
                ],
                [
                    'attribute' => 'text',
                    'label' => Yii::t('app', 'Description'),
                    'value' => function (Transfer $model) {
                        if ($model->text) {
                            if ($model->vars) {
                                return Yii::t('app', $model->text, $model->vars);
                            }

                            return Yii::t('app', $model->text);
                        }

                        switch ($model->type) {
                            case 'payment':
                                if (Transfer::TYPE_INTERKASSA === $model->system) {
                                    return Yii::t('app', 'Interkassa');
                                }
                                $wallet = $model->wallet;
                                if (isset($model->vars)) {
                                    if (!empty($model->vars['senders'])) {
                                        $wallet = $model->vars['senders'][0];
                                    } elseif (!empty($model->vars['sender'])) {
                                        $wallet = $model->vars['sender'];
                                    }
                                }
                                if ('pending' === $model->status) {
                                    return Yii::t('app', '{number} confirmations', ['number' => $model->n]);
                                }
                                return Yii::t('app', 'Payment from {wallet}', ['wallet' => $wallet]);
                            case 'withdraw':
                                return Yii::t('app', 'Withdrawal to {wallet}', ['wallet' => $model->wallet]);
                            case 'lottery':
                                return Yii::t('app', 'Lottery');
                            case 'internal':
                                if ($model->from === Yii::$app->user->id) {
                                    return Yii::t('app', 'Refback to {nick} ({percent}%)', [
                                        'nick' => $model->vars['to_nick'],
                                        'percent' => round($model->vars['percent'] * 100)
                                    ]);
                                }
                                return Yii::t('app', 'Refback from {nick} ({percent}%)', [
                                    'nick' => $model->vars['from_nick'],
                                    'percent' => round($model->vars['percent'] * 100)
                                ]);
                            case 'support':
                                return Yii::t('app', 'Administrative');
                            case 'premium':
                                $grade = Career::getGrade($model->node);
                                if ($grade) {
                                    return Yii::t('app', 'Award for the rank of {name}', [
                                        'name' => Yii::t('app', $grade['name'])
                                    ]);
                                }
                                return 'Premium error';
                            case 'accrue':
                                return Yii::t('app', 'Accrue #{n} of {name}', [
                                    'name' => $model->vars['name'] ?? '',
                                    'n' => $model->n
                                ]);
                            case 'win':
                                return Yii::t('app', 'Lottery winning #{n}', [
                                    'n' => $model->n
                                ]);
                            case 'bonus':
                                if ($model->n > 0) {
                                    return Yii::t('app', 'Bonus for lottery winning #{n}', [
                                        'n' => $model->n
                                    ]);
                                }
                                break;
                            default:
                                return Yii::t('app', $model->text, $model->vars) ?: $model->type;
                        }
                    }
                ],
                [
                    'attribute' => 'status',
                    'label' => Yii::t('app', 'Status'),
                    'value' => function ($model) {
                        if (Transfer::TYPE_INTERKASSA === $model->system && Transfer::STATUS_PENDING === $model->status) {
                            return Yii::t('app', 'Pending');
                        }
                        switch ($model->status) {
                            case Transfer::STATUS_SUCCESS:
                                return "\u{2713}";

                            case 'fail':
                                return "\u{274C}";

                            case Transfer::STATUS_CANCELED:
                                return Yii::t('app', 'Canceled');

                            case Transfer::STATUS_CREATED:
                                return Yii::t('app', 'Created');
                            default:
                                return $model->status;
                        }
                    }
                ]
            ]
        ]) ?>
        <div id="summary" style="display: none">
            <?= Yii::t('app', 'Summary') ?>:
            <span class="money usd"></span>
        </div>
    </div>
</div>