<?php
use app\helpers\Html;

?>

<form action="<?= $url ?>" method="POST" class="autosubmit" ref="autosubmit">
    <?= Html::tag('h1', Yii::t('app', 'Wait please...')) ?>
    <?php
    foreach ($data as $name => $value) {
        echo Html::hiddenInput($name, $value);
    }
    ?>
</form>
